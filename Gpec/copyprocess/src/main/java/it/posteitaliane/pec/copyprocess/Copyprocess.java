package it.posteitaliane.pec.copyprocess;

import it.posteitaliane.pec.copyprocess.customException.FileCorruptedException;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Component
public class Copyprocess {

    private static final Logger LOGGER = LoggerFactory.getLogger(Copyprocess.class);
    private static FileAlterationMonitor monitor;

    public static void watcher() throws Exception{

       /*
        1)implementare un watcher che monitorizza una folder sorgente
        2)ogni file che arriva nella folder sorgente deve essere copiato in una folder di destinazione
        3)path delle folder deve stare in un file di properties ESTERNO al jar
        4)bisogna verificare che la copia sia avvenuta correttamente
        5)a seguito di questa verifica, si puo' cancellare il file nella cartella sorgente  */

        //entering current date
       // DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        CopyFileManager copyFileManager = new CopyFileManager();
        Date entryDate = new Date();
        //System.out.println(dateFormat.format(date));



          Properties mainProperties = new Properties();
          //System.out.println(System.getProperty("user.dir"));

          /*Rimettere percorso relativo*/
          mainProperties.load(new FileInputStream("copyprocess.properties"));

          String folderDestination = mainProperties.getProperty("folder.destination");

          LOGGER.info("folderDestination " +folderDestination );



          Integer pollingIntervalSeconds = Integer.valueOf(mainProperties.getProperty("pollin.interval.seconds"));
          long pollingInterval = pollingIntervalSeconds * 1000;

          monitor = new FileAlterationMonitor(pollingInterval);

          Integer foldersNumber = Integer.valueOf(mainProperties.getProperty("folder.sources.size"));

          ArrayList<String> folderSources = new ArrayList<>();

          IntStream.range(1, foldersNumber +1)
                   .forEach(i -> {
                         folderSources.add(mainProperties.getProperty("folder.source".concat(String.valueOf(i))));
                   });


          for(String folderS: folderSources){

              LOGGER.info("folderSource iesima: " +folderS );

              Path pathSource = Paths.get(folderS);

              FileAlterationObserver observer = new FileAlterationObserver(pathSource.toFile());
              observer.addListener(new FileAlterationListenerImpl(folderS,folderDestination));

              monitor.addObserver(observer);

          }


          monitor.start();


          for (FileAlterationObserver observerDir: monitor.getObservers()){
                LOGGER.info("Monitoring every " +pollingIntervalSeconds + " seconds folder @ path " + observerDir.getDirectory().getAbsolutePath() + " STARTED");
          }


        //recovering crash of service
        LOGGER.info("*** START RECOVERING PROCEDURE ***");

        for(String folderS: folderSources) {

            LOGGER.info("Recupero dalla folder source: " + folderS);

            Path pathSource = Paths.get(folderS);

            File folderSource = pathSource.toFile() ;

            FileFilter filter = new FileFilter() {

                public boolean accept(File file) {

                    if (!file.isFile()) return false;

                    Date fileDate = new Date(file.lastModified());
                    if (fileDate.before(entryDate)) return true;
                    else return false;
                }};

            //per TEST soltanto
            //creo un file dopo un minuto. La procedura NON dovrebbe prenderlo
//            TimeUnit.SECONDS.sleep(10);
//            File tempFile = File.createTempFile("TEST_FILE_", "_END",folderSource);
            //-------------------------------------------

            File[] filesToRecover = folderSource.listFiles(filter);

            LOGGER.info("Numero files recuperati: " + filesToRecover.length);

            for(File file: filesToRecover){
                //LOGGER.info(file.getAbsolutePath());
                String fileCreated = file.getAbsoluteFile().getAbsolutePath();
                try {

                    copyFileManager.copyFile(fileCreated, folderDestination) ;
                    LOGGER.info("[RECUPERATO] COPIATO SU "+ folderDestination);

                } catch (IOException e) {
                    LOGGER.error(e.getMessage());
                }catch (FileCorruptedException e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }

        LOGGER.info("*** END RECOVERING PROCEDURE ***");
        //-----------------------------------

    }


    @PreDestroy
    public void cleanUp() {
        try {
            monitor.stop(3000);
        } catch (Exception e) {
            LOGGER.error("CP ERROR Monitor stopping Monitor: " + e.getMessage(), e);
        }
    }

}
