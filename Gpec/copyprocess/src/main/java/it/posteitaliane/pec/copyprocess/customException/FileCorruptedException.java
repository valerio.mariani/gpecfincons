package it.posteitaliane.pec.copyprocess.customException;

public class FileCorruptedException extends Exception {

    public FileCorruptedException(String sourceFile,String destFile) {
        super("I file "+sourceFile+" e "+destFile+" NON sono uguali, checksum fallito");
    }

}

