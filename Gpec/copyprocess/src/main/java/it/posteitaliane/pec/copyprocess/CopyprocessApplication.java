package it.posteitaliane.pec.copyprocess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.nio.file.Watchable;

@SpringBootApplication
public class CopyprocessApplication {

	public static void main(String[] args) {
        SpringApplication.run(CopyprocessApplication.class, args);
    }


	@Bean(initMethod = "watcher")
	Copyprocess getProcess(){
		return new Copyprocess();
	}

}

