package it.posteitaliane.pec.copyprocess;

import it.posteitaliane.pec.copyprocess.customException.FileCorruptedException;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.*;
import java.util.Properties;


public class Copyprocess_BAK {


    public static void watcher() throws IOException,InterruptedException,FileCorruptedException {

        //1)implementare un watcher che monitorizza una folder sorgente
        //2)ogni file che arriva nella folder sorgente deve essere copiato in una folder di destinazione
        //3)path delle folder deve stare in un file di properties ESTERNO al jar
        //4)bisogna verificare che la copia sia avvenuta correttamente
        //5)a seguito di questa verifica, si puo' cancellare il file nella cartella sorgente


        // riferimenti utili:
        // per il watcher:
        // https://www.baeldung.com/java-nio2-watchservice
        // https://docs.oracle.com/javase/tutorial/essential/io/notification.html
        // occhio all'OVERFLOW: https://stackoverflow.com/questions/39076626/how-to-handle-the-java-watchservice-overflow-event
        // fare test copiando tantissimi file in contemporanea nella folder sorgente
        // https://stackoverflow.com/questions/4628769/verify-file-is-copied-in-java

         Properties mainProperties = new Properties();
         CopyFileManager copyFileManager = new CopyFileManager();


          //TODO: sostituire con path verso il file di properties
          mainProperties.load(new FileInputStream("C:\\Users\\Lele\\GPEC\\watcher.properties"));


          System.out.println(mainProperties.getProperty("folder.source"));


          WatchService watchService = FileSystems.getDefault().newWatchService();

          String folderSource = mainProperties.getProperty("folder.source");
          String folderDestination = mainProperties.getProperty("folder.destination");

          Path pathSource = Paths.get(folderSource);
          Path pathDestination = Paths.get(folderDestination);

          pathSource.register( watchService, StandardWatchEventKinds.ENTRY_CREATE);

          WatchKey key;

          while ((key = watchService.take()) != null) {

              for (WatchEvent<?> event : key.pollEvents()) {

                  final WatchEvent.Kind<?> kind = event.kind ();

                  //Test per Overflow
                  if ( kind == StandardWatchEventKinds.OVERFLOW ) continue;

                  String md5Source = "";
                  String md5Dest = "";

                  Path FROM = Paths.get(pathSource.toString()+"\\"+event.context().toString());
                  Path TO = Paths.get(pathDestination.toString()+"\\"+event.context().toString());

                  copyFileManager.copyFile(pathSource  + "\\" + event.context().toString(), pathDestination.toString()) ;

                  /*Checksum test method call*/
                  if (!copyFileManager.checksumFileTest(FROM.toFile(),TO.toFile())){
                        throw new FileCorruptedException(FROM.toString(),TO.toString());
                  }

                  /*Delete source file after checksum tests*/
                  copyFileManager.deleteFileFromSourceFolder(pathSource  + "\\" + event.context().toString());




                  System.out.println("[ " + event.kind()+
                                     " -> " + event.context() + " ]" );

              }

              //ri-sottometto la dir da controllare dopo un take()
              key.reset();
          }


//        while(true){
//            System.out.println("SONO watcher:cicloInfinito");
//            try {
//                Thread.sleep(3000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
    }


}
