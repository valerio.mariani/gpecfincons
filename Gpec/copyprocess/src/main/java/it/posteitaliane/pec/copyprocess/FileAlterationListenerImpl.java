package it.posteitaliane.pec.copyprocess;

import it.posteitaliane.pec.copyprocess.customException.FileCorruptedException;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileAlterationListenerImpl implements FileAlterationListener {

    private Path pathSource;
    private Path pathDestination;

    private static final Logger LOGGER = LoggerFactory.getLogger(Copyprocess.class);



    @Override
    public void onStart(final FileAlterationObserver observer) {
    }

    @Override
    public void onDirectoryCreate(final File directory) {
    }

    @Override
    public void onDirectoryChange(final File directory) {
    }

    @Override
    public void onDirectoryDelete(final File directory) {
    }

    @Override
    public void onFileCreate(final File file) {
        LOGGER.info("INTERCETTATA NELLA MAILBOX LA CREAZIONE  DEL FILE: "+file.getAbsoluteFile() );

        CopyFileManager copyFileManager = new CopyFileManager();
        String fileCreated = file.getAbsoluteFile().getAbsolutePath();


        try {

            copyFileManager.copyFile(fileCreated, pathDestination.toString()) ;
            LOGGER.info("COPIATO SU "+ pathDestination.toString());

        } catch (IOException e) {
            LOGGER.error("CP ERROR copyFile " + e.getMessage(), e);
        }catch (FileCorruptedException e) {
            LOGGER.error("CP ERROR copyFile " + e.getMessage(), e);
        }

    }

    @Override
    public void onFileChange(final File file) {
    }

    @Override
    public void onFileDelete(final File file) {
    }

    @Override
    public void onStop(final FileAlterationObserver observer) {
    }




    public FileAlterationListenerImpl(String pathSource, String pathDestination) {
        super();
        this.pathSource = Paths.get(pathSource);
        this.pathDestination = Paths.get(pathDestination);
    }
}
