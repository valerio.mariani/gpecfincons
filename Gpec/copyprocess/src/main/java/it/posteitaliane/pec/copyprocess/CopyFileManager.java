package it.posteitaliane.pec.copyprocess;

import it.posteitaliane.pec.copyprocess.customException.FileCorruptedException;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Set;


public class CopyFileManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(CopyFileManager.class);


    public void copyFile(String filePath, String dir) throws IOException,FileCorruptedException{

        Path sourceFile = Paths.get(filePath);
        Path targetDir = Paths.get(dir);

        Path targetFile = targetDir.resolve(sourceFile.getFileName());
        Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
        //add owners permission
        perms.add(PosixFilePermission.OWNER_READ);
        perms.add(PosixFilePermission.OWNER_WRITE);
        //add group permissions
        perms.add(PosixFilePermission.GROUP_READ);
        perms.add(PosixFilePermission.GROUP_WRITE);
        //add others permissions
        perms.add(PosixFilePermission.OTHERS_READ);
        perms.add(PosixFilePermission.OTHERS_WRITE);

        Files.copy(sourceFile, targetFile);

        LOGGER.info("File  "+sourceFile.getFileName()+" CREATO correttamente");


        //only for system POSIX compliant (NO windows system)
       // Files.setPosixFilePermissions(targetFile, perms);

        /*Checksum test method call*/
        if (!checksumFileTest(sourceFile.toFile(),targetFile.toFile())){
            throw new FileCorruptedException(sourceFile.toString(),targetFile.toString());
        }

        //creazione file .t nella dir target
        Path fileT = Paths.get(dir+"/"+sourceFile.getFileName()+".t");

        try{

            Files.createFile(fileT);
            LOGGER.info("File .t per "+sourceFile.getFileName()+" CREATO correttamente");

        } catch (FileAlreadyExistsException x) {
            LOGGER.error("CP ERROR .t-FILE File con nome %s già esistente", fileT.getFileName());
        } catch (IOException x) {
            LOGGER.error("CP ERROR .t-FILE Errore creazione file .t nella target dir: %s %n", dir,x.getMessage());
        }


        //TODO: DA DECIDERE FORSE METTEREMO QUI IL PROCESSO DI CONVERSIONE/PULIZIA DAI BYTE SPORCHI
        // (OPPURE LO METTIAMO NEL RESPONSE SERVICE CHE PRODUCE LO ZIP OUTCOME)

        /*Delete source file after checksum tests*/
        deleteFileFromSourceFolder(sourceFile.toString());


    }



    public boolean checksumFileTest(File sourceFile, File destinationFile) throws IOException {

        String md5Source = "";
        String md5Dest = "";

        try {
            InputStream sourceStream = new FileInputStream(sourceFile);
            md5Source = DigestUtils.md5Hex(sourceStream);
            sourceStream.close();

            InputStream destStream = new FileInputStream(destinationFile);
            md5Dest = DigestUtils.md5Hex(destStream);
            destStream.close();

        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        }

        //System.out.println("MD5 origine[ "+md5Source+" ] - MD5 Destinazione[ "+md5Dest+" ]");
        return (md5Source.equals(md5Dest));

    }




    public void deleteFileFromSourceFolder(String sourceFileToDelete) throws IOException {

        Path filePath = Paths.get(sourceFileToDelete);

        if (!Files.exists(filePath)){
             throw new FileNotFoundException("Il file "+sourceFileToDelete+" non esiste nella cartella sorgente");
        }


        Files.delete(filePath);

        LOGGER.info("File "+sourceFileToDelete+" CANCELLATO");

    }


}