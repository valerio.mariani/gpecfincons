#!/bin/bash 

########## variabili globali
CHECK_PASSED="true"
JAVA_EXE="java"
EXIT_CODE=1
BASEDIR=$( pwd ) 
################### functions

function failure  {
 echo "$1"
 CHECK_PASSED="false"
}

##########################


#calcolo il nome del file da utilizzare
CURRENT_JAR=$( ls  copyprocess*.jar | sort -r | head -n1 )

if [ -z "$CURRENT_JAR" ] ; then
   failure "copyprocess non installato correttamente"
fi

$JAVA_EXE -version 2>/dev/null
if [ $? -gt 0 ] ; then
 failure "java non installato o non  presente nel path $JAVA_EXE" 
fi

JAVA_VERSION=$( java -version   2>&1 | head -n1  | grep '"1.8' )

if [ -z "$JAVA_VERSION"  ] ; then
 failure " versione java non corretta, richiesta versione 1.8.X "
fi


if [ $CHECK_PASSED == "true" ] ; then
 echo "check superati con successo inizio esecuzione jar"
fi

java -Xms256M -Xmx1024M -jar ${CURRENT_JAR} --logging.file=/var/log/CopyProcess.log