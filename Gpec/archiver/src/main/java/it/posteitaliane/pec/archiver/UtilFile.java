package it.posteitaliane.pec.archiver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UtilFile {



    public static String checksum(String filepath, String codeAlgoritm) throws FileNotFoundException, IOException, NoSuchAlgorithmException {
        // DigestInputStream is better, but you also can hash file like this.

        MessageDigest md = null;

        md = MessageDigest.getInstance(codeAlgoritm);
        md.reset();
        InputStream fis = new FileInputStream(filepath);
        byte[] buffer = new byte[1024];
        int nread;
        while ((nread = fis.read(buffer)) != -1) {
            md.update(buffer, 0, nread);
        }


        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }

    public static BigInteger getFileSize(String filepath) {
        BigInteger siz = new BigInteger("0");
        java.io.File file = new java.io.File(filepath);
        if (file.exists()) {
            siz = new BigInteger(new Long(file.length()).toString());
        }
        return siz;
    }
    public static void stringToDom(String xmlSource, String pathFileName)
            throws IOException {
        java.io.FileWriter fw = new java.io.FileWriter(pathFileName);
        fw.write(xmlSource);
        fw.close();
    }




    public static byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            java.io.File file = new java.io.File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return bytesArray;

    }




}
