
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="file" type="{http://bean.service.postecom.it}FileUploadInfo"/>
 *         &lt;element name="md5HashHex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "file",
    "md5HashHex"
})
@XmlRootElement(name = "close")
public class Close {

    @XmlElement(required = true)
    protected FileUploadInfo file;
    @XmlElement(required = true)
    protected String md5HashHex;

    /**
     * Gets the value of the file property.
     * 
     * @return
     *     possible object is
     *     {@link FileUploadInfo }
     *     
     */
    public FileUploadInfo getFile() {
        return file;
    }

    /**
     * Sets the value of the file property.
     * 
     * @param value
     *     allowed object is
     *     {@link FileUploadInfo }
     *     
     */
    public void setFile(FileUploadInfo value) {
        this.file = value;
    }

    /**
     * Gets the value of the md5HashHex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMd5HashHex() {
        return md5HashHex;
    }

    /**
     * Sets the value of the md5HashHex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMd5HashHex(String value) {
        this.md5HashHex = value;
    }

}
