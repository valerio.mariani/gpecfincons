package it.posteitaliane.pec.archiver.service;

import it.posteitaliane.pec.archiver.UtilFile;
import it.posteitaliane.pec.archiver.bean.InfoPathZipBean;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.FileUploadInfo;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.PosteDocException_Exception;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.PosteDocWS;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.PosteDocWSService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

@Service
public class SendConservazioneWSService {
    @Value("${nfs.archiver.ws.user.client.poste.conservazione}")
    String username;
    @Value("${nfs.archiver.ws.password.client.poste.conservazione}")
    String password;
    @Value("${nfs.archiver.ws.blockSize.client.poste.conservazione}")
    int uploadBlockSize;

    public InfoPathZipBean inviaConservazione(InfoPathZipBean infoPathZipBean) throws PosteDocException_Exception, FileNotFoundException, NoSuchAlgorithmException, IOException {
        PosteDocWSService wsService = new PosteDocWSService();
        PosteDocWS pdws = wsService.getPosteDocWS();

        BindingProvider prov = ((BindingProvider) pdws);
        Map http_headers = (Map) ((BindingProvider)pdws).getRequestContext().get(MessageContext.HTTP_REQUEST_HEADERS);

        ((BindingProvider)pdws).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, username);
        ((BindingProvider)pdws).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, password);


        byte[] chkFileByteArray = UtilFile.readBytesFromFile(infoPathZipBean.getPathFileControllo());
        int uploadBlockSize = 512;
        long fileSize = 0;

        it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.File chkFile = new it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.File();

        chkFile.setNome(infoPathZipBean.getNameFileControllo());
        chkFile.setData(chkFileByteArray);
        String lottoFileNameZip =infoPathZipBean.getNameFileZip();
        java.io.File lottoFile = new java.io.File(infoPathZipBean.getPathFileZip());
        // creo lotto
        FileUploadInfo uploadFileInfo = pdws.createLotto(lottoFileNameZip, uploadBlockSize, lottoFile.length(), chkFile);
        // upload dello zip
        FileInputStream file = new FileInputStream(lottoFile);

        byte[] buffer = new byte[uploadBlockSize];
        int n=0;
        int blockId = 0;
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.reset();
        while ((n = file.read(buffer))>0){
            byte[] buff;
            if (n!=buffer.length){
                buff = new byte[n];
                System.arraycopy(buffer, 0, buff, 0, n);
            }else
                buff = buffer;

            pdws.upload(uploadFileInfo, buff, blockId++);
            md.update(buff);
        }
        //    BigInteger number = new BigInteger(1, md.digest());


        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));
        }


        //String md5Hex = number.toString(16);
        String md5Hex =result.toString();
        // CHIUDERE il blocco
        pdws.close(uploadFileInfo,md5Hex);

        return infoPathZipBean;
    }

public it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.Lotto getLottoWs(String dataCreazione, String idLottoAzienda, String tipoDoc)  throws PosteDocException_Exception {
    PosteDocWSService wsService = new PosteDocWSService();
    PosteDocWS pdws = wsService.getPosteDocWS();

    BindingProvider prov = ((BindingProvider) pdws);
    Map http_headers = (Map) ((BindingProvider)pdws).getRequestContext().get(MessageContext.HTTP_REQUEST_HEADERS);

    ((BindingProvider)pdws).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, username);
    ((BindingProvider)pdws).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, password);

    it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.Lotto lottoAnorma = pdws.getLotto(dataCreazione,idLottoAzienda, tipoDoc);
    return lottoAnorma;
}
}
