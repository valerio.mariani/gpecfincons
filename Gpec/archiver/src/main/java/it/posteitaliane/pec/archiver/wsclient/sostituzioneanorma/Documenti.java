
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Documenti complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Documenti">
 *   &lt;complexContent>
 *     &lt;extension base="{http://bean.service.postecom.it}SearchResultSet">
 *       &lt;sequence>
 *         &lt;element name="data" type="{urn:postecom}ArrayOf_tns1_InfoDocumento"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Documenti", namespace = "http://bean.service.postecom.it", propOrder = {
    "data"
})
public class Documenti
    extends SearchResultSet
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfTns1InfoDocumento data;

    /**
     * Gets the value of the data property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTns1InfoDocumento }
     *     
     */
    public ArrayOfTns1InfoDocumento getData() {
        return data;
    }

    /**
     * Sets the value of the data property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTns1InfoDocumento }
     *     
     */
    public void setData(ArrayOfTns1InfoDocumento value) {
        this.data = value;
    }

}
