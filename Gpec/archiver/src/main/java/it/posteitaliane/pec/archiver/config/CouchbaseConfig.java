package it.posteitaliane.pec.archiver.config;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.query.N1qlQuery;
import com.netflix.appinfo.InstanceInfo;
import it.posteitaliane.pec.common.configuration.DefaultCouchBaseConfiguration;
import it.posteitaliane.pec.common.couchbase.DocumentAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.repository.auditing.EnableCouchbaseAuditing;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

import javax.annotation.PostConstruct;

@Configuration
@EnableCouchbaseRepositories(basePackages={"it.posteitaliane.pec.common.repositories"})
@EnableCouchbaseAuditing //this activates auditing
public class CouchbaseConfig extends DefaultCouchBaseConfiguration {

    @Autowired
    private Bucket bucket; // = cluster.openBucket("test");

    @Autowired InstanceInfo currentInstanceInfo;

    public CouchbaseConfig(
            @Value("${spring.couchbase.bucket.password}")
            String lotBucketPassword,
            @Value("${spring.couchbase.bucket.name}")
            String bucketName,
            @Value("${spring.couchbase.bootstrap-hosts}")
            String[] couchbaseBootstrapHosts) {
        super(lotBucketPassword, bucketName, couchbaseBootstrapHosts);
    }


    // this creates the auditor aware bean that will feed the annotations
    @Bean
    public DocumentAuditor testAuditorAware() {
        return new DocumentAuditor(currentInstanceInfo.getId());
    }

    @PostConstruct
    public void createIndexes(){

        bucket.bucketManager().createN1qlIndex("idx_lots", true, false, "mailDeliveriesIDs");

        bucket.bucketManager().createN1qlIndex("idx_service", true, false, "service");

        bucket.bucketManager().createN1qlIndex("idx_lot_status", true, false, "currentStatus");

        bucket.bucketManager().createN1qlIndex("idx_lot_id", true, false, "lotId");

        bucket.bucketManager().createN1qlIndex("idx_lot_message_id", true, false, "messageID");

        bucket.query(N1qlQuery.simple("CREATE INDEX idx_lotto_events ON `" + bucket.name() + "` (ALL ARRAY v.event FOR v IN events END)"));


        //aggiungere:  CREATE INDEX `idx_lotKey` ON lotLock (lotKey)
        //
        //DELETE  FROM `lotLock` WHERE  `lotKey` ="G23L00000000000000001"

//        bucket.bucketManager().createN1qlIndex("idx_mailDeliveriesIDs",
////                Expression.x("mailDeliveriesIDs").in("$" + key),
//                true, false, "mailDeliveriesIDs");
    }

}
