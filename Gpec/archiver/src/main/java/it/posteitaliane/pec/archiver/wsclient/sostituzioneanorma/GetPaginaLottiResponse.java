
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPaginaLottiReturn" type="{http://bean.service.postecom.it}Lotti"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPaginaLottiReturn"
})
@XmlRootElement(name = "getPaginaLottiResponse")
public class GetPaginaLottiResponse {

    @XmlElement(required = true)
    protected Lotti getPaginaLottiReturn;

    /**
     * Gets the value of the getPaginaLottiReturn property.
     * 
     * @return
     *     possible object is
     *     {@link Lotti }
     *     
     */
    public Lotti getGetPaginaLottiReturn() {
        return getPaginaLottiReturn;
    }

    /**
     * Sets the value of the getPaginaLottiReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Lotti }
     *     
     */
    public void setGetPaginaLottiReturn(Lotti value) {
        this.getPaginaLottiReturn = value;
    }

}
