
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="createLottoReturn" type="{http://bean.service.postecom.it}FileUploadInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createLottoReturn"
})
@XmlRootElement(name = "createLottoResponse")
public class CreateLottoResponse {

    @XmlElement(required = true)
    protected FileUploadInfo createLottoReturn;

    /**
     * Gets the value of the createLottoReturn property.
     * 
     * @return
     *     possible object is
     *     {@link FileUploadInfo }
     *     
     */
    public FileUploadInfo getCreateLottoReturn() {
        return createLottoReturn;
    }

    /**
     * Sets the value of the createLottoReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link FileUploadInfo }
     *     
     */
    public void setCreateLottoReturn(FileUploadInfo value) {
        this.createLottoReturn = value;
    }

}
