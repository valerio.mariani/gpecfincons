package it.posteitaliane.pec.archiver.service;

import it.posteitaliane.pec.archiver.UtilFile;
import it.posteitaliane.pec.archiver.bean.InfoPathZipBean;
import it.posteitaliane.pec.archiver.outcome.zip.ZipBuilder;
import it.posteitaliane.pec.archiver.util.jaxb.ESubPECMarshall;
import it.posteitaliane.pec.archiver.xsd.bean.generated.ESubPEC;
import it.posteitaliane.pec.common.model.EventConservazione;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class BuildService {

    @Value("${nfs.archiver.ws.nickAzienda}")
    String nickAzienda;
    @Value("${nfs.archiver.ws.classeDocumentale}")
    String classeDocumentale;
    @Value("${nfs.archiver.ws.limimt.sizefile}")
    long limitSizeFile;
    @Value("${nfs.archiver.ws.limimt.numobj}")
    int limitnumobj;
    long sizeFile = 0;
    int numobj = 0;
    @Autowired
    MailDeliveryRepository mailDeliveryRepository;
    private static final Logger LOGGER = LoggerFactory.getLogger(BuildService.class);
    private int numeroElementi = 0;
    private int megaSize = 0;
    private ZipBuilder zipBuilder;
    @Autowired
    LottoConservazioneService lottoConservazioneService;
    public InfoPathZipBean createZipFromLotto(LotDelivery lotto, String basePathtmp, String lottoIdbase) throws IOException, NoSuchAlgorithmException {


        InfoPathZipBean infoPathZipBean = new InfoPathZipBean();
        // punto sulla cartella shared dentro la carte del lotto
        infoPathZipBean.setBasePath(basePathtmp + "\\" + lotto.getLotId());
        SimpleDateFormat smp = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        infoPathZipBean.setDataCreazione(smp.format(cal.getTime()));

        String nameGeneralFile = nickAzienda + "-" + classeDocumentale + "-" + infoPathZipBean.getDataCreazione() + "-" + lottoIdbase;

        infoPathZipBean = createfileToSend(nameGeneralFile, infoPathZipBean, lottoIdbase, lotto);

        return infoPathZipBean;
    }

    private InfoPathZipBean createfileToSend(String nameGeneralFile, InfoPathZipBean infoPathZipBean, String lottoIdbase, LotDelivery lotto) throws IOException, NoSuchAlgorithmException {


        it.posteitaliane.pec.archiver.xsd.bean.generated.ObjectFactory objFactXsd = new it.posteitaliane.pec.archiver.xsd.bean.generated.ObjectFactory();
        ESubPEC espec = objFactXsd.createESubPEC();
        List<ESubPEC.Documento> listaDoc = espec.getDocumento();
        String pathZip = "";
        try {
            // TODO MODIFICARE LA QUERY FILTRANDO I MESSAGGI GIA' INVIATI
            List<MailDeliveryRequest> messaggiDiUnLotto = mailDeliveryRepository.findByLotId(lotto.getLotId());

            int numMsg = 0;
            // create Zip
            this.zipBuilder = createZip(nameGeneralFile, espec, infoPathZipBean);
            //  condizione che non deve superare i  limiti di mb e di numero obj
            for (; messaggiDiUnLotto != null && numMsg < messaggiDiUnLotto.size() && this.sizeFile < this.limitSizeFile && this.numobj < this.limitnumobj; numMsg++) {
                MailDeliveryRequest md = messaggiDiUnLotto.get(numMsg);
                List<ESubPEC.Documento> listD = populateDoc(md, infoPathZipBean.getBasePath(), lotto);
                // aggiunto il msg alla lista
                if(listD != null) {
                    espec.getDocumento().addAll(listD);
                    lottoConservazioneService.saveInfoConservazioneMessage(md, EventConservazione.MESSAGE_CREATE,infoPathZipBean.getNameFileZip());

                }
            }

            // controllo se i msg inviati dentro il lotto sono tutti , caso contrario bisogna inviare un altro zip contenente il restante numero dei msg.
            if ((numMsg + 1) < messaggiDiUnLotto.size())
                infoPathZipBean.setLoadAllMessageZip(false);
            else
                infoPathZipBean.setLoadAllMessageZip(true);

        } catch (Exception e) {

            LOGGER.error("Errore in create zip: ", e);
        }
        return infoPathZipBean;
    }

    private ZipBuilder createZip(String nameGeneralFile, ESubPEC espec, InfoPathZipBean infoPathZipBean) throws IOException, NoSuchAlgorithmException, FileNotFoundException {
        ESubPECMarshall eSubPECMarshall = new ESubPECMarshall();

        // crearo xml del documento index.xml
        ByteArrayOutputStream byteArrayOutputStream4XML = eSubPECMarshall.creaIndexESubPECXml(espec);
        //System.out.println(esubXml);
        String filenameZip = nameGeneralFile + ".zip";
        infoPathZipBean.setNameFileZip(filenameZip);
        infoPathZipBean.setPathFileZip(infoPathZipBean.getBasePath() + "\\" + filenameZip);


        this.zipBuilder = new ZipBuilder(infoPathZipBean.getPathFileZip());


        return zipBuilder;
    }

    private InfoPathZipBean creaFileControllo(String nameGeneralFile, String dir, InfoPathZipBean infoPathZipBean) throws IOException, FileNotFoundException, NoSuchAlgorithmException {

        BigInteger sizeZip = getFileSize(infoPathZipBean.getPathFileZip());//dir + "\\" + finameZip);
        String cunkMD5 = UtilFile.checksum(infoPathZipBean.getPathFileZip(), "MD5");
        String xmlchk = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" standalone=\"yes\"?>\n" + "<file_chk><file_size>" + sizeZip.toString() + "</file_size><file_hash type=\"MD5\">" + cunkMD5 + "</file_hash></file_chk>";
        //creare file di controllo chk.xml
        infoPathZipBean.setNameFileControllo(nameGeneralFile + "-chk.xml");
        infoPathZipBean.setPathFileControllo(dir + "\\" + infoPathZipBean.getNameFileControllo());
        UtilFile.stringToDom(xmlchk, infoPathZipBean.getPathFileControllo());

        java.io.File filechk = new java.io.File(dir + "\\" + nameGeneralFile + "-chk.xml");
        return infoPathZipBean;
    }

    private BigInteger getFileSize(String filepath) {
        BigInteger siz = new BigInteger("0");
        java.io.File file = new java.io.File(filepath);
        if (file.exists()) {
            siz = new BigInteger(new Long(file.length()).toString());
        }
        return siz;
    }

    private List<ESubPEC.Documento> populateDoc(MailDeliveryRequest md, String basePath, LotDelivery lotto) {
        List<ESubPEC.Documento> list = new ArrayList<ESubPEC.Documento>();

        LOGGER.info("### elaborazione del msg: " + md.getMessageID());
        try {
            // controllo se ha accettazione
            if (md.getAcceptanceFileName() != null && !md.getAcceptanceFileName().trim().isEmpty()) {
                String accettazione = "accettazione";
                // getAcceptanceOutcome
                String allegato = md.getAcceptanceFileName();
                String docType = accettazione;
                String dettDocType = (md.getAcceptanceOutcome() == "1" ? accettazione : "non-" + accettazione);
                ESubPEC.Documento accet = createDocEsub(md, lotto, docType, dettDocType, allegato, basePath);

                if (accet != null)
                    list.add(accet);
                else
                    LOGGER.error("** DOCUMENTO NON CREATO " + md.getMessageID());
            }
            // controllo se ha consegna
            if (md.getDeliveryFileName() != null && !md.getDeliveryFileName().trim().isEmpty()) {
                String consegna = "consegna";
                // getAcceptanceOutcome
                String allegato = md.getDeliveryFileName();
                String docType = consegna;
                String dettDocType = (md.getDeliveryOutcome() == "1" ? "avvenuta-consegna" : "errore-" + consegna);
                ESubPEC.Documento deliv = createDocEsub(md, lotto, docType, dettDocType, allegato, basePath);
                if (deliv != null)
                    list.add(deliv);
                else
                    LOGGER.error("** DOCUMENTO NON CREATO " + md.getMessageID());


            }
//        espec.getDocumento().add(docMsg);
            return list;
        }catch (Exception e){
            LOGGER.error("** DOCUMENTO NON CREATO " + md.getMessageID());
            lottoConservazioneService.saveInfoConservazioneMessage(md, EventConservazione.MESSAGE_CREATE_ERROR, null);
            return  null;
        }
    }

    private ESubPEC.Documento createDocEsub(MailDeliveryRequest md, LotDelivery lotto, String docType, String dettDocType, String allegato, String basePath) throws Exception {


        try {
            ESubPEC.Documento docMsg = new ESubPEC.Documento();
            docMsg.setCustCode(lotto.getCustomerCode());
            SimpleDateFormat datedevformat = new SimpleDateFormat("yyyy-MM-dd");
            GregorianCalendar now = new GregorianCalendar();
            XMLGregorianCalendar xmlGregorianCalendar = null;

            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(now);
            // controllo se esiste allegato
            if (allegato != null && !allegato.trim().isEmpty()) {
                String pathFile = basePath + "\\" + allegato;
                ESubPEC.Documento.File filemsg = populateFile(allegato, pathFile);
                this.numobj++;
                docMsg.setFile(filemsg);
                addSizeZip(filemsg.getSize());
            }
            docMsg.setExtId(md.getExtID());
            docMsg.setGPecLotId(lotto.getLotId());
            docMsg.setHourDev(xmlGregorianCalendar);
            docMsg.setServizio(lotto.getService()); //
            docMsg.setMsgId("todo"); // todo datacer
            docMsg.setMkey(docType + docMsg.getMsgId());
            docMsg.setMitt("todo");// todo
            docMsg.setIdm(md.getMessageID());
            docMsg.setDest(md.getTo());
            docMsg.setDocType(docType);
            docMsg.setDettDocT(dettDocType);
            docMsg.setDateDev(xmlGregorianCalendar);
            // incremento il contatore delle dimensioni file

            return docMsg;
        } catch (Exception e) {
            LOGGER.error("*** Errore in creazione DocEsub ", e.getMessage());
            lottoConservazioneService.saveInfoConservazioneMessage(md, EventConservazione.MESSAGE_CREATE_ERROR,null);
            throw  new Exception();

        }

    }

    private void addSizeZip(BigInteger size) {
        this.sizeFile = this.sizeFile + size.longValue();
    }

    private ESubPEC.Documento.File populateFile(String file, String filepath) throws NoSuchAlgorithmException, IOException, FileNotFoundException {
        ESubPEC.Documento.File filemsg = new ESubPEC.Documento.File();
        String codeAlgoritm = "SHA-256";
        filemsg.setAlgorithm(codeAlgoritm);
        filemsg.setChecksum(UtilFile.checksum(filepath, codeAlgoritm));
        filemsg.setSize(UtilFile.getFileSize(filepath));
        filemsg.setValue(file);
        // aggiungi file allo zip
        this.zipBuilder.addSigleEmlToZipByLottoId(filepath);
        //filemsg.setLabel(label);
        return filemsg;
    }

    public int getNumeroElementi() {
        return numeroElementi;
    }

    public void setNumeroElementi(int numeroElementi) {
        this.numeroElementi = numeroElementi;
    }

    public int getMegaSize() {
        return megaSize;
    }

    public void setMegaSize(int megaSize) {
        this.megaSize = megaSize;
    }

    public ZipBuilder getZipBuilder() {
        return zipBuilder;
    }

    public void setZipBuilder(ZipBuilder zipBuilder) {
        this.zipBuilder = zipBuilder;
    }
}
