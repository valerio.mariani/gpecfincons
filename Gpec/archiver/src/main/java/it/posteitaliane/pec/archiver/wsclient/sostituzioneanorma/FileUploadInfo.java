
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FileUploadInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FileUploadInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nickAzienda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dataCreazione" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="idLottoAzienda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ext" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroBlocchi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="blockSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileUploadInfo", namespace = "http://bean.service.postecom.it", propOrder = {
    "nickAzienda",
    "tipoDoc",
    "dataCreazione",
    "idLottoAzienda",
    "ext",
    "numeroBlocchi",
    "blockSize"
})
public class FileUploadInfo {

    @XmlElement(required = true, nillable = true)
    protected String nickAzienda;
    @XmlElement(required = true, nillable = true)
    protected String tipoDoc;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(required = true, nillable = true)
    protected String idLottoAzienda;
    @XmlElement(required = true, nillable = true)
    protected String ext;
    protected int numeroBlocchi;
    protected int blockSize;

    /**
     * Gets the value of the nickAzienda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNickAzienda() {
        return nickAzienda;
    }

    /**
     * Sets the value of the nickAzienda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNickAzienda(String value) {
        this.nickAzienda = value;
    }

    /**
     * Gets the value of the tipoDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDoc() {
        return tipoDoc;
    }

    /**
     * Sets the value of the tipoDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDoc(String value) {
        this.tipoDoc = value;
    }

    /**
     * Gets the value of the dataCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Sets the value of the dataCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Gets the value of the idLottoAzienda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLottoAzienda() {
        return idLottoAzienda;
    }

    /**
     * Sets the value of the idLottoAzienda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLottoAzienda(String value) {
        this.idLottoAzienda = value;
    }

    /**
     * Gets the value of the ext property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExt() {
        return ext;
    }

    /**
     * Sets the value of the ext property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExt(String value) {
        this.ext = value;
    }

    /**
     * Gets the value of the numeroBlocchi property.
     * 
     */
    public int getNumeroBlocchi() {
        return numeroBlocchi;
    }

    /**
     * Sets the value of the numeroBlocchi property.
     * 
     */
    public void setNumeroBlocchi(int value) {
        this.numeroBlocchi = value;
    }

    /**
     * Gets the value of the blockSize property.
     * 
     */
    public int getBlockSize() {
        return blockSize;
    }

    /**
     * Sets the value of the blockSize property.
     * 
     */
    public void setBlockSize(int value) {
        this.blockSize = value;
    }

}
