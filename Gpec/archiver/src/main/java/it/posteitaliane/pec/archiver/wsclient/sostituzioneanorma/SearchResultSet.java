
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchResultSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchResultSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numPagine" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numTotRecords" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchResultSet", namespace = "http://bean.service.postecom.it", propOrder = {
    "numPagine",
    "numTotRecords"
})
@XmlSeeAlso({
    Documenti.class,
    Lotti.class
})
public class SearchResultSet {

    protected int numPagine;
    protected int numTotRecords;

    /**
     * Gets the value of the numPagine property.
     * 
     */
    public int getNumPagine() {
        return numPagine;
    }

    /**
     * Sets the value of the numPagine property.
     * 
     */
    public void setNumPagine(int value) {
        this.numPagine = value;
    }

    /**
     * Gets the value of the numTotRecords property.
     * 
     */
    public int getNumTotRecords() {
        return numTotRecords;
    }

    /**
     * Sets the value of the numTotRecords property.
     * 
     */
    public void setNumTotRecords(int value) {
        this.numTotRecords = value;
    }

}
