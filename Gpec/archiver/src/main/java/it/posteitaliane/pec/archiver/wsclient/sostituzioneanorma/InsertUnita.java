
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nickAziendaPadre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="unita" type="{http://bean.service.postecom.it}Unita"/>
 *         &lt;element name="admin" type="{http://bean.service.postecom.it}AdminUser"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nickAziendaPadre",
    "unita",
    "admin"
})
@XmlRootElement(name = "insertUnita")
public class InsertUnita {

    @XmlElement(required = true)
    protected String nickAziendaPadre;
    @XmlElement(required = true)
    protected Unita unita;
    @XmlElement(required = true)
    protected AdminUser admin;

    /**
     * Gets the value of the nickAziendaPadre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNickAziendaPadre() {
        return nickAziendaPadre;
    }

    /**
     * Sets the value of the nickAziendaPadre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNickAziendaPadre(String value) {
        this.nickAziendaPadre = value;
    }

    /**
     * Gets the value of the unita property.
     * 
     * @return
     *     possible object is
     *     {@link Unita }
     *     
     */
    public Unita getUnita() {
        return unita;
    }

    /**
     * Sets the value of the unita property.
     * 
     * @param value
     *     allowed object is
     *     {@link Unita }
     *     
     */
    public void setUnita(Unita value) {
        this.unita = value;
    }

    /**
     * Gets the value of the admin property.
     * 
     * @return
     *     possible object is
     *     {@link AdminUser }
     *     
     */
    public AdminUser getAdmin() {
        return admin;
    }

    /**
     * Sets the value of the admin property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdminUser }
     *     
     */
    public void setAdmin(AdminUser value) {
        this.admin = value;
    }

}
