package it.posteitaliane.pec.archiver.service;

import it.posteitaliane.pec.archiver.wsclient.rapportodiversamento.PosteDocException_Exception;
import it.posteitaliane.pec.archiver.wsclient.rapportodiversamento.PosteDocWS;
import it.posteitaliane.pec.archiver.wsclient.rapportodiversamento.PosteDocWSService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.Map;

@Service
public class RapportoVersamentoWSService {
    @Value("${nfs.archiver.ws.user.client.poste.conservazione}")
    String username;
    @Value("${nfs.archiver.ws.password.client.poste.conservazione}")
    String password;


    public String getRapportoDiVersamento(int idLotto) throws PosteDocException_Exception {
        PosteDocWSService wsService = new PosteDocWSService();
        PosteDocWS pdws = wsService.getPosteDocWS();

        BindingProvider prov = ((BindingProvider) pdws);
        Map http_headers = (Map) ((BindingProvider) pdws).getRequestContext().get(MessageContext.HTTP_REQUEST_HEADERS);

        ((BindingProvider) pdws).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, username);
        ((BindingProvider) pdws).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, password);

        String statoVersamento = pdws.getRapportoDiVersamento(idLotto);
        return statoVersamento;
    }
}
