
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Fault_QNAME = new QName("urn:postecom", "fault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Upload }
     * 
     */
    public Upload createUpload() {
        return new Upload();
    }

    /**
     * Create an instance of {@link GetLotto }
     * 
     */
    public GetLotto createGetLotto() {
        return new GetLotto();
    }

    /**
     * Create an instance of {@link GetPaginaLotti }
     * 
     */
    public GetPaginaLotti createGetPaginaLotti() {
        return new GetPaginaLotti();
    }

    /**
     * Create an instance of {@link SearchResultSet }
     * 
     */
    public SearchResultSet createSearchResultSet() {
        return new SearchResultSet();
    }

    /**
     * Create an instance of {@link GetDescClasseDocumentaleResponse }
     * 
     */
    public GetDescClasseDocumentaleResponse createGetDescClasseDocumentaleResponse() {
        return new GetDescClasseDocumentaleResponse();
    }

    /**
     * Create an instance of {@link GetUsedStorageByYearResponse }
     * 
     */
    public GetUsedStorageByYearResponse createGetUsedStorageByYearResponse() {
        return new GetUsedStorageByYearResponse();
    }

    /**
     * Create an instance of {@link GetPaginaDocumenti }
     * 
     */
    public GetPaginaDocumenti createGetPaginaDocumenti() {
        return new GetPaginaDocumenti();
    }

    /**
     * Create an instance of {@link File }
     * 
     */
    public File createFile() {
        return new File();
    }

    /**
     * Create an instance of {@link CreateLottoResponse }
     * 
     */
    public CreateLottoResponse createCreateLottoResponse() {
        return new CreateLottoResponse();
    }

    /**
     * Create an instance of {@link InsertUtente }
     * 
     */
    public InsertUtente createInsertUtente() {
        return new InsertUtente();
    }

    /**
     * Create an instance of {@link AdminUser }
     * 
     */
    public AdminUser createAdminUser() {
        return new AdminUser();
    }

    /**
     * Create an instance of {@link Lotto }
     * 
     */
    public Lotto createLotto() {
        return new Lotto();
    }

    /**
     * Create an instance of {@link GetPaginaLottiResponse }
     * 
     */
    public GetPaginaLottiResponse createGetPaginaLottiResponse() {
        return new GetPaginaLottiResponse();
    }

    /**
     * Create an instance of {@link PosteDocException }
     * 
     */
    public PosteDocException createPosteDocException() {
        return new PosteDocException();
    }

    /**
     * Create an instance of {@link GetUsedStorageByYearForTypeResponse }
     * 
     */
    public GetUsedStorageByYearForTypeResponse createGetUsedStorageByYearForTypeResponse() {
        return new GetUsedStorageByYearForTypeResponse();
    }

    /**
     * Create an instance of {@link Unita }
     * 
     */
    public Unita createUnita() {
        return new Unita();
    }

    /**
     * Create an instance of {@link GetFileNotificaResponse }
     * 
     */
    public GetFileNotificaResponse createGetFileNotificaResponse() {
        return new GetFileNotificaResponse();
    }

    /**
     * Create an instance of {@link AttrDesc }
     * 
     */
    public AttrDesc createAttrDesc() {
        return new AttrDesc();
    }

    /**
     * Create an instance of {@link GetFileNotifica }
     * 
     */
    public GetFileNotifica createGetFileNotifica() {
        return new GetFileNotifica();
    }

    /**
     * Create an instance of {@link GetDocumento }
     * 
     */
    public GetDocumento createGetDocumento() {
        return new GetDocumento();
    }

    /**
     * Create an instance of {@link ArrayOfTns1Lotto }
     * 
     */
    public ArrayOfTns1Lotto createArrayOfTns1Lotto() {
        return new ArrayOfTns1Lotto();
    }

    /**
     * Create an instance of {@link GetPaginaDocumentiResponse }
     * 
     */
    public GetPaginaDocumentiResponse createGetPaginaDocumentiResponse() {
        return new GetPaginaDocumentiResponse();
    }

    /**
     * Create an instance of {@link Documento }
     * 
     */
    public Documento createDocumento() {
        return new Documento();
    }

    /**
     * Create an instance of {@link GetLottoAzienda }
     * 
     */
    public GetLottoAzienda createGetLottoAzienda() {
        return new GetLottoAzienda();
    }

    /**
     * Create an instance of {@link UploadResponse }
     * 
     */
    public UploadResponse createUploadResponse() {
        return new UploadResponse();
    }

    /**
     * Create an instance of {@link Documenti }
     * 
     */
    public Documenti createDocumenti() {
        return new Documenti();
    }

    /**
     * Create an instance of {@link AttrValue }
     * 
     */
    public AttrValue createAttrValue() {
        return new AttrValue();
    }

    /**
     * Create an instance of {@link SetUserGrantResponse }
     * 
     */
    public SetUserGrantResponse createSetUserGrantResponse() {
        return new SetUserGrantResponse();
    }

    /**
     * Create an instance of {@link GetProfiliUtente }
     * 
     */
    public GetProfiliUtente createGetProfiliUtente() {
        return new GetProfiliUtente();
    }

    /**
     * Create an instance of {@link CreateLotto }
     * 
     */
    public CreateLotto createCreateLotto() {
        return new CreateLotto();
    }

    /**
     * Create an instance of {@link Filtro }
     * 
     */
    public Filtro createFiltro() {
        return new Filtro();
    }

    /**
     * Create an instance of {@link InsertUnita }
     * 
     */
    public InsertUnita createInsertUnita() {
        return new InsertUnita();
    }

    /**
     * Create an instance of {@link ArrayOfTns1AttrValue }
     * 
     */
    public ArrayOfTns1AttrValue createArrayOfTns1AttrValue() {
        return new ArrayOfTns1AttrValue();
    }

    /**
     * Create an instance of {@link ArrayOfTns1InfoDocumento }
     * 
     */
    public ArrayOfTns1InfoDocumento createArrayOfTns1InfoDocumento() {
        return new ArrayOfTns1InfoDocumento();
    }

    /**
     * Create an instance of {@link GetDocumentoFirmatoResponse }
     * 
     */
    public GetDocumentoFirmatoResponse createGetDocumentoFirmatoResponse() {
        return new GetDocumentoFirmatoResponse();
    }

    /**
     * Create an instance of {@link GetUsedStorageByYearForType }
     * 
     */
    public GetUsedStorageByYearForType createGetUsedStorageByYearForType() {
        return new GetUsedStorageByYearForType();
    }

    /**
     * Create an instance of {@link InsertUnitaResponse }
     * 
     */
    public InsertUnitaResponse createInsertUnitaResponse() {
        return new InsertUnitaResponse();
    }

    /**
     * Create an instance of {@link ArrayOfXsdString }
     * 
     */
    public ArrayOfXsdString createArrayOfXsdString() {
        return new ArrayOfXsdString();
    }

    /**
     * Create an instance of {@link FileUploadInfo }
     * 
     */
    public FileUploadInfo createFileUploadInfo() {
        return new FileUploadInfo();
    }

    /**
     * Create an instance of {@link InfoDocumento }
     * 
     */
    public InfoDocumento createInfoDocumento() {
        return new InfoDocumento();
    }

    /**
     * Create an instance of {@link SetUserGrant }
     * 
     */
    public SetUserGrant createSetUserGrant() {
        return new SetUserGrant();
    }

    /**
     * Create an instance of {@link Lotti }
     * 
     */
    public Lotti createLotti() {
        return new Lotti();
    }

    /**
     * Create an instance of {@link GetUsedStorageByYear }
     * 
     */
    public GetUsedStorageByYear createGetUsedStorageByYear() {
        return new GetUsedStorageByYear();
    }

    /**
     * Create an instance of {@link CloseResponse }
     * 
     */
    public CloseResponse createCloseResponse() {
        return new CloseResponse();
    }

    /**
     * Create an instance of {@link GetLottoAziendaResponse }
     * 
     */
    public GetLottoAziendaResponse createGetLottoAziendaResponse() {
        return new GetLottoAziendaResponse();
    }

    /**
     * Create an instance of {@link InsertUtenteResponse }
     * 
     */
    public InsertUtenteResponse createInsertUtenteResponse() {
        return new InsertUtenteResponse();
    }

    /**
     * Create an instance of {@link GetDescClasseDocumentale }
     * 
     */
    public GetDescClasseDocumentale createGetDescClasseDocumentale() {
        return new GetDescClasseDocumentale();
    }

    /**
     * Create an instance of {@link ProfiloUtente }
     * 
     */
    public ProfiloUtente createProfiloUtente() {
        return new ProfiloUtente();
    }

    /**
     * Create an instance of {@link GetDocumentoResponse }
     * 
     */
    public GetDocumentoResponse createGetDocumentoResponse() {
        return new GetDocumentoResponse();
    }

    /**
     * Create an instance of {@link GetDocumentoFirmato }
     * 
     */
    public GetDocumentoFirmato createGetDocumentoFirmato() {
        return new GetDocumentoFirmato();
    }

    /**
     * Create an instance of {@link GetProfiliUtenteResponse }
     * 
     */
    public GetProfiliUtenteResponse createGetProfiliUtenteResponse() {
        return new GetProfiliUtenteResponse();
    }

    /**
     * Create an instance of {@link StatoMemoria }
     * 
     */
    public StatoMemoria createStatoMemoria() {
        return new StatoMemoria();
    }

    /**
     * Create an instance of {@link Close }
     * 
     */
    public Close createClose() {
        return new Close();
    }

    /**
     * Create an instance of {@link GetLottoResponse }
     * 
     */
    public GetLottoResponse createGetLottoResponse() {
        return new GetLottoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PosteDocException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:postecom", name = "fault")
    public JAXBElement<PosteDocException> createFault(PosteDocException value) {
        return new JAXBElement<PosteDocException>(_Fault_QNAME, PosteDocException.class, null, value);
    }

}
