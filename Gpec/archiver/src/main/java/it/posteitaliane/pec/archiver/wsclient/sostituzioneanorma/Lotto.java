
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Lotto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Lotto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idUtente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="dataCreazione" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dataArchiviazione" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dataConservazione" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="numDocumenti" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stato" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idLotto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idLottoAzienda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Lotto", namespace = "http://bean.service.postecom.it", propOrder = {
    "idUtente",
    "dataCreazione",
    "dataArchiviazione",
    "dataConservazione",
    "numDocumenti",
    "stato",
    "idLotto",
    "idLottoAzienda"
})
public class Lotto {

    @XmlElement(required = true, nillable = true)
    protected String idUtente;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataArchiviazione;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataConservazione;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer numDocumenti;
    protected int stato;
    protected long idLotto;
    @XmlElement(required = true, nillable = true)
    protected String idLottoAzienda;

    /**
     * Gets the value of the idUtente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUtente() {
        return idUtente;
    }

    /**
     * Sets the value of the idUtente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUtente(String value) {
        this.idUtente = value;
    }

    /**
     * Gets the value of the dataCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Sets the value of the dataCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Gets the value of the dataArchiviazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataArchiviazione() {
        return dataArchiviazione;
    }

    /**
     * Sets the value of the dataArchiviazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataArchiviazione(XMLGregorianCalendar value) {
        this.dataArchiviazione = value;
    }

    /**
     * Gets the value of the dataConservazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataConservazione() {
        return dataConservazione;
    }

    /**
     * Sets the value of the dataConservazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataConservazione(XMLGregorianCalendar value) {
        this.dataConservazione = value;
    }

    /**
     * Gets the value of the numDocumenti property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumDocumenti() {
        return numDocumenti;
    }

    /**
     * Sets the value of the numDocumenti property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumDocumenti(Integer value) {
        this.numDocumenti = value;
    }

    /**
     * Gets the value of the stato property.
     * 
     */
    public int getStato() {
        return stato;
    }

    /**
     * Sets the value of the stato property.
     * 
     */
    public void setStato(int value) {
        this.stato = value;
    }

    /**
     * Gets the value of the idLotto property.
     * 
     */
    public long getIdLotto() {
        return idLotto;
    }

    /**
     * Sets the value of the idLotto property.
     * 
     */
    public void setIdLotto(long value) {
        this.idLotto = value;
    }

    /**
     * Gets the value of the idLottoAzienda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLottoAzienda() {
        return idLottoAzienda;
    }

    /**
     * Sets the value of the idLottoAzienda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLottoAzienda(String value) {
        this.idLottoAzienda = value;
    }

}
