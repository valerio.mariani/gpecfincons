package it.posteitaliane.pec.archiver.integration;

import brave.Tracer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rabbitmq.client.Channel;
import it.posteitaliane.pec.archiver.outcome.zip.ZipBuilder;
import it.posteitaliane.pec.archiver.service.BuildSendService;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.PosteDocWS;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.PosteDocWSService;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.xml.ws.BindingProvider;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

@Component
public class EventsMessageListener implements ChannelAwareMessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventsMessageListener.class);

    @Autowired Gson gson;
    @Autowired Tracer tracer;
    @Autowired MessageConverter converter;

    @Value("${nfs.shared.stage.folder}") String stageFolder;

    @Autowired
    BuildSendService buildSendService;
    @Override
    public void onMessage(Message rabbitMessagePayload, Channel channel) throws Exception {

        LOGGER.info(" * * * * * * * * * * * Payload: " +
                "\n * * * * * " + new String( rabbitMessagePayload.getBody() ) + "\n * * * * * * * * * * * * " );
        long tag = rabbitMessagePayload.getMessageProperties().getDeliveryTag();
        LOGGER.info("ESUB ARCHIVER RECEIPT EVENT CONSUMER[ " +rabbitMessagePayload.getMessageProperties().getReceivedRoutingKey() + "|" + tag + " ] : " + hashCode());
        String eventType = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("EVENT-TYPE");

        switch (EventType.valueOf(eventType)){

            case LOTTO_OUTCOME_GENERATO:
                LOGGER.info("messaggio tipo outcome arrivato " + eventType);
                LotDelivery message = (LotDelivery) converter.fromMessage(rabbitMessagePayload);
                LOGGER.info("\n >>>>>>> " + message);

                String lotId = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("lotId");
                LOGGER.info("Analisi lotto con id: "+lotId );
                //crea il zip e manda al ws
               // buildSendService.buildSend(lotId);




                channel.basicAck(tag, false);
                break;

            case SYSTEM_RESTART:
                // TODO ancora da implementare:
                // difficoltà: non si possono semplicemente riavviare tutti i container (forse)
                // ma bisogna ripristinare lo stato così come è stato lasciato alla ricezione dell'evento SUSPEND
                channel.basicAck(tag, false);
                break;
            default:
                channel.basicAck(tag, false);
                break;
        }

    }

    /**
     *
     * @param payload
     * @param rootValue
     * @return
     */
    protected Map<String, Object> getAsMap(byte[] payload, String rootValue) {
        Type type = new TypeToken<Map<String, Object>>() {}.getType();
        return gson.fromJson(new String(payload), type);
    }



}
