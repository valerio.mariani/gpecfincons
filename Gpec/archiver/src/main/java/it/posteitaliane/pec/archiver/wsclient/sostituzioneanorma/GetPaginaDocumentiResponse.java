
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getPaginaDocumentiReturn" type="{http://bean.service.postecom.it}Documenti"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getPaginaDocumentiReturn"
})
@XmlRootElement(name = "getPaginaDocumentiResponse")
public class GetPaginaDocumentiResponse {

    @XmlElement(required = true)
    protected Documenti getPaginaDocumentiReturn;

    /**
     * Gets the value of the getPaginaDocumentiReturn property.
     * 
     * @return
     *     possible object is
     *     {@link Documenti }
     *     
     */
    public Documenti getGetPaginaDocumentiReturn() {
        return getPaginaDocumentiReturn;
    }

    /**
     * Sets the value of the getPaginaDocumentiReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Documenti }
     *     
     */
    public void setGetPaginaDocumentiReturn(Documenti value) {
        this.getPaginaDocumentiReturn = value;
    }

}
