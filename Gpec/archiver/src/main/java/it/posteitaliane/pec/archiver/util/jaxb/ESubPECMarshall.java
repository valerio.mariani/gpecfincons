package it.posteitaliane.pec.archiver.util.jaxb;
import it.posteitaliane.pec.archiver.xsd.bean.generated.ESubPEC;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class ESubPECMarshall {
    public String convertESubPECToXml(ESubPEC eSubPEC){
        try {
            JAXBContext jc = JAXBContext.newInstance(ESubPEC.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "iso-8859-1");
            StringWriter sw = new StringWriter();
            marshaller.marshal(eSubPEC, sw);
            return sw.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
            System.err.println("ERRORE !!!! !!!! !!! ");
        }

        return "";
    }

    public ByteArrayOutputStream creaIndexESubPECXml(ESubPEC eSubPEC){
        try {
            JAXBContext jc = JAXBContext.newInstance(ESubPEC.class);
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "iso-8859-1");
            //StringWriter sw = new StringWriter();
            ByteArrayOutputStream byteArrayOutputStream4XML = new ByteArrayOutputStream();
            marshaller.marshal(eSubPEC, byteArrayOutputStream4XML);
            return byteArrayOutputStream4XML;
        } catch (JAXBException e) {
            e.printStackTrace();
            System.err.println("ERRORE !!!! !!!! !!! ");
        }

        return null;
    }


}
