package it.posteitaliane.pec.archiver.service;

import brave.Span;
import brave.Tracer;
import it.posteitaliane.pec.archiver.bean.InfoPathZipBean;
import it.posteitaliane.pec.archiver.outcome.zip.ZipBuilder;
import it.posteitaliane.pec.archiver.util.jaxb.ESubPECMarshall;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.*;
//import it.posteitaliane.pec.archiver.xsd.bean.generated.ESubPEC;
//import it.posteitaliane.pec.archiver.xsd.bean.generated.ObjectFactory;
import it.posteitaliane.pec.archiver.xsd.bean.generated.ESubPEC;
import it.posteitaliane.pec.common.model.EventConservazione;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.InfoConservazione;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.repositories.LotRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@RefreshScope
public class BuildSendService {


    //  @Value("${nfs.archiver.ws.nickAzienda}")
    //  String nickAzienda;
    //  @Value("${nfs.archiver.ws.classeDocumentale}")
    //  String classeDocumentale;
    private static final Logger LOGGER = LoggerFactory.getLogger(BuildSendService.class);
    @Autowired
    Tracer tracer;
    //  @Autowired
    //  MailDeliveryRepository mailDeliveryRepository;
    @Autowired
    LotRepository lotRepository;
    @Autowired
    BuildService buildService;
    @Value("${outcome.basepath}")
    String basePath;
    @Autowired
    SendConservazioneWSService sendConservazioneWSService;
    private static final Random r = new Random();
    @Value("${nfs.archiver.ws.nickAzienda}")
    String nickAzienda;
    @Autowired
    LottoConservazioneService lottoConservazioneService;



    @Scheduled(fixedDelayString = "${outcome.fixedDelay.buildsend.conservazioneanorma}") //TODO
    @NewSpan(value = "controller.scheduled.conservazioneanorma")
    public void buildSendService() throws IOException {
        LOGGER.info("Avvio attività schedulata per la conservazione a norma");

        Span scheduledConservazioneSend = tracer.nextSpan().name("controller.scheduled.conservazioneanorma").kind(Span.Kind.CLIENT);
        scheduledConservazioneSend.tag("SCHEDULE", "Avvio attività schedulata mandare a conservazione a norma");
        scheduledConservazioneSend.start();

        try (Tracer.SpanInScope ws = tracer.withSpanInScope(scheduledConservazioneSend)) {
            LotDelivery lottoCurrent = null;
            List<String> eventsToSearch = new ArrayList<>();
            eventsToSearch.add(EventType.LOTTO_OUTCOME_GENERATO.name());
            //TODO DA MODIFICARE LA QUERY IN MANIERA DI FILTRO PER ATTRIBUTO CONSERVAZIONE A NORMA
            List<LotDelivery> deliveryListByStatus = lotRepository.findByCurrentStatusIn(eventsToSearch);

            if (deliveryListByStatus != null) {
                LOGGER.info("TROVATI " + deliveryListByStatus.size() + " LOTTI DA INVIARE A CONSERVAZIONE ");

                for (LotDelivery lotto : deliveryListByStatus) {

                    // todo TEST GESTIONE MARCATURE LOTTO DA INVIARE
                    lotto = lottoConservazioneService.getLottoAconservazione(lotto);
                    if(lotto != null){
                        LOGGER.info("TROVATI " + deliveryListByStatus.size() + " LOTTI DA INVIARE A CONSERVAZIONE ");
                        continue;
                    }
                    lottoCurrent=lotto;
                    InfoPathZipBean infoPathZipBean = new InfoPathZipBean();
                    int numeroLotto=1;
                    // caso in cui il  singolo lotto supera il limite imposto per il zip dell invio. Nel caso ciclo sempre i messaggi dello stesso lotto finchè non si mandano tutti.
                    while (!infoPathZipBean.isLoadAllMessageZip()) {
                            String lottoIdbase = getIdRandom();
                            String idCalcolato=calcolaNumLotto(lottoIdbase,numeroLotto);

                        infoPathZipBean = buildService.createZipFromLotto(lotto, basePath,idCalcolato);
                        try {
                            if (infoPathZipBean != null && infoPathZipBean.getPathFileControllo() != null && !infoPathZipBean.getPathFileControllo().isEmpty() && infoPathZipBean.getPathFileZip() != null && !infoPathZipBean.getPathFileZip().isEmpty()) {
                                infoPathZipBean =  sendConservazioneWSService.inviaConservazione(infoPathZipBean);
                            }
                            // lotto  aggiorno DB
                            populateLottoAfterInvio(lottoCurrent.getLotId(),infoPathZipBean, EventConservazione.CHUNCK_ZIP_LOTTO_SENDER);
                            // todo se tutti i msg del lotto sono strati elaborati aggiornare lo stato del LOTTO in base a se tutti i msg sono andati a  buon fine
                            EventConservazione eventStatoLotto = null;
                            if(infoPathZipBean.isLoadAllMessageZip()) {
                                eventStatoLotto = EventConservazione.LOTTO_FULL_SENDER;
                                lottoConservazioneService.chiudiLottoConservazione(lottoCurrent.getLotId(), eventStatoLotto);
                            }else {
                                eventStatoLotto = EventConservazione.LOTTO_CHUNK_SENDER;
                                lottoConservazioneService.aggiornaStatolotto(lottoCurrent.getLotId(), eventStatoLotto);
                            }
                        } catch (Exception e) {
                            LOGGER.error("### ERRORE IN INVIO FILE WS", e);
                            populateLottoAfterInvio(lottoCurrent.getLotId(), infoPathZipBean, EventConservazione.CHUNCK_ZIP_LOTTO_SENDER_ERROR);
                        }
                    }

                }

            }

        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("### ERRORE ", e);
        } finally {
            scheduledConservazioneSend.finish(); // note the scope is independent of the span. Always finish a span.
        }


    }

    private void populateLottoAfterInvio(String idLotto, InfoPathZipBean infoPathZipBean, EventConservazione conservazione) {
        InfoConservazione infoConservazione = InfoConservazione.builder().stato(conservazione).build();
        infoConservazione.setZipRiferimento(infoPathZipBean.getNameFileZip());
        infoConservazione.setDataCreazione(infoPathZipBean.getDataCreazione());
        infoConservazione.setIdLottoAzienda(infoPathZipBean.getIdLottoAzienda());
        infoConservazione.setTipoDoc(infoPathZipBean.getTipoDoc());
        infoConservazione.setNickAzienda(this.nickAzienda);
        lottoConservazioneService.saveInfoConservazioneLotto( idLotto,infoConservazione);

    }

    private String calcolaNumLotto(String lottoIdbase, int numeroLotto) {
        String numb = Integer.toString(numeroLotto);
        if(numeroLotto < 10){
            numb = "0"+numb;
        }
        return numb;
    }

    public String getIdRandom() {

        int ind = r.ints(1, 99999999).limit(1).findFirst().getAsInt();
        String id = Integer.toString(ind).substring(0, 6);

        return id;
    }

    public void buildSend() {
        //   PATH GENERALI
        //G23L_20190324_
        String lotId = "11123123";
        String basePath = "C:\\prova\\";
        // String dir = basePath + "tmp_" + lotId;
        String nickAzienda = "postepcl";
        String classeDocumentale = "ESubPEC";
        // 20190321 momento chiedere
        SimpleDateFormat smp = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        String dataNameFile = smp.format(cal.getTime());
        String nameGeneralFile = nickAzienda + "-" + classeDocumentale + "-" + dataNameFile + "-" + lotId;
        try {
            // creo il zip e il file di controllo dai i msg
            String path = createfileToSend(nameGeneralFile, basePath, lotId);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }//fine metodo

    private it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.Lotto mandaFileWS(String nameGeneralFile, String dir,String dataNameFile, String classeDocumentale, String lotId) throws PosteDocException_Exception, FileNotFoundException, NoSuchAlgorithmException, IOException {

        PosteDocWSService wsService = new PosteDocWSService();
        PosteDocWS pdws = wsService.getPosteDocWS();

        BindingProvider prov = ((BindingProvider) pdws);
        Map http_headers = (Map) ((BindingProvider) pdws).getRequestContext().get(MessageContext.HTTP_REQUEST_HEADERS);

        ((BindingProvider) pdws).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "postepcl.webserv");
        ((BindingProvider) pdws).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "cambiami+1");

        String nomeFileControllo = nameGeneralFile + "-chk.xml";
        byte[] chkFileByteArray = readBytesFromFile(dir + "//" + nameGeneralFile + "-chk.xml");
        int uploadBlockSize = 512;
        long fileSize = 0;

        it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.File chkFile = new it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.File();

        chkFile.setNome(nomeFileControllo);
        chkFile.setData(chkFileByteArray);
        String lottoFileNameZip = nameGeneralFile + ".zip";
        java.io.File lottoFile = new java.io.File(dir + "//" + lottoFileNameZip);
        // creo lotto
        FileUploadInfo uploadFileInfo = pdws.createLotto(lottoFileNameZip, uploadBlockSize, lottoFile.length(), chkFile);
        // upload dello zip
        FileInputStream file = new FileInputStream(lottoFile);

        byte[] buffer = new byte[uploadBlockSize];
        int n = 0;
        int blockId = 0;
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.reset();
        while ((n = file.read(buffer)) > 0) {
            byte[] buff;
            if (n != buffer.length) {
                buff = new byte[n];
                System.arraycopy(buffer, 0, buff, 0, n);
            } else
                buff = buffer;

            pdws.upload(uploadFileInfo, buff, blockId++);
            md.update(buff);
        }
        //    BigInteger number = new BigInteger(1, md.digest());


        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));
        }


        //String md5Hex = number.toString(16);
        String md5Hex = result.toString();
        // CHIUDERE il blocco
        pdws.close(uploadFileInfo, md5Hex);

        it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.Lotto lottoAnorma = pdws.getLotto(dataNameFile,lotId,classeDocumentale);
        return lottoAnorma;
    }

    //,String dir
    private String createfileToSend(String nameGeneralFile, String basePath, String lotId) throws IOException, DatatypeConfigurationException, FileNotFoundException, NoSuchAlgorithmException {
        //  eliminatCartellaEsistente(dir);
        // creo cartella temporanea
        // new java.io.File(dir).mkdirs();

        it.posteitaliane.pec.archiver.xsd.bean.generated.ObjectFactory objFactXsd = new it.posteitaliane.pec.archiver.xsd.bean.generated.ObjectFactory();
        ESubPEC espec = objFactXsd.createESubPEC();
        List<ESubPEC.Documento> listaDoc = espec.getDocumento();
        String custmcode = "3535"; //lotDelivery.getcustomerCode();
        //      List<MailDeliveryRequest> messaggiDiUnLotto = mailDeliveryRepository.findByLotId(lotId);
//for (MailDeliveryRequest md : messaggiDiUnLotto) {
        ESubPEC.Documento docMsg = new ESubPEC.Documento();
        docMsg.setCustCode(custmcode);
        GregorianCalendar now = new GregorianCalendar();
        XMLGregorianCalendar xmlGregorianCalendar = null;

        xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(now);
        String allegato = "C0A8378F5C7FA0D5.eml";//md.deliveryFileName()

        docMsg.setDocType("accettazione");
        docMsg.setDettDocT("non-accettata");
        String pathFile = basePath + "\\" + allegato;
        // sposto i file nella cartella temporanea
        // prepareFile(basePath + allegato, pathFile);


        ESubPEC.Documento.File filemsg = populateFile("GestioneDocumentaleAdvanced", allegato, pathFile);
        docMsg.setFile(filemsg);
        docMsg.setExtId("8de24496-58e0-46ca-930c-e4a91b3a90db");
        docMsg.setGPecLotId(lotId);
        docMsg.setHourDev(xmlGregorianCalendar);
        docMsg.setServizio("GestorePec"); // todo
        docMsg.setMsgId("567802");
        docMsg.setMkey("391499");
        docMsg.setMitt("gestorePec@pec.yahoo");
        docMsg.setIdm("34352-391499");
        docMsg.setDateDev(xmlGregorianCalendar);
        docMsg.setDest("pectest23l6@coll.postecert.it");
        // aggiunto il msg alla lista
        espec.getDocumento().add(docMsg);
        //  }// chiusura for

        ZipBuilder zipBuilder = createZip(nameGeneralFile, basePath, espec);
//aggiunta allo ZIP delle ricevute .eml nella cartella indicata
        zipBuilder.addSigleEmlToZipByLottoId(pathFile);

        zipBuilder.finalizeZip();
        String finameZip = nameGeneralFile + ".zip";
        return basePath + "\\" + finameZip;
    }

    private void eliminatCartellaEsistente(String dir) {
        java.io.File index = new java.io.File(dir);

        if (index != null) {
            String[] entries = index.list();
            if (entries != null) {
                for (String s : entries) {
                    java.io.File currentFile = new java.io.File(index.getPath(), s);
                    currentFile.delete();
                }
            }
        }
        new java.io.File(dir).delete();
    }

    private ZipBuilder createZip(String nameGeneralFile, String dir, ESubPEC espec) throws IOException, NoSuchAlgorithmException, FileNotFoundException {
        ESubPECMarshall eSubPECMarshall = new ESubPECMarshall();

        // crearo xml del documento index.xml
        ByteArrayOutputStream byteArrayOutputStream4XML = eSubPECMarshall.creaIndexESubPECXml(espec);
        //System.out.println(esubXml);
        String finameZip = nameGeneralFile + ".zip";
        ZipBuilder zipBuilder = null;

        zipBuilder = new ZipBuilder(dir + "\\" + finameZip);

        //Aggiunta file XML riepilogativo
        zipBuilder.zipNewEntryFromByteArray("index.xml", byteArrayOutputStream4XML.toByteArray());


        creaFileControllo(nameGeneralFile, dir, finameZip);
        return zipBuilder;
      //  return dir + "\\" + finameZip;
    }

    private String creaFileControllo(String nameGeneralFile, String dir, String finameZip) throws IOException, FileNotFoundException, NoSuchAlgorithmException {

        BigInteger sizeZip = getFileSize(dir + "\\" + finameZip);
        String cunkMD5 = checksum(dir + "\\" + finameZip, "MD5");
        String xmlchk = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" standalone=\"yes\"?>\n" + "<file_chk><file_size>" + sizeZip.toString() + "</file_size><file_hash type=\"MD5\">" + cunkMD5 + "</file_hash></file_chk>";
        //creare file di controllo chk.xml

        stringToDom(xmlchk, dir + "\\" + nameGeneralFile + "-chk.xml");

        java.io.File filechk = new java.io.File(dir + "\\" + nameGeneralFile + "-chk.xml");
        return filechk.getAbsolutePath();
    }


    private void prepareFile(String pathOrg, String pathDest) {
        java.io.File src = new java.io.File(pathOrg);
        java.io.File dest = new java.io.File(pathDest);
        try {
            copyFile(src, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void copyFile(java.io.File src, java.io.File dest) throws IOException {
        java.nio.file.Files.copy(src.toPath(), dest.toPath());
    }

    public static void stringToDom(String xmlSource, String pathFileName)
            throws IOException {
        java.io.FileWriter fw = new java.io.FileWriter(pathFileName);
        fw.write(xmlSource);
        fw.close();
    }

    private ESubPEC.Documento.File populateFile(String label, String file, String filepath) throws NoSuchAlgorithmException, IOException, FileNotFoundException {
        ESubPEC.Documento.File filemsg = new ESubPEC.Documento.File();
        String codeAlgoritm = "SHA-256";
        filemsg.setAlgorithm(codeAlgoritm);
        filemsg.setChecksum(checksum(filepath, codeAlgoritm));
        filemsg.setSize(getFileSize(filepath));
        filemsg.setValue(file);
        //filemsg.setLabel(label);
        return filemsg;
    }

    private BigInteger getFileSize(String filepath) {
        BigInteger siz = new BigInteger("0");
        java.io.File file = new java.io.File(filepath);
        if (file.exists()) {
            siz = new BigInteger(new Long(file.length()).toString());
        }
        return siz;
    }

    private String checksum(String filepath, String codeAlgoritm) throws FileNotFoundException, IOException, NoSuchAlgorithmException {
        // DigestInputStream is better, but you also can hash file like this.

        MessageDigest md = null;

        md = MessageDigest.getInstance(codeAlgoritm);
        md.reset();
        InputStream fis = new FileInputStream(filepath);
        byte[] buffer = new byte[1024];
        int nread;
        while ((nread = fis.read(buffer)) != -1) {
            md.update(buffer, 0, nread);
        }


        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));
        }
        return result.toString();

    }

    private byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            java.io.File file = new java.io.File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return bytesArray;

    }

}