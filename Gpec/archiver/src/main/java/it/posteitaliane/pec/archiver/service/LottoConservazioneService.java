package it.posteitaliane.pec.archiver.service;

import it.posteitaliane.pec.archiver.coucheBaseConservazione.ConservazioneService;
import it.posteitaliane.pec.common.model.EventConservazione;
import it.posteitaliane.pec.common.model.InfoConservazione;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RefreshScope
public class LottoConservazioneService {
    @Autowired
    ConservazioneService conservazioneService;
    @Autowired
    MailDeliveryRepository mailDeliveryRepository;
    @Autowired
    LotRepository lotRepository;

    // filtra i lotti da prendere in carico
    public LotDelivery getLottoAconservazione(LotDelivery lotDelivery) {

        String lottoId = lotDelivery.getLotId();
        if (conservazioneService.insertLockLot(lottoId, EventConservazione.TAKEN_OVER) != null)
            return lotDelivery;
        else
            return null;
    }


    public void saveInfoConservazioneMessage(MailDeliveryRequest md, EventConservazione eventConservazione, String nameFileZip) {
        InfoConservazione infoConservazione = InfoConservazione.builder().stato(eventConservazione).build();
        if (nameFileZip != null && !nameFileZip.trim().isEmpty()) {
            infoConservazione.setZipRiferimento(nameFileZip);
        }

        md.setInfoConservazione(infoConservazione);
        mailDeliveryRepository.save(md);
    }

    public void saveInfoConservazioneLotto(String idLotto, InfoConservazione info) {
        LotDelivery lot = lotRepository.findByLotId(idLotto);

        List<InfoConservazione> listaInfo = lot.getInfoConservazioneLotto();
        if (listaInfo == null) {
            listaInfo = new ArrayList<>();
        }
        listaInfo.add(info);

        lot.setInfoConservazioneLotto(listaInfo);
        lotRepository.save(lot);

    }


    public void chiudiLottoConservazione(String lotId, EventConservazione eventStatoLotto) {

        conservazioneService.insertStatoLotto(lotId, eventStatoLotto);

    }

    public void aggiornaStatolotto(String lotId, EventConservazione eventStatoLotto) {

    }
}

