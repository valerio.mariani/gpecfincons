
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataCreazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idLottoAzienda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nickAzienda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dataCreazione",
    "idLottoAzienda",
    "tipoDoc",
    "nickAzienda"
})
@XmlRootElement(name = "getFileNotifica")
public class GetFileNotifica {

    @XmlElement(required = true)
    protected String dataCreazione;
    @XmlElement(required = true)
    protected String idLottoAzienda;
    @XmlElement(required = true)
    protected String tipoDoc;
    @XmlElement(required = true)
    protected String nickAzienda;

    /**
     * Gets the value of the dataCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Sets the value of the dataCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataCreazione(String value) {
        this.dataCreazione = value;
    }

    /**
     * Gets the value of the idLottoAzienda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLottoAzienda() {
        return idLottoAzienda;
    }

    /**
     * Sets the value of the idLottoAzienda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLottoAzienda(String value) {
        this.idLottoAzienda = value;
    }

    /**
     * Gets the value of the tipoDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDoc() {
        return tipoDoc;
    }

    /**
     * Sets the value of the tipoDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDoc(String value) {
        this.tipoDoc = value;
    }

    /**
     * Gets the value of the nickAzienda property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNickAzienda() {
        return nickAzienda;
    }

    /**
     * Sets the value of the nickAzienda property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNickAzienda(String value) {
        this.nickAzienda = value;
    }

}
