
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getFileNotificaReturn" type="{http://bean.service.postecom.it}File"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFileNotificaReturn"
})
@XmlRootElement(name = "getFileNotificaResponse")
public class GetFileNotificaResponse {

    @XmlElement(required = true)
    protected File getFileNotificaReturn;

    /**
     * Gets the value of the getFileNotificaReturn property.
     * 
     * @return
     *     possible object is
     *     {@link File }
     *     
     */
    public File getGetFileNotificaReturn() {
        return getFileNotificaReturn;
    }

    /**
     * Sets the value of the getFileNotificaReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link File }
     *     
     */
    public void setGetFileNotificaReturn(File value) {
        this.getFileNotificaReturn = value;
    }

}
