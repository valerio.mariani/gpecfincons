package it.posteitaliane.pec.archiver.service;
import brave.Span;
import brave.Tracer;
import it.posteitaliane.pec.archiver.bean.InfoPathZipBean;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.repositories.LotRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class VersamentoService {
    @Value("${nfs.archiver.ws.user.client.poste.conservazione}")
    String username;
    @Value("${nfs.archiver.ws.password.client.poste.conservazione}")
    String password;
    private static final Logger LOGGER = LoggerFactory.getLogger(VersamentoService.class);
    @Autowired
    Tracer tracer;
    @Autowired
    LotRepository lotRepository;
    @Autowired
    RapportoVersamentoWSService rapportoVersamentoWSService;
    @Autowired
    SendConservazioneWSService sendConservazioneWSService;
/*
    @Scheduled(fixedDelayString = "${outcome.fixedDelay.buildsend.conservazioneanorma}") //TODO
    @NewSpan(value = "controller.scheduled.conservazioneanorma")
    protected void buildSendService() throws IOException {
        LOGGER.info("Avvio attività schedulata per la conservazione a norma");

        Span scheduledConservazioneSend = tracer.nextSpan().name("controller.scheduled.conservazioneanorma").kind(Span.Kind.CLIENT);
        scheduledConservazioneSend.tag("SCHEDULE", "Avvio attività schedulata mandare a conservazione a norma");
        scheduledConservazioneSend.start();

        try (Tracer.SpanInScope ws = tracer.withSpanInScope(scheduledConservazioneSend)) {

            List<String> eventsToSearch = new ArrayList<>();
            //  TODO DA VEDERE EVENTO DI PROPOSITO
            eventsToSearch.add(EventType.LOTTO_OUTCOME_GENERATO.name());

            List<LotDelivery> deliveryListByStatus = lotRepository.findByCurrentStatusIn(eventsToSearch);
            if (deliveryListByStatus != null) {
                LOGGER.info("TROVATI " + deliveryListByStatus.size() + " LOTTI DA INVIARE A CONSERVAZIONE ");

                // TODO GESTIONE MARCATURE LOTTO DA INVIARE

                for (LotDelivery lotto : deliveryListByStatus) {
                    //
                     List<Integer> listaLottiIDMAndati = getIdLottiConservazione(lotto);
                     for(Integer idlottoCons: listaLottiIDMAndati) {
                       try {
                           it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.Lotto lottoaNorma=  sendConservazioneWSService.getLottoWs(dataCreazione, String idLottoAzienda, String tipoDoc);
                           String statoversamento=  rapportoVersamentoWSService.getRapportoDiVersamento(new Long(lottoaNorma.getIdLotto()).intValue());
                           aggiorna(lotto,idlottoCons,statoversamento);

                       }catch (Exception e){
                           LOGGER.error("** Errore nel rapposrto di versamento ",e);
                       }

                     }
                }

            }

        } finally {
            scheduledConservazioneSend.finish(); // note the scope is independent of the span. Always finish a span.
        }


    }*/

    private void aggiorna(LotDelivery lotto, Integer idlottoCons, String statoversamento) {
    }

    private List<Integer> getIdLottiConservazione(LotDelivery lotto) {
        List<Integer> list = new ArrayList<>();

        return list;
    }
}
