
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getDocumentoFirmatoReturn" type="{http://bean.service.postecom.it}Documento"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getDocumentoFirmatoReturn"
})
@XmlRootElement(name = "getDocumentoFirmatoResponse")
public class GetDocumentoFirmatoResponse {

    @XmlElement(required = true)
    protected Documento getDocumentoFirmatoReturn;

    /**
     * Gets the value of the getDocumentoFirmatoReturn property.
     * 
     * @return
     *     possible object is
     *     {@link Documento }
     *     
     */
    public Documento getGetDocumentoFirmatoReturn() {
        return getDocumentoFirmatoReturn;
    }

    /**
     * Sets the value of the getDocumentoFirmatoReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Documento }
     *     
     */
    public void setGetDocumentoFirmatoReturn(Documento value) {
        this.getDocumentoFirmatoReturn = value;
    }

}
