package it.posteitaliane.pec.archiver.util.jaxb;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

public class JaxbContextUtility {
    public static <T> String marshalObject(T oggettoPerMarshal, Class<T> classeOggetto) throws JAXBException{
        JAXBContext contextInput=JAXBContext.newInstance(classeOggetto);
        StringWriter sw2=new StringWriter();
        contextInput.createMarshaller().marshal(oggettoPerMarshal, sw2);
        String stringXml=sw2.toString();
        return stringXml;
    }

    public static <T> String marshalGenericObject(T oggettoPerMarshal, Class<?> classeOggetto) throws JAXBException{
        JAXBContext contextInput=JAXBContext.newInstance(classeOggetto);
        StringWriter sw2=new StringWriter();
        contextInput.createMarshaller().marshal(oggettoPerMarshal, sw2);
        String stringXml=sw2.toString();
        return stringXml;
    }

    public static <T> String marshalObject(T oggettoPerMarshal, Class<T> classeOggetto, String encoding) throws JAXBException{
        JAXBContext contextInput=JAXBContext.newInstance(classeOggetto);
        StringWriter sw2=new StringWriter();
        Marshaller marshaller = contextInput.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, encoding);
        marshaller.marshal(oggettoPerMarshal, sw2);
        String stringXml=sw2.toString();
        return stringXml;
    }

    public static <T> String marshalNoRootObject(T oggettoPerMarshal, Class<T> classeOggetto, QName name) throws JAXBException{
        JAXBContext contextInput=JAXBContext.newInstance(classeOggetto);
        JAXBElement<T> object = new JAXBElement<T>(name, classeOggetto, oggettoPerMarshal);
        StringWriter sw2=new StringWriter();
        contextInput.createMarshaller().marshal(object, sw2);
        String stringXml=sw2.toString();
        return stringXml;
    }

    public static <T> T unmarshalString(String stringXml, Class<T> oggettoPerUnmarshal) throws JAXBException{
        JAXBContext jaxbResultContext=JAXBContext.newInstance(oggettoPerUnmarshal);

        StringReader sr=new StringReader(stringXml);
        Unmarshaller ummarsh= jaxbResultContext.createUnmarshaller();
        Object oResult=ummarsh.unmarshal(sr);
        @SuppressWarnings("unchecked")
        T output=(T) oResult;
        return output;
    }

    public static <T> T unmarshalNoRootElementString(String stringXml, Class<T> oggettoPerUnmarshal) throws JAXBException{
        JAXBContext jaxbResultContext=JAXBContext.newInstance(oggettoPerUnmarshal);

        StringReader sr=new StringReader(stringXml);
        Unmarshaller ummarsh= jaxbResultContext.createUnmarshaller();
        @SuppressWarnings("unchecked")
        JAXBElement<T> object = (JAXBElement<T>) ummarsh.unmarshal(sr);
        return object.getValue();
    }

    public static <T> String marshalJaxbElement(JAXBElement<T> object,
                                                Class<T> classeOggetto) throws JAXBException {
        JAXBContext contextInput=JAXBContext.newInstance(classeOggetto);
        StringWriter sw2=new StringWriter();
        contextInput.createMarshaller().marshal(object, sw2);
        String stringXml=sw2.toString();
        return stringXml;
    }

    public static <T> T unmarshalJaxbElement(String stringXml, String packageName) throws JAXBException{
        JAXBContext jaxbResultContext=JAXBContext.newInstance(packageName);

        StringReader sr=new StringReader(stringXml);
        Unmarshaller ummarsh= jaxbResultContext.createUnmarshaller();
        @SuppressWarnings("unchecked")
        JAXBElement<T> object = (JAXBElement<T>) ummarsh.unmarshal(sr);
        return object.getValue();
    }
}
