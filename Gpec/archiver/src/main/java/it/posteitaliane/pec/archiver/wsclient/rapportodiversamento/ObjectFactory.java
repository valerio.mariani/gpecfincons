
package it.posteitaliane.pec.archiver.wsclient.rapportodiversamento;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.posteitaliane.pec.archiver.wsclient.rapportodiversamento package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Fault_QNAME = new QName("urn:postecom", "fault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.posteitaliane.pec.archiver.wsclient.rapportodiversamento
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetRapportoDiVersamentoResponse }
     * 
     */
    public GetRapportoDiVersamentoResponse createGetRapportoDiVersamentoResponse() {
        return new GetRapportoDiVersamentoResponse();
    }

    /**
     * Create an instance of {@link GetRapportoDiVersamento }
     * 
     */
    public GetRapportoDiVersamento createGetRapportoDiVersamento() {
        return new GetRapportoDiVersamento();
    }

    /**
     * Create an instance of {@link PosteDocException }
     * 
     */
    public PosteDocException createPosteDocException() {
        return new PosteDocException();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PosteDocException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:postecom", name = "fault")
    public JAXBElement<PosteDocException> createFault(PosteDocException value) {
        return new JAXBElement<PosteDocException>(_Fault_QNAME, PosteDocException.class, null, value);
    }

}
