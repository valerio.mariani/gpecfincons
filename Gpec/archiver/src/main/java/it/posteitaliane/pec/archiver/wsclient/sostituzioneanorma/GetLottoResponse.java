
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getLottoReturn" type="{http://bean.service.postecom.it}Lotto"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getLottoReturn"
})
@XmlRootElement(name = "getLottoResponse")
public class GetLottoResponse {

    @XmlElement(required = true)
    protected Lotto getLottoReturn;

    /**
     * Gets the value of the getLottoReturn property.
     * 
     * @return
     *     possible object is
     *     {@link Lotto }
     *     
     */
    public Lotto getGetLottoReturn() {
        return getLottoReturn;
    }

    /**
     * Sets the value of the getLottoReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link Lotto }
     *     
     */
    public void setGetLottoReturn(Lotto value) {
        this.getLottoReturn = value;
    }

}
