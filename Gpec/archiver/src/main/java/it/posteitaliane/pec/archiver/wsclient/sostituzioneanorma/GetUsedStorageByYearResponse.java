
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getUsedStorageByYearReturn" type="{http://bean.service.postecom.it}StatoMemoria"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getUsedStorageByYearReturn"
})
@XmlRootElement(name = "getUsedStorageByYearResponse")
public class GetUsedStorageByYearResponse {

    @XmlElement(required = true)
    protected StatoMemoria getUsedStorageByYearReturn;

    /**
     * Gets the value of the getUsedStorageByYearReturn property.
     * 
     * @return
     *     possible object is
     *     {@link StatoMemoria }
     *     
     */
    public StatoMemoria getGetUsedStorageByYearReturn() {
        return getUsedStorageByYearReturn;
    }

    /**
     * Sets the value of the getUsedStorageByYearReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoMemoria }
     *     
     */
    public void setGetUsedStorageByYearReturn(StatoMemoria value) {
        this.getUsedStorageByYearReturn = value;
    }

}
