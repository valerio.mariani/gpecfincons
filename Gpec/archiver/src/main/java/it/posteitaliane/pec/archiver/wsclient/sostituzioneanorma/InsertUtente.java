
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idUtente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codiceUtente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="eMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idProfilo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stato" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="accessoDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idUtente",
    "codiceUtente",
    "password",
    "eMail",
    "nome",
    "idProfilo",
    "stato",
    "accessoDoc"
})
@XmlRootElement(name = "insertUtente")
public class InsertUtente {

    @XmlElement(required = true)
    protected String idUtente;
    @XmlElement(required = true)
    protected String codiceUtente;
    @XmlElement(required = true)
    protected String password;
    @XmlElement(required = true)
    protected String eMail;
    @XmlElement(required = true)
    protected String nome;
    protected int idProfilo;
    protected short stato;
    @XmlElement(required = true)
    protected String accessoDoc;

    /**
     * Gets the value of the idUtente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUtente() {
        return idUtente;
    }

    /**
     * Sets the value of the idUtente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUtente(String value) {
        this.idUtente = value;
    }

    /**
     * Gets the value of the codiceUtente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceUtente() {
        return codiceUtente;
    }

    /**
     * Sets the value of the codiceUtente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceUtente(String value) {
        this.codiceUtente = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the eMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMail() {
        return eMail;
    }

    /**
     * Sets the value of the eMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMail(String value) {
        this.eMail = value;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the idProfilo property.
     * 
     */
    public int getIdProfilo() {
        return idProfilo;
    }

    /**
     * Sets the value of the idProfilo property.
     * 
     */
    public void setIdProfilo(int value) {
        this.idProfilo = value;
    }

    /**
     * Gets the value of the stato property.
     * 
     */
    public short getStato() {
        return stato;
    }

    /**
     * Sets the value of the stato property.
     * 
     */
    public void setStato(short value) {
        this.stato = value;
    }

    /**
     * Gets the value of the accessoDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessoDoc() {
        return accessoDoc;
    }

    /**
     * Sets the value of the accessoDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessoDoc(String value) {
        this.accessoDoc = value;
    }

}
