
package it.posteitaliane.pec.archiver.wsclient.rapportodiversamento;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idLotto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idLotto"
})
@XmlRootElement(name = "getRapportoDiVersamento")
public class GetRapportoDiVersamento {

    protected int idLotto;

    /**
     * Gets the value of the idLotto property.
     * 
     */
    public int getIdLotto() {
        return idLotto;
    }

    /**
     * Sets the value of the idLotto property.
     * 
     */
    public void setIdLotto(int value) {
        this.idLotto = value;
    }

}
