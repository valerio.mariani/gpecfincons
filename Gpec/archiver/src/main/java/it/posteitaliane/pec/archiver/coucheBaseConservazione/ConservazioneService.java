package it.posteitaliane.pec.archiver.coucheBaseConservazione;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.RawJsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.error.DocumentAlreadyExistsException;
import it.posteitaliane.pec.common.couchbase.CouchBase;
import it.posteitaliane.pec.common.model.EventConservazione;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Arrays;

@Service
@RefreshScope
public class ConservazioneService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConservazioneService.class);

    @Value("${spring.couchbase.bootstrap-hosts}")
    private String hostCB;

    @Value("${spring.couchbase.bucket.name}")
    private String userCB;

    @Value("${spring.couchbase.bucket.password}")
    private String passwordCB;


    public String insertLockLot(String idLot, EventConservazione enumEventConservazione) {

        CouchBase cb = new CouchBase(Arrays.asList(hostCB));
        cb.connect();
        cb.authenticate(userCB, passwordCB);
        Bucket bucketLotLock = cb.open("lotConservazioneLock");

        JsonObject data = JsonObject.create()
                .put("lotKey", idLot)
                .put("dataInizioElab", Instant.now().plusSeconds(3600).toString())
                .put("stato", enumEventConservazione);
        RawJsonDocument document = RawJsonDocument.create(idLot, data.toString());
        try {
            bucketLotLock.insert(document);
        } catch (DocumentAlreadyExistsException e) {

            LOGGER.error("Conservazione DocumentAlreadyExistsException PER lotKey: " + idLot);
            //la insert fallisce vuol dire che qualcuno gia' se lo e' preso in carico -->non faccio nulla, continuo il ciclo
            return null;
        } catch (Exception e) {

            LOGGER.error(e.getMessage(), e);
            return null;
        }

        return idLot;
    }


    public void insertStatoLotto(String lotId, EventConservazione eventStatoLotto) {

    }
}
