package it.posteitaliane.pec.archiver.config;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.netflix.appinfo.InstanceInfo;
import it.posteitaliane.pec.archiver.integration.EventsMessageListener;
import it.posteitaliane.pec.common.configuration.RabbitMQConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.listener.DirectMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
public class RabbitMQConfig implements RabbitListenerConfigurer, RabbitMQConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQConfig.class);

    @Value("${spring.rabbitmq.host}")
    private String host;
    @Value("${spring.rabbitmq.username}")
    private String user;
    @Value("${spring.rabbitmq.password}")
    private String psw;

    @Value("${spring.rabbitmq.admin.username:#{null}}")
    private String adminUser;

    @Value("${spring.rabbitmq.admin.password:#{null}}")
    private String adminPassword;

    @Value("${rabbit.custom.heartbeat.interval}")
    private Integer heartbeatPeriod;


    @Value("${spring.rabbitmq.ssl.enabled:false}")
    private Boolean sslEnabled;
    @Value("${spring.rabbitmq.virtual-host:/}")
    private String virtualHost;

    @Autowired InstanceInfo currentInstanceInfo;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
    }

    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        return messageHandlerMethodFactory(consumerJackson2MessageConverter());
    }

    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() { return new MappingJackson2MessageConverter(); }


    @Bean
    public MessageConverter converter() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(localDateTimeModule());
        Jackson2JsonMessageConverter jackson2Json = new Jackson2JsonMessageConverter(mapper);
        return jackson2Json;
    }


    public Module localDateTimeModule(){
        JavaTimeModule module = new JavaTimeModule();
        LocalDateTimeDeserializer localDateTimeDeserializer =  new
                LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        module.addDeserializer(LocalDateTime.class, localDateTimeDeserializer);
        return module;
    }


    @Bean
    @Primary
    public CachingConnectionFactory factory() {
        return cachingConnectionFactory(sslEnabled, host, virtualHost, user, psw, heartbeatPeriod);
    }

    @Bean
    public SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory(CachingConnectionFactory factory){
        return simpleRabbitListenerContainerFactory( factory, converter());
    }

    @Bean
    public Queue workflowEvents(
            @Value("${rabbit.custom.queue.event.management}") final String queueName){
        Queue eventQueue = new Queue(queueName + "_" +
                currentInstanceInfo.getAppName(), true);
        return eventQueue;
    }

    @Bean
    public TopicExchange globalManagementExchange(@Value("${rabbit.custom.exchange.event.management}") final String eventManagementExchange){
        return new TopicExchange(eventManagementExchange);
    }

    @Bean
    public Binding eventManagementTopicBinding(
            final Queue workflowEvents,
            final TopicExchange globalManagementExchange,
            @Value("${rabbit.custom.routing-key.event.topic.workflow}") final String routingKey){
        return BindingBuilder.bind(workflowEvents).
                to(globalManagementExchange).
                with(routingKey + ".lot.completed");
    }

/*
    @Bean
    public DirectMessageListenerContainer eventsQueueListenerContainer(
            CachingConnectionFactory factory,
            Queue workflowEvents,
            EventsMessageListener listener){

        return eventsQueueListenerContainer(
                factory,
                workflowEvents.getName(),
                listener,
                queue -> currentInstanceInfo.getId() + "." + queue);
    }*/


}


