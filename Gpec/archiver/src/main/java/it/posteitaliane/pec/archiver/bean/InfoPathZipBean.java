package it.posteitaliane.pec.archiver.bean;

public class InfoPathZipBean {
    private String basePath;
    private String  dataCreazione;
    private String idLottoAzienda;
    private String tipoDoc;
    private String nameFileZip;
    private String pathFileZip;
    private String nameFileControllo;
    private String pathFileControllo;
    private boolean loadAllMessageZip;


    public InfoPathZipBean(){

    }
    public String getNameFileZip() {
        return nameFileZip;
    }

    public void setNameFileZip(String nameFileZip) {
        this.nameFileZip = nameFileZip;
    }

    public String getPathFileZip() {
        return pathFileZip;
    }

    public void setPathFileZip(String pathFileZip) {
        this.pathFileZip = pathFileZip;
    }

    public String getNameFileControllo() {
        return nameFileControllo;
    }

    public void setNameFileControllo(String nameFileControllo) {
        this.nameFileControllo = nameFileControllo;
    }

    public String getPathFileControllo() {
        return pathFileControllo;
    }

    public void setPathFileControllo(String pathFileControllo) {
        this.pathFileControllo = pathFileControllo;
    }

    public boolean isLoadAllMessageZip() {
        return loadAllMessageZip;
    }

    public void setLoadAllMessageZip(boolean loadAllMessageZip) {
        this.loadAllMessageZip = loadAllMessageZip;
    }

    public String getDataCreazione() {
        return dataCreazione;
    }

    public void setDataCreazione(String dataCreazione) {
        this.dataCreazione = dataCreazione;
    }

    public String getIdLottoAzienda() {
        return idLottoAzienda;
    }

    public void setIdLottoAzienda(String idLottoAzienda) {
        this.idLottoAzienda = idLottoAzienda;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

}
