package it.posteitaliane.pec.archiver;

import brave.Tracing;
import brave.context.log4j2.ThreadContextScopeDecorator;
import brave.propagation.B3Propagation;
import brave.propagation.CurrentTraceContext;
import brave.propagation.ExtraFieldPropagation;
import brave.propagation.ThreadLocalCurrentTraceContext;
import brave.sampler.Sampler;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.util.StringUtils;
import zipkin2.Span;
import zipkin2.reporter.AsyncReporter;
import zipkin2.reporter.Reporter;
import zipkin2.reporter.Sender;
import zipkin2.reporter.amqp.RabbitMQSender;

import java.io.IOException;
import java.util.Arrays;

@SpringBootApplication
@EnableEurekaClient
public class ArchiverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArchiverApplication.class, args);
    }

    @Autowired private EurekaClient eurekaClient;

    @Bean
    public InstanceInfo getCurrentServiceInstanceEurekaID(){
        return eurekaClient.getApplicationInfoManager().getInfo();
    }


    @Bean
    Sender sender(@Value("${spring.rabbitmq.host}") String rabbitmqHostUrl,
                  @Value("${rabbit.custom.queue.event.actions}") String zipkinQueue,
                  @Value("${spring.rabbitmq.username}") String user,
                  @Value("${spring.rabbitmq.password}") String psw) throws IOException {
        return RabbitMQSender.newBuilder()
                .queue(zipkinQueue)
                .username(user)
                .password(psw)
                .addresses(rabbitmqHostUrl).build();
    }

    @Bean
    Reporter<Span> spanReporter(Sender sender) {
        return AsyncReporter.create(sender);
    }


    @Bean
    CurrentTraceContext log4jTraceContext() {
        return ThreadLocalCurrentTraceContext.newBuilder()
                .addScopeDecorator(ThreadContextScopeDecorator.create())
                .build();
    }


    @Bean
    Tracing tracing(
            @Value("${spring.application.name:spring-tracing}") String serviceName,
            Reporter<Span> spanReporter,
            CurrentTraceContext log4jTraceContext) {

        return Tracing
                .newBuilder()
                .sampler(Sampler.ALWAYS_SAMPLE)
                .localServiceName(StringUtils.capitalize(serviceName))
                .propagationFactory(ExtraFieldPropagation
                        .newFactoryBuilder(B3Propagation.FACTORY).
                                addPrefixedFields("x-baggage-", Arrays.asList("lotId", "service", "customerCode")).
                                build())
                .currentTraceContext(log4jTraceContext)
                .spanReporter(spanReporter)
                .build();
    }
}
