package it.posteitaliane.pec.archiver.outcome.zip;

import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.LotDelivery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class ZipBuilder {
    private FileOutputStream fos;
    private ZipOutputStream zos;


    private static final Logger LOGGER = LoggerFactory.getLogger(ZipBuilder.class);


    public ZipBuilder(String zipFile) throws FileNotFoundException {
        this.fos = new FileOutputStream(zipFile);
        this.zos = new ZipOutputStream(this.fos);
    }


    public void zipNewEntryFromByteArray(String entryFileName, byte[] obj) throws IOException {

        zos.putNextEntry(new ZipEntry(entryFileName));

        zos.write(obj);
        zos.closeEntry();
    }

    public static void addToZipFile(Path fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

       // LOGGER.info("******* aggiunta file '" + fileName + "' nello ZIP ******");

        File file = new File(fileName.toAbsolutePath().toString());
        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(fileName.getFileName().toString());
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }


    public void addEmlToZipByLottoId(String pathFiles) throws FileNotFoundException, IOException {

        Files.list(Paths.get(pathFiles))
                .filter(s -> s.toString().endsWith(".eml"))
                .map(Path::toAbsolutePath)
                .forEach(emlFile -> {
                    try {
                        addToZipFile(emlFile, zos);

                    } catch (FileNotFoundException fnf) {
                        LOGGER.error(fnf.getMessage(), fnf);

                    } catch (IOException is) {
                        LOGGER.error(is.getMessage(), is);
                    }


                });

    }


    public void addSigleEmlToZipByLottoId(String pathFile) throws FileNotFoundException, IOException {


        addToZipFile(Paths.get(pathFile), zos);

    }


    public void finalizeZip() throws IOException {
        try {
            zos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deletefileZip(String zipFilename, String basePath) throws IOException {


        LOGGER.info("Il file " + zipFilename + "  verrà eliminato");

        //rimozione file OUTCOME
        String outcomeFileName = zipFilename;
        Path fileOutcome = Paths.get(basePath + outcomeFileName);

        if (!Files.exists(fileOutcome)) {
            throw new FileNotFoundException("il file " + fileOutcome.toAbsolutePath().toString() + " non esiste nella cartella sorgente");
        }


        Files.delete(fileOutcome);
        LOGGER.info("File " + fileOutcome.toString() + " CANCELLATO correttamente");

    }


}
