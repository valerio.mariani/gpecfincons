
package it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nomeFile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="uploadBlockSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fileSize" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="chkFile" type="{http://bean.service.postecom.it}File"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "nomeFile",
    "uploadBlockSize",
    "fileSize",
    "chkFile"
})
@XmlRootElement(name = "createLotto")
public class CreateLotto {

    @XmlElement(required = true)
    protected String nomeFile;
    protected int uploadBlockSize;
    protected long fileSize;
    @XmlElement(required = true)
    protected File chkFile;

    /**
     * Gets the value of the nomeFile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeFile() {
        return nomeFile;
    }

    /**
     * Sets the value of the nomeFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeFile(String value) {
        this.nomeFile = value;
    }

    /**
     * Gets the value of the uploadBlockSize property.
     * 
     */
    public int getUploadBlockSize() {
        return uploadBlockSize;
    }

    /**
     * Sets the value of the uploadBlockSize property.
     * 
     */
    public void setUploadBlockSize(int value) {
        this.uploadBlockSize = value;
    }

    /**
     * Gets the value of the fileSize property.
     * 
     */
    public long getFileSize() {
        return fileSize;
    }

    /**
     * Sets the value of the fileSize property.
     * 
     */
    public void setFileSize(long value) {
        this.fileSize = value;
    }

    /**
     * Gets the value of the chkFile property.
     * 
     * @return
     *     possible object is
     *     {@link File }
     *     
     */
    public File getChkFile() {
        return chkFile;
    }

    /**
     * Sets the value of the chkFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link File }
     *     
     */
    public void setChkFile(File value) {
        this.chkFile = value;
    }

}
