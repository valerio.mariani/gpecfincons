package it.posteitaliane.pec.archiver.integration;

import it.posteitaliane.pec.archiver.outcome.zip.ZipBuilder;
import it.posteitaliane.pec.archiver.service.BuildSendService;
import it.posteitaliane.pec.archiver.util.jaxb.ESubPECMarshall;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.File;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.FileUploadInfo;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.PosteDocWS;
import it.posteitaliane.pec.archiver.wsclient.sostituzioneanorma.PosteDocWSService;
import it.posteitaliane.pec.archiver.xsd.bean.generated.ESubPEC;
import it.posteitaliane.pec.common.model.xml.Message;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;


public class UtilityTest {

    private Message message = new Message();

    private String lot = "G23L_20181229_00001";

    @Test
    public void testPath(){
        String path="c:\\aaa\\ddd\\fff.zip";
        //String
    }


    @Test
    public void testwebService() throws Exception {

        PosteDocWSService wsService = new PosteDocWSService();

        PosteDocWS pdws = wsService.getPosteDocWS();
        //Map<String, Object>     req_ctx = ((BindingProvider)pdws).getRequestContext();
        BindingProvider prov = ((BindingProvider) pdws);
        ((BindingProvider)pdws).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "postepcl.postecom");
        ((BindingProvider)pdws).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "cambiami+1");
      //  prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "postepcl.webserv");
      //  prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "cambiami+1");
        //((BindingProvider) pdws).getResponseContext()
        String nomeFile = "postepcl-EsubPec-20190321-06205300-chk.xml";
        int uploadBlockSize = 0;
        long fileSize = 0;


//file chk
        //.. carica il file "acme-fatture-20061212-06205300-chk.xml" in un array di byte
        byte[] chkFileByteArray = readBytesFromFile("C:\\prova\\postepcl-EsubPec-20190321-06205300-chk.xml");
        String baseDir = "C:\\prova\\";
        File chkFile = new File();
        chkFile.setNome("postepcl-EsubPec-20190321-06205300.xml");
        chkFile.setData(chkFileByteArray);
        String lottoFileName = "postepcl-EsubPec-20190321-06205300";
        java.io.File lottoFile = new java.io.File(baseDir + lottoFileName);

        FileUploadInfo uploadFileInfo = pdws.createLotto(lottoFileName, 512, lottoFile.length(), chkFile);

        //pdws.createLotto(nomeFile, uploadBlockSize, fileSize, chkFile);


    }
    @Test
    public void testRealWS(){
        BuildSendService buildSendService = new BuildSendService();
        buildSendService.buildSend();
    }

    @Test
    public void testXmlbean() {
        it.posteitaliane.pec.archiver.xsd.bean.generated.ObjectFactory objFactXsd = new it.posteitaliane.pec.archiver.xsd.bean.generated.ObjectFactory();
        String nomeFileGen = "sfssd-222-555";
        ESubPEC espec = objFactXsd.createESubPEC();
        List<ESubPEC.Documento> listaDoc = espec.getDocumento();
        String lotId="3432143-00042342";
        String basePath = "C:\\prova\\";
        String dir = basePath+"tmp_"+lotId;
        // creo cartella temporanea
        new java.io.File(dir).mkdirs();
        ESubPEC.Documento docMsg = new ESubPEC.Documento();
        docMsg.setCustCode("CustCode");
        SimpleDateFormat datedevformat = new SimpleDateFormat("yyyy-MM-dd");
        GregorianCalendar now = new GregorianCalendar();
        XMLGregorianCalendar xmlGregorianCalendar =null;
        try {
            xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(now);
        } catch (Exception e) {

        }
        docMsg.setDateDev(xmlGregorianCalendar);
        docMsg.setDest("Dest");

        docMsg.setDocType("DocType");
        docMsg.setDettDocT("DettDocT");
        String filename = "GestioneDocumentaleAdvance.eml";
        String pathFile=dir+"\\"+filename;
        prepareFile(basePath+filename,pathFile);
        ESubPEC.Documento.File filemsg = populateFile("GestioneDocumentaleAdvanced","GestioneDocumentaleAdvance.eml", pathFile);

        docMsg.setFile(filemsg);
        docMsg.setExtId("extID");
        docMsg.setGPecLotId(lotId);
        docMsg.setHourDev(xmlGregorianCalendar);
        docMsg.setServizio("Servizio"); //   todo
        docMsg.setMsgId("MsgId");
        docMsg.setMkey("Mkey");
        docMsg.setMitt("Mitt");
        docMsg.setIdm("Idm");  //id	Message-ID
        espec.getDocumento().add(docMsg);
        ESubPECMarshall eSubPECMarshall = new ESubPECMarshall();
        String fileIndexName=dir+"\\"+"index.xml";

        ByteArrayOutputStream byteArrayOutputStream4XML = eSubPECMarshall.creaIndexESubPECXml(espec);
        //System.out.println(esubXml);
        String finameZip = nomeFileGen+".zip";
        //java.io.File fzip = new java.io.File(dir+"\\"+finameZip);
        try {

            ZipBuilder zipBuilder=new ZipBuilder(dir+"\\"+finameZip);

            //Aggiunta file XML riepilogativo
            zipBuilder.zipNewEntryFromByteArray("index.xml",byteArrayOutputStream4XML.toByteArray());

            //aggiunta allo ZIP delle ricevute .eml nella cartella indicata
            zipBuilder.addEmlToZipByLottoId(dir);

            zipBuilder.finalizeZip();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BigInteger sizeZip = getFileSize(dir+"\\"+finameZip);
        String cunkMD5=checksum(dir+"\\"+finameZip,"MD5");
        String xmlchk = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" standalone=\"yes\"?>\n"+"<file_chk><file_size>"+sizeZip.toString()+"</file_size><file_hash type=\"MD5\">"+cunkMD5+"</file_hash></file_chk>";
        //creare file di controllo chk.xml
        try {
            stringToDom(xmlchk,dir+"\\"+nomeFileGen+"-chk.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        java.io.File filechk = new java.io.File(dir+"\\"+nomeFileGen+"-chk.xml");



    }

    public String generaFileDaMandare(){
        String nameFileRitorno="";




        return nameFileRitorno;
    }

    public static void stringToDom(String xmlSource, String pathFileName)
            throws IOException {
        java.io.FileWriter fw = new java.io.FileWriter(pathFileName);
        fw.write(xmlSource);
        fw.close();
    }
    private void prepareFile(String pathOrg, String pathDest) {
        java.io.File src = new java.io.File(pathOrg);
        java.io.File dest = new java.io.File(pathDest);
        try {
            copyFile(src, dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private  void copyFile(java.io.File src, java.io.File dest) throws IOException {
        java.nio.file.Files.copy(src.toPath(), dest.toPath());
    }
    private ESubPEC.Documento.File populateFile(String label, String file,String filepath) {
        ESubPEC.Documento.File filemsg = new ESubPEC.Documento.File();
        String codeAlgoritm= "SHA-256";
        filemsg.setAlgorithm(codeAlgoritm);
        filemsg.setChecksum(checksum( filepath, codeAlgoritm) );
        filemsg.setSize(getFileSize(filepath));
        filemsg.setValue(file);
        filemsg.setLabel(label);
        return  filemsg;
    }

    private BigInteger getFileSize(String filepath) {
        BigInteger siz=new BigInteger("0");
        java.io.File file =new java.io.File(filepath);
        if(file.exists()){
            siz=new BigInteger(new Long(file.length()).toString());
        }
        return siz;
    }

    private String checksum(String filepath, String codeAlgoritm) {
        // DigestInputStream is better, but you also can hash file like this.

        MessageDigest md =null;
        try {
            md = MessageDigest.getInstance(codeAlgoritm);
            InputStream fis = new FileInputStream(filepath);
            byte[] buffer = new byte[1024];
            int nread;
            while ((nread = fis.read(buffer)) != -1) {
                md.update(buffer, 0, nread);
            }
        }catch (Exception e) {
            e.printStackTrace();

        }

        // bytes to hex
        StringBuilder result = new StringBuilder();
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));
        }
        return result.toString();

    }
    private static byte[] readBytesFromFile(String filePath) {

        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            java.io.File file = new java.io.File(filePath);
            bytesArray = new byte[(int) file.length()];

            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        return bytesArray;

    }
    private static String toHex(byte[] bytes) {
        return DatatypeConverter.printHexBinary(bytes);
    }

@Test
public void testmkdir(){

    String dir = "C:\\prova\\dir3";
  boolean  success = (new java.io.File(dir)).mkdirs();


}
    @Test
    public void testsubStrg(){
        Random r = new Random();
        int index = r.ints(1, 99999999).limit(1).findFirst().getAsInt();
        System.out.println(index );
        String p1=Integer.toString(index).substring(0,6);
        System.out.println(p1  );
        String ID = StringUtils.leftPad(p1, 8, "0");
        System.out.println(ID  );

    }


}
