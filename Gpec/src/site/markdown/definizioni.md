## Definizioni

Ogni microservizio è stati sviluppatoseguendo delle linee guida comuni:

* Ogni microservizio è rilasciato in un pacchetto applicativo nella forma di un archivio jar;
* l'archivio è rilasciato con il nome del progetto (che non corrisponde all'application.name) seguito dalla versione secodo il seguente pattern:
     ${project.name}-{project.version}.jar
* Le proprietà immutabili di ogni microservizio sono in un file di property all'interno del singolo archivio jar;
* le proprietà di runtime sono esposte da un server di configurazione a cui il microservizio accede durante la fase di bootstrap;
* ogni microservizio ha delle componente le cui proprietà potranno essere aggiornate in tempo reale ed altre che ne richiedono un riavvio;
* Ogni servizio è eseguito con il proprio utente il cui nome corrisponde al valore della property "spring.application.name";
* Se il microservizio necessita di risorse/files/proprietà che devono essere raggiungibili e modificabili esterneamente al pacchetto, queste saranno presenti in una directory nella home dell'utente corrispondente al seguente patter:
    file:///${user.home}/source/fincons/GPEC/ 