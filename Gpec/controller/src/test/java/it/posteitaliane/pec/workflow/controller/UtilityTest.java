//package it.posteitaliane.pec.workflow.controller;
//
//import it.posteitaliane.pec.common.model.EventType;
//import it.posteitaliane.pec.common.model.Events;
//import it.posteitaliane.pec.common.model.MailDeliveryRequest;
//import it.posteitaliane.pec.workflow.service.ResponseService;
//import org.junit.Test;
//
//import javax.validation.constraints.AssertTrue;
//import java.time.LocalDateTime;
//import java.util.*;
//
//import static junit.framework.TestCase.assertTrue;
//import static junit.framework.TestCase.assertFalse;
//
//
//public class UtilityTest {
//
//
//
//    //@Test
//    public void testXmlAckInputConErroriDiValidazione() throws Exception{
//
//        try {
//
//           System.out.println("********* TEST testXmlAckInpt ***************");
//
//            List<String> errorCodes=new ArrayList<>();
//            errorCodes.add("V001");
//            errorCodes.add("V003");
//            String xml= IndexController.generateXmlAckInput("G23L",new Date(),errorCodes);
//
//            System.out.println(xml);
//            System.out.println("********* FINE test testXmlAckInpt ***************\n\n\n\n");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//    //@Test
//    public void testXmlAckInputSenzaErroriDiValidazione() throws Exception{
//
//        try {
//
//            System.out.println("********* TEST testXmlAckInpt ***************");
//
//
//            String xml=IndexController.generateXmlAckInput("G23L",new Date(),null);
//
//            System.out.println(xml);
//            System.out.println("********* FINE test testXmlAckInpt ***************\n\n\n\n");
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//
//    @Test
//    public void messaggo_MESSAGGIO_NON_INVIABILE() throws Exception{
//
//        try {
//
//            ResponseService rs=new ResponseService();
//
//            MailDeliveryRequest messaggio=creaMessaggio();
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_NON_INVIABILE, LocalDateTime.now()));
//
//
//            boolean chiudibile=  rs.messaggioChiudibile(messaggio);
//
//            System.out.println("chiudibile: " +chiudibile);
//
//            assertTrue(chiudibile);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//    @Test
//    public void messaggo_MESSAGGIO_INVIABILE() throws Exception{
//
//        try {
//
//            ResponseService rs=new ResponseService();
//
//
//            MailDeliveryRequest messaggio=creaMessaggio();
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now()));
//
//
//            boolean chiudibile=  rs.messaggioChiudibile(messaggio);
//
//            System.out.println("chiudibile: " +chiudibile);
//
//            assertFalse(chiudibile);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//
//    @Test
//    public void messaggo_MESSAGGIO_INVIO_FALLITO() throws Exception{
//
//        try {
//
//            ResponseService rs=new ResponseService();
//
//
//            MailDeliveryRequest messaggio=creaMessaggio();
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusMinutes(1)));
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIO_FALLITO, LocalDateTime.now()));
//
//
//
//            boolean chiudibile=  rs.messaggioChiudibile(messaggio);
//
//            System.out.println("chiudibile: " +chiudibile);
//
//            assertTrue(chiudibile);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//    @Test
//    public void messaggo_MESSAGGIO_INVIO_SUCCESSO_DA_POCO_TEMPO() throws Exception{
//
//        try {
//
//            ResponseService rs=new ResponseService();
//
//            MailDeliveryRequest messaggio=creaMessaggio();
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusMinutes(4)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusMinutes(3)));
//
//
//            boolean chiudibile=  rs.messaggioChiudibile(messaggio);
//
//            System.out.println("chiudibile: " +chiudibile);
//
//            assertFalse(chiudibile);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//
//    @Test
//    public void messaggo_MESSAGGIO_INVIO_SUCCESSO_DA_TANTO_TEMPO() throws Exception{
//
//        try {
//
//            ResponseService rs=new ResponseService();
//
//            MailDeliveryRequest messaggio=creaMessaggio();
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(26)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(25)));
//
//
//            boolean chiudibile=  rs.messaggioChiudibile(messaggio);
//
//            System.out.println("chiudibile: " +chiudibile);
//
//            assertTrue(chiudibile);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//
//
//
//    @Test
//    public void messaggo_MESSAGGIO_ACCETTAZIONE_KO() throws Exception{
//
//        try {
//
//            ResponseService rs=new ResponseService();
//
//            MailDeliveryRequest messaggio=creaMessaggio();
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusMinutes(3)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusMinutes(2)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_KO, LocalDateTime.now()));
//
//
//            boolean chiudibile=  rs.messaggioChiudibile(messaggio);
//
//            System.out.println("chiudibile: " +chiudibile);
//
//            assertTrue(chiudibile);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//
//    @Test
//    public void messaggo_MESSAGGIO_ACCETTAZIONE_OK() throws Exception{
//
//        try {
//
//            ResponseService rs=new ResponseService();
//
//            MailDeliveryRequest messaggio=creaMessaggio();
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusMinutes(3)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusMinutes(2)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now()));
//
//
//            boolean chiudibile=  rs.messaggioChiudibile(messaggio);
//
//            System.out.println("chiudibile: " +chiudibile);
//
//            assertFalse(chiudibile);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//
//    @Test
//    public void messaggo_MESSAGGIO_CONSEGNA_OK() throws Exception{
//
//        try {
//
//            ResponseService rs=new ResponseService();
//
//            MailDeliveryRequest messaggio=creaMessaggio();
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusMinutes(4)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusMinutes(3)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now().minusMinutes(2)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_CONSEGNA_OK, LocalDateTime.now()));
//
//            boolean chiudibile=  rs.messaggioChiudibile(messaggio);
//
//            System.out.println("chiudibile: " +chiudibile);
//
//            assertTrue(chiudibile);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//
//
//    @Test
//    public void messaggo_MESSAGGIO_CONSEGNA_KO() throws Exception{
//
//        try {
//
//            ResponseService rs=new ResponseService();
//
//
//            MailDeliveryRequest messaggio=creaMessaggio();
//
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusMinutes(4)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusMinutes(3)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now().minusMinutes(2)));
//            messaggio.getEvents().add(new Events(EventType.MESSAGGIO_CONSEGNA_KO, LocalDateTime.now()));
//
//
//            boolean chiudibile=  rs.messaggioChiudibile(messaggio);
//
//            System.out.println("chiudibile: " +chiudibile);
//
//            assertTrue(chiudibile);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            assertTrue(false);
//        }
//
//    }
//
//    private MailDeliveryRequest creaMessaggio(){
//
//
//        MailDeliveryRequest messaggio = MailDeliveryRequest.builder().
//                //to("someone@localhost").
//                        to("pectest23l2@coll.postecert.it").
//                        lotId("PROVAAAAAAAAAAAA").
//                        extID(UUID.randomUUID().toString()).
//                        messageID("asdasdasda").
//                        subject("test da java , dal controller").
//                        content("dal controller java").
//                        events(new ArrayList() ).
//
//                        attachments(new ArrayList<>()).
//                        build();
//
//             return messaggio;
//    }
//
//
//
//
//
//}
