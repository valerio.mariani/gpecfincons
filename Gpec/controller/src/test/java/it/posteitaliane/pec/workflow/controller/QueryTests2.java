package it.posteitaliane.pec.workflow.controller;

import com.couchbase.client.java.Bucket;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.junit.jupiter.api.Test;
import java.util.List;

import static org.junit.jupiter.api.TestInstance.Lifecycle;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
public class QueryTests2 {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryTests2.class);


    @Autowired
    MailDeliveryRepository mailDeliveryRepository;

    @Autowired
    LotRepository lotRepository;

    @Autowired
    Bucket bucket;


    @Test
    public void findMessaggiDiUnLotto   () {



       // List<LotDelivery> result = lotRepository.findLotDeliveryByCurrentStatusIsAndClosedIsNullOrderByUpdatedDesc(EventType.LOTTO_IN_ELABORAZIONE);


        /*Prendo riepilogo lotto*/
        List<MailDeliveryRequest> messageByLottoId = mailDeliveryRepository.findByLotId("G23L_20190325_000008");


        System.out.println("size lotti con service pari a \"LOTTO_IN_ELABORAZIONE\" vale:"+messageByLottoId.size());

    }









}
