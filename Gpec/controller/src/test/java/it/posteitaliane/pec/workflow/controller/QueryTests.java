//package it.posteitaliane.pec.workflow.controller;
//
//import com.couchbase.client.java.Bucket;
//import com.couchbase.client.java.query.N1qlQuery;
//import com.couchbase.client.java.query.N1qlQueryResult;
//import com.couchbase.client.java.query.Statement;
//import it.posteitaliane.pec.common.model.EventType;
//import it.posteitaliane.pec.common.model.Events;
//import it.posteitaliane.pec.common.model.LotDelivery;
//import it.posteitaliane.pec.common.model.MailDeliveryRequest;
//import it.posteitaliane.pec.common.repositories.LotRepository;
//import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
//import org.junit.Ignore;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.TestInstance;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//
//import java.time.LocalDateTime;
//import java.util.*;
//import java.util.stream.Stream;
//
//import static com.couchbase.client.java.query.Select.select;
//import static com.couchbase.client.java.query.dsl.Expression.i;
//import static com.couchbase.client.java.query.dsl.Expression.s;
//import static com.couchbase.client.java.query.dsl.Expression.x;
//import static org.junit.jupiter.api.TestInstance.Lifecycle;
//
//@ExtendWith(SpringExtension.class)
//@SpringBootTest
//@TestInstance(Lifecycle.PER_CLASS)
//public class QueryTests {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(QueryTests.class);
//
//
//    @Autowired
//    MailDeliveryRepository mailDeliveryRepository;
//
//    @Autowired
//    LotRepository lotRepository;
//
//    @Autowired
//    Bucket bucket;
//
//
//
//    @Test
//    public void countElaboratedMessageInDateRange() {
//
//        long ris = mailDeliveryRepository.countElaboratedMessageInDateRange("2016-02-01T00:00:00","2019-06-01T00:00:00") ;
//
//        System.out.println("OK");
//
//    }
//
//
//    @Test
//    public void countElaboratedLotsInDateRange() {
//
//        long ris = lotRepository.countElaboratedLotsInDateRange("2016-02-01T00:00:00","2019-06-01T00:00:00") ;
//
//        System.out.println("OK");
//
//    }
//
//
//
//    @Test
//    public void findMessageWithProcessingTimeTooLong() {
//
//        /*Descrizione dei parametri in MailDeliveryRepository*/
//        List<MailDeliveryRequest> messages = mailDeliveryRepository.findProcessingTimeTooLong("3","day");
//
//        LOGGER.info("OK");
//
//    }
//
//
//
//    public void findById() {
//
//        Optional<LotDelivery> lot= lotRepository.findById("G23L-20181206-000069");
//
//        LotDelivery lotto=lot.get();
//
//        System.out.println("aaaa");
//
//
//    }
//
//
//    public void findByService() {
//        List<LotDelivery> result = lotRepository.findByService("23L");
//
//
//        System.out.println("size lotti con service pari a \"23L\" vale:"+result.size());
//
//    }
//
//
//
//
//
//    public void findTuttiILottiInElaborazione() {
//
//
//
//        List<LotDelivery> result = lotRepository.findLotDeliveryByCurrentStatusIsAndClosedIsNullOrderByUpdatedDesc(EventType.LOTTO_IN_ELABORAZIONE);
//
//
//        /*Prendo riepilogo lotto*/
//        List<MailDeliveryRequest> messageByLottoId = mailDeliveryRepository.findByLotId("G23L_20181229_T00000");
//
//
//
//        /*Tutti i messaggi di un lotto*/
//        // List<LotDelivery> messaggiLotto =  lotRepository.findByLotIdIgnoreCaseContaining("%G23L-20181206%");
//
//
//
//        System.out.println("size lotti con service pari a \"LOTTO_IN_ELABORAZIONE\" vale:"+result.size());
//
//    }
//
//
//
//
//
//    void creaLotto(LotDelivery lottoData){
//
//        lotRepository.save(lottoData);
//
//    }
//
//    void creaMessaggioPerLotto(String lottoId,MailDeliveryRequest messageData){
//
//        mailDeliveryRepository.save(messageData);
//
//    }
//
//
//    String[] messageIds4Lotto(){
//
//        Stream<String> uuidStream = Stream.
//                generate(UUID::randomUUID).
//                map(u -> u.toString());
//
//        String[] messageIds = uuidStream.
//                limit(10).toArray(String[]::new);
//
//        return messageIds;
//    }
//
//
//
//
//    /*
//     * Metodo flushBucket():
//     *
//     * Per svuotare completamente il bucket
//     * Attenzione: la funzione di flush deve essere stata
//     * abilitata da configurazione del bucket su coachbase
//     *
//     * */
//    void flushBucket(){
//
//        try {
//            bucket.bucketManager().flush();
//        } catch (Exception e) {
//            LOGGER.error(e.getMessage());
//        }
//
//    }
//
//
//
//
//
//    @Test
//    public void buildMessagesDataSetForTest3(){
//
//        List<Events> events;
//        List<String> attachments;
//
//
//        String lotto1Id = "G23L-20181130-000090";
//
//        //pararamteri per creazione lotto 1
//        //----- Definizione Lotto START
//
//        events = new ArrayList<>();
//        events.add(new Events(EventType.LOTTO_RICEVUTO, LocalDateTime.now().minusHours(8L)));
//        events.add(new Events(EventType.LOTTO_IN_ELABORAZIONE, LocalDateTime.now().minusHours(3L)));
//
//        LotDelivery lotto1 = LotDelivery.builder()
//                .lotId(lotto1Id)
//                .closed(LocalDateTime.now())
//                .created("2018-12-28")
//                .currentStatus(EventType.LOTTO_IN_ELABORAZIONE)
//                .customerCode("23L")
//                .service("G23L")
//                .events(events)
//                .mailDeliveriesIDs(Arrays.asList(messageIds4Lotto()))
//                .messagesNotAccepted(4)
//                .messagesNotDelivered(3)
//                .messagesNotValidated(6)
//                .messagesProcessed(30)
//                .messagesSuccess(10)
//                .updated(LocalDateTime.now().plusDays(1L))
//
//                .build() ;
//        //----- Definizione Lotto END
//
//        creaLotto(lotto1);
//        LOGGER.info("Creazione lotto " + lotto1Id + "COMPLETATA");
//
//        //parametri per creazione messaggio 1
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_FALLITO, LocalDateTime.now().minusHours(4L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_1 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "1")
//
//                .extID("ExtID_1")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("V001")
//                .acceptanceOutcome(null)
//                .deliveryOutcome(null)
//                .acceptanceFileName(null)
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_1);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_1.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//        //parametri per creazione messaggio 2
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(5L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_KO, LocalDateTime.now().minusHours(3L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_2 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "2")
//
//                .extID("ExtID_2")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("V001")
//                .deliveryOutcome(null)
//                .acceptanceFileName("fileForAccept_1_2.eml")
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_2);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_2.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//        //parametri per creazione messaggio 3
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(5L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now().minusHours(3L)));
//        events.add(new Events(EventType.MESSAGGIO_CONSEGNA_OK, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_3 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "3")
//
//                .extID("ExtID_3")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("1")
//                .deliveryOutcome("1")
//                .acceptanceFileName("fileForAccept_1_3.eml")
//                .deliveryFileName("delivery_1_3.eml")
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_3);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_3.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//
//        //parametri per creazione messaggio 4
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(5L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_KO, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_4 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "4")
//
//                .extID("ExtID_4")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("V001")
//                .deliveryOutcome(null)
//                .acceptanceFileName("fileForAccept_1_4.eml")
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_4);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_4.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//
//        //parametri per creazione messaggio 1
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(4L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now().minusHours(2L)));
//        events.add(new Events(EventType.MESSAGGIO_CONSEGNA_OK, LocalDateTime.now().minusHours(1L)));
//
//
//        attachments = new ArrayList<>();
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_5 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "5")
//
//                .extID("ExtID_5")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("1")
//                .deliveryOutcome("1")
//                .acceptanceFileName("fileForAccept_1_5.eml")
//                .deliveryFileName("delivery_1_5.eml")
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_5);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_5.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//
//        //parametri per creazione messaggio 6
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(3L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_FALLITO, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_6 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "6")
//
//                .extID("ExtID_6")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("V001")
//                .acceptanceOutcome(null)
//                .deliveryOutcome(null)
//                .acceptanceFileName(null)
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_6);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_6.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//
//
//        String lotto2Id = "G23L-20181130-000091";
//
//        //pararamteri per creazione lotto 2
//        //LOTTO SCARTATO quindi no msg sul DB; il lotto non ha passato la validazione
//        //----- Definizione Lotto START
//
//        events = new ArrayList<>();
//        events.add(new Events(EventType.LOTTO_RICEVUTO, LocalDateTime.now().minusHours(8L)));
//        events.add(new Events(EventType.LOTTO_IN_ELABORAZIONE, LocalDateTime.now().minusHours(3L)));
//        events.add(new Events(EventType.LOTTO_OUTCOME_GENERATO, LocalDateTime.now().minusHours(3L)));
//
//        LotDelivery lotto2 = LotDelivery.builder()
//                .lotId(lotto2Id)
//                .closed(LocalDateTime.now())
//                .created("2018-12-28")
//                .currentStatus(EventType.LOTTO_OUTCOME_GENERATO)
//                .customerCode("23L")
//                .service("G23L")
//                .events(events)
//                .mailDeliveriesIDs(Arrays.asList(messageIds4Lotto()))
//                .messagesNotAccepted(4)
//                .messagesNotDelivered(3)
//                .messagesNotValidated(6)
//                .messagesProcessed(30)
//                .messagesSuccess(10)
//                .updated(LocalDateTime.now().plusDays(1L))
//
//                .build() ;
//        //----- Definizione Lotto END
//
//        creaLotto(lotto2);
//        LOGGER.info("Creazione lotto " + lotto2Id + "COMPLETATA");
//
//
//
//        //parametri per creazione messaggio 1
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(8L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now().minusHours(5L)));
//        events.add(new Events(EventType.MESSAGGIO_CONSEGNA_OK, LocalDateTime.now().minusHours(4L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto2_1 = MailDeliveryRequest.builder()
//                .messageID(lotto2Id + "-0000000000" + "1")
//
//                .extID("ExtID_1")
//                .lotId(lotto2Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("1")
//                .deliveryOutcome("1")
//                .acceptanceFileName("accept_2_1.xml")
//                .deliveryFileName("delivery_2_1.xml")
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto2Id,messaggioLotto2_1);
//        LOGGER.info("Creazione messaggio "+messaggioLotto2_1.getMessageID()+" del lotto " + lotto2Id + "COMPLETATA");
//
//
//
//        //parametri per creazione messaggio 2
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(8L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_KO, LocalDateTime.now().minusHours(5L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto2_2 = MailDeliveryRequest.builder()
//                .messageID(lotto2Id + "-0000000000" + "2")
//
//                .extID("ExtID_2")
//                .lotId(lotto2Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("V001")
//                .deliveryOutcome(null)
//                .acceptanceFileName("accept_2_2.eml")
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto2Id,messaggioLotto2_2);
//        LOGGER.info("Creazione messaggio "+messaggioLotto2_2.getMessageID()+" del lotto " + lotto2Id + "COMPLETATA");
//
//
//
//        //parametri per creazione messaggio 3
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_NON_INVIABILE, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto2_3 = MailDeliveryRequest.builder()
//                .messageID(lotto2Id + "-0000000000" + "3")
//
//                .extID("ExtID_3")
//                .lotId(lotto2Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("V001")
//                .acceptanceOutcome(null)
//                .deliveryOutcome(null)
//                .acceptanceFileName(null)
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto2Id,messaggioLotto2_3);
//        LOGGER.info("Creazione messaggio "+messaggioLotto2_3.getMessageID()+" del lotto " + lotto2Id + "COMPLETATA");
//
//
//
//
//    }
//
//
//
//
//
//    @Test
//    public void buildMessagesDataSetForTest2(){
//
//        List<Events> events;
//        List<String> attachments;
//
//
//        String lotto1Id = "G23L-20181130-000080";
//
//        //pararamteri per creazione lotto 1
//        //----- Definizione Lotto START
//
//        events = new ArrayList<>();
//        events.add(new Events(EventType.LOTTO_RICEVUTO, LocalDateTime.now().minusHours(8L)));
//        events.add(new Events(EventType.LOTTO_IN_ELABORAZIONE, LocalDateTime.now().minusHours(3L)));
//
//        LotDelivery lotto1 = LotDelivery.builder()
//                .lotId(lotto1Id)
//                .closed(LocalDateTime.now())
//                .created("2018-12-28")
//                .currentStatus(EventType.LOTTO_IN_ELABORAZIONE)
//                .customerCode("23L")
//                .service("G23L")
//                .events(events)
//                .mailDeliveriesIDs(Arrays.asList(messageIds4Lotto()))
//                .messagesNotAccepted(4)
//                .messagesNotDelivered(3)
//                .messagesNotValidated(6)
//                .messagesProcessed(30)
//                .messagesSuccess(10)
//                .updated(LocalDateTime.now().plusDays(1L))
//
//                .build() ;
//
//        //----- Definizione Lotto END
//
//        creaLotto(lotto1);
//        LOGGER.info("Creazione lotto " + lotto1Id + "COMPLETATA");
//
//
//        //parametri per creazione messaggio 1
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(4L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now().minusHours(2L)));
//        events.add(new Events(EventType.MESSAGGIO_CONSEGNA_OK, LocalDateTime.now().minusHours(1L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//        attachments.add("Attach3.xml");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_1 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "1")
//
//                .extID("ExtID_1")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("1")
//                .deliveryOutcome("1")
//                .acceptanceFileName("fileForAccept_1_1.eml")
//                .deliveryFileName("delivery.eml")
//                .events(events)
//
//                .build() ;
//
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_1);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_1.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//        //parametri per creazione messaggio 2
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(3L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_FALLITO, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_2 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "2")
//
//                .extID("ExtID_2")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("V001")
//                .acceptanceOutcome(null)
//                .deliveryOutcome(null)
//                .acceptanceFileName(null)
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_2);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_2.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//        //parametri per creazione messaggio 3
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(5L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_KO, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_3 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "3")
//
//                .extID("ExtID_3")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("V001")
//                .deliveryOutcome(null)
//                .acceptanceFileName("fileForAccept_1_3.eml")
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_3);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_3.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//
//        //parametri per creazione messaggio 4
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(5L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_KO, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_4 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "4")
//
//                .extID("ExtID_4")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("V001")
//                .deliveryOutcome(null)
//                .acceptanceFileName("fileForAccept_1_4.eml")
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_4);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_4.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//
//        //parametri per creazione messaggio 1
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(4L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now().minusHours(2L)));
//        events.add(new Events(EventType.MESSAGGIO_CONSEGNA_OK, LocalDateTime.now().minusHours(1L)));
//
//
//        attachments = new ArrayList<>();
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_5 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "5")
//
//                .extID("ExtID_5")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("1")
//                .deliveryOutcome("1")
//                .acceptanceFileName("fileForAccept_1_5.eml")
//                .deliveryFileName("delivery_1_5.eml")
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_5);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_5.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//
//        //parametri per creazione messaggio 6
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(3L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_FALLITO, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_6 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "6")
//
//                .extID("ExtID_6")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("V001")
//                .acceptanceOutcome(null)
//                .deliveryOutcome(null)
//                .acceptanceFileName(null)
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_6);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_6.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//
//
//        String lotto2Id = "G23L-20181130-000081";
//
//        //pararamteri per creazione lotto 2
//        //LOTTO SCARTATO quindi no msg sul DB; il lotto non ha passato la validazione
//        //----- Definizione Lotto START
//
//        events = new ArrayList<>();
//        events.add(new Events(EventType.LOTTO_RICEVUTO, LocalDateTime.now().minusHours(8L)));
//        events.add(new Events(EventType.LOTTO_SCARTATO, LocalDateTime.now().minusHours(3L)));
//
//        LotDelivery lotto2 = LotDelivery.builder()
//                .lotId(lotto2Id)
//                .closed(LocalDateTime.now())
//                .created("2018-12-28")
//                .currentStatus(EventType.LOTTO_SCARTATO)
//                .customerCode("23L")
//                .service("G23L")
//                .events(events)
//                .mailDeliveriesIDs(null)
//                .messagesNotAccepted(0)
//                .messagesNotDelivered(0)
//                .messagesNotValidated(0)
//                .messagesProcessed(0)
//                .messagesSuccess(0)
//                .updated(LocalDateTime.now().plusDays(1L))
//
//                .build() ;
//        //----- Definizione Lotto END
//
//        creaLotto(lotto2);
//        LOGGER.info("Creazione lotto " + lotto2Id + "COMPLETATA");
//
//
//    }
//
//
//
//
//
//    @Test
//    public void buildMessagesDataSetForTest1(){
//
//        List<Events> events;
//        List<String> attachments;
//
//
//        String lotto1Id = "G23L-20181130-000068";
//
//        //pararamteri per creazione lotto 1
//        //----- Definizione Lotto START
//
//        events = new ArrayList<>();
//        events.add(new Events(EventType.LOTTO_RICEVUTO, LocalDateTime.now().minusHours(8L)));
//        events.add(new Events(EventType.LOTTO_IN_ELABORAZIONE, LocalDateTime.now().minusHours(3L)));
//
//        LotDelivery lotto1 = LotDelivery.builder()
//                .lotId(lotto1Id)
//                .closed(LocalDateTime.now())
//                .created("2018-12-28")
//                .currentStatus(EventType.LOTTO_IN_ELABORAZIONE)
//                .customerCode("23L")
//                .service("G23L")
//                .events(events)
//                .mailDeliveriesIDs(Arrays.asList(messageIds4Lotto()))
//                .messagesNotAccepted(4)
//                .messagesNotDelivered(3)
//                .messagesNotValidated(6)
//                .messagesProcessed(30)
//                .messagesSuccess(10)
//                .updated(LocalDateTime.now().plusDays(1L))
//
//                .build() ;
//        //----- Definizione Lotto END
//
//        creaLotto(lotto1);
//        LOGGER.info("Creazione lotto " + lotto1Id + "COMPLETATA");
//
//        //parametri per creazione messaggio 1
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(4L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now().minusHours(2L)));
//        events.add(new Events(EventType.MESSAGGIO_CONSEGNA_OK, LocalDateTime.now().minusHours(1L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//        attachments.add("Attach3.xml");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_1 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "1")
//
//                .extID("ExtID_1")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("1")
//                .deliveryOutcome("1")
//                .acceptanceFileName("fileForAccept_1_1.eml")
//                .deliveryFileName("delivery.eml")
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_1);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_1.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//        //parametri per creazione messaggio 2
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(3L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_FALLITO, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_2 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "2")
//
//                .extID("ExtID_2")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("V001")
//                .acceptanceOutcome(null)
//                .deliveryOutcome(null)
//                .acceptanceFileName(null)
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_2);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_2.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//        //parametri per creazione messaggio 3
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(6L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(5L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_KO, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto1_3 = MailDeliveryRequest.builder()
//                .messageID(lotto1Id + "-0000000000" + "3")
//
//                .extID("ExtID_3")
//                .lotId(lotto1Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("V001")
//                .deliveryOutcome(null)
//                .acceptanceFileName("fileForAccept_1_3.eml")
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto1Id,messaggioLotto1_3);
//        LOGGER.info("Creazione messaggio "+messaggioLotto1_3.getMessageID()+" del lotto " + lotto1Id + "COMPLETATA");
//
//
//        String lotto2Id = "G23L-20181130-000069";
//
//        //pararamteri per creazione lotto 2
//        //----- Definizione Lotto START
//
//        events = new ArrayList<>();
//        events.add(new Events(EventType.LOTTO_RICEVUTO, LocalDateTime.now().minusHours(8L)));
//        events.add(new Events(EventType.LOTTO_IN_ELABORAZIONE, LocalDateTime.now().minusHours(3L)));
//
//        LotDelivery lotto2 = LotDelivery.builder()
//                .lotId(lotto2Id)
//                .closed(LocalDateTime.now())
//                .created("2018-12-28")
//                .currentStatus(EventType.LOTTO_IN_ELABORAZIONE)
//                .customerCode("23L")
//                .service("G23L")
//                .events(events)
//                .mailDeliveriesIDs(Arrays.asList(messageIds4Lotto()))
//                .messagesNotAccepted(4)
//                .messagesNotDelivered(3)
//                .messagesNotValidated(6)
//                .messagesProcessed(30)
//                .messagesSuccess(10)
//                .updated(LocalDateTime.now().plusDays(1L))
//
//                .build() ;
//        //----- Definizione Lotto END
//
//        creaLotto(lotto2);
//        LOGGER.info("Creazione lotto " + lotto2Id + "COMPLETATA");
//
//
//        //parametri per creazione messaggio 1
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_NON_INVIABILE, LocalDateTime.now().minusHours(8L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto2_1 = MailDeliveryRequest.builder()
//                .messageID(lotto2Id + "-0000000000" + "1")
//
//                .extID("ExtID_1")
//                .lotId(lotto2Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("V001")
//                .acceptanceOutcome(null)
//                .deliveryOutcome(null)
//                .acceptanceFileName(null)
//                .deliveryFileName(null)
//                .events(events)
//
//                .build() ;
//
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto2Id,messaggioLotto2_1);
//        LOGGER.info("Creazione messaggio "+messaggioLotto2_1.getMessageID()+" del lotto " + lotto2Id + "COMPLETATA");
//
//
//        //parametri per creazione messaggio 2
//        //-- SINGLE MESSAGE DEFINITION START
//        events = new ArrayList<>();
//        events.add(new Events(EventType.MESSAGGIO_INVIABILE, LocalDateTime.now().minusHours(9L)));
//        events.add(new Events(EventType.MESSAGGIO_INVIO_SUCCESSO, LocalDateTime.now().minusHours(7L)));
//        events.add(new Events(EventType.MESSAGGIO_ACCETTAZIONE_OK, LocalDateTime.now().minusHours(4L)));
//        events.add(new Events(EventType.MESSAGGIO_CONSEGNA_KO, LocalDateTime.now().minusHours(2L)));
//
//
//        attachments = new ArrayList<>();
//        attachments.add("Attach1.txt");
//        attachments.add("Attach2.pdf");
//        attachments.add("Attach3.xml");
//
//
//        //definizione messaggio
//        MailDeliveryRequest messaggioLotto2_2 = MailDeliveryRequest.builder()
//                .messageID(lotto2Id + "-0000000000" + "2")
//
//                .extID("ExtID_2")
//                .lotId(lotto2Id)
//                .subject("Creato da JUNIT")
//                .to("someoneCC@localhost")
//                .cc("someoneCC@localhost")
//                .content("test from someone@localhost")
//                .attachments(attachments)
//                .validationOutcome("1")
//                .acceptanceOutcome("1")
//                .deliveryOutcome("V001")
//                .acceptanceFileName("fileForAccept_2_2.em")
//                .deliveryFileName("delivery_2_2.eml")
//                .events(events)
//
//                .build() ;
//
//        //-- SINGLE MESSAGE DEFINITION END
//
//        creaMessaggioPerLotto(lotto2Id,messaggioLotto2_2);
//        LOGGER.info("Creazione messaggio "+messaggioLotto2_2.getMessageID()+" del lotto " + lotto2Id + "COMPLETATA");
//
//
//    }
//
//
//
//
//
//    @Test
//    public void search4StatusAndEventLotto(){
//
//        List<LotDelivery> ris =  lotRepository.findByLotIdAndEvent("G23L-20181130-000081",EventType.LOTTO_SCARTATO.name()) ;
//
//        if(ris!=null){
//            System.out.println("size lista lotti estratti: " + ris.size() );
//        }
//
//    }
//
//
//    @Test
//    public void search4StatusAndEventLottoMail(){
//
//        List<MailDeliveryRequest> ris =  mailDeliveryRepository.findByLotIdAndEvent("G23L_20181229_00001",EventType.MESSAGGIO_INVIABILE.name()) ;
//
//        if(ris!=null){
//            System.out.println("size lista messaggi estratti: " + ris.size() );
//        }
//
//
//    }
//
//
//    @Test
//    public void search4EventLottoMail(){
//
//        List<MailDeliveryRequest> ris =  mailDeliveryRepository.findByEvent(EventType.MESSAGGIO_CONSEGNA_KO.name()) ;
//
//        if(ris!=null){
//            System.out.println("size lista messaggi estratti: " + ris.size() );
//        }
//
//
//        List<MailDeliveryRequest> ris2 =  mailDeliveryRepository.findByEvent(EventType.MESSAGGIO_ACCETTAZIONE_KO.name()) ;
//
//        if(ris!=null){
//            System.out.println("size lista messaggi estratti: " + ris.size() );
//        }
//
//    }
//
//
//}
