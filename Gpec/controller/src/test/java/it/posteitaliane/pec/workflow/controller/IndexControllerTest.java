package it.posteitaliane.pec.workflow.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(IndexController.class)
@DisplayName("Il Microservizio ''Message Composer''")
public class IndexControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("invocando il :path '/' restituisce l'indice delle risorse esposte dal servizio - metodo home()")
    public void homeIndexControllerTest() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(status().isOk())
                // TODO: sostituire con l'elaborazione del risultato atteso reale...
                .andExpect(content().string("index of the MailComposers Microservice - GPec. Poste DIGITAL"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
        ;

    }
}
