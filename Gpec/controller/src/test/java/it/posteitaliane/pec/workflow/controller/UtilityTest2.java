package it.posteitaliane.pec.workflow.controller;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.RawJsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.error.DocumentAlreadyExistsException;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import it.posteitaliane.pec.common.couchbase.CouchBase;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.workflow.service.ResponseService;
import org.junit.Test;

import javax.validation.constraints.AssertTrue;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertTrue;


public class UtilityTest2 {



    @Test
    public void erroreDecodificato() throws Exception{

        try {
            ProcessReceipt pc=new ProcessReceipt();


            String erroreEsteso="5.1.1 - DCMAIL - POSTECOM S.P.A. - GRUPPO POSTE ITAL=IANE - indirizzo non valido";
            String erroreDecodificato= pc.decodeDeliveryKOReturnCode(erroreEsteso);
            System.out.println("erroreDecodificato: "+erroreDecodificato);
            assertTrue("D511".equals(erroreDecodificato));






        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }

    }


    @Test
    public void cicloFinito() throws Exception{


        LocalDateTime tInizioCiclo = LocalDateTime.now();
        System.out.println("tInizioCiclo: "+tInizioCiclo);
        while( true){

            // System.out.println("CICLOOOO: ");

            LocalDateTime now = LocalDateTime.now();

            if(  now.minusSeconds(10).isAfter(tInizioCiclo)  ){
                break;
            }
        }

        System.out.println("ESCO DA CLICLO: ");
        LocalDateTime tFineCiclo = LocalDateTime.now();
        System.out.println("tFineCiclo: "+tFineCiclo);


    }


    @Test
    public void provaScrittura() throws Exception{

       this.provaScritturaaCouchbase();


    }


    public void provaScritturaaCouchbase() {
        System.out.println("************ PROVA COUCHBASE*");

        CouchBase cb = new CouchBase(Arrays.asList("127.0.0.1"));
        cb.connect();
        cb.authenticate("lots", "HwNbVfY2TVsBqgX");
        Bucket bucketLotLock = cb.open("lotLock");





//        N1qlQueryResult result = bucketLotLock.query(
//                N1qlQuery.simple("SELECT * FROM `travel-sample` LIMIT 5")
//        );

        String lotKey="G23L00000000000000013";

        JsonObject data = JsonObject.create()
                .put("lotKey", lotKey)

          //      .put("dataInizioElab", new Date().toString() ) //Unsupported type for JsonObject: class java.util.Date

                //   .put("dataInizioElab", LocalDateTime.now() ) //Unsupported type for JsonObject: class java.util.Date
                .put("dataInizioElab",Instant.now().plusSeconds(3600).toString())

                //.put("dataInizioElab",Instant.now().toString())


                ;

        ;

        //bucketLotLock.


//                N1qlQueryResult result = bucketLotLock.query(
//                        N1qlQuery.simple("SELECT * FROM `travel-sample` LIMIT 5")
//                );


        //faccio la insert sulla "tabella" di lock:  lotId, timestamp
        //se la insert fallisce vuol dire che qualcuno gia' se lo e' preso in carico -->non faccio nulla
        //se la insert non fallisce, vuol dire che nessuno l'ha ancora preso in carico

        RawJsonDocument document = RawJsonDocument.create(lotKey, data.toString());
        try{
            bucketLotLock.insert(document);
        }
        catch (DocumentAlreadyExistsException e){

            System.out.println("DocumentAlreadyExistsException");
        }
        catch (Exception e){

           e.printStackTrace();
        }

        //estrae da CB il documento con lotKey  e dataInizioElab > sysdate + 10
        // bucketLotLock.query("");


    }

    @Test
    public void provaLettura() throws Exception{

        this.provaLetturaCouchbase();


    }

    public void provaLetturaCouchbase() {
        System.out.println("************ PROVA COUCHBASE*");

        CouchBase cb = new CouchBase(Arrays.asList("127.0.0.1"));
        cb.connect();
        cb.authenticate("lots", "HwNbVfY2TVsBqgX");
        Bucket bucketLotLock = cb.open("lotLock");



        String select="SELECT *  FROM  lotLock WHERE  NOW_LOCAL() > DATE_ADD_STR(dataInizioElab, 3, 'hour')  ";
        //  String delete="DELETE  FROM  lotLock WHERE  NOW_LOCAL() > DATE_ADD_STR(dataInizioElab, 1, 'minute')  ";

        //DELETE SU LOTLOCK WHERE DATA_INIZ_ELAB > NOW + 30 MINUTI
        //IN QUESTO MODO
        N1qlQueryResult result = bucketLotLock.query(
                N1qlQuery.simple(select)
        );
        // String delete="DELETE  FROM `lotLock` WHERE  dataInizioElab ";

        List listResult=result.allRows();



//        N1qlQueryResult result = bucketLotLock.query(
//                N1qlQuery.simple("SELECT * FROM `travel-sample` LIMIT 5")
//        );






    }


    @Test
    public void provaDelete() throws Exception{

        this.provaDeleteCouchbase();


    }

    public void provaDeleteCouchbase() {
        System.out.println("************ PROVA COUCHBASE*");

        CouchBase cb = new CouchBase(Arrays.asList("127.0.0.1"));
        cb.connect();
        cb.authenticate("lots", "HwNbVfY2TVsBqgX");
        Bucket bucketLotLock = cb.open("lotLock");



        //String select="SELECT *  FROM  lotLock WHERE  NOW_LOCAL() > DATE_ADD_STR(dataInizioElab, 3, 'hour')  ";
        //String delete="DELETE  FROM  lotLock WHERE  NOW_LOCAL() > DATE_ADD_STR(dataInizioElab, 1, 'minute')  ";

      //  String delete="DELETE FROM  lotLock USE KEYS G23L00000000000000011";

        String lotKey="G23L00000000000000011";
        String delete="DELETE FROM lotLock WHERE  lotKey = '"+lotKey+"'";

        //DELETE SU LOTLOCK WHERE DATA_INIZ_ELAB > NOW + 30 MINUTI
        //IN QUESTO MODO
        N1qlQueryResult result = bucketLotLock.query(
                N1qlQuery.simple(delete)
        );
        // String delete="DELETE  FROM `lotLock` WHERE  dataInizioElab ";

        List listResult=result.allRows();



//        N1qlQueryResult result = bucketLotLock.query(
//                N1qlQuery.simple("SELECT * FROM `travel-sample` LIMIT 5")
//        );



    }
}
