package it.posteitaliane.pec.workflow.extlib;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import java.io.InputStream;

public class ExtMimeMessage extends MimeMessage {

    public ExtMimeMessage(Session session) {
        super(session);
    }

    public ExtMimeMessage(Session session, InputStream is) throws MessagingException {
        super(session, is);
    }

    public ExtMimeMessage(MimeMessage source) throws MessagingException {
        super(source);
    }

    protected ExtMimeMessage(Folder folder, int msgnum) {
        super(folder, msgnum);
    }

    protected ExtMimeMessage(Folder folder, InputStream is, int msgnum) throws MessagingException {
        super(folder, is, msgnum);
    }

    protected ExtMimeMessage(Folder folder, InternetHeaders headers, byte[] content, int msgnum) throws MessagingException {
        super(folder, headers, content, msgnum);
    }

    public void deleteHeader() {
        this.headers = new ExtInternetHeaders();
    }

    public void addHeader(String name, String value) {
        this.headers.addHeader(name, value);
    }

    protected void updateMessageID() throws MessagingException {
        // non faccio nulla: evito la generazione di un message id durante la scrittura
    }
}
