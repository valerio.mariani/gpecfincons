package it.posteitaliane.pec.workflow.outcome.zip;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


public class OutcomeZipBuilder{

   private FileOutputStream fos;
   private ZipOutputStream zos;
   private String lottoId;

    private static final Logger LOGGER = LoggerFactory.getLogger(OutcomeZipBuilder.class);


    public OutcomeZipBuilder (String zipFile,String lottoId) throws FileNotFoundException {
        this.fos = new FileOutputStream(zipFile);
        this.zos = new ZipOutputStream(this.fos);
        this.lottoId = lottoId;
    }


    public void zipNewEntryFromByteArray(String entryFileName,byte[] obj) throws IOException {

        zos.putNextEntry(new ZipEntry(entryFileName));

        zos.write(obj);
        zos.closeEntry();
    }

    public  static void addToZipFile(Path fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

       // LOGGER.info("******* aggiunta file '" + fileName + "' nello ZIP ******");

        File file = new File(fileName.toAbsolutePath().toString());
        FileInputStream fis = new FileInputStream(file);
        ZipEntry zipEntry = new ZipEntry(fileName.getFileName().toString());
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }


    public void addEmlToZipByLottoId(String pathFiles) throws IOException {

        Files.list(Paths.get(pathFiles))
                .filter(s -> s.toString().endsWith(".eml"))
                .map(Path::toAbsolutePath)
                .forEach(emlFile -> {

                    try {

                        addToZipFile(emlFile, zos);

                    } catch (FileNotFoundException fnf) {
                        LOGGER.error(fnf.getMessage(),fnf);
                    }catch (IOException is) {
                        LOGGER.error(is.getMessage(),is);
                    }

                });

    }


    public void finalizeZip() throws IOException {
        try {
            zos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
