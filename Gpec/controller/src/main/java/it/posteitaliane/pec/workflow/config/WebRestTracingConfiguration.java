package it.posteitaliane.pec.workflow.config;

import brave.Tracer;
import brave.Tracing;
import brave.http.HttpTracing;
import brave.spring.web.TracingClientHttpRequestInterceptor;
import it.posteitaliane.pec.common.tracing.LotIdTracingFilter;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class WebRestTracingConfiguration { // implements WebMvcConfigurer {

    private final HttpTracing httpTracing;

    public WebRestTracingConfiguration(HttpTracing httpTracing) {
        this.httpTracing = httpTracing;
    }

    @Bean
    RestTemplate restTemplate(HttpTracing tracing) {
        return new RestTemplateBuilder()
                .additionalInterceptors(TracingClientHttpRequestInterceptor.create(tracing))
                .build();
    }

    @Bean
    LotIdTracingFilter lotIdTracingFilter(Tracer tracer, Tracing tracing){
        return new LotIdTracingFilter(tracer, tracing);
    }


    /**
     * per MVC, se mai ci sarà una GUI...
     */
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(  TracingHandlerIntercepto.create(httpTracing));
//    }
}
