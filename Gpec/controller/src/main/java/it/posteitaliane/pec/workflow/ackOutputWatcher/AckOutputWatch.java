package it.posteitaliane.pec.workflow.ackOutputWatcher;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

@Configuration
public class AckOutputWatch {


    private static final Logger LOGGER = LoggerFactory.getLogger(AckOutputWatch.class);

    @Value("${controllo.watcher.file.to.find.prefix}")
    private String fileTofindPrefix ;

    @Value("${folder.file.ack.output.polling.interval.seconds}")
    private String pollingIntervalSeconds ;

    @Value("${folder.file.ack.output}")
    private String folderSourcePoller ;



    @Autowired
    FileAlterationListener fileAlterationListenerImpl;

    @Bean
    public FileAlterationMonitor watcher() throws Exception{

        //Properties mainProperties = new Properties();
        //System.out.println(System.getProperty("user.dir"));

        /*Rimettere percorso relativo*/
        //mainProperties.load(new FileInputStream("C:\\Users\\Lele\\GPEC\\GPEC\\Gpec\\controller\\watcher.properties"));

        //String fileTofindPrefix = mainProperties.getProperty("controllo.watcher.file.to.find.prefix");

        //Integer pollingIntervalSeconds = Integer.valueOf(mainProperties.getProperty("pollin.interval.seconds"));
        long pollingInterval = Integer.valueOf(pollingIntervalSeconds) * 1000;

        final FileAlterationMonitor monitor = new FileAlterationMonitor(pollingInterval);


        IOFileFilter filesAckOutput   = FileFilterUtils.and( FileFilterUtils.fileFileFilter(),
                                                             FileFilterUtils.prefixFileFilter(fileTofindPrefix));


        //String folderSource = mainProperties.getProperty("folder.source");


        Path pathSource = Paths.get(folderSourcePoller);

        FileAlterationObserver observer = new FileAlterationObserver(pathSource.toFile(),filesAckOutput);
        observer.addListener(fileAlterationListenerImpl);

        monitor.addObserver(observer);


        monitor.start();

        for (FileAlterationObserver observerDir: monitor.getObservers()){
            LOGGER.info("Monitoring every " +pollingIntervalSeconds + " seconds folder @ path " + observerDir.getDirectory().getAbsolutePath() + " STARTED");
        }

        return monitor;

    }


}
