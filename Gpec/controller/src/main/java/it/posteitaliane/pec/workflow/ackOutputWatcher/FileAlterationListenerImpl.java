package it.posteitaliane.pec.workflow.ackOutputWatcher;

import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.repositories.LotRepository;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.time.LocalDateTime;


@Component
public class FileAlterationListenerImpl implements FileAlterationListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileAlterationListenerImpl.class);


    @Value("${controllo.watcher.file.to.find.prefix}")
    private String fileTofindPrefix ;


    @Autowired
    LotRepository lotRepository;



    @Override
    public void onStart(final FileAlterationObserver observer) {
    }

    @Override
    public void onDirectoryCreate(final File directory) {
    }

    @Override
    public void onDirectoryChange(final File directory) {
    }

    @Override
    public void onDirectoryDelete(final File directory) {
    }


    @Override
    public void onFileCreate(final File file) {

         String fileCreated =  file.getAbsoluteFile().getName() ;

         String lottoId = fileCreated.substring(fileTofindPrefix.length(),fileCreated.indexOf(".")) ;

         LotDelivery lotto = lotRepository.findById(lottoId).get();

         lotto.setCurrentStatus(EventType.LOTTO_ACK_OUTPUT_RICEVUTO);
         lotto.getEvents().add( Events.builder().event(lotto.getCurrentStatus()).time(LocalDateTime.now()).build() );


        lotRepository.save(lotto);


         LOGGER.info("Passaggio di stato per il lotto con ID "+lottoId+": "+EventType.LOTTO_ACK_OUTPUT_RICEVUTO);

    }


    @Override
    public void onFileChange(final File file) {
    }

    @Override
    public void onFileDelete(final File file) {
    }

    @Override
    public void onStop(final FileAlterationObserver observer) {
    }

}
