package it.posteitaliane.pec.workflow.outcome.model.builder;

import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.model.xml.outcome.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class OutcomeModelBuilder {

    private OutcomeLot outcomeObj;

    private Headers header;
    private MessageList listaMessaggiParent ;
    private List<Message> messageList;


    public OutcomeModelBuilder() {

        this.outcomeObj = new OutcomeLot();
        this.header = new Headers();
        this.listaMessaggiParent = new MessageList();
        this.messageList = new ArrayList<>();
    }


    public void setHeader(LotDelivery lotto) {

        header.setCustomerCode(lotto.getCustomerCode());
        header.setLotId(lotto.getLotId());
        header.setService(lotto.getService());

        Result result = new Result();
        result.setNotAccepted(BigInteger.valueOf(lotto.getMessagesNotAccepted()));
        result.setNotDelivered(BigInteger.valueOf(lotto.getMessagesNotDelivered()));
        result.setNotValidated(BigInteger.valueOf(lotto.getMessagesNotValidated()));
        result.setProcessed(BigInteger.valueOf(lotto.getMessagesProcessed()));
        result.setSuccess(BigInteger.valueOf(lotto.getMessagesSuccess()));

        header.setResult(result);

        this.header = header;
    }



    public void setMessageList(List<MailDeliveryRequest> lottoMessages){

        for(MailDeliveryRequest message: lottoMessages) {

            Message mess = new Message();

            mess.setId(message.getMessageID());
            mess.setExtId(message.getExtID());

            Validation validation = new Validation();
            validation.setOutcome(message.getValidationOutcome());
            mess.setValidation(validation);

            if ("1".equals(message.getValidationOutcome())) {

                //Acceptance section
                Acceptance acceptance = new Acceptance();
                acceptance.setFileName(message.getAcceptanceFileName());
                acceptance.setOutcome(message.getAcceptanceOutcome());

                mess.setAcceptance(acceptance);

                if ("1".equals(message.getAcceptanceOutcome())){

                    //Delivery section
                    Delivery delivery = new Delivery();

                    delivery.setFileName(message.getDeliveryFileName());
                    delivery.setOutcome(message.getDeliveryOutcome());

                    List listDelivery=new ArrayList();
                    listDelivery.add(delivery);

                    mess.setDelivery(listDelivery);

                }

            }


            messageList.add(mess);

        };

        listaMessaggiParent.setMessage(messageList);

    }



    public OutcomeLot build(){

        outcomeObj.setHeaders(header);
        outcomeObj.setMessageList(listaMessaggiParent);

        return outcomeObj ;
    }


}
