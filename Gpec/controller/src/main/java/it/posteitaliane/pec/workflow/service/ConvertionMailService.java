package it.posteitaliane.pec.workflow.service;

import it.posteitaliane.pec.workflow.extlib.ExtMimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.PosixFilePermission;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ConvertionMailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConvertionMailService.class);

    // /opt/SHARE/GPEC/msg/cleaned/
    @Value("${email.path.output}")
    String outputPathMail;

    // /opt/SHARE/GPEC/msg/receipt/tmp/
    @Value("${email.path.input}")
    String inputPathMail;
//    @Value("${email.path.old}")
//    String original;
    @Value("${email.file.extension}")
    String mailExtension;

    private String separator = "-";
    private String firstHeaderKey = "Return-Path";
    private SimpleDateFormat dateTimeFileFormatter = new SimpleDateFormat("yyyyMMdd_HHmmss");

    public void convertMail(String filename) throws IOException, MessagingException {
        LOGGER.info("Conversione del file: " + filename);

        String inputFile = Stream.of(inputPathMail, filename).collect(Collectors.joining());
        //String originalFile = Stream.of(original, filename).collect(Collectors.joining());
        String outputFile = Stream.of(outputPathMail, filename).collect(Collectors.joining());

        String outputFileConPunto= Stream.of(outputPathMail, "."+filename).collect(Collectors.joining());

        File messageFile = new File(inputFile);
        InputStream source = new FileInputStream(messageFile);
        ExtMimeMessage message = new ExtMimeMessage(null, source);
        ExtMimeMessage outputMessage = null;

        // chiudo il file di origine e lo sposto nella directory dei vecchi files
        source.close();
        //NON BACKAPPO PIU' I FILE ORIGINALI PRIMA DELLA CONVERSIONE, PERCHE' NON SAPREI CHE POLITICA DI SVECCHIAMENTO ADOTTARE
//        try {
//            LOGGER.info("Sposto la vecchia mail su: " + originalFile);
//            Files.move(Paths.get(inputFile), Paths.get(originalFile));
//        } catch (FileAlreadyExistsException ex) {
//            String directoryOldFileTAG = filename.replace(mailExtension, Stream.of(separator, dateTimeFileFormatter.format(new Date()), mailExtension).collect(Collectors.joining()));
//            originalFile = Stream.of(original, directoryOldFileTAG).collect(Collectors.joining());
//            LOGGER.info("La mail sulla directory già esiste, creo un nuovo file: " + originalFile);
//            Files.move(Paths.get(inputFile), Paths.get(originalFile), StandardCopyOption.REPLACE_EXISTING);
//        }

        // scrivo il nuovo file

        LOGGER.info("Creazione del messaggio senza byte sporchi con punto davanti: " + outputFileConPunto);// PER ESIGENZE DI NIFI
        File emlFile = new File(outputFileConPunto);
        emlFile.createNewFile();
        FileOutputStream outFile = new FileOutputStream(emlFile);
        outputMessage = writeMessage(message);
        outputMessage.writeTo(outFile);
        outFile.close();

        Path fileEml=Paths.get( emlFile.getPath());

        //devo creare il file pulito prima con un . davanti
        //poi gli cambio i permessi
        // poi lo rinomino senza il .


        Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
        //add owners permission
        perms.add(PosixFilePermission.OWNER_READ);
        perms.add(PosixFilePermission.OWNER_WRITE);
        //add group permissions
        perms.add(PosixFilePermission.GROUP_READ);
        perms.add(PosixFilePermission.GROUP_WRITE);
        //add others permissions
        perms.add(PosixFilePermission.OTHERS_READ);

        LOGGER.info("Cambio permessi al file col punto: " + fileEml.getFileName());
        //only for system POSIX compliant (NO windows system)
        Files.setPosixFilePermissions(fileEml, perms);


        LOGGER.info("Rinomino il file col punto in senza punto : " + outputFile);

        File oldfile =new File(outputFileConPunto);
        File newfile =new File(outputFile);

        if(oldfile.renameTo(newfile)){
            LOGGER.info("Rinomino il file col punto in senza punto : " + outputFile);
        }else{
            LOGGER.info("ERRORE NELLA RENAME DEL file col punto in senza punto : " + outputFile);
        }

        LOGGER.info("FINE METODO Conversione di "+filename );
    }

    private ExtMimeMessage writeMessage(Message message) throws MessagingException, IOException {
        ExtMimeMessage outputMessage = new ExtMimeMessage(message.getSession());
        // cancello gli header vuoti creati da javaxmail
        outputMessage.deleteHeader();

        // creo la lista degli header leggendo gli originali
        // ed escludendo i caratteri sporchi all'inizio del primo header
        Enumeration<Header> headers = message.getAllHeaders();

        while(headers.hasMoreElements()) {
            Header h = headers.nextElement();
            // javax mail ha trimmato correttamente la chiave di inizio (non ci sono ':' tra i caratteri sporchi)
            if (h.getName().contains(firstHeaderKey)) {
                outputMessage.addHeader(firstHeaderKey, h.getValue());
            }
            // si verifica quando all'interno dei caratteri sporchi ci sono i ':', quindi verifico e trimmo il contenuto del value
            else if (h.getValue().contains(firstHeaderKey)) {
                outputMessage.addHeader(firstHeaderKey, h.getValue().substring(h.getValue().indexOf(":")+1,h.getValue().length()).trim());
            }
            // inserisco le altre chiavi
            else {
                outputMessage.addHeader(h.getName(), h.getValue());
            }
        }

        Multipart multipart = (MimeMultipart) message.getContent();
        outputMessage.setContent(multipart);

        return outputMessage;
    }
}
