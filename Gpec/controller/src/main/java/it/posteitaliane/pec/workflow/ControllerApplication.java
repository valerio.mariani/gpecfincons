package it.posteitaliane.pec.workflow;

import brave.Tracer;
import brave.Tracing;
import brave.context.log4j2.ThreadContextScopeDecorator;
import brave.http.HttpAdapter;
import brave.http.HttpRuleSampler;
import brave.http.HttpSampler;
import brave.http.HttpTracing;
import brave.propagation.B3Propagation;
import brave.propagation.CurrentTraceContext;
import brave.propagation.ExtraFieldPropagation;
import brave.propagation.ThreadLocalCurrentTraceContext;
import brave.sampler.Sampler;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.ribbon.proxy.annotation.Http;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import it.posteitaliane.pec.common.integration.EventMessagePublisher;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.sleuth.instrument.web.ServerSampler;
import org.springframework.cloud.sleuth.instrument.web.SkipPatternProvider;
import org.springframework.cloud.sleuth.zipkin2.ZipkinProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.StringUtils;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import zipkin2.Span;
import zipkin2.reporter.AsyncReporter;
import zipkin2.reporter.Reporter;
import zipkin2.reporter.Sender;
import zipkin2.reporter.amqp.RabbitMQSender;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

@SpringBootApplication
@EnableAdminServer
@EnableScheduling
@EnableEurekaClient
@EnableSwagger2
public class ControllerApplication {

	@Autowired
	private EurekaClient eurekaClient;

//	@Autowired
//	private SpanMetricReporter spanMetricReporter;

	@Autowired
	private ZipkinProperties zipkinProperties;


	@Value("${spring.sleuth.web.skipPattern}")
	private String skipPattern;

	public static void main(String[] args) {

		SpringApplication.run(ControllerApplication.class, args);

	}

	@Bean
	public InstanceInfo getCurrentServiceInstanceEurekaID(){
		return eurekaClient.getApplicationInfoManager().getInfo();
	}


	@Bean
	CurrentTraceContext log4jTraceContext() {
		return ThreadLocalCurrentTraceContext.newBuilder()
				.addScopeDecorator(ThreadContextScopeDecorator.create())
				.build();
	}

	@Bean
	HttpTracing httpTracing(Tracing tracing) {
//		return 	HttpTracing.create(tracing);
		return  HttpTracing.newBuilder(tracing).serverSampler(
				HttpRuleSampler.newBuilder()
						.addRule(null, "/actuator/health", 0.0f)
						.addRule(null, "/actuator/info", 0.1f)
						.addRule(null, "/getXmlAckInput", 1.0f)
						.addRule(null, "/processReceipt", 1.0f)
						.addRule(null, "/lot", 1.0f)
						.build()
			).build();

	}

	@Bean
	Reporter<Span> spanReporter(Sender sender) {
		return AsyncReporter.create(sender);
	}

	@Bean
	Sender sender(@Value("${spring.rabbitmq.host}") String rabbitmqHostUrl,
				  @Value("${rabbit.custom.queue.event.actions}") String zipkinQueue,
				  @Value("${spring.rabbitmq.username}") String user,
				  @Value("${spring.rabbitmq.password}") String psw) throws IOException {
		return RabbitMQSender.newBuilder()
				.queue(zipkinQueue)
				.username(user)
				.password(psw)
				.addresses(rabbitmqHostUrl).build();
	}


//	@Bean(name = ServerSampler.NAME)
//	HttpSampler myHttpSampler(SkipPatternProvider provider) {
//		Pattern pattern = provider.skipPattern();
//		return new HttpSampler() {
//
//			@Override
//			public <Req> Boolean trySample(HttpAdapter<Req, ?> adapter, Req request) {
//				String url = adapter.path(request);
//				boolean shouldSkip = pattern.matcher(url).matches();
//				if (shouldSkip) {
//					return false;
//				}
//				return null;
//			}
//		};
//	}

	@Bean
	Tracing tracing(
			@Value("${spring.application.name:spring-tracing}") String serviceName,
			Reporter<Span> spanReporter,
			CurrentTraceContext log4jTraceContext) {

		return Tracing
				.newBuilder()
				.sampler(Sampler.ALWAYS_SAMPLE)
//				.sampler( )
				.localServiceName(StringUtils.capitalize(serviceName))
				.propagationFactory(ExtraFieldPropagation
						.newFactoryBuilder(B3Propagation.FACTORY).
								addField("segment.original.filename").
								addPrefixedFields("x-baggage-", Arrays.asList("lotId", "service", "customerCode")).
								build())
				.currentTraceContext(log4jTraceContext)
				.spanReporter(spanReporter)
				.build();
	}


	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("it.posteitaliane.pec.mail.producer")) // RequestHandlerSelectors.any()
				.paths(PathSelectors.any())
				.build();
	}

	@Value("${spring.application.name:Controller}")
	String applicationName;

	@Bean
//    @RefreshScope
	public EventMessagePublisher eventsPublisher(
			@Value("${rabbit.custom.routing-key.event.topic.workflow}")
					String managementTopicRoutingKey,
			Tracer tracer,
			RabbitTemplate rabbitTemplate,
			TopicExchange globalManagementExchange ){
		return new EventMessagePublisher(
				applicationName.toUpperCase(),
				managementTopicRoutingKey,
				tracer,
				rabbitTemplate, globalManagementExchange);
	}


}

