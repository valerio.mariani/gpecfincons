package it.posteitaliane.pec.workflow.service;

import brave.Span;
import brave.Tracer;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.document.RawJsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.error.DocumentAlreadyExistsException;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import it.posteitaliane.pec.common.couchbase.CouchBase;
import it.posteitaliane.pec.common.integration.EventMessagePublisher;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.model.xml.outcome.OutcomeLot;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import it.posteitaliane.pec.workflow.outcome.delete.older.zip.DeleteOlderOutcomeFile;
import it.posteitaliane.pec.workflow.outcome.model.builder.OutcomeModelBuilder;
import it.posteitaliane.pec.workflow.outcome.zip.OutcomeZipBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@RefreshScope
public class ResponseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResponseService.class);

    @Autowired MailDeliveryRepository mailDeliveryRepository;
    @Autowired LotRepository lotRepository;
    @Autowired DeleteOlderOutcomeFile deleteOlderOutcomeFile;
    @Autowired EventMessagePublisher eventMessagePublisher;
    @Autowired Tracer tracer;

    @Autowired ApplicationContext applicationContext ;

    @Autowired
    private ResourceLoader resourceLoader;




    @Value("${spring.couchbase.bootstrap-hosts}")
    private String hostCB;

    @Value("${spring.couchbase.bucket.name}")
    private String userCB;

    @Value("${spring.couchbase.bucket.password}")
    private String passwordCB;




    @Value("${outcome.basepath}")
    private String basePath;


    @Value("${outcome.zippath}")
    private String zipPath;

    @Value("${lot.max-processing-time}")
    private String maxProcessingTime;

    @Value("${range.time.for.delete.outcome.for.lotto.in.state.outcome.generato}")
    private String rangeTimeForDeleteOutcomeForLottoInStateOutcomeGenerato;

    @Value("${range.time.for.delete.outcome.for.lotto.in.state.ack.output.ricevuto}")
    private String rangeTimeForDeleteOutcomeForLottoInStateAckOutputRicevuto;


    /*
    *
    * Input per metodo newUpdateLimit:
    * timeLimitPropInput -> stringa nel formato <valore numerico>-<tipo_intervallo>
    *     vedere in controller-local.yml per spiegazione esaustiva
    *
    * lastUpdate: formato LocalDarteTime; data che deve essere estesa di un delta timeLimitPropInput
    * es: 2019-04-07 02:11:55 con timeLimitPropInput = 1-d diventa: 2019-04-08 02:11:55
    *
    * timeLimitPropInput è contenuta nel file di properties
    *
    *
    * */
    public LocalDateTime newUpdateLimit(String timeLimitPropInput,LocalDateTime lastUpdate) {

        String[] elapsedTimeLimit = timeLimitPropInput.split("-");
        long amountOfInterval = Long.valueOf(elapsedTimeLimit[0]) ;

        //setting del tipo di intervallo
        switch(elapsedTimeLimit[1]){
            case "d":
                lastUpdate = lastUpdate.plusDays(amountOfInterval);
                break;
            case "h":
                lastUpdate = lastUpdate.plusHours(amountOfInterval);
                break;
            case "m":
                lastUpdate = lastUpdate.plusMinutes(amountOfInterval);
                break;
            case "s":
                lastUpdate = lastUpdate.plusSeconds(amountOfInterval);
                break;
            case "w":
                lastUpdate = lastUpdate.plusWeeks(amountOfInterval);
                break;
            case "M":
                lastUpdate = lastUpdate.plusMonths(amountOfInterval);
                break;
            case "Y":
                lastUpdate = lastUpdate.plusYears(amountOfInterval);
                break;
        }


        return lastUpdate;

    }





    @Scheduled(fixedDelayString = "${outcome.fixedDelay.milliseconds.zip.old.delete}")
    @NewSpan( value = "controller.scheduled.outcome.zip.delete-process")
    public void outcomeFindAndDeleteOlderZipService() throws IOException {

        LOGGER.info("Avvio attività schedulata per eliminazione vecchi file zip di OUTCOME");

        Span scheduledDeleteZipScope = tracer.nextSpan().name("controller.scheduled.outcome.zip.delete-process").kind(Span.Kind.PRODUCER);
        scheduledDeleteZipScope.tag("SCHEDULE", "Avvio attività schedulata per eliminazione vecchi file zip di OUTCOME");
        scheduledDeleteZipScope.start();

        try (Tracer.SpanInScope ws = tracer.withSpanInScope(scheduledDeleteZipScope)) {

            /*Recupero i lotti*/
            List<String> eventsToSearch = new ArrayList<>();
            eventsToSearch.add(EventType.LOTTO_OUTCOME_GENERATO.name());
            eventsToSearch.add(EventType.LOTTO_ACK_OUTPUT_RICEVUTO.name());
            eventsToSearch.add(EventType.LOTTO_SCARTATO.name());

            List<LotDelivery> deliveryListByStatus = lotRepository.findByCurrentStatusIn(eventsToSearch);

            if(deliveryListByStatus!=null){
                LOGGER.info("TROVATI "+ deliveryListByStatus.size()+" LOTTI DA ELIMINARE ");
            }


            LocalDateTime now = LocalDateTime.now();
            LocalDateTime newUpdateLimit;


            for (LotDelivery lotto : deliveryListByStatus) {

                switch (lotto.getCurrentStatus()) {

                    case LOTTO_OUTCOME_GENERATO:

                        newUpdateLimit = newUpdateLimit(rangeTimeForDeleteOutcomeForLottoInStateOutcomeGenerato, lotto.getUpdated());

                        if (now.isAfter(newUpdateLimit)) {
                            //se il lotto ha superato da elaborato la data limite
                            //delete del relativo zip di OUTCOME
                            try {
                                deleteOlderOutcomeFile.outcomeFindAndDeleteOlderZipService(lotto, zipPath);
                            } catch (Exception e) {
                                populateExceptionSpan(scheduledDeleteZipScope, e);
                            }
                        }

                        break;

                    case LOTTO_ACK_OUTPUT_RICEVUTO:

                        newUpdateLimit = newUpdateLimit(rangeTimeForDeleteOutcomeForLottoInStateAckOutputRicevuto, lotto.getUpdated());

                        if (now.isAfter(newUpdateLimit)) {
                            //se il lotto ha superato da elaborato la data limite
                            //delete del relativo zip di OUTCOME
                            try{
                                deleteOlderOutcomeFile.outcomeFindAndDeleteOlderZipService(lotto, zipPath);
                            } catch (Exception e) {
                                populateExceptionSpan(scheduledDeleteZipScope, e);
                            }
                        }

                        break;


                    case LOTTO_SCARTATO:

                        //TODO: FARE CANCELLAZIONE DEL FILE LOTTO INPUT
                        scheduledDeleteZipScope.annotate("Elemento scartato - do nothing " + lotto.getLotId());
                        break;

                }

            }
        }finally {
            scheduledDeleteZipScope.finish(); // note the scope is independent of the span. Always finish a span.
        }
    }


    /**
     * popola lo span in caso di errore
     * @param scheduledDeleteZipScope
     * @param e
     */
    private void populateExceptionSpan(Span scheduledDeleteZipScope, Exception e) {
        LOGGER.error("Errore nel metodo 'outcomeFindAndDeleteOlderZipService': " + e.getMessage());
        scheduledDeleteZipScope.annotate(e.getMessage());
        scheduledDeleteZipScope.error(e);
    }
    private File getTuringKeyStoreFile() throws IOException {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("outcome-lot-pec.xsd");

        File jks = File.createTempFile("outcome_schema", ".xsd");

        try {
            FileUtils.copyInputStreamToFile(inputStream, jks);
        } finally {
            inputStream.close();
        }

        return jks;
    }




    /**
     *  validateXMLSchema
     *
     *  A fronte di un XSD di riferimento, valida l'oggetto prodotto per il marshal finale
     *  In caso di fallita validazione viene sollevata eccezione.
     *  La gestione di quest'ultima prevede il logging in ERROR mode
     *
     * */
    public void validateXMLSchema (String xsdPath, JAXBContext jaxbContext, OutcomeLot xmlPojo ){

        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        //outcome-lot-pec.xsd
        File myFile = null;

        try{

            Resource resource = resourceLoader.getResource("classpath:"+xsdPath);
            InputStream outcome = resource.getInputStream();

            OutputStream opStream = null;
            myFile = File.createTempFile("temp-file-outcome", ".tmp");

            opStream = new FileOutputStream(myFile);
            opStream.write(IOUtils.toByteArray(outcome));
            opStream.flush();

            Schema schema = factory.newSchema(myFile);

            JAXBSource source = new JAXBSource(jaxbContext, xmlPojo);
            Validator validator = schema.newValidator();
            //validator.setErrorHandler(new CustomValidationErrorHandler());
            validator.validate(source);

        } catch (SAXException | JAXBException validationError) {
            LOGGER.error("Errore nella validazione dell'XML prodotto per il lotto: "+xmlPojo.getHeaders().getLotId() + " - " + validationError.getMessage());
        } catch (IOException ioEx){
            LOGGER.error("File XSD "+ xsdPath + " per la validazione del lotto: "+xmlPojo.getHeaders().getLotId() + " NON trovato. " + ioEx.getMessage()) ;
        } finally {
            if (myFile.exists()) {
                myFile.delete();
            }
        }



        LOGGER.info("************ VALIDAZIONE XML COMPLETATA PER LOTTO: "+xmlPojo.getHeaders().getLotId()+"************************");
    }

   // @Scheduled(fixedDelayString = "${outcome.fixedDelay.milliseconds.zip.builder}")
   // @NewSpan( value = "controller.scheduled.outcome.zip.create-process")




    @Scheduled(fixedDelayString = "${outcome.fixedDelay.milliseconds.zip.builder}")
    @NewSpan( value = "controller.scheduled.outcome.zip.create-process")
    public void responseService() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date toDay = new Date();
        String strDate = sdf.format(toDay);

        LOGGER.info("AVVIO ATTIVITÀ SCHEDULATA PER CREAZIONE FILE ZIP DI OUTCOME: " + strDate);

        CouchBase cb = null;

        Bucket bucketLotLock ;

        try {

            cb = new CouchBase(Arrays.asList(hostCB));
            cb.connect();
            cb.authenticate(userCB, passwordCB);
            bucketLotLock = cb.open("lotLock");






            // 1)     query  su CB di tutti i lotti in elaborazione
            // 2)     per ognuno di essi:
            // 2.1)   verificare se tutti i suoi messaggi "sono chiudibili"

            // 2.2)   verificare se la data di creazione di un lotto è più vecchia di ${lot.maxprocessingtime}
            //UPDATE al 2.2: usare updated e NON created

            // 2.2.2) se no non fare niente
            // 2.2.2) se si vai a 3:
            // 3)    creare l'xml tipo OUTCOME_G23L_20181103_000001
            // 3.1)  impacchettare tutte le ricevute e l'xml appena creato
            // 3.2)  spostare tale zip nella cartella di axway


            List<String> eventsToSearch = new ArrayList<>();
            eventsToSearch.add(EventType.LOTTO_IN_ELABORAZIONE.name());
            eventsToSearch.add(EventType.LOTTO_RICEVUTO.name());
            //per sicurezza metto anche lo stato LOTTO_RICEVUTO


            //DELETE SU LOTLOCK WHERE DATA_INIZ_ELAB > NOW + 50 MINUTI
            //in tal modo se qualche controller aveva inziato ad elaborare l'outcome ma per qualche ragione è caduto
            // cancello il relativo record su lotLock

            String delete = "DELETE  FROM  lotLock WHERE  NOW_LOCAL() > DATE_ADD_STR(dataInizioElab, 30, 'minute')  ";

            N1qlQueryResult result = bucketLotLock.query(
                    N1qlQuery.simple(delete)
            );

            LOGGER.info("cancellati eventuali record obsoleti su lotLocklotLock ");


            /*Recupero i lotti in elaborazione*/
            List<LotDelivery> lottiInElaborazione = lotRepository.findByCurrentStatusIn(eventsToSearch);

            if (lottiInElaborazione != null) {
                LOGGER.info("TROVATI " + lottiInElaborazione.size() + " LOTTI DA ANALIZZARE PER DECIDERE SE CHIUDIBILI ");
            }

            for (LotDelivery lottoInElab : lottiInElaborazione) {

                String nomeZipFileOutcome = "OUTCOME_" + lottoInElab.getLotId() + ".zip";

                String destinazioneZipFileOutcome = zipPath + nomeZipFileOutcome;

                LOGGER.info("PATH FINALE DOVE ANDRA' SALVATO LO ZIP " + destinazioneZipFileOutcome);
                File pathFinaleZipFileOutcome = new File(destinazioneZipFileOutcome);

                if (pathFinaleZipFileOutcome.exists()) {
                    //Casistica che non dovrebbe accadere, messa per sicurezza
                    //il file è stato lavorato da un  altro controller
                    continue;
                }


                //devo verificare se il lotto e' in uno stato per cui posso preparare il FILE OUTCOME
                //innanzi tutto analizzo i suoi messaggi :

                //estraggo tutti i messaggi di un lotto
                LOGGER.info("Analisi lotto con id: " + lottoInElab.getLotId());
                List<MailDeliveryRequest> messaggiDiUnLotto = mailDeliveryRepository.findByLotId(lottoInElab.getLotId());

                if (messaggiDiUnLotto == null || messaggiDiUnLotto.size() == 0) {
                    LOGGER.info("NESSUN MESSAGGIO RITROVATO PER LOTTO con id: " + lottoInElab.getLotId());
                    continue;
                }

                boolean messaggiTuttiChiudibili = true;


                for (MailDeliveryRequest message : messaggiDiUnLotto) {

                    if (!messaggioChiudibile(message)) {

                        messaggiTuttiChiudibili = false;
                        //appena trovo un messaggio NON chiudibile, esco dal ciclo
                        break;
                    }

                }


                // generava una strano null pointer
                // int numeroMessaggiContenutiNelLotto=lottoInElab.getMessagesProcessed();


                LocalDateTime now = LocalDateTime.now();
                LocalDateTime newUpdateLimit;

                newUpdateLimit = newUpdateLimit(maxProcessingTime, lottoInElab.getUpdated());
                //valore ottenuto sommando alla data update del lotto il massimo intervallo di tempo ammissibile

                //posso chiudere il lotto se si verifica almeno una delle 2 condizioni:
                //1) tutti i messaggi sono stati elaborati
                //oppure
                //2) e' trascorso un  intervallo di tempo da quando è stata aggiornaa la data updated del lotto (e cio' e' avvenuto quando e' passato in stato LOTTO_IN_ELABORAZIONE )
                //   maggiore dell'intervallo consentito maxProcessingTime

                boolean lottoChiudibile = messaggiTuttiChiudibili || now.isAfter(newUpdateLimit);

                if (lottoChiudibile) {

                    // faccio la insert sulla "tabella" di lock:  lotId, timestamp
                    // se la insert fallisce vuol dire che qualcuno gia' se lo e' preso in carico -->non faccio nulla
                    // se la insert non fallisce, vuol dire che nessuno l'ha ancora preso in carico


                    JsonObject data = JsonObject.create()
                            .put("lotKey", lottoInElab.getLotId())
                            .put("dataInizioElab", Instant.now().toString()) //SALVA LA DATA NEL FORMATO  ISO 8601 , QUELLO USATO DA NIFI
                            ;


//                //faccio la insert sulla "tabella" di lock:  lotId, timestamp
//                  //se la insert fallisce vuol dire che qualcuno gia' se lo e' preso in carico -->non faccio nulla
//                  //se la insert non fallisce, vuol dire che nessuno l'ha ancora preso in carico

                    RawJsonDocument document = RawJsonDocument.create(lottoInElab.getLotId(), data.toString());
                    try {
                        bucketLotLock.insert(document);
                    } catch (DocumentAlreadyExistsException e) {

                        LOGGER.error("DocumentAlreadyExistsException PER lotKey: " + lottoInElab.getLotId() + " --> LOTTO PRESO IN CARICO DA ALTRO CONTROLLER");
                        //la insert fallisce vuol dire che qualcuno gia' se lo e' preso in carico -->non faccio nulla, continuo il ciclo
                        continue;
                    } catch (Exception e) {

                        LOGGER.error(e.getMessage(), e);
                        continue;
                    }


                    LOGGER.info("Inizio produzione zip OUTCOME per lotto con id: " + lottoInElab.getLotId());

                    impostaContatoriLotto(lottoInElab);

                    //Build oggetto Outcome
                    OutcomeModelBuilder outcomeModelBuilder = new OutcomeModelBuilder();

                    outcomeModelBuilder.setHeader(lottoInElab);

                    outcomeModelBuilder.setMessageList(messaggiDiUnLotto);


                    Marshaller jaxbMarshaller = null;
                    try {

                        JAXBContext jaxbContext = JAXBContext.newInstance(OutcomeLot.class);
                        jaxbMarshaller = jaxbContext.createMarshaller();

                        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

                        ByteArrayOutputStream byteArrayOutputStream4XML = new ByteArrayOutputStream();
                        OutcomeLot out = outcomeModelBuilder.build();

                        String xsdValidation = "outcome-lot-pec.xsd";

                        //validazione dell'XML ottenuto via XSD validation
                        validateXMLSchema(xsdValidation, jaxbContext, out);

                        jaxbMarshaller.marshal(out, byteArrayOutputStream4XML);


                        //Preparazione ZIP
                        //  String nomeZipFileOutcome = "OUTCOME_"+lottoInElab.getLotId() + ".zip";

                        try {

                            OutcomeZipBuilder outcomeZipBuilder = new OutcomeZipBuilder(basePath + nomeZipFileOutcome, lottoInElab.getLotId());

                            //Aggiunta file XML riepilogativo
                            outcomeZipBuilder.zipNewEntryFromByteArray("OUTCOME_" + lottoInElab.getLotId() + ".xml", byteArrayOutputStream4XML.toByteArray());

                            String pathMessaggiLotto = basePath + lottoInElab.getLotId();

                            // possibili errori: la cartella del lotto non è stata creata perchè il processo di nifi corrispondente
                            // era inoperativo..oppure perche' non e' arrivata nessuna ricevuta per quel lotto
                            //verifico esistenza della folder:

                            File folderMessaggiLotto = new File(pathMessaggiLotto);
                            boolean exists = folderMessaggiLotto.exists();

                            if (exists) {
                                //aggiungo allo ZIP outcome gli .eml nella cartella indicata
                                outcomeZipBuilder.addEmlToZipByLottoId(pathMessaggiLotto);
                            }

                            outcomeZipBuilder.finalizeZip();


                            //
                            pathFinaleZipFileOutcome = new File(destinazioneZipFileOutcome);

                            if (pathFinaleZipFileOutcome.exists()) {
                                //Casistica che non dovrebbe accadere, messa per sicurezza
                                //il file è stato lavorato da un  altro controller
                                continue;
                            }


                            //spostamento in dest folder
                            Files.move(Paths.get(basePath + nomeZipFileOutcome), Paths.get(zipPath + nomeZipFileOutcome), StandardCopyOption.REPLACE_EXISTING);

                            LOGGER.info("sorgente copy: ***" + basePath + nomeZipFileOutcome + "***");
                            LOGGER.info("destinazione copy: ***" + zipPath + nomeZipFileOutcome + "***");

                            //aggiornare su CB il documento del lotto : campo "updated"  e currentStatus a LOTTO_OUTCOME_GENERATO
                            lottoInElab.setCurrentStatus(EventType.LOTTO_OUTCOME_GENERATO); //
                            lottoInElab.getEvents().add(Events.builder().event(lottoInElab.getCurrentStatus()).time(LocalDateTime.now()).build());

                            LocalDateTime updateDate = LocalDateTime.now();
                            lottoInElab.setUpdated(updateDate);

                            lotRepository.save(lottoInElab); //


                            //LOGGER.info("Valori lotto " + lottoInElab.toString());
                            LOGGER.info("Creazione ZIP OUTCOME per lotto " + lottoInElab.getLotId() + " terminata");
                            LOGGER.info("pubblico evento su rabbit per il lotto  " + lottoInElab.toString());

                            eventMessagePublisher.publishLotOutcomeEvent(lottoInElab);

                            //CANCELLARE IL RECORD SU LOTLOCK

                            delete = "DELETE FROM lotLock WHERE  lotKey = '" + lottoInElab.getLotId() + "'";

                            result = bucketLotLock.query(
                                    N1qlQuery.simple(delete)
                            );

                            LOGGER.info("CANCELLATO DOCUMENTO DA lotLock CON lotKey = '" + lottoInElab.getLotId() + "'");


                        } catch (IOException ioe) {
                            LOGGER.error("IOException in fase creazione FILE OUTCOME :" + ioe.getMessage(), ioe);
                        } catch (Exception e) {
                            LOGGER.error("Errore in fase creazione FILE OUTCOME :" + e.getMessage(), e);
                        }

                    } catch (JAXBException JAXBEx) {
                        // JAXBEx.printStackTrace();
                        LOGGER.error("Errore JAXB in fase creazione FILE OUTCOME :" + JAXBEx.getMessage(), JAXBEx);
                    }

                } else {
                    LOGGER.info("Il lotto " + lottoInElab.getLotId() + " non è ancora pronto per la creazione dello zip di OUTCOME");
                }

            }//fine for

        }catch (Exception e){
            LOGGER.error(e.getMessage(), e);
        }finally {
            if(cb!=null) {
                cb.disconnect();
            }
        }

    }

    private void impostaContatoriLotto(LotDelivery lottoInElab) {

        //TODO: in futuro, capire se su CB si puo' sincronizzare update ad un documento per far aumentare questi contatori di volta in volta all'arrivo dei messaggi senza fare i conti alla fine.

        int messagesNotValidated=0;
        int messagesNotAccepted=0;
        int messagesNotDelivered=0;
        int messagesSuccess=0;

        //estraggo tutti i messaggi del lotto
        LOGGER.info("Imposto contatori lotto con id: "+lottoInElab.getLotId() );
        List<MailDeliveryRequest> messaggiDelLotto = mailDeliveryRepository.findByLotId(lottoInElab.getLotId());

        for(MailDeliveryRequest message: messaggiDelLotto) {

            if(message.getValidationOutcome()==null && message.getAcceptanceOutcome()==null && message.getDeliveryOutcome()==null){
                //questa casistica in cui mancano tutti e 3 gli esiti è anomala, non dovrebbe accadere, ma comqune consideriamola

                messagesNotValidated++;

            }
            //VALIDAZIONE PRESENTE E DIVERSA DA 1
            else if(message.getValidationOutcome()!=null && !message.getValidationOutcome().equals("1")){
                messagesNotValidated++;
            }
            //ACCETTAZIONE PRESENTE E DIVERSA DA 1
            else if(message.getAcceptanceOutcome()!=null && !message.getAcceptanceOutcome().equals("1")){
                messagesNotAccepted++;
            }
            //ACCETTAZIONE PRESENTE , UGUALE A 1 E CONSEGNA NON PRESENTE  (CASO DI INVIO VERSO INDIRIZZO PEO)
            else if(  (  message.getAcceptanceOutcome()!=null &&  message.getAcceptanceOutcome().equals("1") ) && (  message.getDeliveryOutcome()==null ||  message.getDeliveryOutcome().equals("")   )  ){
                messagesNotDelivered++;
            }
            //DELIVERY PRESENTE E DIVERSA DA 1
            else if(message.getDeliveryOutcome()!=null && !message.getDeliveryOutcome().equals("1")){
                messagesNotDelivered++;
            }
            //DELIVERY PRESENTE E UGUALE A 1
            else if(message.getDeliveryOutcome()!=null && message.getDeliveryOutcome().equals("1")){
                messagesSuccess++;
            }

        }
        lottoInElab.setMessagesNotValidated(messagesNotValidated);
        lottoInElab.setMessagesNotAccepted(messagesNotAccepted);
        lottoInElab.setMessagesNotDelivered(messagesNotDelivered);
        lottoInElab.setMessagesSuccess(messagesSuccess);

    }


    // *** PER UN MESSAGGIO POSSO CONSIDERARE FINITA LA SUA ELABORAZIONE, E LO CHIAMO CHIUDIBILE, ***
    // METTENDO IN OR DUE CONDIZIONI:
    // 1) IL MESSAGGIO SI TROVA IN UNO DI QUESTI STATI:
    //    MESSAGGIO_NON_INVIABILE,
    //    MESSAGGIO_INVIO_FALLITO
    //    MESSAGGIO_ACCETTAZIONE_KO
    //    MESSAGGIO_CONSEGNA_OK
    //    MESSAGGIO_CONSEGNA_KO
    // OR
    // 2) SONO TRASCORSCE PIU' DI 24 ORE DAL SUO INVIO

    public boolean messaggioChiudibile(MailDeliveryRequest message){




        Events unoStatoChiudibile =message.getEvents().
                                    stream().
                                    filter( event ->   event.getEvent().equals(EventType.MESSAGGIO_NON_INVIABILE)
                                                       ||
                                                       event.getEvent().equals(EventType.MESSAGGIO_INVIO_FALLITO)
                                                        ||
                                                       event.getEvent().equals(EventType.MESSAGGIO_ACCETTAZIONE_KO)
                                                        ||
                                                       event.getEvent().equals(EventType.MESSAGGIO_CONSEGNA_OK)
                                                        ||
                                                       event.getEvent().equals(EventType.MESSAGGIO_CONSEGNA_KO)

                ).
                findAny().orElse(null);


        if(unoStatoChiudibile!=null){

            LOGGER.info("IL MESSAGGIO con ID: ***"+message.getMessageID()+"*** SI TROVA NELLO STATO CHIUDIBILE:"+   unoStatoChiudibile.getEvent() );

            //il messaggio ha uno di quei 4 stati in cui puo' essere chiuso
            return true;
        }


        //METODO OBSOLETO CHE SI BASAVA SULL'ORDINE CRONOLOGICO DEGLI EVENTI, CHE ABBIAMO VISTO CHE NON VA BENE
//        //ordino array degli eventi
//        message.getEvents().sort(Comparator.comparing(a -> a.getTime()));
//
//        //prendo l'evento che si è verificato per ultimo -> lo considero lo  stato attuale del messaggio
//        Events lastEvent = message.getEvents().get(message.getEvents().size() - 1);
//
//
//
//        boolean messaggioInUnoStatoChiudibile = (lastEvent.getEvent().equals(EventType.MESSAGGIO_NON_INVIABILE) ||
//                                                 lastEvent.getEvent().equals(EventType.MESSAGGIO_ACCETTAZIONE_KO) ||
//                                                 lastEvent.getEvent().equals(EventType.MESSAGGIO_CONSEGNA_OK)   ||
//                                                 lastEvent.getEvent().equals(EventType.MESSAGGIO_CONSEGNA_KO));
//
//        if (messaggioInUnoStatoChiudibile) return true;

        //se non è in uno stato chiudibile, verifico se siamo in un caso di messaggio inviato con successo ma troppo tempo passato in attesa di ricevuta

        //cerco fra gli eventi del messaggio se c'e' lo stato MESSAGGIO_INVIO_SUCCESSO
        //(non e' necessariamente il lastEvent, perche' potrebbe essere successivamente arrivato l'evento MESSAGGIO_ACCETTAZIONE_OK)
        Events messInviato = message.getEvents().stream()
                                                .filter(event -> EventType.MESSAGGIO_INVIO_SUCCESSO.equals(event.getEvent()))
                                                .findAny()
                                                .orElse(null);


        if (messInviato != null) {
            //l'evento di tipo MESSAGGIO_INVIO_SUCCESSO è stato trovato
            //verifico se SONO TRASCORSCE PIU' DI 25 ore


            if (LocalDateTime.now().isAfter(messInviato.getTime().plusHours(24L))) {

                LOGGER.info("IL MESSAGGIO con ID: ***"+message.getMessageID()+"*** SI TROVA NELLO STATO :"+   messInviato.getEvent() +" DA PIU' DI 24 ORE , QUINDI LO CONSIDERO CHIUDIBILE" );

                return true;

            }
        }


        return false;
    }
}
