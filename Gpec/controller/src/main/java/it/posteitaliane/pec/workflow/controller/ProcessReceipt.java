package it.posteitaliane.pec.workflow.controller;

import brave.Span;
import brave.Tracer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.model.SendingDetails;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import it.posteitaliane.pec.workflow.service.ConvertionMailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.sleuth.annotation.ContinueSpan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import javax.mail.MessagingException;
import java.io.*;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 *
 */
@RestController
@CrossOrigin(origins="*", maxAge = 3600)
public class ProcessReceipt {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessReceipt.class);

    @Autowired
    Gson gson;
    @Autowired
    MailDeliveryRepository mailDeliveryRepository;
    @Autowired
    ConvertionMailService convertionMailService;

    @Autowired
    Tracer tracer;

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");

    /**
     * Metodo invocato da NIFI - EvaluateReceipt
     *
     * Ricevera' come informazioni:
     *
     * message id
     * tipo ricevuta
     * nome del file.eml
     *
     *
     * Usa tali informazioni per aggiornare su CouchBase il documento MailDeliveryRequest
     *
     *
     * l'input è un HTTP entity con un raw JSON in quanto il contenuto inviato da NiFI è privo di schema.
     * Si è scelto quindi di non creare un Bean ad uso di un serializzatore, ma di accedere ai dati in una struttura key-value
     *
     *
     * @param httpEntity un Json corrispondente al set di attributi del flow-file corrispondente allo ZIP del lotto
     * @return un ack-input
     *
     */
    @RequestMapping(value = "/processReceipt", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_XML_VALUE )
    @ResponseBody
    public void processReceipt(HttpEntity<String> httpEntity){

        LOGGER.trace("called... ");
        Span span = tracer.nextSpan().name("response.service.receipt.process").kind(Span.Kind.SERVER);
        span.tag("EVENT", "NEW.RECEIPT");
        span.start();

        try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {
            HttpHeaders headers = httpEntity.getHeaders();
            headers.forEach((s, strings) -> {
                if(!s.contains("x-b3")) {
                    span.tag(s, strings.get(0));
                }
            });
            // rispetto al service 'checkIncomingLotFile' il json è salvato da NiFi dentro un header con chiave 'jsonattributes'
            // TODO vedere se migliorare questo passaggio lato NiFi
            String json = headers.get("jsonattributes").get(0);
            LOGGER.info("PUT '/processReceipt': " + json);

            // conversione dell'header in una MAP
            Map<String, Object> checkData = this.getAsMap(json, null);
            LOGGER.info("JSON: " + checkData);


            // si estraggono le variabili

            String messageId = (String)checkData.get("email.headers.x-riferimento-message-id");
            String receiptOutcome = (String) checkData.get("email.headers.x-ricevuta");
            String filename = (String)checkData.get("email.attachment.parent.filename");


            String filenameWithoutExtension = filename.substring(0,filename.lastIndexOf('.'));
            //stringa usata solo per il nome da mettere su CB, la rename vera e propria del file e' fatta altrove
            String filenameEml = filenameWithoutExtension+".eml";


            // String dataDiInvio = (String)checkData.get("email.headers.sent_date");//non so se la useremo, in caso va parsata , da nifi arriva cosi: ""   (sul .msg originale  e' cosi "Tue, 12 Feb 2019 13:17:38 +0100")

            //rimuoviamo dall'header il primo e l'ultimo carattere < e > se presente...
            if(messageId.indexOf("<") != -1 || messageId.indexOf(">") != -1 )  messageId =  messageId.substring(1, messageId.length()-1);

            Optional<MailDeliveryRequest> mailDelivery = mailDeliveryRepository.findById(messageId);


            //Se l'optional contiene la chiave, aggiorna il messaggio e salvalo

            if(receiptOutcome != null && filename != null && mailDelivery.isPresent()) {
                try {
                    mailDelivery.ifPresent(receiptInfo -> {
                        if (receiptOutcome.equalsIgnoreCase("accettazione")) {
                            receiptInfo.setAcceptanceOutcome("1");
                            receiptInfo.setAcceptanceFileName(filenameEml);
                            receiptInfo.getEvents().add(Events.builder().event(EventType.MESSAGGIO_ACCETTAZIONE_OK).time( LocalDateTime.now()).build());

                        } else if (receiptOutcome.equalsIgnoreCase(" non-accettazione")) {

                            //TODO: FARSI RITORNARE ANCHE IL SUBJECT PERCHE E' UN MODO PER VEDERE SE SI TRATTA DI NON ACCETTAZIONE PER VIRUS INFORMATICO
                            //INFATTI IN TAL CASO IL SUBJETC SARA': Subject: AVVISO DI NON ACCETTAZIONE PER VIRUS: [subject originale]
                            //IN QUEL CASO ANDRA' IMPOSTATO:
                            //receiptInfo.setAcceptanceOutcome("A002"); //"NON ACCETTATO PER VIRUS INFORMATICO"
                            // ALTRIMENTI:
                            receiptInfo.setAcceptanceOutcome("A001"); //"NON ACCETTATO PER ERRORI FORMALI"

                            receiptInfo.setAcceptanceFileName(filenameEml);
                            receiptInfo.getEvents().add( Events.builder().event(EventType.MESSAGGIO_ACCETTAZIONE_KO).time(LocalDateTime.now()).build() );

                        } else if (receiptOutcome.equalsIgnoreCase("avvenuta-consegna")) {
                            receiptInfo.setDeliveryOutcome("1");
                            receiptInfo.setDeliveryFileName(filenameEml);
                            receiptInfo.getEvents().add( Events.builder().event(EventType.MESSAGGIO_CONSEGNA_OK).time(LocalDateTime.now()).build() );

                        } else if (receiptOutcome.equalsIgnoreCase("errore-consegna")) {
                            receiptInfo.setDeliveryOutcome(decodeDeliveryKOReturnCode( (String) checkData.get("errore-esteso")) );
                            receiptInfo.setDeliveryFileName(filenameEml);
                            receiptInfo.getEvents().add( Events.builder().
                                    event(EventType.MESSAGGIO_CONSEGNA_KO).
                                    time(LocalDateTime.now())
                                    .build()
                            );
                        }

                        //TODO: ANDRA' FATTO REWORK IN CUI CAMBIEREMO MODALITA' DI SALVATAGGIO SU CB PER FARE IN MODO CHE NON CI SIANO SOVRASCRITTURE
                        // USANDO MAGARE LE API A BASSO LIVELLO SE NON SI PUO' FARE CON SPRING DATA
                        mailDeliveryRepository.save(receiptInfo);
                    });
                } catch (Exception e) {
                    LOGGER.error("Error saving state in couchbase: " + e.getMessage(), e);
                }
            }


            /**
             * comunque convertiamo la ricevuta anche se non abbiamo aggiornato couchbase?
             */
            try {
                convertionMailService.convertMail(filename);
            } catch (IOException e) {
                LOGGER.error("Converter EML failure: " + e.getMessage(), e);
            } catch (MessagingException e) {
                LOGGER.error("Converter EML failure: " + e.getMessage(), e);
            } catch (Exception e){
                LOGGER.error("Converter EML failure: " + e.getMessage(), e);
            }
        } finally {
            span.finish();
        }

    }






    /**
     *
     * @param payload
     * @param rootValue
     * @return
     */
    protected Map<String, Object> getAsMap(String payload, String rootValue) {
        Type type = new TypeToken<Map<String, Object>>() {}.getType();
        return gson.fromJson(payload, type);
    }


    /**
     * @param errorCode A string containing error string
     * @return A string containing only the error code (without dots)
     */
    public String decodeDeliveryKOReturnCode(String errorCode ) {
        Pattern pattern = Pattern.compile("(\\d{1}[\\.]\\d{1}[\\.]\\d{1})");
        Matcher matcher = pattern.matcher(errorCode);
        String errorMatched = null;
        if (matcher.find()) {
            errorMatched = matcher.group(1);
            errorMatched = errorMatched.replace(".", "");
            errorMatched = "D"+errorMatched;
        }
        return errorMatched;
    }

}
