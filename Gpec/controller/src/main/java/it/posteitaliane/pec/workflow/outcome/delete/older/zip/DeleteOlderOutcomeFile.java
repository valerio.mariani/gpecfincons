package it.posteitaliane.pec.workflow.outcome.delete.older.zip;

import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.workflow.service.ResponseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;


@Component
public class DeleteOlderOutcomeFile {

    @Autowired
    LotRepository lotRepository;


    private static final Logger LOGGER = LoggerFactory.getLogger(ResponseService.class);


    //cancello il FILE OUTCOME e metto lo stato ad LOTTO_ELIMINATO
    //TODO: PROBABILMENTE QUI METTEREMO ANCHE LA CANCELLAZIONE DEL FILE LOTTO DI INPUT
    public void outcomeFindAndDeleteOlderZipService(LotDelivery lotto, String basePath) throws IOException {


            LOGGER.info("Il lotto "+lotto.getLotId()+" in stato LOTTO_ELABORATO ha superato la data limite, per cui il suo zip di OUTCOME verrà eliminato");

            //rimozione file OUTCOME
            String outcomeFileName = "OUTCOME_" + lotto.getLotId() + ".zip";
            Path fileOutcome = Paths.get(basePath + outcomeFileName);

            if (!Files.exists(fileOutcome)) {
                throw new FileNotFoundException("il file " + fileOutcome.toAbsolutePath().toString() + " non esiste nella cartella sorgente");
            }


            Files.delete(fileOutcome);
            LOGGER.info("File " + fileOutcome.toString() + " CANCELLATO correttamente");

            //Update campo Updated e stato lotto a eliminato
            LocalDateTime now = LocalDateTime.now();
            lotto.setUpdated(now);
            lotto.setCurrentStatus(EventType.LOTTO_ELIMINATO);

            lotRepository.save(lotto);
            LOGGER.info("Stato e data aggiornamento per lotto: " + lotto.getLotId() + " eseguito");

        }


}
