package it.posteitaliane.pec.workflow.config;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;
import com.netflix.appinfo.InstanceInfo;
import it.posteitaliane.pec.common.configuration.DefaultHazelcastConfiguration;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


/**
 * Hazelcast è una distributed data grid che può svolgere le funzioni di una cache distribuita
 * tra i diversi nodi di un cluster o istanze di un servizio in bilanciamento
 */
@Configuration
@Profile("local")
public class HazelcastConfiguration extends DefaultHazelcastConfiguration {

    @Bean
    public HazelcastInstance node() {

        HazelcastInstance hzInstance = Hazelcast.newHazelcastInstance();
        return hzInstance;
    }

    @Bean
    public Config config(InstanceInfo currentInstanceInfo){
         return hazelCastConfig(currentInstanceInfo);
    }


    @Bean
    public CacheManager cacheManager(HazelcastInstance hazelcastInstance) {
        // The Stormpath SDK knows to use the Spring CacheManager automatically
        return new HazelcastCacheManager(hazelcastInstance);
    }



}
