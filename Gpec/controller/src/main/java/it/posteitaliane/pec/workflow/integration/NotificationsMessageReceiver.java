package it.posteitaliane.pec.workflow.integration;

import brave.Tracer;
import com.rabbitmq.client.Channel;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class NotificationsMessageReceiver implements ChannelAwareMessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationsMessageReceiver.class);

    @Autowired Tracer tracer;
    @Autowired LotRepository lotRepository;
    @Autowired MailDeliveryRepository mailDeliveryRepository;

    private String spanName = "controller.notifications.";


    public Error receiveNotification(Message rabbitMessagePayload){

        return null;
    }

    @Autowired MessageConverter converter;

    @Override
    public void onMessage(Message rabbitMessagePayload,  Channel channel) throws Exception {
        LOGGER.info("NOTIFICATION: " + rabbitMessagePayload.getMessageProperties() );
        String domainType = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("NOTIFICATION-TYPE");
        String operation = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("OPERATION");
        String documentId = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("DOCUMENT-ID");
        switch (domainType){
            case "LotDelivery":

                LotDelivery lotDelivery = (LotDelivery) converter.fromMessage(rabbitMessagePayload);
                switch (operation){
                    case "save":
                        lotRepository.save(lotDelivery);
                        break;
                    case "update":
                        Optional<LotDelivery> persistedLot = lotRepository.findById(documentId);


                        break;
                }

                break;
            case "MailDeliveryRequest":
                MailDeliveryRequest mailDeliveryRequest = (MailDeliveryRequest) converter.fromMessage(rabbitMessagePayload);
                break;
            default:
                break;
        }

        channel.basicAck(rabbitMessagePayload.getMessageProperties().getDeliveryTag(), false);
    }

}
