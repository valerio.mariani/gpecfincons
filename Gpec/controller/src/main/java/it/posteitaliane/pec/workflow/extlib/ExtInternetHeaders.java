package it.posteitaliane.pec.workflow.extlib;

import javax.mail.internet.InternetHeaders;
import java.util.ArrayList;

public class ExtInternetHeaders extends InternetHeaders {

    public ExtInternetHeaders() {
        this.headers = new ArrayList();
    }

    @Override
    public void addHeader(String name, String value) {
        this.headers.add(new InternetHeaders.InternetHeader(name, value));
    }
}
