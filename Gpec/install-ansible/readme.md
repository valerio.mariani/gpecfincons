#Gestore Pec (Gpec) Standard environment stack - Ansible PLAYBOOK

Playbook files:
 - rabbitmq.yml
 - registry.yml
 - gateway.yml
 - composer.yml
 - sender.yml
 - controller.yml
 - tracer.yml
#### install and configure environment for Gpec based on provided host inventory.

ambienti supportati: 
 - Debian (ubuntu etc)
 - RedHAT (RHEL - Cento6 - Centos7 - Fedora)

**Roles** disponibili (l'ordine di dichiarazione coincide con l'ordine di esecuzione):

  1. RabbitMQ (update dei package manager, installazione di repo, etc)
  2. Java ()
  3. Services (idem)
  4. Nifi (non si occupa dell'installazione del componente, ma solo del load dei processi e configurazione )

**prerequisiti:**

se gli script sono eseguiti da un utente che accede in SSH sugli host senza password allora è necessario commentare 
dal file _group_vars/all_ la variabile 'ansible_ssh_pass'. In tutti gli altri casi la password deve essere valorizzata
per ogni singolo host dichiarato in _inventory/host_vars_ valorizzando lì dentro il placeholder 'vault_ansible_password' con i valore ottenuto dal 
_vault_ della password dell'utenza usata per connettersi via SSH:

    ansible-vault encrypt_string --name 'vault_ansible_ssh_pass' '<<true-password>>'
____

run command per un singolo playbook:

    sudo ansible-playbook rabbitmq.yml -i inventory/hosts  <optional: --tags: java> --check --diff -vvv
____

_l'attributo '--check' significa che le modifiche sono eseguite solamente in un **'virtualenv'** e non sono mantenute realmente sull'host, è una modalità ideale per testare la sintassi ed eventuali problemi prima di modificare realmente le macchine._

 > _**IMPORTANTE**: la modalità check non è 'magica', alcune modifiche ai server possono fallire successivamente in quanto i passi eseguiti in questa modalità non sono 'reali' (esempio: un task prevede la creazione di una variabile d'ambiente che servirà ad un task successivo. >> in modalità CHECK la variabile non è creata realmente, quindi il task successivo non la trova valorizzata sull'host in cui è eseguito e produce un errore. Ove possibile alcuni task hanno il check-mode disabilitato._ 

_l'attributo '--diff' mostra le modifiche tra 'prima' e 'dopo' l'esecuzione del task, evidenziate con colori diversi nella shell, soprattutto se le modifiche coinvolgono dei file di configurazione pre-esistenti nell'host configurato._

Tags disponibili (all'incirca una per ogni _role_):

 - queue-services
 - java


Il comando eseguito senza l'opzione **tags** avvia tutti i _role_ del playbook. nell'ordine in cui sono dichiarati nel file.

