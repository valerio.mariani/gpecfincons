package it.posteitaliane.pec.mail.producer;


import brave.Tracer;
import brave.Tracing;
import brave.context.log4j2.ThreadContextScopeDecorator;
import brave.http.HttpTracing;
import brave.propagation.B3Propagation;
import brave.propagation.CurrentTraceContext;
import brave.propagation.ExtraFieldPropagation;
import brave.propagation.ThreadLocalCurrentTraceContext;
import brave.sampler.Sampler;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import it.posteitaliane.pec.common.integration.DataOperationsMessagePublisher;
import it.posteitaliane.pec.common.integration.EventMessagePublisher;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import zipkin2.Span;
import zipkin2.reporter.AsyncReporter;
import zipkin2.reporter.Reporter;
import zipkin2.reporter.Sender;
import zipkin2.reporter.amqp.RabbitMQSender;

import java.io.IOException;

@SpringBootApplication
@EnableCaching
@EnableEurekaClient
@EnableSwagger2
public class MailProducerApplication {

    @Autowired
    private EurekaClient eurekaClient;

    public static void main(String[] args) {
        SpringApplication.run(MailProducerApplication.class, args);
    }

    @Bean
    public InstanceInfo getCurrentServiceInstanceEurekaID(){
        return eurekaClient.getApplicationInfoManager().getInfo();
    }


    @Bean
    CurrentTraceContext log4jTraceContext() {
        return ThreadLocalCurrentTraceContext.newBuilder()
                .addScopeDecorator(ThreadContextScopeDecorator.create())
                .build();
    }

    @Bean
    HttpTracing httpTracing(Tracing tracing) {
        return HttpTracing.create(tracing);
    }

    @Bean
    Reporter<Span> spanReporter(Sender sender) {
        return AsyncReporter.create(sender);
    }

    @Bean
    Sender sender(@Value("${spring.rabbitmq.host}") String rabbitmqHostUrl,
                  @Value("${rabbit.custom.queue.event.actions}") String zipkinQueue,
                  @Value("${spring.rabbitmq.username}") String user,
                  @Value("${spring.rabbitmq.password}") String psw) throws IOException {
        return RabbitMQSender.newBuilder()
                .queue(zipkinQueue)
                .username(user)
                .password(psw)
                .addresses(rabbitmqHostUrl).build();
    }

    @Bean
    Tracing tracing(
            @Value("${spring.application.name:spring-tracing}") String serviceName,
            Reporter<Span> spanReporter,
            CurrentTraceContext log4jTraceContext) {
        return Tracing
                .newBuilder()
                .sampler(Sampler.ALWAYS_SAMPLE)
                .localServiceName(StringUtils.capitalize(serviceName))
                .propagationFactory(ExtraFieldPropagation
                        .newFactory(B3Propagation.FACTORY, "client-id"))
                .currentTraceContext(log4jTraceContext)
                .spanReporter(spanReporter)
                .build();
    }

//	@Bean
//	CorsConfigurationSource corsConfigurationSource() {
//		CorsConfiguration configuration = new CorsConfiguration();
//		configuration.setAllowedOrigins(Arrays.asList("*"));
//		configuration.setAllowCredentials(true);
//		configuration.setAllowedHeaders(Arrays.asList("Access-Control-Allow-Headers","Access-Control-Allow-Origin","Access-Control-Request-Method", "Access-Control-Request-Headers","Origin","Cache-Control", "Content-Type", "Authorization"));
//		configuration.setAllowedMethods(Arrays.asList("DELETE", "GET", "POST", "PATCH", "PUT"));
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//		source.registerCorsConfiguration("/**", configuration);
//		return source;
//	}

//	@Bean
//	public Docket api() throws IOException, XmlPullParserException {
//		MavenXpp3Reader reader = new MavenXpp3Reader();
//		Model model = reader.read(new FileReader("pom.xml"));
//		return new Docket(DocumentationType.SWAGGER_2)
//				.select()
//				.apis(RequestHandlerSelectors.basePackage("it.posteitaliane.pec.mail.producer"))
//				.paths(PathSelectors.any())
//				.build().apiInfo(
//						new ApiInfo(
//								"Message Composer Service Api Documentation",
//								"Documentation automatically generated",
//								model.getParent().getVersion(),
//								null,
//								new Contact("Fincons Group", "piotrminkowski.wordpress.com", "sviluppo@finconsgroup.com"),
//								null,
//								null,
//								null) );
//	}

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("it.posteitaliane.pec.mail.producer")) // RequestHandlerSelectors.any()
                .paths(PathSelectors.any())
                .build();
    }

    @Value("${spring.application.name:MailProducer}")
    String applicationName;

    @Bean
//    @RefreshScope
    public EventMessagePublisher eventsPublisher(
            @Value("${rabbit.custom.routing-key.event.topic.workflow}")
            String managementTopicRoutingKey,
            Tracer tracer,
            RabbitTemplate rabbitTemplate,
            TopicExchange globalManagementExchange ){
        return new EventMessagePublisher(
                applicationName.toUpperCase(),
                managementTopicRoutingKey,
                tracer,
                rabbitTemplate, globalManagementExchange);
    }

    @Bean
    public DataOperationsMessagePublisher dataOperationsPublisher(
            Tracer tracer,
            RabbitTemplate rabbitTemplate,
            TopicExchange globalNotificationsExchange){
        return new DataOperationsMessagePublisher(
                applicationName.toUpperCase(),
                tracer,
                rabbitTemplate, globalNotificationsExchange);
    }

}
