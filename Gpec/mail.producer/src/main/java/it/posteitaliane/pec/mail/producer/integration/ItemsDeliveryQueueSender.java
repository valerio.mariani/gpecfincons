package it.posteitaliane.pec.mail.producer.integration;

import brave.Span;
import brave.Tracer;
import brave.Tracing;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.QueueInfo;
import it.posteitaliane.pec.common.integration.message.MessageSender;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import static java.text.MessageFormat.format;

@Component
@RefreshScope
public class ItemsDeliveryQueueSender implements MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemsDeliveryQueueSender.class);

    @Autowired private RabbitTemplate rabbitTemplate;
    @Autowired private DirectExchange lotItemDeliveryExchange;
//    @Autowired Queue lotItemDeliveryQueue;
    @Autowired private RabbitAdmin rabbitAdmin;

    @Value("${spring.rabbitmq.virtual-host:/}")
    private String virtualHost;

    @Autowired Client rabbitClient;
    @Autowired Tracing tracing;
    @Autowired Tracer tracer;

    /**
     * @param correlationId
     * @param msg
     * @param routingKey
     * @param messagePostProcessor
     */
    @Override
    public void produceMsg(String correlationId, MailDeliveryRequest msg, String routingKey, MessagePostProcessor messagePostProcessor) {
        Span messageInQueue = tracer.nextSpan().name("publish.mail.message").kind(Span.Kind.PRODUCER);
        messageInQueue.tag("Lot-ID", msg.getLotId());
        messageInQueue.tag("Message-ID", msg.getMessageID());
        messageInQueue.tag("extId", msg.getExtID());
        messageInQueue.start();

        Binding queueBinding = this.bindToKey(msg.getLotId(), new Queue(msg.getLotId(), true, false, false));

        try (Tracer.SpanInScope ws = tracer.withSpanInScope(messageInQueue)) {

            CorrelationData cd = new CorrelationData(correlationId);
            rabbitTemplate.convertAndSend(lotItemDeliveryExchange.getName(), routingKey, msg,
                    messagePostProcessor == null ? message -> message : messagePostProcessor,
                    cd);
            LOGGER.info("PUBLISH MESSAGE to EXCHANGE[" + lotItemDeliveryExchange.getName() + "] :: Sender msg = " + msg + " routingKey[" + routingKey + "]");

        } finally {
            // start the client side and flush instead of finish
            messageInQueue.flush();
        }

    }

//METODO TOLTO DA QUESTA CLASSE A SEGUITO DEL REWORK DELLO SPOSTAMENTO DI IndexController NEL PROGETTO CONTROLLER

    public Binding bindToKey(String lotIdAsRoutingKey, Queue lotQueue) {

        QueueInfo q = rabbitClient.getQueue(virtualHost, lotQueue.getName());
        if(q != null ){

            LOGGER.info(format("richiesta di dichiarare coda ''{0}'' già esistente sul nodo ''{1}''. ignore.", q.getName()), q.getNode() );
            return null;

        }else {

            rabbitAdmin.declareQueue(lotQueue);
            LOGGER.info(format("Creata coda ''{0}'' per pubblicare messaggi con routingKey[{1}]", lotQueue.getActualName(), lotIdAsRoutingKey));

            Binding binding = BindingBuilder.bind(lotQueue).to(lotItemDeliveryExchange).with(lotIdAsRoutingKey);
            rabbitAdmin.declareBinding(binding); // re-declare binding if mask changed
            LOGGER.info(format("Creato binding su ''{0}'' per pubblicare messaggi con routingKey[{1}]", lotItemDeliveryExchange.getName(), lotIdAsRoutingKey));
            return binding;
        }
    }


    @Override
    public void deleteBind(Binding binding){
        rabbitAdmin.removeBinding(binding);
    }

}
