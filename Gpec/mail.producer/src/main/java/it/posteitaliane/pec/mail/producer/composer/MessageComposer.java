package it.posteitaliane.pec.mail.producer.composer;


import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.model.xml.Message;

/**
 *
 */
public interface MessageComposer {

    public MailDeliveryRequest compose(String lotId, Message xmlFragment) throws Exception;

}
