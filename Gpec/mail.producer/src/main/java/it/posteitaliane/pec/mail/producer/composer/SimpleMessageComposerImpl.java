package it.posteitaliane.pec.mail.producer.composer;

import brave.CurrentSpanCustomizer;
import it.posteitaliane.pec.common.custom.exception.TemplateNotFoundException;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.model.xml.Message;
import it.posteitaliane.pec.common.model.xml.MessageTemplate;
import it.posteitaliane.pec.common.model.xml.Parameter;
import it.posteitaliane.pec.common.model.xml.PlaceHolderType;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.sleuth.annotation.ContinueSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 *
 */
@Component
public class SimpleMessageComposerImpl implements MessageComposer {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleMessageComposerImpl.class);
    private static final Random r = new Random();

    @Value("${message-templates.folder}")
    private String messageTemplateFolder;

    // The user code can then inject this without a chance of it being null.
    @Autowired CurrentSpanCustomizer span;

    @Autowired MailDeliveryRepository mailDeliveryRepository;

    @Cacheable("templateXmlMessage")
    public MessageTemplate getTemplate(@SpanTag("template") String template) throws Exception {

        LOGGER.debug("[Message-Composer] Requesting template '" + messageTemplateFolder + " " + template + "'");

     /* accedi alla folder su file system dove sono i template
        recuperi il template giusto col templateId che corrisponde alle ultime 4 cifre del nome
        lo converti in java....
        effetti la sostituzione dei placeholder con i valori contenuti in xmlFragment per produrre il content e il subject  di MailDeliveryRequest */

        File directory = new File(messageTemplateFolder);

        //In previsione di xml multipli [possibile modifica]
        File[] listaTemplates = directory.listFiles((dir, name) -> name.endsWith(template.trim()+".xml"));
        if (listaTemplates == null || listaTemplates.length == 0) {
            LOGGER.error("Nessun template trovato per " + template);
            throw new TemplateNotFoundException(template);
        }

        File messageTemplate = new File(listaTemplates[0].getAbsolutePath());

        JAXBContext jc = JAXBContext.newInstance(MessageTemplate.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        MessageTemplate message = (MessageTemplate) unmarshaller.unmarshal(messageTemplate);

        return message;
    }



    /**
     * Dato un Message, questo metodo compone l'oggetto MailDeliveryRequest
     * che contiene tutte le informazioni del messaggio PEC da inviare
     * In particolare, questo metodo effettua la sostituzione dei placeholder contenuti nel template
     * con i relativi parametri contenuti nel Message
     *
     * Dopo di che salva il messaggio su CouchBase e lo restituisce in output
     * (non invia il messaggio alla coda, lo farà il chiamante di questo metodo, ossia asyncConvertAndCompose)
     *
     * Nota: il messaggio viene salvato su CB anche se ci sono errori di validazione
     *
     *
     * @param xmlFragment
     * @return
     * @throws Exception
     */
    @Override
    @ContinueSpan(log = "Begin Message Composition")
    public MailDeliveryRequest compose(String lotId, Message xmlFragment) throws Exception {

        //TODO: DECIDERE SE VA BENE QUESTO METODO DI GENERAZIONE DI ID, OPPURE fare:lotId + "_" + xmlFragment.getExtId()
        int index = r.ints(1, 99999999).limit(1).findFirst().getAsInt();
        String ID = lotId + "_" + StringUtils.leftPad("" + index, 8, "0");

        MessageTemplate messageTemplate;
        String templateRef = xmlFragment.getTemplateRef().getId();
        try {
            messageTemplate = getTemplate(templateRef);
        } catch(TemplateNotFoundException e) {
            span.annotate("Error getting template" + templateRef);

            LOGGER.error("V001 Errore al recupero del template");

            MailDeliveryRequest message= MailDeliveryRequest.builder().
                    to(xmlFragment.getTo()).
                    cc(xmlFragment.getCc()).
                    messageID(ID).
                    extID(xmlFragment.getExtId()).
                    lotId(lotId).
                    events( Arrays.asList(Events.builder().event(EventType.MESSAGGIO_NON_INVIABILE).time(LocalDateTime.now()).build()) ).
                    attachments(xmlFragment.getAttachmentList().getAttachment().stream().collect(Collectors.toList())).
                    validationOutcome("V001"). // "IL MESSAGGIO INDICA UN TEMPLATE NON PRESENTE A SISTEMA"
                    build();

            //SALVIAMO IL MESSAGGIO SU CB ( PRIMO SALVATAGGIO ) CON L'ERRORE DI VALIDAZIONE
            mailDeliveryRepository.save(message);

            // impediamo al messaggio di venire accodato nuovamente
            throw new AmqpRejectAndDontRequeueException("EMAIL impossibie da costruire: Item scartato per template inesistente.");
        }

        String body = messageTemplate.getBody();
        String subject = messageTemplate.getSubject();

        //estraggo i placeholder dall'xml del template
        List<PlaceHolderType> listaPlaceHolders=messageTemplate.getPlaceHolders().getPlaceHolder();

        //estraggo i Parametri dall'xml del messaggio
        List<Parameter> listParam = xmlFragment.getTemplateRef().getParameters().getParameter();

        //TODO implementare il controllo per ogni singolo parametro, se assente lanciare errore
        if(listaPlaceHolders.size() != listParam.size()) {
            span.annotate("Error related to template placeholders: size placeholders different from size parameters" );
            LOGGER.error("V002 NON SONO PRESENTI TUTTI I PARAMETRI NECESSARI PER LA COMPOSIZIONE DEL MESSAGGIO COL TEMPLATE INDICATO");
            MailDeliveryRequest message= MailDeliveryRequest.builder().
                    to(xmlFragment.getTo()).
                    cc(xmlFragment.getCc()).
                    messageID(ID).
                    extID(xmlFragment.getExtId()).
                    lotId(lotId).
                    events( Arrays.asList(Events.builder().event( EventType.MESSAGGIO_NON_INVIABILE ).time( LocalDateTime.now() ).build()) ).
                    attachments(xmlFragment.getAttachmentList().getAttachment().stream().collect(Collectors.toList())).
                    validationOutcome("V002"). // "NON SONO PRESENTI TUTTI I PARAMETRI NECESSARI PER LA COMPOSIZIONE DEL MESSAGGIO COL TEMPLATE INDICATO"
                    build();

            //SALVIAMO IL MESSAGGIO SU CB ( PRIMO SALVATAGGIO ) CON L'ERRORE DI VALIDAZIONE
            mailDeliveryRepository.save(message);

            LOGGER.info("SALVATAGGIO SU CB DEL MESSAGGE NON INVIABILE CON ID: "+ID);

            // impediamo al messaggio di venire accodato nuovamente
            throw new AmqpRejectAndDontRequeueException("EMAIL impossibile da costruire: Item scartato per parametri insufficienti al template .");
        }

        for (Parameter item : listParam) {

            body = body.replace("#{" + item.getName().trim() + "}#", item.getVal());
            subject = subject.replace("#{" + item.getName().trim() + "}#", item.getVal());

        }

        MailDeliveryRequest message= MailDeliveryRequest.builder().
                to(xmlFragment.getTo()).
                cc(xmlFragment.getCc()).
                messageID(ID).
                extID(xmlFragment.getExtId()).
                subject(subject).
                lotId(lotId).
                content(body).
                events( Arrays.asList(Events.builder().event(EventType.MESSAGGIO_INVIABILE).time(LocalDateTime.now()).build()) ).
                attachments(xmlFragment.getAttachmentList().getAttachment().stream().collect(Collectors.toList())).
                build();

        //SALVIAMO IL MESSAGGIO SU CB ( PRIMO SALVATAGGIO )
        mailDeliveryRepository.save(message);

        LOGGER.info("SALVATAGGIO SU CB DEL MESSAGGE CON ID: "+ID);

        //non invio il messaggio alla coda( lo farà il chiamante di questo metodo, ossia asyncConvertAndCompose)

        return message;
    }

}