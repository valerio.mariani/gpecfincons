package it.posteitaliane.pec.mail.producer.config;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.netflix.appinfo.InstanceInfo;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.OverviewResponse;
import it.posteitaliane.pec.common.configuration.RabbitMQConfiguration;
import it.posteitaliane.pec.mail.producer.integration.EventsMessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.DirectMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Configuration
@RefreshScope
public class RabbitMQConfig implements RabbitListenerConfigurer, RabbitMQConfiguration {

    public static final String rabbitListenerContainerFactory =  "simpleRabbitListenerContainerFactory";
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQConfig.class);

    @Value("${spring.rabbitmq.host}")
    private String host;
    @Value("${spring.rabbitmq.username}")
    private String user;
    @Value("${spring.rabbitmq.password}")
    private String psw;


    @Value("${rabbit.custom.min.consumer}")
    private Integer minConsumer;
    @Value("${rabbit.custom.max.consumer}")
    private Integer maxConsumer;
    @Value("${rabbit.custom.active.trigger}")
    private Integer activeTrigger;
    @Value("${rabbit.custom.start.min.interval}")
    private Long startMinInterval;
    @Value("${rabbit.custom.stop.min.consumer}")
    private Long stopMinInterval;
    @Value("${rabbit.custom.consecutive.idle}")
    private Integer consecutiveIdle;
    @Value("${rabbit.custom.heartbeat.interval}")
    private Integer heartbeatPeriod;

    @Value("${spring.rabbitmq.admin.username:#{null}}")
    private String adminUser;

    @Value("${spring.rabbitmq.admin.password:#{null}}")
    private String adminPassword;

    @Value("${spring.rabbitmq.ssl.enabled:false}")
    private Boolean sslEnabled;
    @Value("${spring.rabbitmq.virtual-host:/}")
    private String virtualHost;


    @Bean
    @Primary
    public CachingConnectionFactory factory() { return cachingConnectionFactory(sslEnabled, host, virtualHost, user, psw, heartbeatPeriod); }


    @Bean
    public ConnectionFactory declareConnectionFactory(){
            String userName = (adminUser != null ? adminUser : user);
            String userPsw = (adminPassword != null ? adminPassword : psw);
            return adminConnectionFactory(host, userName, userPsw);
    }

    @Bean(name = competingConsumersContainerFactory)
    @Primary
    public SimpleRabbitListenerContainerFactory competingConsumersContainerFactory(CachingConnectionFactory factory){

        return competingConsumersContainerFactory(this.minConsumer, this.maxConsumer,
                factory, converter(),
                activeTrigger, consecutiveIdle, startMinInterval, stopMinInterval);

    }

    @Bean
    public SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory(){
        return simpleRabbitListenerContainerFactory( factory(), converter());
    }

    /**
     *
     * @return
     */
    @Bean
    public Client rabbitAdminClient(){

        String userName = (adminUser != null ? adminUser : user);
        String userPsw = (adminPassword != null ? adminPassword : psw);

        Client c = null;
        try {
            c = createRabbitAPIClient(userName, userPsw, host);
        } catch (IllegalStateException e) {
            LOGGER.error("Errore nell'accesso all'API rest di RabbitMQ (hosts:" + host + ")", e);
        }

        return c;
    }


    @Bean
    public MessageConverter converter() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(localDateTimeModule());
        Jackson2JsonMessageConverter jackson2Json = new Jackson2JsonMessageConverter(mapper);
        return jackson2Json;
    }

    public Module localDateTimeModule(){
        JavaTimeModule module = new JavaTimeModule();
        LocalDateTimeDeserializer localDateTimeDeserializer =  new
                LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        module.addDeserializer(LocalDateTime.class, localDateTimeDeserializer);
        return module;
    }



    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
    }

    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        return messageHandlerMethodFactory(consumerJackson2MessageConverter());
    }

    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() {
        return defaultJackson2MessageConverter();
    }

    /**
     * Required for executing administration functions against an AMQP Broker
     */
    @Bean
    @Primary
    public RabbitAdmin rabbitAdmin() {
        return new RabbitAdmin(declareConnectionFactory());
    }


    @Bean
    @Primary
    public RabbitTemplate configuredRabbitTemplate( MessageConverter converter ) {
        return configuredRabbitTemplate(factory(), converter);
    }

    /**
     * MANAGEMENT -- coda di ascolto + binding, e exhange per poter pubblicare
     */
    @Bean
    public TopicExchange globalManagementExchange(@Value("${rabbit.custom.exchange.event.management}") final String exchangeName){
        return new TopicExchange(exchangeName);
    }

    @Bean
    public TopicExchange globalNotificationsExchange(@Value("${rabbit.custom.exchange.event.notifications}") final String notificationsExchange){
        return new TopicExchange(notificationsExchange);
    }


    @Autowired
    InstanceInfo currentInstanceInfo;


    // coda di ascolto del service Message Composer
    @Bean
    public Queue workflowEvents(@Value("${rabbit.custom.queue.event.management}") final String queueName){
        return new Queue(queueName + "_" +
                currentInstanceInfo.getAppName() + "[" +
                currentInstanceInfo.getId().split(":")[0] + //.split(".")[0] +
                "]", true);
    }

    @Bean
    public Binding eventManagementTopicBinding(
            final Queue workflowEvents,
            final TopicExchange globalManagementExchange,
            @Value("${rabbit.custom.routing-key.event.topic.workflow}") final String routingKey){
        return BindingBuilder.bind(workflowEvents).
                to(globalManagementExchange).
                with(routingKey + ".lot");
    }


    // gpec_lot_mail_items
    @Bean
    public Queue lotItemsQueue(@Value("${rabbit.custom.queue.lot.items}") final String queueName){
        return new Queue(queueName, true);
    }

    @Bean
    public DirectExchange lotItemExchange(@Value("${rabbit.custom.exchange.lot.items}") final String exchangeName){
        return new DirectExchange(exchangeName);
    }

    @Bean
    public DirectExchange lotItemDeliveryExchange(@Value("${rabbit.custom.exchange.lot.deliveries}") final String exchangeName){
        return new DirectExchange(exchangeName);
    }

    @Bean
    public Binding lotItemsQueueBinding(final Queue lotItemsQueue,
                                        final DirectExchange lotItemExchange,
                                        @Value("${rabbit.custom.routing-key.lot.items}") final String routingKey){
        return BindingBuilder.bind(lotItemsQueue).to(lotItemExchange).with(routingKey);
    }

    @Bean
    public Queue notificationsQueue(@Value("${rabbit.custom.queue.event.notifications}") final String queueName){
        return new Queue(queueName, true);
    }


    @Bean
    public DirectMessageListenerContainer eventsQueueListenerContainer(
            CachingConnectionFactory factory,
            Queue workflowEvents,
            EventsMessageListener listener){
        DirectMessageListenerContainer container = new DirectMessageListenerContainer(factory);
        container.setQueueNames(workflowEvents.getName());
        container.setMessageListener(listener);

        // Set Exclusive Consumer 'ON'
//        container.setExclusive(true);
        container.setConsumerTagStrategy(queue -> currentInstanceInfo.getId() + "." + queue);
//        container.setMessageConverter();
        // Should be restricted to '1' to maintain data consistency.
        container.setConsumersPerQueue(1);
//        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        return container;
    }


}
