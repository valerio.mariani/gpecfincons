package it.posteitaliane.pec.mail.producer.model;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@ToString
@Getter
@EqualsAndHashCode
public final class Template {
    private final Integer idTemplate;
    private final String customerCode;

    public Template() {
        idTemplate = -1;
        customerCode = "<not-assigned>";
    }

}
