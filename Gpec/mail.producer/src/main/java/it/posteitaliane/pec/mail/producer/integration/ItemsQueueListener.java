package it.posteitaliane.pec.mail.producer.integration;

import brave.Span;
import brave.Tracer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import it.posteitaliane.pec.common.integration.message.MessageSender;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.mail.producer.composer.MessageComposer;
import it.posteitaliane.pec.mail.producer.config.RabbitMQConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.Map;

@Component
public class ItemsQueueListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemsQueueListener.class);

    @Autowired Gson gson;
    @Autowired MessageComposer mailComposer;
    @Autowired MessageSender mailPublisher;

    @Autowired Jaxb2Marshaller marshaller;
    @Autowired Tracer tracer;


    /**
     * Consumer dei messaggi messi su RabbitMQ da ApacheNiFi dopo lo split dell'xml di input..
     */
    @RabbitListener(
            id = "COMPOSER_items_producer",
            containerFactory = RabbitMQConfig.rabbitListenerContainerFactory,
            queues = {"${rabbit.custom.queue.lot.items}"} )
    public void handleMessage(@Payload Message rabbitMessagePayload) {

        Span handleItemScope = tracer.nextSpan().name("item.receive.message." +
                rabbitMessagePayload.getMessageProperties().getMessageId()).
                kind(Span.Kind.CONSUMER);
        handleItemScope.annotate("Item ricevuto dal Lot Acquisition service (Apache NiFI)");
        handleItemScope.start();

        try (Tracer.SpanInScope ws = tracer.withSpanInScope(handleItemScope)) {
            LOGGER.debug("COMPOSER ITEMS CONSUMER[ " + hashCode() + " ] : " + rabbitMessagePayload);

            LOGGER.info("RECEIVED ITEM FROM NiFi Lot Acquisition Layer:" +
                    "\n * * * * * * * * * * * Payload: " +
                    "\n * * * * * " + rabbitMessagePayload.getMessageProperties() +
                    "\n * * * * * Body \n" +
                    new String(rabbitMessagePayload.getBody()) +
                    "\n * * * * * * * * * * * * ");

            try {

                asyncConvertAndCompose(rabbitMessagePayload);

            } catch (Exception e) {
                LOGGER.error(e.getLocalizedMessage(), e);
                handleItemScope.error(e);
                throw new RuntimeException(e); // l'unica exception che permette il requeue del messaggio
            }
        } finally {
            handleItemScope.flush();
        }

    }


    /**
     * Questo metodo:
     * -chiama il metodo che effettua:  la compose del body col template e il salvataggio su CB
     * -invia il messaggio su RabbitMQ
     */
    @Async
    public void asyncConvertAndCompose(Message rabbitMessagePayload) throws Exception {
        if(LOGGER.isTraceEnabled())
            LOGGER.trace("Execute method asynchronously. "
                    + Thread.currentThread().getName());

        Span span = tracer.nextSpan().name("compose and publish").start();
        try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {
            LOGGER.info("RabbitMQ message -> " + rabbitMessagePayload.getMessageProperties());

            String lotId = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("lotId");
            span.tag("LOT_ID", lotId);

            it.posteitaliane.pec.common.model.xml.Message payloadBean = (it.posteitaliane.pec.common.model.xml.Message)
                    marshaller.unmarshal(
                            new StreamSource( new StringReader(new String(rabbitMessagePayload.getBody())) ));

            MailDeliveryRequest delivery = mailComposer.compose(lotId, payloadBean);
            span.tag("MESSAGE_ID", delivery.getMessageID());

            mailPublisher.produceMsg(
                    rabbitMessagePayload.getMessageProperties().getCorrelationId(),
                    delivery,
                    (String)rabbitMessagePayload.getMessageProperties().getHeaders().get("lotId"),
                    message -> {
                        message.getMessageProperties().getHeaders().put("service",
                                rabbitMessagePayload.getMessageProperties().getHeaders().get("service"));
                        return message;
                     }
            );

        } catch (Exception e) {
            LOGGER.error("Error while composing and producing email", e);
            span.error(e);

        } finally {
            span.finish(); // note the scope is independent of the span. Always finish a span.
        }

    }

    /**
     *
     * @param payload
     * @param rootValue
     * @return
     */
    protected Map<String, Object> getAsMap(byte[] payload, String rootValue) {
        Type type = new TypeToken<Map<String, Object>>() {}.getType();
        return gson.fromJson(new String(payload), type);
    }

}
