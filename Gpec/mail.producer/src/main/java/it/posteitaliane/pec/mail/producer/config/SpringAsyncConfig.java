package it.posteitaliane.pec.mail.producer.config;

import it.posteitaliane.pec.common.configuration.AsyncConfiguration;
import it.posteitaliane.pec.common.model.xml.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.xml.bind.JAXBContext;
import java.util.HashMap;

@Configuration
@EnableAsync
public class SpringAsyncConfig extends AsyncConfiguration {

    static JAXBContext context;


    @Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(
                AttachmentList.class,
                Headers.class,
                Lot.class,
                Message.class,
                MessageList.class,
                MessageTemplate.class,
                Parameter.class,
                Parameters.class,
                PlaceHoldersType.class,
                PlaceHolderType.class,
                TemplateRef.class);

        marshaller.setMarshallerProperties(new HashMap<String, Object>() {{
            put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
        }});

        return marshaller;
    }

}
