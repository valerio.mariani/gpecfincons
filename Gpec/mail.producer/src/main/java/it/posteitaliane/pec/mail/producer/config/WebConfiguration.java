package it.posteitaliane.pec.mail.producer.config;

import brave.http.HttpTracing;
import brave.spring.web.TracingClientHttpRequestInterceptor;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
//@EnableWebMvc
public class WebConfiguration { //implements WebMvcConfigurer {

    private final HttpTracing httpTracing;

    public WebConfiguration(HttpTracing httpTracing) {
        this.httpTracing = httpTracing;
    }

    /**
     * Abilita il Cross-Origin Resource Sharing (CORS)
     *
     * Deprecato? adesso è sufficiente l'annotazione @Cross-origin sulla classe Controller.
     * @param
     */
//    public void addCorsMappings(final CorsRegistry registry) {
//        registry.addMapping("/**");
//    }


    @Bean
    RestTemplate restTemplate(HttpTracing tracing) {
        return new RestTemplateBuilder()
                .additionalInterceptors(TracingClientHttpRequestInterceptor.create(tracing))
                .build();
    }
}
