package it.posteitaliane.pec.mail.producer.template;

import it.posteitaliane.pec.common.model.xml.MessageTemplate;
import it.posteitaliane.pec.mail.producer.model.Template;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/template")
@CrossOrigin(origins="*")
public class TemplateController {

    @Value("${message-templates.folder}")
    private String messageTemplateFolder;

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public List<MessageTemplate> getTemplates(){

        File directory = new File(messageTemplateFolder);
        File[] listaTemplates = directory.listFiles((dir, name) -> name.endsWith(".xml"));
        return Arrays.stream(listaTemplates).
                    map(file -> getTemplatesFromFile(file.getAbsolutePath())).
                    collect(Collectors.toList());

    }

    @Cacheable("templateXmlMessage")
    public MessageTemplate getTemplatesFromFile(String templatePath){

        File messageTemplate = new File(templatePath);
        JAXBContext jc = null;
        try {
            jc = JAXBContext.newInstance(MessageTemplate.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            return (MessageTemplate) unmarshaller.unmarshal(messageTemplate);
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }

}
