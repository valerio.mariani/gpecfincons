package it.posteitaliane.pec.mail.producer.integration;

import brave.Span;
import brave.Tracer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rabbitmq.client.Channel;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.BindingInfo;
import it.posteitaliane.pec.common.integration.message.MessageSender;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.mail.producer.config.RabbitMQConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

@Component
public class EventsMessageListener implements ChannelAwareMessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventsMessageListener.class);

    @Autowired Gson gson;
    @Autowired TopicExchange globalManagementExchange;
    @Autowired Tracer tracer;
    @Autowired Client rabbitAdminClient;
    @Autowired RabbitListenerEndpointRegistry rabbitListenerEndpointRegistry;

//    @RabbitListener(
//            id = "COMPOSER_events_receiver",
//            containerFactory = RabbitMQConfig.rabbitListenerContainerFactory,
//            queues = {
//                "${rabbit.custom.queue.event.management}" + "_composer[1]"
//            })
    @Override
    public void onMessage(Message rabbitMessagePayload, Channel channel) throws Exception {

        String response = StringUtils.EMPTY;
        LOGGER.info(" * * * * * * * * * * * Payload: " +
                "\n * * * * * " + new String( rabbitMessagePayload.getBody() ) + "\n * * * * * * * * * * * * " );

        LOGGER.info("MAIL COMPOSER EVENT CONSUMER[ " + hashCode() + " ] : " + rabbitMessagePayload);
        String eventType = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("EVENT-TYPE");

        long tag = rabbitMessagePayload.getMessageProperties().getDeliveryTag();

        switch (EventType.valueOf(eventType)){

            case LOTTO_OUTCOME_GENERATO:
            case LOTTO_ELIMINATO:
            case LOTTO_SCARTATO:

                String lotId = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("lotId");
                Span span = tracer.nextSpan().name("lot[" + lotId + "].remove.queue").start();
                try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {

                    List<BindingInfo> bindings = rabbitAdminClient.getQueueBindings("/", lotId);
                    bindings.stream().forEachOrdered(bindingInfo ->
                            LOGGER.info("Bind " + bindingInfo + " will be deleted") );
                    rabbitAdminClient.deleteQueue("/", lotId);

                } catch (Exception e) {
                    LOGGER.error(e.getLocalizedMessage());
                    span.error(e);
                } finally {
                    span.finish(); // always finish the span
                    channel.basicAck(tag, false);
                }
                break;
            case SYSTEM_SUSPEND:
                span = tracer.nextSpan().name("system.suspend.processing").start();
                try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {

                    rabbitListenerEndpointRegistry.getListenerContainers().stream().forEach(
                            container -> {
                                LOGGER.debug("Check container[" + container + "] - status " + ((container.isRunning()) ? "RUNNING" : "STOPPED"));
                                if (container.isRunning()) {
                                    final SimpleMessageListenerContainer sListenerContainer = (SimpleMessageListenerContainer) container;
                                    sListenerContainer.stop();
                                }
                            });

                } catch (Exception e) {
                    LOGGER.error(e.getLocalizedMessage());
                    span.error(e);
                } finally {
                    span.finish(); // always finish the span
                }
                break;

            case SYSTEM_RESTART:
                // TODO ancora da implementare:
                // difficoltà: non si possono semplicemente riavviare tutti i container (forse)
                // ma bisogna ripristinare lo stato così come è stato lasciato alla ricezione dell'evento SUSPEND
                break;
            default:
                break;
        }

    }

    /**
     *
     * @param payload
     * @param rootValue
     * @return
     */
    protected Map<String, Object> getAsMap(byte[] payload, String rootValue) {
        Type type = new TypeToken<Map<String, Object>>() {}.getType();
        return gson.fromJson(new String(payload), type);
    }

}
