//TUTTA COMMENTATA, PERCHE' E' STATA SPOSTATA NEL PROGETTO CONTROLLER
// ANCORA NON CANCELLATA PERCHE' PUO' ESSERE UTILE IN FASE DI MERGE DI BRANCH
//package it.posteitaliane.pec.mail.producer;
//
//import brave.ScopedSpan;
//import brave.Span;
//import brave.Tracer;
//import com.couchbase.client.java.error.DocumentAlreadyExistsException;
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//import it.posteitaliane.pec.common.integration.DataOperationsMessagePublisher;
//import it.posteitaliane.pec.common.integration.message.MessageSender;
//import it.posteitaliane.pec.common.model.LotDelivery;
//import it.posteitaliane.pec.common.repositories.LotRepository;
//import it.posteitaliane.pec.common.integration.EventMessagePublisher;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.amqp.core.Queue;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.OptimisticLockingFailureException;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import java.lang.reflect.Type;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.time.LocalDateTime;
//import java.time.ZoneOffset;
//import java.util.*;
//
//import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
//import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
//import static org.springframework.web.bind.annotation.RequestMethod.POST;
//import static org.springframework.web.bind.annotation.RequestMethod.PUT;
//
///**
// * TODO: metodi che dovrebbero probabilmente stare nel 'Controller Service'
// */
//@RestController
//@CrossOrigin(origins="*", maxAge = 3600)
//public class IndexController {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);
//
//    @Autowired Gson gson;
//    @Autowired MessageSender mailPublisher;
//    @Autowired EventMessagePublisher eventPublisher;
//    @Autowired DataOperationsMessagePublisher dataOperationsPublisher;
//    @Autowired LotRepository lotRepository;
//    @Autowired Tracer tracer;
//
//
//    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
//
//    @GetMapping("/")
//    public String home(){
//        return "index of the MailComposers Microservice - GPec. Poste DIGITAL";
//    }
//
//
//    /**
//     * Questo servizio è attualmente invocato da 2 componenti Invoke HTTP NIFI posizionati successivamente
//     * al componente Check FileSize and FileName (che per ora fa solo controllo della file size, e forse in futuro anche la naming convention)
//     *     *
//     * in caso tale check abbia dato esito negativo, arrivera' RouteOnAttribute.Route valorizzato "ko"
//     * in caso tale check abbia dato esito positivo, arrivera' RouteOnAttribute.Route valorizzato "ok"
//     *
//
//     *
//     * Nel caso "ko", questo metodo deve:
//     * -segnalare sulla coda degli eventi del message broker la ricezione del lotto con evento di tipo EventType.LOTTO_SCARTATO,
//     * -ritornare a NIFI l'xml corrispondente al file ACK_INPUT con l'errore di validazione V003
//     *
//     * Nel caso "ok", questo metodo deve:
//     * -segnalare sulla coda degli eventi del message broker la ricezione del lotto con evento di tipo EventType.LOTTO_RICEVUTO,
//     * -salvare il lotDelivery su couchbase
//     * -il ritorno a NIFI in questo caso non ha importanza, non viene usato
//     *
//     * ----->>>> (valutare se sdoppiare questo metodo in 2 servizi REST)<<------
//     *
//     *
//     * l'input è un HTTP entity con un raw JSON in quanto il contenuto inviato da NiFI è privo di schema.
//     * Si è scelto quindi di non creare un Bean ad uso di un serializzatore, ma di accedere ai dati in una struttura key-value
//     *
//     *
//     * @param httpEntity un Json corrispondente al set di attributi del flow-file corrispondente allo ZIP del lotto
//     * @return un ack-input
//     *
//     */
//    //nome nel property nifi: "gpec.acquireCheckLot.invokeHTTP"
//    @RequestMapping(value = "/lot", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_XML_VALUE )
//    @ResponseBody
//    public ResponseEntity<?> checkIncomingLotFile(HttpEntity<String> httpEntity){
//
//        LOGGER.trace("called... ");
//        Span span = tracer.nextSpan().name("lot.check.incoming").
//                kind(Span.Kind.SERVER);
//        span.annotate("Lotto ricevuto dal Lot Acquisition service (Apache NiFI)");
//        span.start();
//
//        try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {
//            String requestBody = httpEntity.getBody();
//            LOGGER.info("PUT: " + requestBody);
//
//            HttpHeaders headers = httpEntity.getHeaders();
//            headers.forEach((s, strings) -> {
//                if(!s.contains("x-b3")) {
//                    span.tag(s, strings.get(0));
//                }
//            });
//            // conversione del body in una MAP
//            Map<String, Object> checkData = this.getAsMap(requestBody, null);
//            LOGGER.info("JSON: " + checkData);
//
//            // si estraggono le variabili
//            String filename = (String) checkData.get("filename");
//            String lotId = filename.substring(0, filename.indexOf("."));
//            // ... altri attributi?
//            String lotFileStatus = (String) checkData.get("RouteOnAttribute.Route");
//
////        Boolean checkFileSizeResult = "ok".equalsIgnoreCase( (String)checkData.get("RouteOnAttribute.Route") );
//            String lotCreationDateString = (String) checkData.get("file.creationTime");
//
//
//            LotDelivery lotInfo = LotDelivery.builder()
//                    .created(lotCreationDateString)
//                    .lotId(lotId)
////                .version(0L)
//                    .messagesProcessed(new Integer(0))
//                    .events(new ArrayList<>())
//                    .updated(LocalDateTime.now())
//                    .customerCode(filename.substring(0, 4))   //ricavarselo dal nome dello ZIP
////                .service("G23L")        //impossibile ricavare in quanto è all'interno del file .xml...
//                    .build();
//
//
//            String xmlAckInput = "";
//            if (lotFileStatus.equalsIgnoreCase("ko")) {
//                eventPublisher.publishLotDiscardedEvent(lotInfo); //CurrentStatus passa a LOTTO_SCARTATO
//                //genero l'xml per il file di ACK_INPUT da restiturie a NIFI
//                List<String> errorCodes = new ArrayList<>();
//                errorCodes.add("V003");//"DIMENSIONE FILE ECCEDE IL LIMITE MASSIMO CONSENTITO"
//                xmlAckInput = generateXmlAckInput(lotId, new Date(), errorCodes);
//                LOGGER.info("SCARTO LOTTO E CREO ACK INPUT CON ERRORE V003");
//
//            } else if (lotFileStatus.equalsIgnoreCase("koFileName")) {
//                eventPublisher.publishLotDiscardedEvent(lotInfo); //CurrentStatus passa a LOTTO_SCARTATO
//                //genero l'xml per il file di ACK_INPUT da restiturie a NIFI
//                List<String> errorCodes = new ArrayList<>();
//                errorCodes.add("V001");//"NOME FILE ZIP NON CORRISPONDENTE ALLE SPECIFICHE"
//                xmlAckInput = generateXmlAckInput(lotId, new Date(), errorCodes);
//                LOGGER.info("SCARTO LOTTO E CREO ACK INPUT CON ERRORE V001");
//
//            } else {
//                //  ora creiamo le code e tutta l'infrastruttura su rabbitMQ per poter elaborare gli invii
//                LOGGER.info("LOTTO VALIDO ---> ORA CREIAMO LE CODE E TUTTA L'INFRASTRUTTURA SU RABBITMQ PER POTER ELABORARE GLI INVII");
//                mailPublisher.bindToKey(lotId, new Queue(lotId, true, false, false));
//                eventPublisher.publishNewLotArrivedEvent(lotInfo); //CurrentStatus passa a LOTTO_RICEVUTO
//            }
//
//            //Salvo in ogni caso lotDelivery su couchbase, anche se LOTTO_SCARTATO, perche' mi serve poi per pulire il FILE ZIP
////        dataOperationsPublisher.publishLotCreationRequest(lotInfo);
//            Optional<LotDelivery> persisted;
//            try {
//                LotDelivery lotDoc = lotRepository.save(lotInfo);//TODO: DA TESTARE
//                LOGGER.info("Save lotDelivery " + lotDoc.getLotId());
//            } catch (OptimisticLockingFailureException e) {
//                e.printStackTrace();
//            } finally {
//                persisted = lotRepository.findById(lotId);
//            }
//
//            LOGGER.info("created LOT[ " + lotId + "] ? " + persisted.isPresent() + " " + persisted.get());
//
//            return new ResponseEntity<>(xmlAckInput, HttpStatus.OK);
//        } finally {
//            // start the client side and flush instead of finish
//            span.flush();
//        }
//    }
//
//
//
//
//
//    /**
//     * il servizio è invocato da NiFi, gruppo "Lot Acquisition Flow", componente "Get Start ACK-input"
//     *
//     * dopo che tutte le validazioni sono state superate
//     *
//     *
//     * Il servizio :
//     *  <ol>
//     *      <li>cerca su couchbase il documento LotDelivery per un dato LotId </li>
//     *      <li>aggiorna lo stato del lotto a IN_ELABORAZIONE </li>
//     *      <li>pubblica l' evento corrispondenti sulla topic degli eventi in rabbitMQ</li>
//     *      <li>aggiorna su couchbase il documento e gli aggiunge l'evento alla lista degli eventi</li>
//     *  </ol>
//     *
//     * @param httpEntity un XML corrispondente al set di attributi del flow-file corrispondente all'XML descrittore del lotto
//     * @return un ack-input contenente l'esito della validazione e della presa in carico per la lavorazione
//     */
//    @RequestMapping(value = "/lot/start-process", method = POST, consumes = APPLICATION_XML_VALUE, produces = APPLICATION_XML_VALUE )
//    @ResponseBody
//    public ResponseEntity<?> lotAcquisitionAcknowledgment(HttpEntity<String> httpEntity){
//
//
//        LOGGER.info("called... ");
//        Span span = tracer.nextSpan().name("lot.acknowledgment.ok").
//                kind(Span.Kind.SERVER);
//        span.annotate("Lotto elaborabile");
//        span.start();
//
//        try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {
//            HttpHeaders headers = httpEntity.getHeaders();
//            headers.forEach((s, strings) -> {
//                if(!s.contains("x-b3")) {
//                    span.tag(s, strings.get(0));
//                }
//            });
//            // rispetto al service 'checkIncomingLotFile' il json è salvato da NiFi dentro un header con chiave 'jsonattributes'
//            String json = headers.get("jsonattributes").get(0);
//            LOGGER.info("POST '/lot/start-process': " + json);
//
//            Map<String, Object> acquisitionResult = this.getAsMap(json, null);
//            LOGGER.info("JSON: " + acquisitionResult);
//            List<String> errorsCodeLotValidationVuoto = new ArrayList<>();//dopo il rework su Nifi fatto giorni fa da D.D. questo metodo produca solo l'ACK_INPUT senza errori
//
//
//            String lotId = (String) acquisitionResult.get("segment.original.filename");
//            String lotCreationDateString = (String) acquisitionResult.get("file.creationTime");// trasformare in date?
//
//            /* * * * in caso di comportamenti 'imprevedibili' di NiFI in cui questo endpoint è chiamato prima del precedente (!!!)* * */
//            LOGGER.info("LOTTO VALIDO ---> PER SICUREZZA DICHIARIAMO LE CODE SE ANCORA NON SONO STATE CREATE");
//            mailPublisher.bindToKey(lotId, new Queue(lotId, true, false, false));
//            /* * * * * * */
////        LocalDateTime lotCreationDate = getLocalDateTime(lotCreationDateString);
//
////        ZonedDateTime zdt = ZonedDateTime.parse(lotCreationDateString,
////                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss+SSSS")
////        );
//////        Instant instant = Instant.parse(lotCreationDateString);
////        System.out.println("Instant : " + instant);
//            //get date time only
//
////        LocalDateTime lotCreationDate =  zdt.toLocalDateTime(); // LocalDateTime.ofInstant(instant, ZoneId.of(ZoneOffset.UTC.getId()));
////        LocalDate lotCreationDate = LocalDate.parse(lotCreationDateString);
//
//
//
//
//            //cerco il lotto che dovrebbe gia' esistere su CB
//            Optional<LotDelivery> lotto = lotRepository.findById(lotId); //TODO: DA TESTARE,
//
//            LocalDateTime t1 = LocalDateTime.now();
//            while(!lotto.isPresent()){
//
//                LOGGER.debug("LOTTO NON ANCORA PRESENTE SU CB -startProcess ");
//
//                lotto = lotRepository.findById(lotId);
//                //ripeto la find finchè non trovo il lotto che gia' deve essere salvato su CB
//                // magari aggiungere un controllo sul tempo, che so: se sono pasatti piu' di 2 miunuti, esco cmq dal ciclo e in  qualche modo gestisco l'incongruenza
//            }
//
//            LotDelivery lotInfo = lotto.get();
//            lotInfo.setUpdated(LocalDateTime.now());
//            lotInfo.setMessagesProcessed(new Integer((String)acquisitionResult.get("messageCount")));//TODO: DA TESTARE SE ARRIVA
//            lotInfo.setCustomerCode((String)acquisitionResult.get("customerCode"));// queste 2 info in teoria dovrebbero essere gia' state messe alla salvataggio del lotto...
//            lotInfo.setService((String)acquisitionResult.get("service"));//...ma mi sa che ancora non l'abbiamo messe...poco male le mettiamo qui..
//
//            //
////        lotto.ifPresent(lotInfo -> {
////            lotInfo.setUpdated(LocalDateTime.now());
////            lotInfo.setMessagesProcessed(new Integer((String)acquisitionResult.get("messageCount")));//TODO: DA TESTARE SE ARRIVA
////            lotInfo.setCustomerCode((String)acquisitionResult.get("customerCode"));// queste 2 info in teoria dovrebbero essere gia' state messe alla salvataggio del lotto...
////            lotInfo.setService((String)acquisitionResult.get("service"));//...ma mi sa che ancora non l'abbiamo messe...poco male le mettiamo qui..
////
////        });
////
////        //..se non lo trovo qualcosa non ha funzionato
////        LotDelivery lotInfo = lotto.orElseGet(
////                () -> {
////
////                    LotDelivery lottoCreatedNow =
////
////                            LotDelivery.builder()
////                                    .created(lotCreationDateString)
////                                    .lotId(lotId)
////                                    .events(new ArrayList<>())
////                                    .updated(LocalDateTime.now())
////                                    .customerCode((String)acquisitionResult.get("customerCode"))
////                                    .service((String)acquisitionResult.get("service"))
////                                    .build();
////
////
////                    return lottoCreatedNow;
////                });
//
//            //pubblico evento su rabbit
//            eventPublisher.publishLotStartProcessingEvent(lotInfo);//CurrentStatus passa a LOTTO_IN_ELABORAZIONE
//
//            //salvo info lotto su CB ( dovrebbe essere un update)
//            lotRepository.save(lotInfo);
//
//            //genero l'XML per il file ACKINPUT.
//            //tale xml non contine errori di validazione
//            String xmlAckInput = generateXmlAckInput(lotId,new Date(),errorsCodeLotValidationVuoto);
//            return new ResponseEntity<>(xmlAckInput, HttpStatus.OK);
//        } finally {
//            // start the client side and flush instead of finish
//            span.flush();
//        }
//    }
//
//
//
//
//    @RequestMapping(value = "/getXmlAckInput", method = PUT, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_XML_VALUE )
//    @ResponseBody
//    public ResponseEntity<?> getXmlAckInput(HttpEntity<String> httpEntity){
//
//        List<String> errorsCodeLotValidation=new ArrayList<>();
//
//        LOGGER.trace("called... ");
//        HttpHeaders headers = httpEntity.getHeaders();
//        // rispetto al service 'checkIncomingLotFile' il json è salvato da NiFi dentro un header con chiave 'jsonattributes'
//        // TODO vedere se migliorare questo passaggio lato NiFi
//        String json = headers.get("jsonattributes").get(0);
//        LOGGER.info("PUT '/getXmlAckInput': " + json);
//
//        // conversione dell'header in una MAP
//        Map<String, Object> checkData = this.getAsMap(json, null);
//        LOGGER.info("JSON: " + checkData);
//
//
//        // si estraggono le variabili
//        String filename = (String)checkData.get("filename");
//        String lotId = filename.substring(0, filename.indexOf(".") );
//        // ... altri attributi?
//
//        String unpackError = (String)checkData.get("UnpackContent.Content");
//        if(unpackError!=null && unpackError.equals("failure")){
//            errorsCodeLotValidation.add("V004");//FILE ZIP CORROTTO
//        }
//
//        String validationError = (String)checkData.get("validatexml.invalid.error");
//        if(validationError != null){
//            LOGGER.info("ERRORI DI VALIDAZIONE XML LOTTO: " + validationError);
//            errorsCodeLotValidation.add("V005");//FILE XML NON VALIDO
//        }
//
//        String thresholdError =(String)checkData.get("RouteOnAttribute.Route");
//        if(thresholdError!=null && thresholdError.equals("koForThreshold")){
//            errorsCodeLotValidation.add("V006");//Numero messaggi eccede gpec.messageLimitChecker.messageThreshold
//        }
//
//
////       se si riesciuscce a generalizza, a rifattorizzare i vari metodi rest che fanno validazione e generazione di xml ack input
////       qui ci potrebbero essere una serie di IF (NON ELSEIF MA IF!!!) che verificano la presenza di erroi e incrementan la lista ...
//
//
//        String lotCreationDateString = (String) checkData.get("file.creationTime");
//
//
//
////        //..se lo trovo lo aggiorno
////        lotto.ifPresent(lotInfo -> {
////            lotInfo.setUpdated(LocalDateTime.now());
//////            lotInfo.setCustomerCode((String)acquisitionResult.get("customerCode"));// queste 2 info in teoria dovrebbero essere gia' state messe alla salvataggio del lotto...
//////            lotInfo.setService((String)acquisitionResult.get("service"));//...ma mi sa che ancora non l'abbiamo messe...poco male le mettiamo qui..
////
////        });
////        //in teoria ci deve essere..quindi se non c'e'..boh..
////        LotDelivery lotInfo = lotto.get();
//
//        if(errorsCodeLotValidation.size()>0){
//
//            //cerco il lotto che dovrebbe gia' esistere su CB
//            Optional<LotDelivery> lotto = lotRepository.findById(lotId); //TODO: DA TESTARE,
//
//            LocalDateTime t1 = LocalDateTime.now();
//            while(!lotto.isPresent()){
//
//                LOGGER.info("LOTTO NON ANCORA PRESENTE SU CB -getXmlAckInput ");
//
//                lotto = lotRepository.findById(lotId);
//                //ripeto la find finchè non trovo il lotto che gia' deve essere salvato su CB
//                // magari aggiungere un controllo sul tempo, che so: se sono pasatti piu' di 2 miunuti, esco cmq dal ciclo e in  qualche modo gestisco l'incongruenza
//            }
//
//            LotDelivery lotInfo = lotto.get();
//
//            lotInfo.setUpdated(LocalDateTime.now());
//
//            eventPublisher.publishLotDiscardedEvent(lotInfo);//CurrentStatus passa a LOTTO_SCARTATO
//
//            //salvo info lotto su CB ( dovrebbe essere un update)
//            lotRepository.save(lotInfo); //TODO: DA TESTARE
//        }
//
//        //genero in ogni caso l'XML per il file ACKINPUT.
//        String xmlAckInput = generateXmlAckInput(lotId,new Date(),errorsCodeLotValidation);
//
//        return new ResponseEntity<>(xmlAckInput, HttpStatus.OK);
//    }
//
//
//
//    public static String generateXmlAckInput(String lotId, Date ackTime,List<String> errorCodes) {
//
//        SimpleDateFormat  sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
//        String dateTime = sdf.format(ackTime);
//
//
//
//        String xmlErrorCodes="";
//
//        if(errorCodes!=null && errorCodes.size()>0){
//
//            xmlErrorCodes+="\t<Errors>\n";
//            for(String errorCode :errorCodes){
//                xmlErrorCodes+= "\t\t<Error errorCode=\""+errorCode+"\"/>\n" ;
//            }
//            xmlErrorCodes+="\t</Errors>";
//        }
//
//        String xmlAckInput= "<?xml version=\"1.0\" encoding = \"UTF-8\"?>\n" +
//                "<AckInput xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
//                "\t<LotId>"+lotId+"</LotId>\n" +
//                "\t<AckTime>"+dateTime+"</AckTime>\n" +
//                  xmlErrorCodes +
//
////                    "    <Errors>\n" +
////                    "        <Error errorCode=\"V001\"/>\n" +
////                    "        <Error errorCode=\"V002\"/>\n" +
////                    "        <Error errorCode=\"V003\"/>\n" +
////                    "    </Errors>\n" +
//                "\n" +
//                "</AckInput>";
//
//        return  xmlAckInput;
//    }
//
//
//
//    // non sono riuscito a farlo funzionare.. restiamo con la data creazione ZIP del lotto come stringa per ora...
//    @Deprecated
//    private LocalDateTime getLocalDateTime(String lotCreationDateString) {
//        LocalDateTime lotCreationDate = null;
//        try {
//            lotCreationDate = LocalDateTime.from(df.parse(lotCreationDateString).toInstant().
////                            with(TimeZone.getTimeZone("GMT")).
////                    atZone(ZoneId.of("")).
//                    atOffset(ZoneOffset.UTC));
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return lotCreationDate;
//    }
//
//    /**
//     *
//     * @param payload
//     * @param rootValue
//     * @return
//     */
//    protected Map<String, Object> getAsMap(String payload, String rootValue) {
//        Type type = new TypeToken<Map<String, Object>>() {}.getType();
//        return gson.fromJson(payload, type);
//    }
//
//
//}
