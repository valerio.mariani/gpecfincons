package it.posteitaliane.pec.mail.producer;


import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.integration.EventMessagePublisher;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.jupiter.api.TestInstance.Lifecycle;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@DisplayName("I messaggi degli eventi devono essere consegnati a tutti i listener")
public class MessageQueueSendingTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageQueueSendingTest.class);

    private Map<String,List<SimpleMessageListenerContainer>> containers = new ConcurrentHashMap<>();
    @Autowired ConnectionFactory factory;
    @Autowired EventMessagePublisher publisher;

    @Autowired TopicExchange globalManagementExchange;
    @Autowired RabbitAdmin rabbitAdmin;

    @Value("${rabbit.custom.routing-key.event.workflow}")
    String managementTopicRoutingKey;

    Queue q1;
    Queue q2;
    @BeforeAll
    public void setUp(){

        q1 = new Queue("management_queue_for_sender_1", false, false, false);
        q2 = new Queue("management_queue_for_sender_2", false, false, false);
        rabbitAdmin.declareQueue(q1);
        rabbitAdmin.declareQueue(q2);

        Binding b1 = BindingBuilder.bind(q1).to(globalManagementExchange).with(managementTopicRoutingKey);
        Binding b2 = BindingBuilder.bind(q2).to(globalManagementExchange).with(managementTopicRoutingKey);

        rabbitAdmin.declareBinding(b1);
        rabbitAdmin.declareBinding(b2);

        SimpleMessageListenerContainer container1 = new SimpleMessageListenerContainer();
        container1.setConnectionFactory(factory);
        container1.setMessageListener(new Subscriber1());
        container1.setConsumerTagStrategy( p -> "SENDER 1 - topic events" );
        container1.addQueueNames("management_queue_for_sender_1");

        SimpleMessageListenerContainer container2 = new SimpleMessageListenerContainer();
        container2.setConnectionFactory(factory);
        container2.setMessageListener(new Subscriber2());
        container2.setConsumerTagStrategy( p -> "SENDER 2 - topic events" );
        container2.addQueueNames("management_queue_for_sender_2");

        List<SimpleMessageListenerContainer> group = new ArrayList<>();
        group.add(container1);
        group.add(container2);
        containers.put(globalManagementExchange.getName(),  group);

        container1.start();
        container2.start();
    }

//    @Test
//    public void eventToMultipleTopicListenerTest(){
//
//        publisher.publishLotValidationEvent(LotDelivery.builder()
//                .lotId("G23L_20181130_0000001")
//                .created("20190103")
//                .customerCode("CLIENTE-TEST")
//                .service("23L")
//                .build());
//
//        try {
//            Thread.sleep(20000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        Assertions.assertThat(containers.get(globalManagementExchange.getName())).
//                extracting("messageReceived").isEqualTo(Boolean.TRUE);
//
//    }


    @AfterAll
    public void tearDown(){
        containers.get(globalManagementExchange.getName()).stream().forEach(container ->
                container.stop());
        containers.clear();

        rabbitAdmin.deleteQueue(q1.getName());
        rabbitAdmin.deleteQueue(q2.getName());
    }

    class Subscriber1 implements MessageListener {

        boolean messageReceived = false;

        @Override
        public void onMessage(Message message) {
            LOGGER.info("Subscriber[1] has received message " + message);
            messageReceived = true;
            //assertions
        }
    }

    class Subscriber2 implements MessageListener {

        boolean messageReceived = false;

        @Override
        public void onMessage(Message message) {
            LOGGER.info("Subscriber[2] has received message " + message);
            messageReceived = true;
            //assertions

        }
    }

}
