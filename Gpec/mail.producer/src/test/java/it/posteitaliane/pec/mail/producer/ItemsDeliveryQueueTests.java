package it.posteitaliane.pec.mail.producer;

import it.posteitaliane.pec.common.integration.message.MessageSender;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.TestInstance.Lifecycle;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@DisplayName("Il microservizio MESSAGE-COMPOSER")
public class ItemsDeliveryQueueTests implements MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemsDeliveryQueueTests.class);

    private SimpleMessageListenerContainer container;
    ConcurrentMap<String, List<MessageReceipt>> messages = new ConcurrentHashMap<>();

	@Autowired MessageSender producer;
//	@Autowired Queue lotItemDeliveryQueue;
    @Autowired ConnectionFactory factory;
    @Autowired MessageConverter converter;
	@Value("${rabbit.custom.routing-key.event.workflow}") String eventQueueRoutingKey;


    @BeforeAll
    public void setUp(){
        container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(factory);
        container.setMessageListener(new MessageListenerAdapter(this, this.converter));

    }
//    @AfterAll
//    public void tearDown(){
//        container.stop();
//    }

	@Test
	public void contextLoads() {
	}

	//METODO CHE NON FUNZIONA PIU' A SEGUITO DEL REWORK DELLO SPOSTAMENTO DI IndexController NEL PROGETTO Controller, perche' è stato tolto il metodo bindToKey dall'interffaccia Message Sender
    //TODO: AGGIORNARE APPENA POSSIBILE
//    @DisplayName("Pubblica batches di invii sulla coda ")
//    @ParameterizedTest
//    @CsvSource({
//            "10, G23L-20181130-000002, message-content-file",
//            "50, G23L-20181214-000003, message-content-file" })
//    public void testPublishMsg(Integer batchSize, String lotId, String content){
//        // TODO tutti questi passi dovrebbero essere ospitati dentro un estensione JUnit5...
//        // dichiara una coda dinamica per ospitare il lotto:
//        Queue lotQueue = new Queue(lotId, true, false, false);
//        // dichiara il bing con la routing key per permettere a producer di pubblicare tramite lo stesso exchange sulla coda
//        Binding lotBinding = producer.bindToKey(lotId, lotQueue);
//
//        messages.put(lotId, new ArrayList<MessageReceipt>());
//        IntStream.range(1, batchSize + 1).forEach(idNumber -> {
//            MailDeliveryRequest req =
//                    MailDeliveryRequest.builder().
//                            messageID(lotId + "_" + idNumber).
//                            to("PEC1.multe@comune.roma.it").
//                            lotId(lotId).
//                            subject("INVIO 23L " + idNumber).
//                            events(Arrays.asList(Events.builder().event(EventType.MESSAGGIO_INVIO_SUCCESSO).time(LocalDateTime.now()).build())).
//                            content(content + " : " + idNumber).
//                            build();
//            producer.produceMsg("INVIO-" + lotId + "-" + idNumber, req, lotId, null);
//        });
//        // imposta la coda come punto d'ascolto del MessageListener
//        container.setQueues(lotQueue);
//        // facciamo partire il listener
//        container.start();
//
//        try {
//            Thread.sleep(6000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        // assertions..
//        assertThat(messages.get(lotId)).hasSize(batchSize);
////        assertThat(messages.get(lotId)).allSatisfy(
////                receipt -> receipt.message.
////                        getMessageProperties().
////                        getCorrelationId().
////                        startsWith("INVIO-" + lotId + "-")
////        );
//
//
//        // CLEAN UP
//        container.stop();
//        producer.deleteBind(lotBinding);
//    }


	@Test
	public void testReceivingItemsFromQueue(){

    }


    /**
     * Riceve i messaggi
     * @param message
     */
    @Override
    public void onMessage(Message message) {
        messages.get(message.getMessageProperties().getReceivedRoutingKey())
                .add(new MessageReceipt(message, LocalDateTime.now()));
        LOGGER.info(message.getMessageProperties().toString());;
        LOGGER.info(new String(message.getBody()));
    }

    /**
     * usato solo per i test come raggruppatore di dati per le asserzioni
     */
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    private class MessageReceipt {
        Message message;
        LocalDateTime received;
    }
}
