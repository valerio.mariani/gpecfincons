package it.posteitaliane.pec.mail.producer.template;

import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.model.xml.*;
import it.posteitaliane.pec.mail.producer.composer.MessageComposer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@DisplayName("Il microservizio MESSAGE-COMPOSER")
public class MessageComposerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageComposerTest.class);

    @Autowired MessageComposer composer;

    private String lot = "G23L_20181229_00001";

//    @Autowired
//    Jaxb2Marshaller marshaller;

    @Test
    public void composeDaMessaggioinXML() {

        try {
            File xmlMessaggioDiTest = new File("..\\..\\xml-esempio\\SINGLE-MESSAGE-TEST.xml");



//            it.posteitaliane.pec.common.model.xml.Message message = (it.posteitaliane.pec.common.model.xml.Message)
//                    marshaller.unmarshal(
//                            new StreamSource( xmlMessaggioDiTest ));
//
//            anche con questo marshaller da l'errore:
//            Caused by: javax.xml.bind.UnmarshalException: unexpected element (uri:"", local:"Message"). Expected elements are <{}AttachmentList>,<{}Headers>,<{}Lot>,<{}MessageList>,<{}MessageTemplate>,<{}Parameter>,<{}Parameters>,<{}TemplateRef>
//            se non mettiamo @XmlRootElement(name = "Message") al Message.java

//            TODO: RISOLVERE!!!!!
//             O FACENDO UN JAXBINDING CHE AGGIUNGE  @XmlRootElement AL Message.java (VEDI ESEMPIO QUI: https://codereview.stackexchange.com/questions/1877/jaxb-xjc-code-generation-adding-xmlrootelement-and-joda-datetime )
//             O MODIFICANDO L'XSD
//             O IN ALTRO MODO



           JAXBContext jc = JAXBContext.newInstance(Message.class);

            Unmarshaller unmarshaller = jc.createUnmarshaller();

            Message message = (Message) unmarshaller.unmarshal(xmlMessaggioDiTest);






            MailDeliveryRequest response = composer.compose(lot, message) ;
            assertTrue(response instanceof MailDeliveryRequest);
            System.out.println(response.getSubject());
            System.out.println(response.getContent());

        }catch (Exception e){
            e.printStackTrace();
            assertTrue(false);
        }
    }


    @Test
    public void composeDaMessaggioinJava() {

        try{
            Message message = new Message();

            message.setCc("pluto@pluto-pec.it");
            message.setExtId("id3445355");
            message.setTo("pippo@pippo-pec.it");

            AttachmentList listAtt = new AttachmentList();

            List<String> attList = new ArrayList<>();
            attList.add(0,"Attach1");
            attList.add(1,"Attach2");
            attList.add(2,"Attach3");

            listAtt.setAttachment(attList);
            message.setAttachmentList(listAtt);


            TemplateRef tRef = new TemplateRef();
            tRef.setId("G23L");
            //tRef.setId("KKKKKK"); //per test eccezione

            Parameters param = new Parameters();

            List<Parameter> listParam = new ArrayList<>();

            Parameter param1 = new Parameter();
            param1.setName("DENOMINAZIONE_ENTE");
            param1.setVal("Ente Tal dei Tali");

            Parameter param2 = new Parameter();
            param2.setName("CODICE_AG");
            param2.setVal("COD_AG344355");

            Parameter param3 = new Parameter();
            param3.setName("CODICE_AR");
            param3.setVal("AR-CODE3346635");

            Parameter param4 = new Parameter();
            param4.setName("COGNOME");
            param4.setVal("Martelletti");

            Parameter param5 = new Parameter();
            param5.setName("NOME");
            param5.setVal("Aldo");


            listParam.add(param1);
            listParam.add(param2);
            listParam.add(param3);
            listParam.add(param4);
            listParam.add(param5);


            param.setParameter(listParam);

            tRef.setParameters(param);
            message.setTemplateRef(tRef);

            MailDeliveryRequest response = composer.compose(lot, message) ;
            assertTrue(response instanceof MailDeliveryRequest);
            System.out.println(response.getSubject());
            System.out.println(response.getContent());


        }catch (Exception e){
            e.printStackTrace();
            assertTrue(false);
        }

    }

    @Test
    public void composeWithWrongTemplate() {
        //TODO implement
    }
}
