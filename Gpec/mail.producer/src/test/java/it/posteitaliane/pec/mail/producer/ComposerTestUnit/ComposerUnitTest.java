package it.posteitaliane.pec.mail.producer.ComposerTestUnit;

import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.model.xml.*;
import it.posteitaliane.pec.mail.producer.composer.SimpleMessageComposerImpl;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;


public class ComposerUnitTest {

    private Message message = new Message();

    private String lot = "G23L_20181229_00001";

    @Test
    public void testComposerXml() throws Exception{

        SimpleMessageComposerImpl testComposer = new SimpleMessageComposerImpl();

        initSingleMessageXml();

        try {
            MailDeliveryRequest response = testComposer.compose(lot, message) ;
            assertTrue(response instanceof MailDeliveryRequest);

            System.out.println("********* TEST testComposerXml ***************");
            System.out.println(response.getSubject());
            System.out.println(response.getContent());
            System.out.println("********* FINE test testComposerXml ***************\n\n\n\n");

        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }

    }


    @Test
    public void testComposerManual() {

        SimpleMessageComposerImpl testComposer = new SimpleMessageComposerImpl();

        initManualMessage();

        try {
            MailDeliveryRequest response = testComposer.compose(lot, message) ;
            assertTrue(response instanceof MailDeliveryRequest);

            System.out.println("********* TEST testComposerManual ***************");
            System.out.println(response.getSubject());
            System.out.println(response.getContent());
            System.out.println("********* FINE test testComposerManual ***************\n\n\n\n");

        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(false);
        }

    }

    /*
    *
    * Creazione oggetto per input composer tramite XML e Jaxb unmarshall
    *
    * */
    public void initSingleMessageXml() throws Exception {

        File xml = new File("..\\..\\xml-esempio\\SINGLE-MESSAGE-TEST.xml");

        JAXBContext jc = JAXBContext.newInstance(Message.class);

        Unmarshaller unmarshaller = jc.createUnmarshaller();

        message = (Message) unmarshaller.unmarshal(xml);

    }




    /*
    *
    * Creazione manuale oggetto da passare alla composer
    *
    *
    * */

    public void initManualMessage() {

        message.setCc("pluto@pluto-pec.it");
        message.setExtId("id3445355");
        message.setTo("pippo@pippo-pec.it");

        AttachmentList listAtt = new AttachmentList();

        List<String> attList = new ArrayList<>();
        attList.add(0,"Attach1");
        attList.add(1,"Attach2");
        attList.add(2,"Attach3");

        listAtt.setAttachment(attList);
        message.setAttachmentList(listAtt);


        TemplateRef tRef = new TemplateRef();
        tRef.setId("G23L");
        //tRef.setId("KKKKKK"); //per test eccezione

        Parameters param = new Parameters();

        List<Parameter> listParam = new ArrayList<>();

        Parameter param1 = new Parameter();
        param1.setName("DENOMINAZIONE_ENTE");
        param1.setVal("Ente Tal dei Tali");

        Parameter param2 = new Parameter();
        param2.setName("CODICE_AG");
        param2.setVal("COD_AG344355");

        Parameter param3 = new Parameter();
        param3.setName("CODICE_AR");
        param3.setVal("AR-CODE3346635");

        Parameter param4 = new Parameter();
        param4.setName("COGNOME");
        param4.setVal("Martelletti");

        Parameter param5 = new Parameter();
        param5.setName("NOME");
        param5.setVal("Aldo");


        listParam.add(param1);
        listParam.add(param2);
        listParam.add(param3);
        listParam.add(param4);
        listParam.add(param5);


        param.setParameter(listParam);

        tRef.setParameters(param);
        message.setTemplateRef(tRef);

    }





    //Test per marshall from Pojo
    public void testObjectToXml() throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(MessageTemplate.class);

        Marshaller marshaller = jaxbContext.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        marshaller.marshal(message, System.out);

        message = null;

    }

}
