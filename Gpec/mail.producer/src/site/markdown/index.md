## esecuzione dai sorgenti:

per un esecuzione di default sulla porta 8001
`java -jar  -Dspring.profiles.active=local composer-0.0.1-SNAPSHOT.jar` 

specificando invece un altra porta non occupata per una seconda istanza sulla stessa macchina:
`java -jar  -Dspring.profiles.active=local -Dserver.port=8012 composer-0.0.1-SNAPSHOT.jar`