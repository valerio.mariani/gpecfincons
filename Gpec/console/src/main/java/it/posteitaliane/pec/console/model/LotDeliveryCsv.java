package it.posteitaliane.pec.console.model;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import it.posteitaliane.pec.common.model.EventType;
import lombok.*;
import org.springframework.data.couchbase.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class LotDeliveryCsv {

    @NotNull
    @Id
    private String lotId;

    @Field
    private EventType currentStatus;

    @NotNull
    @Field
    private String customerCode;

    @Field
    private String service;


    /**
     * Numero di messaggi contenuti nel lotto
     */
    @Field
    private Integer messagesProcessed;

}