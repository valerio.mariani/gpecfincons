package it.posteitaliane.pec.console.service;

import it.posteitaliane.pec.common.model.MailCsv;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;

import java.util.List;

public interface MessageFindService {
    public abstract List<MailDeliveryRequest> getDetailsOfMessageByEvent(String eventMessage);
    public abstract long getCountOfMessageByEvent(String eventMessage);
    public abstract List<MailDeliveryRequest> findByEvent(String event);
    public abstract List<MailCsv> elaboratedMessageInDateRange(String dateIsoFrom, String dateIsoTo);
}