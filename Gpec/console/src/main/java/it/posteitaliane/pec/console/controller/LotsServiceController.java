package it.posteitaliane.pec.console.controller;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import it.posteitaliane.pec.common.model.*;
import it.posteitaliane.pec.console.model.LotDeliveryCsv;
import it.posteitaliane.pec.console.service.LotsFindService;
import it.posteitaliane.pec.console.service.MessageFindService;
import org.apache.commons.codec.Charsets;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Controller
@EnableEurekaClient
public class LotsServiceController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(LotsServiceController.class);


    @Autowired
    LotsFindService lotsFindService;

    @Autowired
    MessageFindService messageFindService;


    @Autowired
    private EurekaClient discoveryClient;



    @GetMapping("/")
    public String homePage(Model model) {
        //model.addAttribute("name", name);
        return "index";
    }



    @GetMapping("/getLotsById/{lottoId}")
    @ResponseBody
    public LotDelivery getLotsById(@PathVariable("lottoId") String lottoId){

        LotDelivery lotto = lotsFindService.findById(lottoId);

        return lotto ;

    }




    public File getMessageCsv(String dataDa,String dataA) throws  IOException ,CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        List<MailCsv> messageForCsv = messageFindService.elaboratedMessageInDateRange(dataDa,dataA);

        File tempFile = File.createTempFile("CsvInputFormatTest" , ".csv");
        //tempFile.deleteOnExit();
        tempFile.setWritable(true);

        Writer writer = new OutputStreamWriter(new FileOutputStream(tempFile), Charsets.UTF_8);

        StatefulBeanToCsv<MailCsv> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(',')
                .build();

        beanToCsv.write(messageForCsv);

        writer.flush();
        writer.close();


        //String test =  new String ( Files.readAllBytes( Paths.get(tempFile.getAbsolutePath()) ) ) ;


        return tempFile;

    }


    public File getLotsCsv(String dataDa,String dataA) throws  IOException ,CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        List<LotCsv> lotForCsv = lotsFindService.findByCurrentStatusInOrderByCurrentStatusDesc(dataDa,dataA) ;

        File tempFile = File.createTempFile("CsvInputFormatTest" , ".csv");
        //tempFile.deleteOnExit();
        tempFile.setWritable(true);

        Writer writer = new OutputStreamWriter(new FileOutputStream(tempFile), Charsets.UTF_8);

        StatefulBeanToCsv<LotCsv> beanToCsv = new StatefulBeanToCsvBuilder(writer)
                                                                        .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                                                                        .withSeparator(',')
                                                                        .build();

        beanToCsv.write(lotForCsv);

        writer.flush();
        writer.close();


        //String test =  new String ( Files.readAllBytes( Paths.get(tempFile.getAbsolutePath()) ) ) ;


        return tempFile;

    }



    @GetMapping("/downloadFileCsv/{tipoCsv}/{dataDa}/{dataA}")
    public ResponseEntity<byte[]> downloadFile(@PathVariable String tipoCsv, @PathVariable String dataDa, @PathVariable String dataA, HttpServletRequest request) throws  IOException ,CsvDataTypeMismatchException, CsvRequiredFieldEmptyException{

        File resource = null;
        String nomeFile = "" ;
        String pattern = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date());

        dataDa = dataDa.replaceAll("^\"|\"$", "");
        dataA = dataA.replaceAll("^\"|\"$", "");

        if ("LOTTI".equals(tipoCsv)) {
            // Load file as Resource
             resource = getLotsCsv(dataDa, dataA);
             nomeFile = "Report_LOTTI_"+date+".csv" ;
        }
        else if ("MESSAGGI".equals(tipoCsv)){
            // Load file as Resource
            resource = getMessageCsv(dataDa, dataA);
            nomeFile = "Report_MESSAGGI_"+date+".csv" ;
        }


        String contentType = null;

        contentType = request.getServletContext().getMimeType(resource.getAbsolutePath());


        if(contentType == null) {
            contentType = "application/octet-stream";
        }



        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + nomeFile + "\"")
                .body(Files.readAllBytes(Paths.get(resource.getAbsolutePath())));
    }




    /* SEZIONE COUNT */
    @GetMapping("/getNumberOfMessageByEvent/{event}")
    @ResponseBody
    public long getNumberOfMessageConsegnaKo(@PathVariable String event){

        long number = messageFindService.getCountOfMessageByEvent(event);

        return number;
    }



    @GetMapping("/getMessageByEvent/{event}")
    @ResponseBody
    public List<MailDeliveryRequest> getMessageByEvent(@PathVariable String event){

        List<MailDeliveryRequest> messaggi = messageFindService.findByEvent(event);

        return messaggi;
    }



    @GetMapping("/getNumberOfMessageFails")
    @ResponseBody
    public HashMap<String,Long> getNumberOfMessageFails(){

        HashMap<String,Long> numberOfMessageFails = new HashMap<String,Long>();

        numberOfMessageFails.put(EventType.MESSAGGIO_ACCETTAZIONE_KO.name() , messageFindService.getCountOfMessageByEvent(EventType.MESSAGGIO_ACCETTAZIONE_KO.name()));
        numberOfMessageFails.put(EventType.MESSAGGIO_CONSEGNA_KO.name() , messageFindService.getCountOfMessageByEvent(EventType.MESSAGGIO_CONSEGNA_KO.name()));
        numberOfMessageFails.put(EventType.MESSAGGIO_INVIO_FALLITO.name() , messageFindService.getCountOfMessageByEvent(EventType.MESSAGGIO_INVIO_FALLITO.name()));


        return numberOfMessageFails;
    }


    @GetMapping("/getNumberOfLotsFails")
    @ResponseBody
    public HashMap<String,Long> getNumberOfLotsFails(){

        HashMap<String,Long> numberOfLotsFails = new HashMap<String,Long>();

        numberOfLotsFails.put(EventType.LOTTO_SCARTATO.name() , lotsFindService.countLotsInStatus(EventType.LOTTO_SCARTATO.name()));

        List<String> stats = new ArrayList<>();
        stats.add(EventType.LOTTO_RICEVUTO.name());
        stats.add(EventType.LOTTO_IN_ELABORAZIONE.name());
        numberOfLotsFails.put("LOTTI_IN_ATTESA_CHIUSURA" , lotsFindService.countByCurrentStatusIn(stats));



        return numberOfLotsFails;
    }


    /*  FINE SEZIONE COUNT */


    @GetMapping("/getLotsAttesaChiusura")
    @ResponseBody
    public List<LotDelivery> getLotsAttesaChiusura(){

        List<String> stats = new ArrayList<>();
        stats.add(EventType.LOTTO_RICEVUTO.name());
        stats.add(EventType.LOTTO_IN_ELABORAZIONE.name());
        List<LotDelivery> lottiChiusura = lotsFindService.findByCurrentStatusIn(stats);


        return lottiChiusura;
    }


    @GetMapping("/getLotsByStatus/{status}")
    @ResponseBody
    public List<LotDelivery> findByCurrentStatus(@PathVariable String status){

        List<LotDelivery> lots = lotsFindService.findLotsByStatus(status) ;

        return lots;
    }

    @GetMapping("/getServiceStatus/{serviceCode}")
    @ResponseBody
    public ServiceStatus getServiceStatus(@PathVariable String serviceCode){

        List<Application> applications = discoveryClient.getApplications().getRegisteredApplications();
        RestTemplate oRESTTemplate = new RestTemplate();

        ResponseEntity<ServiceStatus> oHTTPResponseEntity = null ;
        String serviceCheckHealthURl = null;

        for (Application application : applications) {
            if(application.getName().equals(serviceCode)){
                List<InstanceInfo> applicationsInstances = application.getInstances();
                serviceCheckHealthURl = applicationsInstances.get(0).getStatusPageUrl();
            }
        }


        if (serviceCheckHealthURl != null) {
            try {
                oHTTPResponseEntity = oRESTTemplate.getForEntity(serviceCheckHealthURl , ServiceStatus.class);
                System.out.println("OK");
            } catch (ResourceAccessException e) {
                LOGGER.error("ConnectException caught. Downstream dependency is offline");
            }
        }



       return (oHTTPResponseEntity == null)?null:oHTTPResponseEntity.getBody();




    }








}


