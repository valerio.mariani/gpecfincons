package it.posteitaliane.pec.console.serviceimpl;

import it.posteitaliane.pec.common.model.MailCsv;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import it.posteitaliane.pec.console.service.MessageFindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageFindServiceImpl implements MessageFindService {

    @Autowired
    MailDeliveryRepository mailDeliveryRepository;


    @Override
    public List<MailDeliveryRequest> getDetailsOfMessageByEvent(String eventMessage) {

        List<MailDeliveryRequest> messages = mailDeliveryRepository.findByEvent(eventMessage) ;

        return messages;
    }

    @Override
    public long getCountOfMessageByEvent(String eventMessage) {

        long numberOfMessages = mailDeliveryRepository.findByEventAndCount(eventMessage);

        return numberOfMessages;
    }

    @Override
    public List<MailDeliveryRequest> findByEvent(String event) {

        List<MailDeliveryRequest> messaggi = mailDeliveryRepository.findByEvent(event);

        return messaggi;

    }

    @Override
    public List<MailCsv> elaboratedMessageInDateRange(String dateIsoFrom, String dateIsoTo) {
        return mailDeliveryRepository.elaboratedMessageInDateRange(dateIsoFrom,dateIsoTo);
    }
}

