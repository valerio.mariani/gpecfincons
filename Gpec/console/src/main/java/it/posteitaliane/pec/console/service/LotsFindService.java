package it.posteitaliane.pec.console.service;

import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.LotCsv;
import it.posteitaliane.pec.common.model.LotDelivery;
import org.springframework.boot.configurationprocessor.json.JSONArray;

import java.util.List;

public interface LotsFindService {
    public abstract List<LotDelivery> findLotsByStatus(String status);
    public abstract long countLotsInStatus(String status);
    public abstract long countByCurrentStatusIn(List<String> status);
    public abstract List<LotDelivery> findByLotIdAndEvent(String lotId,String event);
    public abstract LotDelivery findById(String lotId);
    public abstract List<LotDelivery> findByCurrentStatusIn(List<String> stats);
    public abstract List<LotCsv> findByCurrentStatusInOrderByCurrentStatusDesc(String dataDa,String dataA);
}