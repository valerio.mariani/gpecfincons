package it.posteitaliane.pec.console.serviceimpl;

import it.posteitaliane.pec.common.model.LotCsv;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.console.service.LotsFindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LotsFindServiceImpl implements LotsFindService {

    @Autowired
    LotRepository lotRepository;


    @Override
    public List<LotDelivery> findLotsByStatus(String status) {

        List<LotDelivery> lotsByStatus = lotRepository.findByCurrentStatus(status);

        return lotsByStatus;
    }

    @Override
    public long countLotsInStatus(String status) {

       long lotsNumber = lotRepository.countLotsInStatus(status) ;


       return lotsNumber;
    }

    @Override
    public long countByCurrentStatusIn(List<String> status) {

        long lotsNumber = lotRepository.countByCurrentStatusIn(status);


        return lotsNumber;
    }

    @Override
    public List<LotDelivery> findByLotIdAndEvent(String lotId, String event) {
       return lotRepository.findByLotIdAndEvent(lotId,event);
    }

    @Override
    public LotDelivery findById(String lotId) {
        return lotRepository.findById(lotId).get();
    }

    @Override
    public List<LotDelivery> findByCurrentStatusIn(List<String> stats) {
        return  lotRepository.findByCurrentStatusIn(stats);
    }

    @Override
    public List<LotCsv> findByCurrentStatusInOrderByCurrentStatusDesc(String dataDa,String dataA) {
        return lotRepository.findByCurrentStatusInOrderByCurrentStatusDesc(dataDa,dataA);
    }


}

