
function getMessageFails($scope, $http,$routeParams,NgTableParams){

    console.log("AVVIO CONTROLLER getMessageFails");


    var response;
    var $table = $('#table');
    var rows = {};


    switch($routeParams.event) {
        case 'MESSAGGIO_ACCETTAZIONE_KO':

            $scope.statusMessages = "Dettaglio messaggi non accettati";

            break;
        case 'MESSAGGIO_CONSEGNA_KO':

            $scope.statusMessages = "Dettaglio messaggi non consegnati";

            break;
        case 'MESSAGGIO_INVIO_FALLITO':

            $scope.statusMessages = "Dettaglio messaggi invio fallito";

            break;
    }


    var self = this;
    var arrayData = [];


    $http({
        method : "GET",
        url : "/getMessageByEvent/" + $routeParams.event
    }).then(function(response) {
        console.log(response.data);
        response = response.data ;

        $( response ).each(function( index , value) {
            row = {
                lottoId: value.lotId,
                destinatario: value.to,
                subject: value.subject,
                chiaveEsterna: value.extID,
                cc: value.cc
            };

            arrayData.push(row);

        });


        function createUsingFullOptions() {
            var initialParams = {
                count: 10 // initial page size
            };
            var initialSettings = {
                // page size buttons (right set of buttons)
                counts: [],
                // determines the pager buttons (left set of buttons)
                paginationMaxBlocks: 13,
                paginationMinBlocks: 2,
                dataset: arrayData
            };
            return new NgTableParams(initialParams, initialSettings);
        }

        $scope.tableParams = createUsingFullOptions();

    }) ;





}



function getLottoFails($scope, $http,$routeParams,NgTableParams){

    console.log("AVVIO CONTROLLER getLottoFails");
    console.log("Params: "+$routeParams.status);

    var response;
    //init table data view
    var $table = $('#table');
    var rows = {};

    //terminare implementazione
    $scope.$on('$viewContentLoaded', function(event) {
        console.log("************ CARICATA VIEW *****************");

    });


    //determino quale radio è stato selezionato
    switch($routeParams.status){
        case 'LOTTO_SCARTATO':

            $scope.statusLotti = "LOTTI SCARTATI";

            var self = this;
            var arrayData = [];


            $http({
                method : "GET",
                url : "/getLotsByStatus/LOTTO_SCARTATO"
            }).then(function(response) {
                console.log(response.data);
                response = response.data ;

                $( response ).each(function( index , value) {
                    row = {
                        lottoId: value.lotId,
                        servizio: value.service,
                        codiceCliente: value.customerCode,
                        numMessaggi: value.messagesProcessed,
                        dataCreazione: value.created
                    };

                    arrayData.push(row);

                });

                function createUsingFullOptions() {
                    var initialParams = {
                        count: 10 // initial page size
                    };
                    var initialSettings = {
                        // page size buttons (right set of buttons)
                        counts: [],
                        // determines the pager buttons (left set of buttons)
                        paginationMaxBlocks: 13,
                        paginationMinBlocks: 2,
                        dataset: arrayData
                    };
                    return new NgTableParams(initialParams, initialSettings);
                }

                $scope.tableParams = createUsingFullOptions();

            }) ;

            break;
        case 'LOTTI_IN_ATTESA_CHIUSURA':

            $scope.statusLotti = "LOTTI IN ATTESA DI CHIUSURA";

            var self = this;
            var arrayData = [];
            //var data = [{name: "Moroni", age: 50} , {name: "Moroni", age: 50} , .....];

            $http({
                method : "GET",
                url : "/getLotsAttesaChiusura"
            }).then(function(response) {
                console.log(response.data);
                response = response.data ;

                $( response ).each(function( index , value) {
                    row = {
                        lottoId: value.lotId,
                        servizio: value.service,
                        codiceCliente: value.customerCode,
                        numMessaggi: value.messagesProcessed,
                        dataCreazione: value.created
                    };

                    arrayData.push(row);

                });


                function createUsingFullOptions() {
                    var initialParams = {
                        count: 10 // initial page size
                    };
                    var initialSettings = {
                        // page size buttons (right set of buttons)
                        counts: [],
                        // determines the pager buttons (left set of buttons)
                        paginationMaxBlocks: 13,
                        paginationMinBlocks: 2,
                        dataset: arrayData
                    };
                    return new NgTableParams(initialParams, initialSettings);
                }

                $scope.tableParams = createUsingFullOptions();
                //$scope.tableParams = new NgTableParams({}, { dataset: arrayData});

            }) ;









            break;
    }
}

function reportMessaggi($scope, $http){

    console.log("REPORT MESSAGGI CONTROLLER");

}



function reportLotti($scope, $http){

    console.log("REPORT LOTTI CONTROLLER");

    $scope.myDate = new Date();
    $scope.isOpen = false;

}


function getLotsByIdStatus($scope, $http,SearchSection_Factory,NgTableParams,$location) {
    //alert("SEARCH PAGE!");

    $scope.beta = SearchSection_Factory ;



    console.log("AVVIO CONTROLLER getLotsByIdStatus per lots search page");

    // console.log("valoreA "+$scope.beta.search.lottoId);
    // console.log("valoreB "+$scope.beta.search.status);
    // console.log("valoreC "+$scope.beta.search.radioSelection);



//     function ajaxRequest(params) {
//
//         // data you may need
//         console.log(params.data);
//
//         $.ajax({
//             type: "GET",
//             url: "/getLotsByStatus/LOTTO_IN_ELABORAZIONE",
//             //data: "user-id=1",
// // You are expected to receive the generated JSON (json_encode($data))
//             dataType: "json",
//             success: function (data) {
//                 params.success({
// // By default, Bootstrap table wants a "rows" property with the data
//                     "rows": data,
// // You must provide the total item ; here let's say it is for array length
//                     "total": data.length
//                 })
//             },
//             error: function (er) {
//                 params.error(er);
//             }
//         });
//     }



        var response;
        //init table data view
        var $table = $('#table');
        var rows = {};

        var self = this;
        var arrayData = [];

        function createUsingFullOptions(arrayData) {
            var initialParams = {
                count: 10 // initial page size
            };
            var initialSettings = {
                // page size buttons (right set of buttons)
                counts: [],
                // determines the pager buttons (left set of buttons)
                paginationMaxBlocks: 13,
                paginationMinBlocks: 2,
                dataset: arrayData
            };
            return new NgTableParams(initialParams, initialSettings);
        }



        if (undefined !== $scope.beta.search && undefined !== $scope.beta.search.lottoId && $scope.beta.search.lottoId.trim() != "") {

            $scope.tipoRicerca = "DETTAGLIO LOTTI RICERCA PER ID LOTTO";

            $http({
                method : "GET",
                url : " /getLotsById/" + $scope.beta.search.lottoId
            }).then(function(response) {
                console.log(response.data);
                response = response.data ;


                row = {
                    lottoId: response.lotId,
                    servizio: response.service,
                    codiceCliente: response.customerCode,
                    numMessaggi: response.messagesProcessed,
                    dataCreazione: response.created
                };

                arrayData.push(row);
                $scope.tableParams = createUsingFullOptions(arrayData);
            }) ;



        }
        else if(undefined !== $scope.beta.search && $scope.beta.search.status.trim() != ""){

            $scope.tipoRicerca = "DETTAGLIO LOTTI RICERCA PER STATO";

            $http({
                method : "GET",
                url : "/getLotsByStatus/" + $scope.beta.search.status
            }).then(function(response) {
                console.log(response.data);
                response = response.data ;

                $( response ).each(function( index , value) {
                    row = {
                        lottoId: value.lotId,
                        servizio: value.service,
                        codiceCliente: value.customerCode,
                        numMessaggi: value.messagesProcessed,
                        dataCreazione: value.created
                    };

                    arrayData.push(row);

                });

                $scope.tableParams = createUsingFullOptions(arrayData);

            }) ;

        } else {
            //errore, nessun èarametro di ricerca inserito
            // row = {
            //     lottoId: "",
            //     servizio: "",
            //     codiceCliente: "",
            //     numMessaggi: "",
            //     dataCreazione: ""
            // };
            //
            // arrayData.push(row);

            //$scope.tableParams = createUsingFullOptions();
            //$scope.tipoRicerca = "<b style='color: red;'>Nessun parametro di ricerca impostato</b>";
            alert("Errore. Inserire almeno un parametro per la ricerca puntuale");
            $location.path("index.html");
        }


        // //determino quale radio è stato selezionato
        // switch($scope.beta.search.radioSelection){
        //     case 'radio_status':
        //
        //         $scope.tipoRicerca = "DETTAGLIO LOTTI RICERCA PER STATO";
        //
        //         $http({
        //             method : "GET",
        //             url : "/getLotsByStatus/" + $scope.beta.search.status
        //         }).then(function(response) {
        //             console.log(response.data);
        //             response = response.data ;
        //
        //             $( response ).each(function( index , value) {
        //                 row = {
        //                     lottoId: value.lotId,
        //                     servizio: value.service,
        //                     codiceCliente: value.customerCode,
        //                     numMessaggi: value.messagesProcessed,
        //                     dataCreazione: value.created
        //                 };
        //
        //
        //                 $table.bootstrapTable('insertRow', {
        //                     index: 1 + index ,
        //                     row:  row
        //                 })
        //
        //             });
        //         }) ;
        //
        //         break;
        //     case 'radio_lottoid':
        //
        //         $scope.tipoRicerca = "DETTAGLIO LOTTI RICERCA PER ID LOTTO";
        //
        //         $http({
        //             method : "GET",
        //             url : " /getLotsById/" + $scope.beta.search.lottoId
        //         }).then(function(response) {
        //             console.log(response.data);
        //             response = response.data ;
        //
        //             row = {
        //                 lottoId: response.lotId,
        //                 servizio: response.service,
        //                 codiceCliente: response.customerCode,
        //                 numMessaggi: response.messagesProcessed,
        //                 dataCreazione: response.created
        //             }
        //
        //             $table.bootstrapTable('insertRow', {
        //                 index: 1 ,
        //                 row:  row
        //             })
        //         }) ;
        //
        //
        //
        //         break;
        // }




}



function getMessageStatus($scope, $http) {
         console.log("AVVIO CONTROLLER getMessageStatus");

        $http({
            method : "GET",
            url : "/getNumberOfMessageFails"
        }).then(function(response) {
            //console.log(response.data);
            $scope.NumberOfMessageFails = response.data;

            if ( $scope.NumberOfMessageFails.MESSAGGIO_ACCETTAZIONE_KO == 0){
                $scope.classMessageAccettKO = "badge badge-success"
            }
            else{
                $scope.classMessageAccettKO = "badge badge-danger"
            }

            if ( $scope.NumberOfMessageFails.MESSAGGIO_CONSEGNA_KO == 0){
                $scope.classMessageConsegnaKO = "badge badge-success"
            }
            else{
                $scope.classMessageConsegnaKO = "badge badge-danger"
            }

            if ( $scope.NumberOfMessageFails.MESSAGGIO_INVIO_FALLITO == 0){
                $scope.classMessageInvioKO = "badge badge-success"
            }
            else{
                $scope.classMessageInvioKO = "badge badge-danger"
            }

        });
}



function serviceStatus($scope, $http){

    console.log("AVVIO CONTROLLER serviceStatus");


    $http({
        method: "GET",
        url: " /getServiceStatus/COMPOSER"
    }).then(function (response) {
        console.log(response.data);
        if (response.data == null || response.data == ""){
            $scope.imgMessaggiServeStatus = "img/red-dot.png";
        }
        else{
            $scope.imgMessaggiServeStatus = "img/green-dot.png";
        }
    });

    $http({
        method: "GET",
        url: " /getServiceStatus/SENDER"
    }).then(function (response) {
        console.log(response.data);
        if (response.data == null || response.data == ""){
            $scope.imgInvioServeStatus = "img/red-dot.png";
        }
        else{
            $scope.imgInvioServeStatus = "img/green-dot.png";
        }
    });

    $http({
        method: "GET",
        url: " /getServiceStatus/CONTROLLER"
    }).then(function (response) {
        console.log(response.data);
        if (response.data == null || response.data == ""){
            $scope.imgControlloServeStatus = "img/red-dot.png";
        }
        else{
            $scope.imgControlloServeStatus = "img/green-dot.png";
        }
    });

}



function getLotsStatus($scope, $http) {
    console.log("AVVIO CONTROLLER getLotsStatus");

    $http({
        method: "GET",
        url: "/getNumberOfLotsFails"
    }).then(function (response) {
        console.log(response.data);
        $scope.LotsOfMessageFails = response.data;

        if ( $scope.LotsOfMessageFails.LOTTO_SCARTATO == 0){
            $scope.classLotScartatoKO = "badge badge-success"
        }
        else{
            $scope.classLotScartatoKO = "badge badge-danger"
        }

        if ( $scope.LotsOfMessageFails.LOTTI_IN_ATTESA_CHIUSURA == 0){
            $scope.classLotAttChiusura = "badge badge-success"
        }
        else{
            $scope.classLotAttChiusura = "badge badge-danger"
        }



    });

}


    function ricercaLottiForm($scope, $http,SearchSection_Factory) {
        console.log("AVVIO CONTROLLER ricercaLottiForm");


            console.log($scope.lottoid);
            console.log($scope.lottoStatus);
            console.log($scope.chooseSearch);


            $scope.searchForm = SearchSection_Factory;





    }




