package it.posteitaliane.pec.sender;

import com.icegreen.greenmail.util.GreenMail;

import javax.mail.internet.MimeMessage;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 *
 */
public class EmailMessageAssertions {

    private final List<MimeMessage> messages;

    public static EmailMessageAssertions assertReceivedMessage(GreenMail smtpServer) {
        return new EmailMessageAssertions(Arrays.asList( smtpServer.getReceivedMessages() ) );
    }

    private EmailMessageAssertions(List<MimeMessage> messages) {
        this.messages = messages;
    }

    public EmailMessageAssertions from(String from) {
        findFirstOrElseThrow(m -> unchecked(m::getFrom).equals(from),
                assertionError("No message from [{0}] found!", from));
        return this;
    }

    public EmailMessageAssertions to(String... to) {
        findFirstOrElseThrow(m -> unchecked(m::getAllRecipients).equals(to),
                assertionError("No message to [{0}] found!", to));
        return this;
    }

    public EmailMessageAssertions withSubject(String subject) {
        Predicate<MimeMessage> predicate = m -> subject.equals(unchecked(m::getSubject));
        findFirstOrElseThrow(predicate,
                assertionError("No message with subject [{0}] found!", subject));
        return this;
    }

    public EmailMessageAssertions withContent(String content) {
        findFirstOrElseThrow(m -> {
            ThrowingSupplier<String> contentAsString =
                    () -> ((String) m.getContent()).trim();
            return content.equals(unchecked(contentAsString));
        }, assertionError("No message with content [{0}] found!", content));
        return this;
    }

    private void findFirstOrElseThrow(Predicate<MimeMessage> predicate, Supplier<AssertionError> exceptionSupplier) {
        messages.stream().filter(predicate)
                .findFirst().orElseThrow(exceptionSupplier);
    }


    private static Supplier<AssertionError> assertionError(String errorMessage, String... args) {
        return () -> new AssertionError(MessageFormat.format(errorMessage, args));
    }

    public static <T> T unchecked(ThrowingSupplier<T> supplier) {
        try {
            return supplier.get();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public interface ThrowingSupplier<T> {
        T get() throws Throwable;
    }

}
