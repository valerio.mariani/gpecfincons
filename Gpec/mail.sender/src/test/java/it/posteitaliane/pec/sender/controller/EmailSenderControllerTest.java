package it.posteitaliane.pec.sender.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.TestInstance.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(EmailSenderController.class)
@TestInstance(Lifecycle.PER_CLASS)
@DisplayName("Il Microservizio ''Message Sender'' invia le email")
public class EmailSenderControllerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendManagementControllerTest.class);

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("invocando il :path '/email' restituisce l'indice delle risorse esposte dal servizio - metodo home()")
    public void putProcessedValueForLotTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/email/send/someone@localhost"))
                .andExpect(status().isOk())
                // TODO: sostituire con l'elaborazione del risultato atteso reale...
                .andExpect(content().string("SUCCESS"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
        ;

    }

}
