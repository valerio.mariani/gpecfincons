package it.posteitaliane.pec.sender.services;


import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.mail.internet.AddressException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.stream.Stream;

import static it.posteitaliane.pec.sender.EmailMessageAssertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
public class EmailSenderServiceTest {

	@Value("${spring.mail.host}")
	private String host;

	@Value("${spring.mail.protocol}")
	private String protocol;

	@Value("${spring.mail.port}")
	private int port;

	@Value("${spring.mail.username:#{null}}")
	private String username;

	@Value("${spring.mail.password:#{null}}")
	private String password;

//	@Autowired
	private GreenMail smtpServer;
	@Autowired
	private EmailSenderService emailService;

	@BeforeAll
	public void setUp(){
		smtpServer = new GreenMail(new ServerSetup(port, null, protocol));
		smtpServer.setUser(username, password);
		smtpServer.start();
	}

	@AfterAll
	public void tearDown() throws Exception {
		smtpServer.stop();
	}

	@Test
	public void contextLoads() { }


	@Test
	public void sendSingleMail() throws AddressException {
		MailDeliveryRequest sending = MailDeliveryRequest.builder().
				to("test@localhost.com").
				messageID("G23L_20181229_T000_000000001").
				subject("TEST").
				lotId("G23L_20181229_000000").
				content("test from someone@localhost").
				events(Arrays.asList( Events.builder().event(EventType.MESSAGGIO_INVIO_SUCCESSO).time( LocalDateTime.now()).build())).
		build();

		emailService.sendEmail(sending,"someone@localhost.com", "G23L" );

		assertTrue(smtpServer.waitForIncomingEmail(5000, 1));

		Stream.of(smtpServer.getReceivedMessages()).forEach(msg -> {
			String content = (String) unchecked( msg::getContent );
			assertTrue(content.contains(sending.getContent()));
		});
//		EmailMessageAssertions.assertReceivedMessage(smtpServer)
////				.from("someone@localhost.com")
//				.to(sending.getEmail())
//				.withSubject(sending.getSubject())
//				.withContent(sending.getContent());
	}


}
