package it.posteitaliane.pec.sender;

import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.TestInstance.Lifecycle;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
@DisplayName("Ogni singolo messaggio inviato deve essere tracciato:")
public class MessageEventTrackingTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageEventTrackingTests.class);

    @Autowired
    MailDeliveryRepository mailDeliveryRepository;

    @Autowired
    LotRepository lotRepository;

    @Test
    public void loads() { }


    @Nested
    @TestInstance(Lifecycle.PER_CLASS)
    @DisplayName("salvando i dati del messaggio come documento su couchbase")
    class TrackMessage {

        String messageId = "G23L_20181229_T00000_000000001";
        String lotId = "G23L_20181229_T00000";

        @DisplayName("e di volta in volta  aggiungendo alla lista ''events'' un'evento di cui vogliamo tenere traccia ed il relativo timestamp, ad esempio: ")
        @ParameterizedTest(name = "traccio l''evento {0}")
        @ValueSource(strings = {"MESSAGGIO_INVIO_SUCCESSO", "MESSAGGIO_CONSEGNA_PARZIALE", "MESSAGGIO_ATTESA_RICEVUTA"})
        public void checkTrackingAppending(EventType event) {


            Optional<MailDeliveryRequest> message = mailDeliveryRepository.findById(messageId);


            MailDeliveryRequest deliveryMessage = message.orElseGet(
                    () -> {
                        //se non esiste su couchbase il messaggio con quell'id, lo creo ora:
                        MailDeliveryRequest messaggeCreatedNow =

                                MailDeliveryRequest.builder().
                                        to("someone@localhost").
                                        messageID(messageId).
                                        lotId(lotId).
                                        subject("test from someone@localhost").
                                        content("MESSAGGIO CREATO DA JUNIT TEST").
                                        events(Arrays.asList( Events.builder().event( EventType.MESSAGGIO_INVIO_SUCCESSO).time( LocalDateTime.now()).build())).
                                        build();


                        mailDeliveryRepository.save(messaggeCreatedNow);

                        return mailDeliveryRepository.findById(messageId).get();
                    });

            List<Events> events = deliveryMessage.getEvents();

            events.add( Events.builder().event(event).time( LocalDateTime.now()).build());

            mailDeliveryRepository.save(deliveryMessage);
        }

        @AfterAll
        public void checkTrackingData(){
            Optional<MailDeliveryRequest> delivery = mailDeliveryRepository.findById(messageId);
            delivery.orElseThrow(() -> new IllegalStateException("Messaggio corrispondente a messageId[" + messageId + "] non trovato!"));
            List<Events> events = delivery.get().getEvents();

            assertThat(events).extracting("event")
                    .containsOnly( /* EventType.MESSAGGIO_INVIATO, */ EventType.MESSAGGIO_INVIO_SUCCESSO, EventType.MESSAGGIO_CONSEGNA_PARZIALE );
            events.stream().forEach(e -> LOGGER.info("found event trace: " + e.toString()));

       //     mailDeliveryRepository.delete(delivery.get());
        }
    }//FINE CLASSE ANNIDATA TrackMessage

    //insieme di lotId di esempio
    final static String[] lots = new String[]{
            "G23L-20181130-000068",
            "G23L-20181206-000069",
            "G23L-20181206-000075",
            "G23L-20181130-000084"};

    @Nested
    @TestInstance(Lifecycle.PER_CLASS)
    @DisplayName("potendo poi rintracciare tutti gli invii del lotto... ")
    class CountTraces {

        Random r = new Random();

        Stream<Arguments> lotIdsAndMessageIdsProvider() {

            return Stream.of( lots ).map( lotId -> {

                int index = r.ints(0, 9).limit(1).findFirst().getAsInt();
                String[] messageIds = map.get(lotId);
                //ritorno un Arguments di lotId e uno dei messageId di quel lotto
                return Arguments.of( lotId, messageIds[index] );
             });

        }

        List<String> lotIds = Arrays.asList( lots ) ;
        Map<String, String[]> map = new HashMap<>();

        @BeforeAll //preparo , per i lotti forniti in input, una serie di message id randomici, e salvo tutto su CB
        public void buildMessagesDataSet(){

            lotIds.stream().forEach(lotId -> {

                Stream<String> uuidStream = Stream.
                        generate(UUID::randomUUID).
                        map(u -> u.toString());

                String[] messageIds = uuidStream.
                        limit(10).toArray(String[]::new);

                LotDelivery lotDoc = lotRepository.save(LotDelivery.builder().
                        lotId(lotId).
                        messagesProcessed(58).
                        customerCode("G23L").
                        service("23L").
                        created("2018-12-28").
                        currentStatus(EventType.LOTTO_IN_ELABORAZIONE).
                        mailDeliveriesIDs(Arrays.asList(messageIds)).
                        build());
                LOGGER.info("created LOT " + lotDoc);
                map.put(lotId, messageIds);
            });
        }

        @DisplayName("a partire dal message-id")
        @ParameterizedTest(name = "lotto da ottenere: {0} - message-id {1}")
        @MethodSource("lotIdsAndMessageIdsProvider")
        public void extract_all_messages_by_given_event_of_the_same_LOT(String lotId, String oneOfTheMessageId){
            LotDelivery result = lotRepository.findLotDeliveryByMailDeliveriesIDsIsIn(oneOfTheMessageId);
            assertThat(result.getLotId()).isEqualTo(lotId);
            assertThat(result.getMailDeliveriesIDs()).contains(oneOfTheMessageId);
        }

    }



}
