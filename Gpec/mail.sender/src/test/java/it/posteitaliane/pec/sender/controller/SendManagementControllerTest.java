package it.posteitaliane.pec.sender.controller;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(SendManagementController.class)
@DisplayName("Il Microservizio ''Message Sender'' aggiorna il numero di invii per ogni lotto")
public class SendManagementControllerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendManagementControllerTest.class);

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("invocando il :path '/lot-proccessing' restituisce l'indice delle risorse esposte dal servizio - metodo home()")
    public void putProcessedValueForLotTest() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.post("/lot-processing/create", "lot=G23L_20181229_T000","value=45"))
                .andExpect(status().isOk())
                // TODO: sostituire con l'elaborazione del risultato atteso reale...
                .andExpect(content().string("Data is stored."))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
        ;


        mockMvc.perform(MockMvcRequestBuilders.post("/lot-processing/current", "lot=G23L_20181229_T000"))
                .andExpect(status().isOk())
                // TODO: sostituire con l'elaborazione del risultato atteso reale...
                .andExpect(content().string("45"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.TEXT_PLAIN))
        ;
    }

}
