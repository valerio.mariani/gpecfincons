package it.posteitaliane.pec.smtp.transport.factory;

import it.posteitaliane.pec.smtp.transport.connection.ClosableSmtpConnection;
import it.posteitaliane.pec.smtp.transport.connection.DefaultClosableSmtpConnection;
import it.posteitaliane.pec.smtp.transport.strategy.ConnectionStrategy;
import it.posteitaliane.pec.smtp.transport.strategy.TransportStrategy;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.event.TransportListener;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.text.MessageFormat.*;

/**
 * A part of the code of this class is taken from the Spring <a href="http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/mail/javamail/JavaMailSenderImpl.html">JavaMailSenderImpl class</a>.
 */
public class SmtpConnectionFactory implements PooledObjectFactory<ClosableSmtpConnection> {

  private static final Logger LOG = LoggerFactory.getLogger(SmtpConnectionFactory.class);

  protected final Session session;

  protected final TransportStrategy transportFactory;
  protected final ConnectionStrategy connectionStrategy;

  protected Collection<TransportListener> defaultTransportListeners;

  public SmtpConnectionFactory(Session session, TransportStrategy transportStrategy, ConnectionStrategy connectionStrategy, Collection<TransportListener> defaultTransportListeners) {
    this.session = session;
    this.transportFactory = transportStrategy;
    this.connectionStrategy = connectionStrategy;
    this.defaultTransportListeners = new ArrayList<>(defaultTransportListeners);
  }

  public SmtpConnectionFactory(Session session, TransportStrategy transportFactory, ConnectionStrategy connectionStrategy) {
    this(session, transportFactory, connectionStrategy, Collections.<TransportListener>emptyList());
  }


  @Override
  public PooledObject<ClosableSmtpConnection> makeObject() throws Exception {
    LOG.debug(format("makeSmtpConnection [{0}]", session.getProperties()) );

    Transport transport = transportFactory.getTransport(session);
    connectionStrategy.connect(transport);

    DefaultClosableSmtpConnection closableSmtpTransport = new DefaultClosableSmtpConnection(transport);
    initDefaultListeners(closableSmtpTransport);

    return new DefaultPooledObject(closableSmtpTransport);
  }

  @Override
  public void destroyObject(PooledObject<ClosableSmtpConnection> pooledObject) throws Exception {
    try {
      if (LOG.isDebugEnabled()) {
        LOG.debug(format("destroySmtpConnection [{0}]", pooledObject.getObject().isConnected()) );
      }
      pooledObject.getObject().clearListeners();
      pooledObject.getObject().getDelegate().close();
    } catch (Exception e) {
      LOG.warn(e.getMessage(), e);
    }
  }

  @Override
  public boolean validateObject(PooledObject<ClosableSmtpConnection> pooledObject) {
    boolean connected = pooledObject.getObject().isConnected();
    LOG.debug(format("Is connected [{0}]", connected) );
    return connected;
  }


  @Override
  public void activateObject(PooledObject<ClosableSmtpConnection> pooledObject) throws Exception {
    initDefaultListeners(pooledObject.getObject());
  }

  @Override
  public void passivateObject(PooledObject<ClosableSmtpConnection> pooledObject) throws Exception {
    if (LOG.isDebugEnabled()) {
      LOG.debug( format("passivateSmtpConnection [{0}]", pooledObject.getObject().isConnected()) );
    }
    pooledObject.getObject().clearListeners();
  }


  public void setDefaultListeners(Collection<TransportListener> listeners) {
    defaultTransportListeners = new CopyOnWriteArrayList<>(Objects.requireNonNull(listeners));
  }

  public List<TransportListener> getDefaultListeners() {
    return new ArrayList<>(defaultTransportListeners);
  }

  public Session getSession() {
    return session;
  }

  public TransportStrategy getTransportFactory() {
    return transportFactory;
  }

  public ConnectionStrategy getConnectionStrategy() {
    return connectionStrategy;
  }

  private void initDefaultListeners(ClosableSmtpConnection smtpTransport) {
    LOG.debug("ClosableSmtpConnection: configuring transport listeners " + defaultTransportListeners.size() );
    for (TransportListener transportListener : defaultTransportListeners) {
      smtpTransport.addTransportListener(transportListener);
    }
  }
}