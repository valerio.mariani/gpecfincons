package it.posteitaliane.pec.smtp.pool;

import it.posteitaliane.pec.smtp.transport.connection.ClosableSmtpConnection;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.SwallowedExceptionListener;
import org.apache.commons.pool2.impl.AbandonedConfig;
import org.apache.commons.pool2.impl.DefaultPooledObjectInfo;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import javax.mail.Session;
import javax.management.ObjectName;
import java.util.Set;

/**
 *
 */
public class SmtpConnectionPoolDelegator {

    SmtpConnectionPool smtpConnectionPool;

    public SmtpConnectionPoolDelegator(SmtpConnectionPool smtpConnectionPool) {
        this.smtpConnectionPool = smtpConnectionPool;
    }

    public ClosableSmtpConnection borrowObject() throws Exception {
        return smtpConnectionPool.borrowObject();
    }

    public ClosableSmtpConnection borrowObject(long borrowMaxWaitMillis) throws Exception {
        return smtpConnectionPool.borrowObject(borrowMaxWaitMillis);
    }

    public Session getSession() {
        return smtpConnectionPool.getSession();
    }

    public int getMaxIdle() {
        return smtpConnectionPool.getMaxIdle();
    }

    public void setMaxIdle(int maxIdle) {
        smtpConnectionPool.setMaxIdle(maxIdle);
    }

    public void setMinIdle(int minIdle) {
        smtpConnectionPool.setMinIdle(minIdle);
    }

    public int getMinIdle() {
        return smtpConnectionPool.getMinIdle();
    }

    public boolean isAbandonedConfig() {
        return smtpConnectionPool.isAbandonedConfig();
    }

    public boolean getLogAbandoned() {
        return smtpConnectionPool.getLogAbandoned();
    }

    public boolean getRemoveAbandonedOnBorrow() {
        return smtpConnectionPool.getRemoveAbandonedOnBorrow();
    }

    public boolean getRemoveAbandonedOnMaintenance() {
        return smtpConnectionPool.getRemoveAbandonedOnMaintenance();
    }

    public int getRemoveAbandonedTimeout() {
        return smtpConnectionPool.getRemoveAbandonedTimeout();
    }

    public void setConfig(GenericObjectPoolConfig conf) {
        smtpConnectionPool.setConfig(conf);
    }

    public void setAbandonedConfig(AbandonedConfig abandonedConfig) {
        smtpConnectionPool.setAbandonedConfig(abandonedConfig);
    }

    public PooledObjectFactory<ClosableSmtpConnection> getFactory() {
        return smtpConnectionPool.getFactory();
    }

    public void returnObject(ClosableSmtpConnection obj) {
        smtpConnectionPool.returnObject(obj);
    }

    public void invalidateObject(ClosableSmtpConnection obj) throws Exception {
        smtpConnectionPool.invalidateObject(obj);
    }

    public void clear() {
        smtpConnectionPool.clear();
    }

    public int getNumActive() {
        return smtpConnectionPool.getNumActive();
    }

    public int getNumIdle() {
        return smtpConnectionPool.getNumIdle();
    }

    public void close() {
        smtpConnectionPool.close();
    }

    public void evict() throws Exception {
        smtpConnectionPool.evict();
    }

    public void preparePool() throws Exception {
        smtpConnectionPool.preparePool();
    }

    public void addObject() throws Exception {
        smtpConnectionPool.addObject();
    }

    public void use(ClosableSmtpConnection pooledObject) {
        smtpConnectionPool.use(pooledObject);
    }

    public int getNumWaiters() {
        return smtpConnectionPool.getNumWaiters();
    }

    public String getFactoryType() {
        return smtpConnectionPool.getFactoryType();
    }

    public Set<DefaultPooledObjectInfo> listAllObjects() {
        return smtpConnectionPool.listAllObjects();
    }

    public int getMaxTotal() {
        return smtpConnectionPool.getMaxTotal();
    }

    public void setMaxTotal(int maxTotal) {
        smtpConnectionPool.setMaxTotal(maxTotal);
    }

    public boolean getBlockWhenExhausted() {
        return smtpConnectionPool.getBlockWhenExhausted();
    }

    public void setBlockWhenExhausted(boolean blockWhenExhausted) {
        smtpConnectionPool.setBlockWhenExhausted(blockWhenExhausted);
    }

    public long getMaxWaitMillis() {
        return smtpConnectionPool.getMaxWaitMillis();
    }

    public void setMaxWaitMillis(long maxWaitMillis) {
        smtpConnectionPool.setMaxWaitMillis(maxWaitMillis);
    }

    public boolean getLifo() {
        return smtpConnectionPool.getLifo();
    }

    public boolean getFairness() {
        return smtpConnectionPool.getFairness();
    }

    public void setLifo(boolean lifo) {
        smtpConnectionPool.setLifo(lifo);
    }

    public boolean getTestOnCreate() {
        return smtpConnectionPool.getTestOnCreate();
    }

    public void setTestOnCreate(boolean testOnCreate) {
        smtpConnectionPool.setTestOnCreate(testOnCreate);
    }

    public boolean getTestOnBorrow() {
        return smtpConnectionPool.getTestOnBorrow();
    }

    public void setTestOnBorrow(boolean testOnBorrow) {
        smtpConnectionPool.setTestOnBorrow(testOnBorrow);
    }

    public boolean getTestOnReturn() {
        return smtpConnectionPool.getTestOnReturn();
    }

    public void setTestOnReturn(boolean testOnReturn) {
        smtpConnectionPool.setTestOnReturn(testOnReturn);
    }

    public boolean getTestWhileIdle() {
        return smtpConnectionPool.getTestWhileIdle();
    }

    public void setTestWhileIdle(boolean testWhileIdle) {
        smtpConnectionPool.setTestWhileIdle(testWhileIdle);
    }

    public long getTimeBetweenEvictionRunsMillis() {
        return smtpConnectionPool.getTimeBetweenEvictionRunsMillis();
    }

    public void setTimeBetweenEvictionRunsMillis(long timeBetweenEvictionRunsMillis) {
        smtpConnectionPool.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
    }

    public int getNumTestsPerEvictionRun() {
        return smtpConnectionPool.getNumTestsPerEvictionRun();
    }

    public void setNumTestsPerEvictionRun(int numTestsPerEvictionRun) {
        smtpConnectionPool.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
    }

    public long getMinEvictableIdleTimeMillis() {
        return smtpConnectionPool.getMinEvictableIdleTimeMillis();
    }

    public void setMinEvictableIdleTimeMillis(long minEvictableIdleTimeMillis) {
        smtpConnectionPool.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
    }

    public long getSoftMinEvictableIdleTimeMillis() {
        return smtpConnectionPool.getSoftMinEvictableIdleTimeMillis();
    }

    public void setSoftMinEvictableIdleTimeMillis(long softMinEvictableIdleTimeMillis) {
        smtpConnectionPool.setSoftMinEvictableIdleTimeMillis(softMinEvictableIdleTimeMillis);
    }

    public String getEvictionPolicyClassName() {
        return smtpConnectionPool.getEvictionPolicyClassName();
    }

    public void setEvictionPolicyClassName(String evictionPolicyClassName) {
        smtpConnectionPool.setEvictionPolicyClassName(evictionPolicyClassName);
    }

    public long getEvictorShutdownTimeoutMillis() {
        return smtpConnectionPool.getEvictorShutdownTimeoutMillis();
    }

    public void setEvictorShutdownTimeoutMillis(long evictorShutdownTimeoutMillis) {
        smtpConnectionPool.setEvictorShutdownTimeoutMillis(evictorShutdownTimeoutMillis);
    }

    public boolean isClosed() {
        return smtpConnectionPool.isClosed();
    }

    public ObjectName getJmxName() {
        return smtpConnectionPool.getJmxName();
    }

    public String getCreationStackTrace() {
        return smtpConnectionPool.getCreationStackTrace();
    }

    public long getBorrowedCount() {
        return smtpConnectionPool.getBorrowedCount();
    }

    public long getReturnedCount() {
        return smtpConnectionPool.getReturnedCount();
    }

    public long getCreatedCount() {
        return smtpConnectionPool.getCreatedCount();
    }

    public long getDestroyedCount() {
        return smtpConnectionPool.getDestroyedCount();
    }

    public long getDestroyedByEvictorCount() {
        return smtpConnectionPool.getDestroyedByEvictorCount();
    }

    public long getDestroyedByBorrowValidationCount() {
        return smtpConnectionPool.getDestroyedByBorrowValidationCount();
    }

    public long getMeanActiveTimeMillis() {
        return smtpConnectionPool.getMeanActiveTimeMillis();
    }

    public long getMeanIdleTimeMillis() {
        return smtpConnectionPool.getMeanIdleTimeMillis();
    }

    public long getMeanBorrowWaitTimeMillis() {
        return smtpConnectionPool.getMeanBorrowWaitTimeMillis();
    }

    public long getMaxBorrowWaitTimeMillis() {
        return smtpConnectionPool.getMaxBorrowWaitTimeMillis();
    }

    public SwallowedExceptionListener getSwallowedExceptionListener() {
        return smtpConnectionPool.getSwallowedExceptionListener();
    }

    public void setSwallowedExceptionListener(SwallowedExceptionListener swallowedExceptionListener) {
        smtpConnectionPool.setSwallowedExceptionListener(swallowedExceptionListener);
    }
}
