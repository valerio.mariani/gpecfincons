package it.posteitaliane.pec.smtp.transport.factory;


import it.posteitaliane.pec.sender.model.ServiceConfiguration;
import it.posteitaliane.pec.smtp.transport.strategy.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.event.TransportListener;
import java.util.*;

import static java.util.Objects.requireNonNull;
import static it.posteitaliane.pec.smtp.transport.strategy.ConnectionStrategyFactory.newConnectionStrategy;

/**
 * A part of the code of this class is taken from the Spring
 * <a href="http://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/mail/javamail/JavaMailSenderImpl.html">JavaMailSenderImpl class</a>.
 * <br><br>
 * {@link SmtpConnectionFactory} builder<br><br>
 * <p>
 * If no {@link Session} is provided, a default one is created.<br>
 * If any of the host , port, username, password properties are provided the factory is initialized with the {@link ConnectionStrategyFactory#newConnectionStrategy(String, int, String, String)}
 * otherwise with the {@link ConnectionStrategyFactory#newConnectionStrategy()}<br>
 * If the protocol is provided the factory is initialized with the {@link TransportStrategyFactory#newProtocolStrategy}
 * otherwise with the {@link TransportStrategyFactory#newSessiontStrategy()} ()}<br>
 */
public class SmtpConnectionFactoryBuilder {

    protected Session session = null;
    protected String protocol = null;
    protected String host = null;
    protected int port = -1;
    protected String username;
    protected String password;

    protected List<TransportListener> defaultTransportListeners = Collections.emptyList();
//    private Map<String, List<MailAccount>> accounts;
    private List<ServiceConfiguration> serviceGroup;
    private FETCHING_STRATEGY accountFetchStrategy = null;

    private static final Logger LOG = LoggerFactory.getLogger(SmtpConnectionFactoryBuilder.class);

    private SmtpConnectionFactoryBuilder() {
    }

    public static SmtpConnectionFactoryBuilder newSmtpBuilder() {
        return new SmtpConnectionFactoryBuilder();
    }

    public SmtpConnectionFactoryBuilder session(Properties properties) {
        this.session = Session.getInstance(properties);
        return this;
    }

    public SmtpConnectionFactoryBuilder session(Properties properties, Authenticator authenticator) {
        this.session = Session.getInstance(properties, authenticator);
        return this;
    }

    public SmtpConnectionFactoryBuilder session(Session session) {
        this.session = requireNonNull(session);
        return this;
    }

    public SmtpConnectionFactoryBuilder protocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public SmtpConnectionFactoryBuilder serviceGroup(List<ServiceConfiguration> serviceGroup) {
        this.serviceGroup = serviceGroup;
        return this;
    }


    public SmtpConnectionFactoryBuilder accountFetchStrategy(FETCHING_STRATEGY fetchStrategy) {
        this.accountFetchStrategy = fetchStrategy;
        return this;
    }


//    public SmtpConnectionFactoryBuilder accounts( Map<String, List<MailAccount>>  accounts) {
//        this.accounts = accounts;
//        return this;
//    }

    public SmtpConnectionFactoryBuilder host(String host) {
        this.host = host;
        return this;
    }

    public SmtpConnectionFactoryBuilder port(int port) {
        this.port = port;
        return this;
    }

    public SmtpConnectionFactoryBuilder username(String username) {
        this.username = username;
        return this;
    }

    public SmtpConnectionFactoryBuilder password(String password) {
        this.password = password;
        return this;
    }

    public SmtpConnectionFactoryBuilder defaultTransportListeners(TransportListener... listeners) {
        defaultTransportListeners = Arrays.asList(requireNonNull(listeners));
        return this;
    }

    /**
     * Build the {@link SmtpConnectionFactory}
     *
     * @return
     */
    public SmtpConnectionFactory build() {
        if (session == null) {
            session = Session.getInstance(new Properties());
        }

        TransportStrategy transportStrategy = protocol == null ? TransportStrategyFactory.newSessiontStrategy() : TransportStrategyFactory.newProtocolStrategy(protocol);

        ConnectionStrategy connectionStrategy;

        if (host == null && port == -1 && username == null && password == null) {

            connectionStrategy = ConnectionStrategyFactory.newConnectionStrategy();

        } else if(accountFetchStrategy != null) {

            LOG.info("accountFetchStrategy diverso da NULL, host, port "+host +" , " +port);

            connectionStrategy = ConnectionStrategyFactory.accountFetchConnectionStrategy(host, port, accountFetchStrategy, serviceGroup);
        } else {
            LOG.info(" accountFetchStrategy e' NULL host, port "+host +" , " +port);
            LOG.info("username "+username );
            connectionStrategy = ConnectionStrategyFactory.newConnectionStrategy(host, port, username, password);
        }

        return new SmtpConnectionFactory(session, transportStrategy, connectionStrategy, defaultTransportListeners);
    }
}