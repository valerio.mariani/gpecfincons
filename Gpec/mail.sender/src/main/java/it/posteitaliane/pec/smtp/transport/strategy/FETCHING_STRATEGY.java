package it.posteitaliane.pec.smtp.transport.strategy;

public enum FETCHING_STRATEGY {
    RANDOM,
    ROUND_ROBIN,
    SERVICE_BASED
}
