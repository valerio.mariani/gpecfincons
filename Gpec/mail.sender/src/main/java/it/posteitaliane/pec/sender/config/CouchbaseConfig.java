package it.posteitaliane.pec.sender.config;

import com.couchbase.client.java.Bucket;
import com.netflix.appinfo.InstanceInfo;
import it.posteitaliane.pec.common.configuration.DefaultCouchBaseConfiguration;
import it.posteitaliane.pec.common.couchbase.DocumentAuditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.core.query.Consistency;
import org.springframework.data.couchbase.repository.auditing.EnableCouchbaseAuditing;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;
import org.springframework.data.couchbase.repository.support.IndexManager;

import java.util.Arrays;
import java.util.List;

@Configuration
@EnableCouchbaseRepositories(basePackages={"it.posteitaliane.pec.common.repositories"})
@EnableCouchbaseAuditing //this activates auditing
public class CouchbaseConfig extends DefaultCouchBaseConfiguration {

    @Autowired
    InstanceInfo currentInstanceInfo;

    public CouchbaseConfig(
            @Value("${spring.couchbase.bucket.password}")
                    String lotBucketPassword,
            @Value("${spring.couchbase.bucket.name}")
                    String bucketName,
            @Value("${spring.couchbase.bootstrap-hosts}")
                    String[] couchbaseBootstrapHosts) {
        super(lotBucketPassword, bucketName, couchbaseBootstrapHosts);
    }


    // this creates the auditor aware bean that will feed the annotations
    @Bean
    public DocumentAuditor testAuditorAware() {
        return new DocumentAuditor(currentInstanceInfo.getId());
    }}
