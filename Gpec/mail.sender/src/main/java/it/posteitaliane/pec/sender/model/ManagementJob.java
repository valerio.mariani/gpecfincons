package it.posteitaliane.pec.sender.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

@Getter @Setter
@ToString
@Builder
public class ManagementJob implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManagementJob.class);

    private String jobName;
    private JobPriority jobPriority;
    private Runnable task;

    @Override
    public void run() {
        LOGGER.debug("Running management job: " + toString());
        try {
            task.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
