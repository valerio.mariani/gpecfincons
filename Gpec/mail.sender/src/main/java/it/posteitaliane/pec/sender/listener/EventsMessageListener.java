package it.posteitaliane.pec.sender.listener;

import brave.Span;
import brave.Tracer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rabbitmq.client.Channel;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.sender.config.RabbitMQConfig;
import it.posteitaliane.pec.sender.services.ListenersContainersRegistry;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.Map;

@Component
public class EventsMessageListener implements ChannelAwareMessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventsMessageListener.class);

    @Autowired Gson gson;
    @Autowired TopicExchange globalManagementExchange;
    @Autowired ListenersContainersRegistry listenersContainersRegistry;
    @Autowired Tracer tracer;

//    @RabbitListener(
//            id = "SENDER_events_receiver",
//            containerFactory = RabbitMQConfig.rabbitListenerContainerFactory,
////            bindings = {
////                    @QueueBinding(value =
////                            @org.springframework.amqp.rabbit.annotation.
////                                    Queue(value = "management_queue_for_sender_1"),
////                            exchange = @Exchange(value = ""))
////            },
//            queues = {
//                "${rabbit.custom.queue.event.management}" + "_sender[1]"
////                , "${rabbit.custom.queue.event.process}"
//            })
    @Override
    public void onMessage(Message rabbitMessagePayload, Channel channel) throws Exception {

        String response = StringUtils.EMPTY;
        LOGGER.info(" * * * * * * * * * * * Payload: " +
                "\n * * * * * " + new String( rabbitMessagePayload.getBody() ) + "\n * * * * * * * * * * * * " );

        LOGGER.info("SENDER EVENT CONSUMER[ " + hashCode() + " ] : " + rabbitMessagePayload);
        String eventType = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("EVENT-TYPE");

        switch (EventType.valueOf(eventType)){
            case LOTTO_RICEVUTO:
                String lotId = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("lotId");
                // Start a new trace or a span within an existing trace representing an operation
                Span span = tracer.nextSpan().name("lot[" + lotId + "].prepare.consumers").start();

                try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {

                    // il registry si occupa di creare e mantenere un riferimento ai container del lotto in modo da
                    // gestirne lo start/stop e cancellazione alla fine della fase di elaborazione..
                    listenersContainersRegistry.createAndRegisterLotListenerContainer(lotId);

                } catch (Exception e) {
                    LOGGER.error(e.getLocalizedMessage());
                    span.error(e);
                } finally {
                    span.finish(); // always finish the span
                }
                break;

            case LOTTO_OUTCOME_GENERATO:
                lotId = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("lotId");
                span = tracer.nextSpan().name("lot[" + lotId + "].remove.consumers").start();
                try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {

                    listenersContainersRegistry.stopListener(lotId, true);

                } catch (Exception e) {
                    LOGGER.error(e.getLocalizedMessage());
                    span.error(e);
                } finally {
                    span.finish(); // always finish the span
                }
                break;
            case SYSTEM_SUSPEND:
                span = tracer.nextSpan().name("system.suspend.processing").start();
                try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {

                    listenersContainersRegistry.stopAllRunningContainers();

                } catch (Exception e) {
                    LOGGER.error(e.getLocalizedMessage());
                    span.error(e);
                } finally {
                    span.finish(); // always finish the span
                }
                break;

            case SYSTEM_RESTART:
                // TODO ancora da implementare:
                // difficoltà: non si possono semplicemente riavviare tutti i container (forse)
                // ma bisogna ripristinare lo stato così come è stato lasciato alla ricezione dell'evento SUSPEND
                break;
            default:
                break;
        }

    }

    /**
     *
     * @param payload
     * @param rootValue
     * @return
     */
    protected Map<String, Object> getAsMap(byte[] payload, String rootValue) {
        Type type = new TypeToken<Map<String, Object>>() {}.getType();
        return gson.fromJson(new String(payload), type);
    }


}
