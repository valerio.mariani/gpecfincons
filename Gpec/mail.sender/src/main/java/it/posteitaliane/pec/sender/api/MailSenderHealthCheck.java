package it.posteitaliane.pec.sender.api;

import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.QueueInfo;
import it.posteitaliane.pec.smtp.pool.SmtpConnectionPoolDelegator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


/**
 * per customizzare le metriche da cui estrarre lo stato di salute di questo servizio per il monitoraggio
 */
@Component
public class MailSenderHealthCheck implements HealthIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailSenderHealthCheck.class);

    @Autowired RabbitListenerEndpointRegistry rabbitListenerEndpointRegistry;
    @Autowired Client rabbitApiClient;

    public Health health() {
        int errorCode = check(); // perform some specific health check
        if (errorCode != 0) {
            return Health.down()
                    .withDetail("Error Code", errorCode).build();
        } // else return checkSmtpConnectionPool();
        return Health.up().build();
    }

    public int check() {
        // Our logic to check health
        if(smtpConnectionPool == null) { return 1; }
        if(competingConsumersContainerFactory == null) return 2;
        if(simpleRabbitListenerContainerFactory == null) return 3;
        if(factory == null) return 4;
        if(smtpConnectionPool.isClosed()) return 5;

        LOGGER.info( checkSmtpConnectionPool().toString() );

        return 0;
    }




    @Autowired CachingConnectionFactory factory;
    @Autowired SmtpConnectionPoolDelegator smtpConnectionPool;
    @Autowired SimpleRabbitListenerContainerFactory competingConsumersContainerFactory;
    @Autowired SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory;

    public Health checkSmtpConnectionPool(){
        Long meanWaitConnectionCreationTime = smtpConnectionPool.getMeanBorrowWaitTimeMillis();
        Long meanIdleTime = smtpConnectionPool.getMeanIdleTimeMillis();
        Integer connectionIdle = smtpConnectionPool.getNumIdle();
        Integer waitingConnections = smtpConnectionPool.getNumWaiters();
        Integer activeConnections = smtpConnectionPool.getNumActive();

        Map<String, Long> details = new HashMap<>();
        details.put("SMTP Idle connects", connectionIdle.longValue());
        details.put("SMTP Waiting connects", waitingConnections.longValue());
        details.put("SMTP Active connects", activeConnections.longValue());
        details.put("SMTP mean idle time", meanIdleTime);
        details.put("SMTP mean wait time to borrow object from pool", meanWaitConnectionCreationTime);
        return Health.status(
                ( connectionIdle == activeConnections  ||  ( activeConnections - connectionIdle > 3 ) ||
                    waitingConnections > 100  ? Status.DOWN : Status.UP)
            ).withDetails(details).build();
    }


    public Health checkMessageBrokerStatus(){
        if(rabbitListenerEndpointRegistry.isRunning()){

            int containers = rabbitListenerEndpointRegistry.getListenerContainers().size();
            rabbitListenerEndpointRegistry.getListenerContainers().stream().forEach(container ->
            {
                SimpleMessageListenerContainer smlc = (SimpleMessageListenerContainer)container;
                smlc.getActiveConsumerCount();
                Arrays.stream(smlc.getQueueNames()).forEachOrdered(s -> {
                    QueueInfo queueInfo = rabbitApiClient.getQueue("/", s);
                    String idleTime = queueInfo.getIdleSince();
                    queueInfo.getMessageStats().getBasicDeliverNoAck();

                    if(queueInfo.getConsumerCount()// totale dei consumer della coda
                            >= smlc.getActiveConsumerCount()){

                    }
                });

            });
        }

        return Health.up().withDetail("rabbit clients status", "not_evaluated").build();
    }
}
