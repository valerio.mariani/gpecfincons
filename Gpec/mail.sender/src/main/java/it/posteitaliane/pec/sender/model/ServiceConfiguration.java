package it.posteitaliane.pec.sender.model;


import lombok.*;

import java.util.ArrayList;
import java.util.List;


//@Data
//@ToString
//@AllArgsConstructor
//@Builder
//@EqualsAndHashCode
public class ServiceConfiguration {

    private String service;
    private List<MailAccount> accounts = new ArrayList<>();

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public List<MailAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<MailAccount> accounts) {
        this.accounts = accounts;
    }
}
