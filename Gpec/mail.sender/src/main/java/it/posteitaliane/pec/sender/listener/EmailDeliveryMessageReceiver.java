package it.posteitaliane.pec.sender.listener;

import brave.ScopedSpan;
import brave.Tracer;
import com.rabbitmq.client.Channel;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import it.posteitaliane.pec.sender.services.EmailSenderService;
import org.assertj.core.util.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.internet.AddressException;

import static org.assertj.core.api.Assertions.*;

@Component
public class EmailDeliveryMessageReceiver implements ChannelAwareMessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailDeliveryMessageReceiver.class);

    //TODO: AL POSTO DI QUESTO fromAccount STATICO VA CREATA E INIETTATA UNA CLASSE CHE FA DA PROVIDER DI fromAccount
    @Value("${sender.email.defaults.account.from}")
    private String fromAccount;

    @Autowired Tracer tracer;
    @Autowired EmailSenderService emailService;
    @Autowired MailDeliveryRepository mailDeliveryRepository;
    @Autowired MessageConverter converter;

    /**
     *
     * @param rabbitMessagePayload
     */
    public void onMessage(Message rabbitMessagePayload, Channel channel) throws Exception {
        ScopedSpan span = tracer.startScopedSpan("mail.sender.message.receive");
        long tag = rabbitMessagePayload.getMessageProperties().getDeliveryTag();
        try {
            assertThat( rabbitMessagePayload.getMessageProperties().getHeaders())
                .as("Verificare se gli headers del rabbitMessagePayload contengono i valori obbligatori")
                .containsKeys("service");
            String service = (String) rabbitMessagePayload.getMessageProperties().getHeaders().get("service");

            MailDeliveryRequest message = (MailDeliveryRequest) converter.fromMessage(rabbitMessagePayload);

            LOGGER.debug("Received from RABBIT-MQ <" + message + ">");
            span.annotate("mail delivery arrived");
            span.tag("RabbitMQ - Message ID", message.getMessageID());
            span.tag("RabbitMQ - service", service );
            // Tolta la save su CB da qui e messa nel composer
            // L'update del messagge su CB lo fara', dopo l'invio mail, il traceEventToMessageDelivery di MailStatusListener
            emailService.sendEmail(message, fromAccount, service);

        } catch (AddressException addressError){
            span.error(addressError);
            channel.basicNack(tag, false, false);
        } catch (AssertionError ae){
            span.error(ae);
            throw new AmqpRejectAndDontRequeueException("MESSAGE PARAMETERS ERRORS: " + ae.getMessage());
        } finally {
            span.finish(); // always finish the span
        }
    }
}
