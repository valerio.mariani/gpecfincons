package it.posteitaliane.pec.sender.model;

/**
 * usata per definire la priorità dei job creati dall'interfaccia di monitoraggio
 */
public enum JobPriority {
    HIGH,
    MEDIUM,
    LOW
}
