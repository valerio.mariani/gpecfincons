package it.posteitaliane.pec.sender.config;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPStore;

import it.posteitaliane.pec.sender.model.MailAccount;
import it.posteitaliane.pec.sender.model.MailBoxesConfig;
import it.posteitaliane.pec.sender.model.ServiceConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.mail.ImapMailReceiver;
import org.springframework.integration.mail.MailReceiver;
import org.springframework.integration.mail.MailReceivingMessageSource;
import org.springframework.integration.mail.dsl.Mail;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import javax.mail.*;
import javax.mail.event.MessageCountEvent;
import javax.mail.event.MessageCountListener;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.search.FlagTerm;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * USato in sviluppo per simulare l'azione di un server SMTP e tutti i componenti coinvolti, solo se lo start-up profile è
 * è 'local'
 */
@Component
@EnableIntegration
@Profile("local")
@RefreshScope
public class LocalSmtpServer {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocalSmtpServer.class);

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.protocol}")
    private String protocol;

    @Value("${spring.mail.port}")
    private int port;

    @Value("${spring.mail.username:#{null}}")
    private String username;

    @Value("${spring.mail.password:#{null}}")
    private String password;

    @Value("${sender.email.defaults.account.from}")
    String defaultFromAccount;

    @Value("${sender.email.destination.folder}")
    String defaultEMLStorageFolder;

    @Autowired
    MailBoxesConfig // Map<String, String>
            pecAccounts;

    private GreenMail smtpServer;
    private Folder inbox;
    private IdleThread idleThread;

    @Bean
    public GreenMail startSmtpServer(){
        LOGGER.info("starting Smtp Server for local development.. ");
        smtpServer = new GreenMail(ServerSetup.SMTP_POP3_IMAP);  // new ServerSetup(port, null, protocol)

        if( !pecAccounts.getServices().isEmpty() ) {

            pecAccounts.getServices().stream().map(ServiceConfiguration::getAccounts).collect(Collectors.toList()).
                    stream().flatMap(Collection::stream).collect(Collectors.toList()).
                    stream().filter(mailAccount -> mailAccount.isAvailable()).
                    forEachOrdered(mailAccount ->
                        smtpServer.setUser(mailAccount.getAddress(), mailAccount.getAddress(), mailAccount.getPassword())
            );
        }else{
            smtpServer.setUser(defaultFromAccount, username, password);

        }

        // nei test usiamo al momento questi destinatari:
        smtpServer.setUser("PEC1.multe@localhost.com", "PEC1.multe@localhost.com", "bar"); // password workaround

        smtpServer.start();
        return smtpServer;
    }

    /**
     * esperimenti per perfezionamenti possibili con Spring integration - Mail
     */
//    @Bean
//    @InboundChannelAdapter(
//            value = "testReceiveEmailChannel",
//            poller = @Poller(fixedDelay = "20000", taskExecutor = "asyncTaskExecutor")
//    )
//    public MessageSource mailMessageSource(MailReceiver mailReceiver) {
//        MailReceivingMessageSource mailReceivingMessageSource = new MailReceivingMessageSource(mailReceiver);
//        // other setters here
//        mailReceivingMessageSource.setLoggingEnabled(true);
//        mailReceivingMessageSource.setCountsEnabled(true);
//        return mailReceivingMessageSource;
//    }
//
//    @Bean
//    public MailReceiver imapMailReceiver() {
//        ImapMailReceiver imapMailReceiver = new ImapMailReceiver("imaps://${login}:${pass}@${host}:993/inbox");
//        // other setters here
//        imapMailReceiver.setShouldMarkMessagesAsRead(true);
//
//        return imapMailReceiver;
//    }


//    @Bean
    public IntegrationFlow mailListener() {
        return
                IntegrationFlows.from(Mail.
//                                pop3InboundAdapter(host, port, username, password)
                        pop3InboundAdapter(
//                      imapInboundAdapter(
//                      "imap://" +
                        "pop3://" +
                                username + ":" +
                                password + "@" +
                                host +
                                "/INBOX")
                .shouldDeleteMessages(true).get(),
                    e -> e.poller(Pollers.fixedRate(10000).maxMessagesPerPoll(10)))
                .<Message>handle( (payload, header) -> printMessage(payload) )
                .get();
    }


    @Bean
    public Folder inbox(
            GreenMail server) throws MessagingException {

        Store imapStore =
                smtpServer.
                        getPop3().
//                        getImap().
                        createStore();
        imapStore.connect("PEC1.multe@localhost.com", "bar"); // workaround temporaneo
        inbox = imapStore.getFolder("INBOX");
        inbox.open(Folder.READ_ONLY);

        inbox.addMessageCountListener(new MessageCountListener() {
            @Override
            public void messagesAdded(MessageCountEvent event) {
                LOGGER.debug("message listener invoked.");
                Message[] messages = event.getMessages();
                LOGGER.debug("Got " + messages.length + " new messages");
                for (Message message : messages) {
                    printMessage(message);
                }
            }

            @Override
            public void messagesRemoved(MessageCountEvent event) {
                LOGGER.debug("message listener invoked. MESSAGE REMOVED FROM SERVER");
            }
        });

//        idleThread = new IdleThread(inbox);
//        idleThread.setDaemon(false);
//        idleThread.start();

//        try {
//            idleThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        return inbox;
    }


    private Message printMessage(Message msg){

        StringBuilder print = new StringBuilder();
        String attachFiles = "";

        try {

            final Address[] in = msg.getFrom();
            for (final Address address : in) {
                print.append("\n").append("FROM:" + address.toString());
            }

            final Multipart mp = (Multipart) msg.getContent();
            final BodyPart bp = mp.getBodyPart(0);
            if (msg.getContentType().contains("multipart")) {

                final int numberOfParts = mp.getCount();
                for (int partCount = 0; partCount < numberOfParts; partCount++)
                {
                    final MimeBodyPart part = (MimeBodyPart) mp.getBodyPart(partCount);
                    if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                        // this part is attachment
                        final String fileName = part.getFileName();
                        attachFiles += fileName + ", ";
//                        part.saveFile("E:/" + File.separator + fileName);
                    } else {
                        print.append("\n").append("MultiPart Message Content :" + part.getContent().toString());
                    }
                }

                if (attachFiles.length() > 1) {
                    attachFiles = attachFiles.substring(0, attachFiles.length() - 2);
                }

                print.append("\n").append("Attachments: " + attachFiles);
            }
            print.append("\n").append("SENT DATE:" + msg.getSentDate());
            print.append("\n").append("SUBJECT:" + msg.getSubject());
            print.append("\n").append("CONTENT:" + bp.getContent());

            LOGGER.info("Arrived Message: " + print.toString());

            // write to file system
            FileOutputStream fos = new FileOutputStream(new File(defaultEMLStorageFolder + "/" + msg.getHeader("Message-ID") + ".eml"));
            msg.writeTo( fos );

            return msg;
        } catch (final Exception mex) {
            mex.printStackTrace();
            return null;
        }
    }


    @PreDestroy
    public void onExit() throws MessagingException {

//        idleThread.kill();
//
//        inbox.getStore().close();
//        inbox.close(false);
        smtpServer.stop();

        LOGGER.info("Smtp Server... STOPPED");
    }


    @Scheduled( fixedRate = 60000 )
    public void pollInbox(){
        if(inbox != null) {
            LOGGER.debug("enter idle");
            try {
                ensureOpen(inbox);
                if (supportsIdle(inbox)) ((IMAPFolder) inbox).idle();
                else {
                    Message[] messages = inbox.search(new FlagTerm(new Flags(Flags.Flag.RECENT), true));
                    Arrays.stream(messages).forEach(message -> printMessage(message));
                }
            } catch (Exception e) {
                // something went wrong
                // wait and try again
                e.printStackTrace();
            }
        }
    }



    // * * * * * * * * * * * * * * * *

    private static class IdleThread extends Thread {
        private final Folder folder;
        private volatile boolean running = true;

        public IdleThread(Folder folder) {
            super();
            this.folder = folder;
        }

        public synchronized void kill() {

            if (!running)
                return;
            this.running = false;
        }

        @Override
        public void run() {
            LOGGER.debug("enter idle");
            while (running) {

                try {
                    ensureOpen(folder);
                    if(supportsIdle(folder)) ((IMAPFolder) folder).idle();
                    else folder.search(new FlagTerm(new Flags(Flags.Flag.RECENT), true));
                } catch (Exception e) {
                    // something went wrong
                    // wait and try again
                    e.printStackTrace();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        // ignore
                    }
                }

            }
        }
    }


    public static void ensureOpen(final Folder folder) throws MessagingException {

//        if (folder != null) {
//            Store store = folder.getStore();
//            if (store != null && !store.isConnected()) {
//                store.connect(username, password);
//            }
//        } else {
//            throw new MessagingException("Unable to open a null folder");
//        }

        if (folder.exists() && !folder.isOpen() && (folder.getType() & Folder.HOLDS_MESSAGES) != 0) {
            LOGGER.info("open folder " + folder.getFullName());
            folder.open(Folder.READ_ONLY);
            if (!folder.isOpen())
                throw new MessagingException("Unable to open folder " + folder.getFullName());
        }

    }


    private static boolean supportsIdle(Folder folder) throws MessagingException {
        Store store = folder.getStore();

        if (store instanceof IMAPStore) {
            IMAPStore imapStore = (IMAPStore) store;
            return imapStore.hasCapability("IDLE") && folder instanceof IMAPFolder;
        } else {
            return false;
        }
    }


}
