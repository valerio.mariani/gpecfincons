package it.posteitaliane.pec.sender.model;

import com.fasterxml.jackson.annotation.JsonProperty;


public class RabbitMessagePayload {

    private final String PLATFORM_ANDROID = "And";
    private final String PLATFORM_IOS = "iOS";

    @JsonProperty("applicationbuild")
    private Integer applicationBuild;

    @JsonProperty("regid")
    private String deviceId;

    @JsonProperty("isSilent")
    private boolean silent;

    @JsonProperty("platform")
    private String platform;

    @JsonProperty("payload")
    private String payload;

    @JsonProperty("messageId")
    private Integer messageId;

    public boolean isAndroid() {
        return PLATFORM_ANDROID.equals(this.platform);
    }

    public boolean isIOS() {
        return PLATFORM_IOS.equals(this.platform);
    }

    @Override
    public String toString() {
        return "RabbitMessagePayload{" +
                "applicationBuild=" + applicationBuild +
                ", deviceId='" + deviceId + '\'' +
                ", silent=" + silent +
                ", platform='" + platform + '\'' +
                ", payload='" + payload + '\'' +
                ", messageId=" + messageId +
                '}';
    }

    public Integer getApplicationBuild() {
        return applicationBuild;
    }

    public void setApplicationBuild(Integer applicationBuild) {
        this.applicationBuild = applicationBuild;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isSilent() {
        return silent;
    }

    public void setSilent(boolean silent) {
        this.silent = silent;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }


}
