package it.posteitaliane.pec.smtp.transport.strategy;

import it.posteitaliane.pec.sender.model.MailAccount;
import it.posteitaliane.pec.sender.model.ServiceConfiguration;
import it.posteitaliane.pec.smtp.transport.context.CurrentSendingContextHolder;
import it.posteitaliane.pec.smtp.transport.context.SendingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.Transport;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import static it.posteitaliane.pec.sender.SenderConsumersApplication.*;

/**
 * {@link Transport} supports actually 4 types of connections which are handled by this connection strategy factory
 * <ol>
 * <li>{@link Transport#connect()} =&gt; {@link  #newConnectionStrategy()} </li>
 * <li>{@link Transport#connect(String, String)} ()} =&gt; {@link  #newConnectionStrategy(String, String)} </li>
 * <li>{@link Transport#connect(String, String, String)} ()} =&gt; {@link  #newConnectionStrategy(String, String, String)} </li>
 * <li>{@link Transport#connect(String, int, String, String)} ()} =&gt; {@link  #newConnectionStrategy(String, int, String, String)} </li>
 * </ol>
 * <p>
 * Created by nlabrot on 04/06/15.
 */
public class ConnectionStrategyFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionStrategyFactory.class);

    public static ConnectionStrategy newConnectionStrategy() {
        return new ConnectionStrategy() {
            @Override
            public void connect(Transport transport) throws MessagingException {
                transport.connect();
            }
        };
    }


    public static ConnectionStrategy accountFetchConnectionStrategy(
            final String host, final int port,
            FETCHING_STRATEGY strategy,
            final // Map<String, List<MailAccount>>
                    List<ServiceConfiguration> accounts ) {

        final Random r = new Random();
        MailAccount[] mailAccounts = null;
        if( strategy.equals( FETCHING_STRATEGY.RANDOM )) {
            mailAccounts = accounts.
                    stream().map(ServiceConfiguration::getAccounts).collect(Collectors.toList()).
                    stream().flatMap(Collection::stream).collect(Collectors.toList()).
                    stream().filter(mailAccount -> mailAccount.isAvailable()).toArray(MailAccount[]::new);
        }

        final MailAccount[] mailAccountsAvailable = mailAccounts;
        return transport -> {
            MailAccount mailAccount = null;
            SendingContext sendingContext = CurrentSendingContextHolder.get();
            if( strategy.equals( FETCHING_STRATEGY.RANDOM ) ){
                int index = r.ints(0, mailAccountsAvailable.length ).limit(1).findFirst().getAsInt();
                mailAccount = mailAccountsAvailable[index];
            }
            if( strategy.equals( FETCHING_STRATEGY.SERVICE_BASED )) {
                LOGGER.debug("fetching account from " + sendingContext);
                MailAccount[] accountsForService = accounts.
                        stream().filter(sc -> sc.getService().equalsIgnoreCase(sendingContext.getService())).
                        map(ServiceConfiguration::getAccounts).flatMap(Collection::stream).
                        filter(acc -> acc.isAvailable()).toArray(MailAccount[]::new);
                int index = r.ints(0, accountsForService.length ).limit(1).findFirst().getAsInt();
                mailAccount = accountsForService[index];
            }

            sendingContext.setAccount(mailAccount);
            transport.connect(host, port, mailAccount.getAddress(), mailAccount.getPassword());
        };
    }

    public static ConnectionStrategy newConnectionStrategy(final String username, final String password) {
        return new ConnectionStrategy() {
            @Override
            public void connect(Transport transport) throws MessagingException {
                transport.connect(username, password);
            }

            @Override
            public String toString() {
                return "ConnectionStrategy{" +
                        "username=" + username +
                        '}';
            }

        };
    }

    public static ConnectionStrategy newConnectionStrategy(final String host, final String username, final String password) {
        return new ConnectionStrategy() {
            @Override
            public void connect(Transport transport) throws MessagingException {
                transport.connect(host, username, password);
            }

            @Override
            public String toString() {
                return "ConnectionStrategy{" +
                        "host=" + host +
                        ", username=" + username +
                        '}';
            }
        };
    }

    public static ConnectionStrategy newConnectionStrategy(final String host, final int port, final String username, final String password) {
        return new ConnectionStrategy() {
            @Override
            public void connect(Transport transport) throws MessagingException {
                transport.connect(host, port, username, password);
            }

            @Override
            public String toString() {
                return "ConnectionStrategy{" +
                        "host=" + host +
                        ", port=" + port +
                        ", username=" + username +
                        '}';
            }
        };
    }
}
