package it.posteitaliane.pec.sender.services;

import brave.Span;
import brave.Tracer;
import com.hazelcast.core.HazelcastInstance;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import it.posteitaliane.pec.smtp.pool.SmtpConnectionPoolDelegator;
import it.posteitaliane.pec.smtp.transport.connection.ClosableSmtpConnection;
import it.posteitaliane.pec.smtp.transport.context.CurrentSendingContextHolder;
import it.posteitaliane.pec.smtp.transport.context.SendingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.apache.commons.lang3.StringUtils.*;

/**
 *
 */
@Service
@RefreshScope
public class EmailSenderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailSenderService.class);

    private final JavaMailSender                javaMailSender;
    private final static String                 spanPrefix = "mail.sender.email";
    private static Clock                        clock = Clock.systemDefaultZone();
    private final Map<String, SendingContext>   sendingContextMap;
    private ExecutorService                     quickService = Executors.newFixedThreadPool(10);
//    private ScheduledExecutorService          quickService = Executors.newScheduledThreadPool(10); // Creates a thread pool that reuses fixed number of threads.


    @Autowired LotRepository                    lotRepository;
    @Autowired MailDeliveryRepository           mailDeliveryRepository;
    @Autowired SmtpConnectionPoolDelegator      smtpConnectionPool;
    @Autowired Tracer                           tracer;
    @Autowired private CacheManager             cacheManager;

    @Value("${nfs.shared.stage.folder}")        String stageFolder;
    @Value("${sender.maxTotalSizeAttachments}") Long MAX_TOTAL_SIZE_ATTACHMENTS;

    @Value("${spring.mail.username}")           String username;

    @Autowired
    public EmailSenderService( JavaMailSender javaMailSender, final HazelcastInstance hazelcastInstance) {
        this.javaMailSender = javaMailSender;
        this.sendingContextMap = hazelcastInstance.getMap("current-mail-sendings");

    }

    /**
     *
     * @param messageSuCoda
     * @return
     */
    public void sendEmail(MailDeliveryRequest messageSuCoda, String fromAccount, final String service) throws AddressException {
        // Start a new trace or a span within an existing trace representing an operation
        Span span = tracer.nextSpan().name( spanPrefix + ".prepare").start();

        MailDeliveryRequest mailDeliveryRequest=null;

        try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {

            String messageId = messageSuCoda.getMessageID();
            LOGGER.info("MesageId recuperato da oggetto su Rabbit: ***"+messageId+"***");

            //recupero oggetto su CB per sincronizzare il tutto
            Optional<MailDeliveryRequest> messageSuCB = mailDeliveryRepository.findById(messageId);

            LocalDateTime tInizioCiclo = LocalDateTime.now();
            while(!messageSuCB.isPresent()){

                LOGGER.info("MESSAGGIO CON messageId =**"+messageId+"**NON ANCORA PRESENTE SU CB - sendEmail ");

                messageSuCB = mailDeliveryRepository.findById(messageId);
                // ripeto la find finchè non trovo il messagio che gia' deve essere salvato su CB (se c'e', a questo punto del giro, l'unico stato che puo' avere e' "MESSAGGIO_INVIABILE", altrimenti non sarebbe stato messo in coda e qui non me lo ritroverei

                LocalDateTime now = LocalDateTime.now();

                if(  now.minusMinutes(1).isAfter(tInizioCiclo)  ){
                    break;
                    // ho aggiunto controllo sul tempo, se è passati piu' di 1 miunuti, esco cmq dal ciclo
                    //TODO: da gestire ancora pero' l'incongruenza successiva...
                }

            }

            MailDeliveryRequest messaggioSuCB = messageSuCB.get();

            LOGGER.info("MESSAGGIO recuperato da oggetto su Rabbit: ***"+messageSuCoda.toString()+"***");

            LOGGER.info("MESSAGGIO recuperato da oggetto su Couchbase: ***"+messaggioSuCB.toString()+"***");

            //a questo punto i due oggetti messageSuCoda e messaggioSuCB devono essere uguali, dal punto di vista del contenuto di business,
            // uso l'oggetto preso da CB perchè poi devo usarlo per fare la save su CB
             mailDeliveryRequest = messaggioSuCB;
            MimeMessage mimeMessage;
            MimeMessageHelper mimeMessageHelper;
            try {
                mimeMessage = javaMailSender.createMimeMessage();

                // inserisco l'header "Message-ID" valorizzato con l'externalId
                mimeMessage.setHeader("Message-ID", mailDeliveryRequest.getMessageID());
                //TODO  altri header dello standard PEC qui...

                boolean multiPart = false;
                if (mailDeliveryRequest.getAttachments() != null) {  // in realta' questa condizione dovrebbe essere sempre soddisfatta
                    multiPart = !mailDeliveryRequest.getAttachments().isEmpty();
                    // se la lista è vuota il mimemessage non sarà di tipo Multipart -->COME PRIMA: NON DOVREBBE MAI ACCADERE QUESTA CONDIZIONE
                }
                mimeMessageHelper = new MimeMessageHelper(mimeMessage, multiPart);

                // il from address e il transport saranno impostati dal connection pool estraendole dalle caselle disponibili per il service
//            mimeMessageHelper.setFrom(fromAccount);

                //mimeMessageHelper.setTo(mailDeliveryRequest.getTo().split(",")); // TODO <-- irrobustire qui. potrebbero esservi casi in cui dopo la virgola è estratta una stringa blank
                mimeMessageHelper.setTo(mailDeliveryRequest.getTo()); //MODIFICA: SI SPEDISCE AD UN SOLO DESTINATARIO, QUINDI IL CAMPO "TO" DEVE AVERE UN SOLO INDIRIZZO MAIL
                if (!isEmpty(mailDeliveryRequest.getCc())) {
                    mimeMessageHelper.setCc(mailDeliveryRequest.getCc());
                }
                mimeMessageHelper.setText(mailDeliveryRequest.getContent(), false); // <-- dinamico, in futuro potrebbe dipendere dalla Doc Composition
                mimeMessageHelper.setSubject(mailDeliveryRequest.getSubject());
            }catch (Exception e){
                mailDeliveryRequest.getEvents().add( Events.builder().event(EventType.MESSAGGIO_NON_INVIABILE).time( LocalDateTime.now()).build());
                mailDeliveryRequest.setValidationOutcome("V006");//"MESSAGGIO NON INVIABILE PER ERRORI FORMALI" //TODO: AGGIUNGERE TALE CODIFICA ALL'ICD
                mailDeliveryRepository.save(mailDeliveryRequest);
                AmqpRejectAndDontRequeueException rejectMessageException = new AmqpRejectAndDontRequeueException("MESSAGGIO NON INVIABILE PER ERRORI FORMALI");
                span.error(rejectMessageException);
                throw rejectMessageException;

            }

            final long[] sizeTotaleDegliAllegati = {0};

            MailDeliveryRequest finalMailDeliveryRequest = mailDeliveryRequest;
            Optional.ofNullable(mailDeliveryRequest.getAttachments()).ifPresent(
                    x -> x.stream().forEach(
                    attachment -> {

                        String filePath=stageFolder + "/" + finalMailDeliveryRequest.getLotId() + "/" + attachment;

                        FileSystemResource file //File System strategy
                                = new FileSystemResource(new File(filePath));
                        try {

                            LOGGER.info("path file che sto per allegare: ***"+ filePath );

                            LocalDateTime tIniCiclo = LocalDateTime.now();
                            while(  !file.exists()   ){

                                LOGGER.trace("ALLEGATO **"+filePath+"**NON ANCORA PRESENTE SULLA SHARE ");

                                file = new FileSystemResource(new File(filePath));
                                // ripeto la creazione del file

                                LocalDateTime now = LocalDateTime.now();

                                if(  now.minusSeconds(40).isAfter(tIniCiclo)  ){
                                    break;
                                    // se passano piu' di tot secondi, esco cmq dal ciclo

                                }

                            }


                            if( !file.exists() ){
                                LOGGER.error("ALLEGATO NON ESISTENTE");
                                throw new Exception("ALLEGATO NON ESISTENTE");
                            }


                            LOGGER.info("INSERIMENTO ALLEGATO: **"+file.getFilename()+"**");

                            mimeMessageHelper.addAttachment(file.getFilename(), file);

                            //TODO: ancora da testare
                            long fileSize= file.getFile().length();
                            sizeTotaleDegliAllegati[0] = sizeTotaleDegliAllegati[0] + fileSize;

                        } catch (Exception e) {

                            LOGGER.error("ERRORE NELLA FASE DI INSERIMENTO  ALLEGATO  " , e );

                            //LOGGER.error("impossibile inserire l'allegato " + attachment + " [" + e.getMessage() );

                            //TODO gestire bene questo errore in ottica SLA e response report generator
                            finalMailDeliveryRequest.getEvents().add( Events.builder().event( EventType.MESSAGGIO_NON_INVIABILE).time( LocalDateTime.now()).build());
                            finalMailDeliveryRequest.setValidationOutcome("V003");//"IL MESSAGGIO INDICA UN ALLEGATO NON PRESENTE NEL FILE DI INPUT"
                            //TODO: PER ORA FACCIAMO DIRETTAMENTE LA SAVE SU CB SENZA UNA PRELIMINARE FINDBYID, AFFINDANDOCI AL FATTO CHE IL MODEL mailDeliveryRequest PRESENTE SU RABBIT SIA EQUIVALENTE AL MODEL SALVATO SU CB IN FASE DI COMPOSITION
                            mailDeliveryRepository.save(finalMailDeliveryRequest);
                            AmqpRejectAndDontRequeueException rejectMessageException = new AmqpRejectAndDontRequeueException("Messaggio non inviabile causa allegato non trovato");
                            span.error(rejectMessageException);
                            throw rejectMessageException;
                        }
                    }
            ));

            String errorSuSize=null;

            //TODO:  ancora da testare
            if(sizeTotaleDegliAllegati[0] > MAX_TOTAL_SIZE_ATTACHMENTS ){
                errorSuSize="V004";//"LA SOMMA DELLE DIMENSIONI DEGLI ALLEGATI DEL MESSAGGIO SUPERA IL VALORE MASSIMO CONSENTITO"
            }else if (sizeTotaleDegliAllegati[0]  ==  0){
                errorSuSize="V005";//"ALLEGATI CON DIMENSIONE PARI A 0"
            }



            if (  errorSuSize != null   ){

                LOGGER.error( errorSuSize + " " + mailDeliveryRequest.getMessageID() + " errore sulla size degli allegati , che e pari a : ****" + sizeTotaleDegliAllegati[0]+"***" );
                //TODO gestire bene questo errore in ottica SLA e response report generator
                mailDeliveryRequest.getEvents().add( Events.builder().event(EventType.MESSAGGIO_NON_INVIABILE).time( LocalDateTime.now()).build());

                mailDeliveryRequest.setValidationOutcome(errorSuSize);

                //TODO: PER ORA FACCIAMO DIRETTAMENTE LA SAVE SU CB SENZA UNA PRELIMINARE FINDBYID, AFFINDANDOCI AL FATTO CHE IL MODEL mailDeliveryRequest PRESENTE SU RABBIT SIA EQUIVALENTE AL MODEL SALAVATO SU CB IN FASE DI COMPOSITION
                mailDeliveryRepository.save(mailDeliveryRequest);
                AmqpRejectAndDontRequeueException rejectMessageException = new AmqpRejectAndDontRequeueException("Messaggio non inviabile causa dimensioni allegati che eccedono il massimo consentito");
                span.error(rejectMessageException);
                throw rejectMessageException;

            }

            //TODO: PER ORA FACCIAMO DIRETTAMENTE LA SAVE SU CB SENZA UNA PRELIMINARE FINDBYID, AFFINDANDOCI AL FATTO CHE IL MODEL mailDeliveryRequest PRESENTE SU RABBIT SIA EQUIVALENTE AL MODEL SALAVATO SU CB IN FASE DI COMPOSITION
            mailDeliveryRequest.setValidationOutcome("1");//INDICA CHE E'SUPERATA LA VALIDAZIONE E POSSO PROVARE AD INVIARLO
//            mailDeliveryRepository.save(mailDeliveryRequest);

            MailDeliveryRequest finalMailDeliveryRequest1 = mailDeliveryRequest;
            quickService.submit(() -> {
                Span continuedSpan =  tracer.nextSpan().name(spanPrefix + "sending").kind(Span.Kind.CLIENT);
//                        this.tracer.startScopedSpan(spanPrefix + "sending");
                try {
                    LOGGER.info("EMAIL ID[" + mimeMessage.getMessageID() + "]");
                    SendingContext context = new SendingContext(service);
                    sendingContextMap.put(mimeMessage.getMessageID(), context);
                    CurrentSendingContextHolder.set( context );

                    LOGGER.info("usernamefrom [" + username + "]");

                    mimeMessage.setFrom(username);
                    javaMailSender.send(mimeMessage);


                    finalMailDeliveryRequest1.getEvents().add( Events.builder().event(EventType.MESSAGGIO_INVIO_SUCCESSO).time( LocalDateTime.now()).build());
                    finalMailDeliveryRequest1.setValidationOutcome("1");


                    // send(mimeMessage);
                    LOGGER.debug("DOPO di send");
                }catch(MailException e ){  // TODO gestire tutta la gerarchia delle eccezioni?
                    continuedSpan.error(e);
                    LOGGER.error("E001 Exception occur while send a mail : ", e);

                    //TODO: MOLTO IMPORTANTE
                    // QUI VA DECISO SE IMPLMENTATARE POLITICHE DI RETRY IN CASO DI ERRORE AL SEND, SIA PER MailException CHE PER Exception
                    // IN TAL CASO, L'EVENTO MESSAGGIO_INVIO_FALLITO DOVRA' ESSERE CAMBIATO DA UN EVENTO DEL TIPO: MESSAGGIO_TENTATIVO_INVIO_FALLITO
                    // CHE DIVENTERA': MESSAGGIO_INVIO_FALLITO SOLO DOPO AVER ESAURITO TUTTI GLI EVENTUALI TENTATIVI DI RETRY


                    finalMailDeliveryRequest1.getEvents().add( Events.builder().event(EventType.MESSAGGIO_INVIO_FALLITO).time( LocalDateTime.now()).build());
                    finalMailDeliveryRequest1.setValidationOutcome("E001");//"SI E' VERIFICATO UN PROBLEMA DURANTE L'INVIO DELLA MAIL"
                } catch (Exception e) {
                    continuedSpan.error(e);
                    LOGGER.error("E001 Exception occur while send a mail : ", e); //TODO: decidere se tentare il resend
                    //OCCHIO: QUESTI MESSAGGI DI ERRORE DEVONO ESSERE SCOLPITI NELLA PIETRA PERCHE' DEVONO ESSERE DATI AL MONITORAGGIO
                    finalMailDeliveryRequest1.getEvents().add( Events.builder().event(EventType.MESSAGGIO_INVIO_FALLITO).time( LocalDateTime.now()).build());
                    finalMailDeliveryRequest1.setValidationOutcome("E001");//"SI E' VERIFICATO UN PROBLEMA DURANTE L'INVIO DELLA MAIL"
                } finally {

                    LOGGER.debug("finally del send");
                    continuedSpan.finish();



                    //TODO: PER ORA FACCIAMO DIRETTAMENTE LA SAVE SU CB SENZA UNA PRELIMINARE FINDBYID, AFFINDANDOCI AL FATTO CHE IL MODEL mailDeliveryRequest PRESENTE SU RABBIT SIA EQUIVALENTE AL MODEL SALAVATO SU CB IN FASE DI COMPOSITION
                    mailDeliveryRepository.save(finalMailDeliveryRequest1);

                    //TODO: DA CAPIRE SE CI SONO TENTATIVI AUTOMATICI DI RETRY E IN TAL CASO COME GESTIRE LA MODIFICA DELL'OUTCOME

                }
            });
        } catch (Exception e) {
            LOGGER.error("Error while sending or preparing email", e);
            span.error(e);
        } finally {
            // Once done remember to flush the span. That means that
            // it will get reported but the span itself is not yet finished
            span.flush();
        }
    }


    /**
     * estrae una SMTP connection dal pool e la usa come transport per l'inivo del messaggio
     * @param mimeMessage
     * @throws Exception
     */
    private void send(MimeMessage mimeMessage) throws Exception {

        LOGGER.debug("DENTRO send");
        long borrowMaxWaitMillis = 4000l;  // questa proprietà
        // borrow an object in a try-with-resource statement or call `close` by yourself
        try (ClosableSmtpConnection transport = smtpConnectionPool.borrowObject(borrowMaxWaitMillis)) {
            SendingContext currentSendingContext = CurrentSendingContextHolder.get();

            if(mimeMessage==null){
                LOGGER.error("ERRORE nel send: mimeMessage è NULL");
            }else if(currentSendingContext==null){
                LOGGER.error("ERRORE nel send: currentSendingContext è NULL");
            } else if(currentSendingContext.getAccount()==null){
                LOGGER.error("ERRORE nel send: currentSendingContext.getAccount()  è NULL");
            } else{
                LOGGER.info("currentSendingContext.getAccount() :" +currentSendingContext.getAccount());
            }

            mimeMessage.setFrom( currentSendingContext.getAccount().getAddress() );
            LOGGER.debug("sendMessage EMAIL: " + mimeMessage.getFrom()[0].toString() );
            // pubblicare log e statistiche/metriche di esecuzione
            currentSendingContext.setStartSendingTime(clock.millis());
            sendingContextMap.replace(mimeMessage.getMessageID(), currentSendingContext);
            transport.sendMessage(mimeMessage);
            LOGGER.debug("DOPO sendMessage");

        }

    }

}
