package it.posteitaliane.pec.smtp.transport.context;

import it.posteitaliane.pec.sender.SenderConsumersApplication;
import it.posteitaliane.pec.sender.model.MailAccount;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@ToString
//@AllArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@EqualsAndHashCode
public class SendingContext implements Serializable {

    @NotNull
    final String service;

    MailAccount account;
    Long startSendingTime;

}
