package it.posteitaliane.pec.sender.services;

import brave.Span;
import brave.Tracer;
import com.hazelcast.core.HazelcastInstance;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.model.SendingDetails;
import it.posteitaliane.pec.common.repositories.LotRepository;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import it.posteitaliane.pec.smtp.transport.context.SendingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import javax.mail.Address;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.event.TransportEvent;
import javax.mail.event.TransportListener;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Enumeration;
import java.util.Map;
import java.util.Optional;


/**
 *
 */
@Component
public class MailStatusListener implements TransportListener {

    Logger LOGGER = LoggerFactory.getLogger(MailStatusListener.class);

    private static final String headerMessageID = "Message-ID";

    @Autowired Tracer tracer;
    @Autowired LotRepository lotRepository;
    @Autowired MailDeliveryRepository mailDeliveryRepository;
    @Autowired private CacheManager cacheManager;

    private final Map<String, SendingContext> sendingContextMap;
    private static Clock clock = Clock.systemDefaultZone();
    private String spanName = "mail.smtp.response";

    public MailStatusListener( final HazelcastInstance hazelcastInstance)  {  //(CacheManager cacheManager) {
//        Cache cache = cacheManager.getCache("current-mail-sendings");
        this.sendingContextMap = hazelcastInstance.getMap("current-mail-sendings");
    }

    @Override
    public void messageDelivered(TransportEvent e) {

        LOGGER.debug(e + "\n * * * * * * * * * MAIL.DELIVERY.STATUS = messageDelivered" );
        Span span = tracer.nextSpan().name(spanName).kind(Span.Kind.CLIENT).start();
        span.tag("delivery", "SUCCESS");
        span.annotate("asynchronous response from SMTP transport " + e);
        try ( Tracer.SpanInScope ws = tracer.withSpanInScope(span) ) {
            String messageDocumentID = null;
            try {
                messageDocumentID = getMessageIdFromHeader(e.getMessage());
            }catch (Exception ex){
                span.error(ex);
                LOGGER.error("Errore nella gestione dello stato invio mail", ex);
            }
            if (messageDocumentID != null) {
                String validationOutcome="1";
                traceEventToMessageDelivery(messageDocumentID, EventType.MESSAGGIO_INVIO_SUCCESSO, validationOutcome, e);
            } else {
                LOGGER.error("NO MESSAGE ID");
            }
            // TODO tracciare acnhe i possibili destinatari non raggiungibili, invalidi etc
        } finally {
            span.finish(); // note the scope is independent of the span. Always finish a span.
        }
    }


    /**
     * ??? una consegna parziale?? come gestirla?
     * @param e
     */
    @Override
    public void messageNotDelivered(TransportEvent e) {

        LOGGER.debug(e + "\n * * * * * * * * * MAIL.DELIVERY.STATUS = messageNotDelivered");

        Span span = tracer.nextSpan().name(spanName).kind(Span.Kind.CLIENT).start();
        span.tag("delivery", "NOT_DELIVERED");
        span.annotate("asynchronous response from SMTP transport " + e);
        try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {
            String messageDocumentID = null;
            try {
                messageDocumentID = getMessageIdFromHeader(e.getMessage());
            }catch (Exception ex){
                span.error(ex);
                LOGGER.error("Errore nella gestione dello stato invio mail", ex);
            }
            if (messageDocumentID != null) {
                String validationOutcome="E001";//"MESSAGGIO NON INVIATO PER SERVER PEC NON RAGGIUNGIBILE"
                //SE IN FUTURO IMPLEMENTASSIMO UN QUALCHE MECCANISMO DI RETRY, ALLORA MESSAGGIO_INVIO_FALLITO DOVRA'
                // ESSERE IMPOSTATO SOLO ALLA FINE DEI TENTATIVI DI RETRY E NON PIU' QUI
                traceEventToMessageDelivery(messageDocumentID, EventType.MESSAGGIO_INVIO_FALLITO, validationOutcome, e);
            }else{
                LOGGER.error("NO MESSAGE ID");
            }
        }finally {
            span.finish(); // note the scope is independent of the span. Always finish a span.
        }
    }


    @Override
    public void messagePartiallyDelivered(TransportEvent e) {

        LOGGER.debug(e + "\n * * * * * * * * * MAIL.DELIVERY.STATUS = messagePartiallyDelivered");

        Span span = tracer.nextSpan().name(spanName).kind(Span.Kind.CLIENT).start();
        span.tag("delivery", "PARTIAL");
        span.annotate("asynchronous response from SMTP transport " + e);
        try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {

            String messageDocumentID = null;
            try {
                messageDocumentID = getMessageIdFromHeader(e.getMessage());
            }catch (Exception ex){
                span.error(ex);
                LOGGER.error("Errore nella gestione dello stato invio mail", ex);
            }
            if (messageDocumentID != null) {
                String validationOutcome="?";//TODO: CAPIRE COSA SI INTENDE CON messagePartiallyDelivered
                traceEventToMessageDelivery(messageDocumentID, EventType.MESSAGGIO_CONSEGNA_PARZIALE,validationOutcome, e);
            } else{
                LOGGER.error("NO MESSAGE ID");
            }
        } finally {
            span.finish(); // note the scope is independent of the span. Always finish a span.
        }
    }


    private String getMessageIdFromHeader(Message message) {

        try {
            String messageDocumentID = null;
            Enumeration<Header> headers = message.getMatchingHeaders(new String[]{headerMessageID});
            while(headers.hasMoreElements()){
                Header header = headers.nextElement();
                LOGGER.info("valore dell'header -> " + header.getValue());
                messageDocumentID = header.getValue();
                break;
            }
            if(messageDocumentID == null){
                throw new IllegalStateException("impossibile rintracciare il message-ID in " + message);
            }
            return messageDocumentID;
        } catch (MessagingException e1) {
            LOGGER.warn("Impossibile leggere e verificare i metadati del messaggio inviato, ma risulta inviato");
            //TODO salvare il contenuto da qualche parte in modo che sia possibile a mano modificare gli stati di questi invii prima della chiusura del lotto
            return null;
        }
    }

    /**
     * TODO: spostare in un service?
     * Si occupa di verificare se esiste il documento relativo ad il messaggio su couchbase
     * ed aggiunge un evento da tracciare
     *  @param messageDocumentID
     * @param event
     * @param transportEvent
     */
    private void traceEventToMessageDelivery(String messageDocumentID, EventType event, String validationOutcome, TransportEvent transportEvent) {
        Span span = tracer.nextSpan().name(spanName + ".append.event").start();
        try (Tracer.SpanInScope ws = tracer.withSpanInScope(span)) {
            String log = "append event '" + event + "' to message[" + messageDocumentID + "]";
            span.annotate(log);
            LOGGER.info(log);
            Optional<MailDeliveryRequest> delivery = mailDeliveryRepository.findById(messageDocumentID);
            LOGGER.debug("\n * * * * * * * * * * * * * * * * * * * * * * * * * * \n" + delivery);
            delivery.ifPresent(deliveryRequest -> {
                // raccogliamo le proprietà necessarie a compilare un dettaglio invio
                String from = null;
                long sendTimeMillis = 0l;
                SendingContext currentSendingContext = null;
                try{
                    // leggiamo il contesto di invio corrente
                    currentSendingContext =  this.sendingContextMap.get(messageDocumentID); // cacheManager.getCache("").get(messageDocumentID);
                    Address[] addresses = transportEvent.getMessage().getFrom();
                    from = addresses[0].toString();
                    sendTimeMillis =  clock.millis(); // transportEvent.getMessage().getSentDate().getTime();

                } catch(Exception e) {
                    span.error(e);
                    LOGGER.error("Error accessing message properties", e);
                }
                try {
                    Events eventSubDocument = Events.builder().   // new Events(event, LocalDateTime.now());
                        event(event).
                        details(SendingDetails.builder().
                                address(from).
                                sendAttempt(1).
                                executionTime(
                                        sendTimeMillis -
                                        currentSendingContext.getStartSendingTime()
                                ).build()
                        ).
                        time(LocalDateTime.now()).build();
                    deliveryRequest.getEvents().add(eventSubDocument);
                    deliveryRequest.setValidationOutcome(validationOutcome);

                    mailDeliveryRepository.save(deliveryRequest);

                } catch (Exception e) {
                    LOGGER.error("Errore durante il salvataggio dei dati invio in COUCHBASE", e);
                    span.error(e);
                }finally {
                    Optional<MailDeliveryRequest> updated = mailDeliveryRepository.findById(deliveryRequest.getMessageID());
                    LOGGER.info("Updated MailDelivery[ " + messageDocumentID + "] ? " + updated.isPresent() + " " + updated.get());
                }
            });


            delivery.orElseThrow(() -> {
                IllegalStateException e = new IllegalStateException("nessun documento per messageID[" + messageDocumentID + "]");
                span.error(e);
                return e;
            });

        } finally {
            span.flush(); // note the scope is independent of the span. Always finish a span.
        }
    }

}
