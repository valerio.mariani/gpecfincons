package it.posteitaliane.pec.smtp.transport.factory;

import it.posteitaliane.pec.smtp.transport.strategy.ConnectionStrategyFactory;
import it.posteitaliane.pec.smtp.transport.strategy.TransportStrategyFactory;

import javax.mail.Session;
import java.util.Properties;

/**
 * {@link SmtpConnectionFactory} factory
 */
public final class SmtpConnectionFactories {

  private SmtpConnectionFactories() {
  }

  /**
   * Initialize the {@link SmtpConnectionFactory} with a
   * {@link Session} initialized to {@code Session.getInstance(new Properties())},
   * {@link TransportStrategyFactory#newSessiontStrategy},
   * {@link ConnectionStrategyFactory#newConnectionStrategy}
   *
   * @return
   */
  public static SmtpConnectionFactory newSmtpFactory() {
    return new SmtpConnectionFactory(Session.getInstance(new Properties()), TransportStrategyFactory.newSessiontStrategy(), ConnectionStrategyFactory.newConnectionStrategy());
  }

  /**
   * Initialize the {@link SmtpConnectionFactory} using the provided
   * {@link Session} and
   * {@link TransportStrategyFactory#newSessiontStrategy},
   * {@link ConnectionStrategyFactory#newConnectionStrategy}
   *
   * @param session
   * @return
   */
  public static SmtpConnectionFactory newSmtpFactory(Session session) {
    return new SmtpConnectionFactory(session, TransportStrategyFactory.newSessiontStrategy(), ConnectionStrategyFactory.newConnectionStrategy());
  }


}