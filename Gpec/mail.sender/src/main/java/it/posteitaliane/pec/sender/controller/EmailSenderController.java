package it.posteitaliane.pec.sender.controller;

import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import it.posteitaliane.pec.common.repositories.MailDeliveryRepository;
import it.posteitaliane.pec.sender.services.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.mail.internet.AddressException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/email")
@CrossOrigin(origins="*")
public class EmailSenderController {

    @Autowired
    EmailSenderService emailService;

    @Autowired
    MailDeliveryRepository mailDeliveryRepository;

    @RequestMapping(value = "/send/{service}/{account-id}", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE )
    @ResponseBody
    public ResponseEntity<?> sendEmailWithTestConfiguration(
            @PathVariable("account-id") String fromAccount,
            @PathVariable("service") String service){

        String messageID=UUID.randomUUID().toString();

        System.out.println("******messageID:  ***"+messageID+"***");

        MailDeliveryRequest sending = MailDeliveryRequest.builder().
                //to("someone@localhost").
                to("pectest23l2@coll.postecert.it").
                lotId("PROVa2222222222222").
                extID(UUID.randomUUID().toString()).
                messageID(messageID).
                subject("test da java , dal controller").
                content("dal controller java").
                //events(Arrays.asList(new Events(EventType.MESSAGGIO_INVIATO , LocalDateTime.now()))).
                events(Arrays.asList( Events.builder().event(EventType.MESSAGGIO_INVIABILE).time( LocalDateTime.now()).build())).
                attachments(new ArrayList<>()).
                build();
        mailDeliveryRepository.save(sending);
        try {
            emailService.sendEmail( sending, fromAccount, service);
        } catch (AddressException e) {
            e.printStackTrace();
        }
        return  new ResponseEntity<>("SUCCESS", HttpStatus.OK);
    }
}
