package it.posteitaliane.pec.smtp.transport.context;

import java.util.function.Supplier;


/***
 *
 */
public class CurrentSendingContextHolder {

    // No SendingContext is logical error, so we prevent that:
    private static final Supplier<SendingContext> STATE_CHECKER = () -> {
        throw new IllegalStateException("Il contesto di invio deve essere impostato!");
    };

    private static final ThreadLocal<SendingContext> sendingContext = ThreadLocal.withInitial(STATE_CHECKER);

    public static SendingContext get() {
        return sendingContext.get();
    }

    public static void set(SendingContext id) {
        sendingContext.set(id);
    }

}
