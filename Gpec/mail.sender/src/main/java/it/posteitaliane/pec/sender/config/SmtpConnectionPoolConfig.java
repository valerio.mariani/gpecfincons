package it.posteitaliane.pec.sender.config;

import it.posteitaliane.pec.sender.model.MailAccount;
import it.posteitaliane.pec.sender.model.MailBoxesConfig;
import it.posteitaliane.pec.sender.model.ServiceConfiguration;
import it.posteitaliane.pec.smtp.pool.PoolConfigs;
import it.posteitaliane.pec.smtp.pool.SmtpConnectionPool;
import it.posteitaliane.pec.smtp.pool.SmtpConnectionPoolDelegator;
import it.posteitaliane.pec.smtp.transport.factory.SmtpConnectionFactoryBuilder;
import it.posteitaliane.pec.smtp.transport.strategy.FETCHING_STRATEGY;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import javax.mail.event.TransportListener;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Configuration
public class SmtpConnectionPoolConfig {

    @Value("${spring.mail.host}")
    private String host;

    @Value("${spring.mail.protocol}")
    private String protocol;

    @Value("${spring.mail.port}")
    private int port;

    @Value("${spring.mail.username:#{null}}")
    private String username;

    @Value("${spring.mail.password:#{null}}")
    private String password;

    @Value("${smtp.pool.max.connections}")
    private int smtpConnectionPoolMaxSize;

    @Autowired TransportListener mailStatusListener;

    @Autowired Properties smtpMailProperties;
    @Autowired(required = false) MailBoxesConfig  serviceGroup;
    @Autowired(required = false) Map<String, List<MailAccount>> accounts;


    @Bean
    @RefreshScope
    public SmtpConnectionPoolDelegator smtpConnectionPool(){
        //Create the configuration
        GenericObjectPoolConfig config = PoolConfigs.standardConfig();
        config.setJmxEnabled(false);
        config.setMaxTotal(smtpConnectionPoolMaxSize);
        config.setMaxWaitMillis(2000l);  // default... poi esportiamo in una variabile esterna

        //Declare the factory and the connection pool, usually at application startup
        SmtpConnectionPool smtpConnectionPool = new SmtpConnectionPool(
                SmtpConnectionFactoryBuilder.newSmtpBuilder()
						.session(smtpMailProperties)
                        .defaultTransportListeners(mailStatusListener)
                        .protocol(protocol)
                        .host(host)
                        .port(port)
                        .username(username)  // le strategie username/password e accountFetchStrategy sono al momento mutualmente esclusive // commentare le properties
                        .password(password)
                        .accountFetchStrategy(FETCHING_STRATEGY.SERVICE_BASED)
                        .serviceGroup(serviceGroup.getServices())
                        .build(),
                config);

        return new SmtpConnectionPoolDelegator( smtpConnectionPool );
    }

    @PreDestroy
    public void onExit(){
        smtpConnectionPool().close();
    }


}
