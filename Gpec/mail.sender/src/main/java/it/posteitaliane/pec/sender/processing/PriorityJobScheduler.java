package it.posteitaliane.pec.sender.processing;

import it.posteitaliane.pec.sender.model.ManagementJob;

import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;


/**
 *
 */
public class PriorityJobScheduler {

    private ExecutorService priorityJobPoolExecutor;
    private ExecutorService priorityJobScheduler = Executors.newSingleThreadExecutor();
    private PriorityBlockingQueue<ManagementJob> priorityQueue;

    public PriorityJobScheduler(Integer poolSize, Integer queueSize) {

        priorityJobPoolExecutor = Executors.newFixedThreadPool(poolSize);

        priorityQueue = new PriorityBlockingQueue<>(
                queueSize,
                Comparator.comparing(ManagementJob::getJobPriority));

        priorityJobScheduler.execute(() -> {
            while (true) {
                try {
                    priorityJobPoolExecutor.execute(priorityQueue.take());
                } catch (InterruptedException e) {
                    // exception needs special handling
                    break;
                }
            }
        });
    }

    public void scheduleJob(ManagementJob job) {
        priorityQueue.add(job);
    }

}
