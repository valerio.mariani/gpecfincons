package it.posteitaliane.pec.sender.services;


import com.netflix.appinfo.InstanceInfo;
import it.posteitaliane.pec.sender.listener.EmailDeliveryMessageReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerEndpoint;
import org.springframework.amqp.rabbit.listener.*;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;


/**
 *
 */
@Service
//@RefreshScope
public class ListenersContainersRegistry {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListenersContainersRegistry.class);

    @Value("${rabbit.custom.min.consumer}")
    private Integer minConsumer;
    @Value("${rabbit.custom.max.consumer}")
    private Integer maxConsumer;
    @Value("${rabbit.custom.consumer.max.listening.minutes:#{null}}")
    private Integer maxMinutesListeningTimeForLot;

    @Autowired RabbitListenerEndpointRegistry rabbitListenerEndpointRegistry;
    @Autowired SimpleRabbitListenerContainerFactory competingConsumersContainerFactory;
    @Autowired MessageListenerAdapter listenerAdapter;  // da deprecare?
    @Autowired EmailDeliveryMessageReceiver receiver;
    @Autowired ErrorHandler deserializeMessagesErrorHandler;
    @Autowired InstanceInfo applicationInstance;

    private static List<AbstractMessageListenerContainer> containerListeners = new ArrayList<>();
    private static ConcurrentMap<String, Long> idleTimeContainerCollector = new ConcurrentHashMap<>();
    private ExecutorService containerLifecycleJobExecutor;
    private ExecutorService batchContainerLifecycleJobExecutor;
    private ExecutorCompletionService<String> completionService; // verificare se possa servire...

    public ListenersContainersRegistry() {
        this.containerLifecycleJobExecutor = Executors.newSingleThreadExecutor();
        this.batchContainerLifecycleJobExecutor = Executors.newCachedThreadPool();
        this.completionService = new ExecutorCompletionService<>(this.batchContainerLifecycleJobExecutor);
    }

    /**
     * Si occupa di creare un container di consumers/listeners per gestire una coda identificata da 'lotId'
     *
     * il ciclo di vita del container sarà gestito da un 'RabbitListenerEndpointRegistry'
     * @param lotId il lotto e la coda di cui il listener container verrà agganciato e registrato
     * @return l'id se l'operazione ha successo
     */
    public String  createAndRegisterLotListenerContainer(String lotId){

        MessageListenerContainer listenerContainer = rabbitListenerEndpointRegistry.getListenerContainer(lotId);
        if (listenerContainer != null) {
            // verificare se è stoppato e riavviare
            this.containerLifecycleJobExecutor.submit(() -> {
                if(!listenerContainer.isRunning()) listenerContainer.start();
            });
            return lotId;
        }

        Queue queue = new Queue(lotId); // <-- si assume come già creata a seguito dell'esito positivo della fase di validazione...
        SimpleRabbitListenerEndpoint endpoint = new SimpleRabbitListenerEndpoint();
        endpoint.setId(lotId);
        endpoint.setQueues(queue);
        endpoint.setMessageListener(receiver); // al posto dell'adapter...

        rabbitListenerEndpointRegistry.registerListenerContainer(endpoint, competingConsumersContainerFactory);
        SimpleMessageListenerContainer newLotProcessorListenerContainer =
                (SimpleMessageListenerContainer) rabbitListenerEndpointRegistry.getListenerContainer(lotId); // <- questa invocazione crea materialmente il container

        newLotProcessorListenerContainer.setConsumerTagStrategy(s -> applicationInstance.getInstanceId() + "-" + lotId);
        newLotProcessorListenerContainer.setIdleEventInterval(60000L);
        newLotProcessorListenerContainer.setErrorHandler(deserializeMessagesErrorHandler);

        newLotProcessorListenerContainer.start(); // mai dimenticare di avviare il container altrimenti nessun messaggio verrà mai letto :-)

        idleTimeContainerCollector.put(lotId, new Long(0l));

        return lotId;
    }

    // todo: spostare in un oggetto idleListenerManager specifico per container
    private AtomicLong idleEventsCount = new AtomicLong();


    /**
     * gli eventi di idle (periodo di inattività del container e dei consumers associati) sono
     * pubblicati come application events a livello dell'application context di Spring
     * questo listener riceve tutti gli eventi di tipo ListenerContainerIdleEvent nell'intera app
     * @param event un evento di tipo 'ListenerContainerIdleEvent'
     */
    @Async
    @EventListener
    public void onContainerIdleEvent(ListenerContainerIdleEvent event) {

        Long totalIdleTime = event.getIdleTime();
        LOGGER.debug("IDLE TIME EVENT for container[" + event.getListenerId() + "] = " + totalIdleTime );

        if(idleTimeContainerCollector.containsKey(event.getListenerId())) {
            idleTimeContainerCollector.put(event.getListenerId(), event.getIdleTime());
            idleEventsCount.incrementAndGet();
        }
    }

    /**
     *
     * @param event
     */
    @Async
    @EventListener
    public void onConsumerFailEvent(ListenerContainerConsumerFailedEvent event){
        LOGGER.debug("Consumer fail event [" + event.getReason() + "] -> " + event.getSource() );
        LOGGER.trace(event.toString());
    }


    /**
     * ogni 70 secondi controlliamo lo stato dei container per verificare che debbano essere distrutti o meno
     */
    @Scheduled(fixedDelay = 70000)
    public void checkContainersStatus(){

        if(maxMinutesListeningTimeForLot != null) {

            rabbitListenerEndpointRegistry.getListenerContainers().stream().forEach(
                    container -> {
                        LOGGER.debug("Check container[" + container + "] - status " + ((container.isRunning()) ? "RUNNING" : "STOPPED"));
                        if (container.isRunning()) {
                            SimpleMessageListenerContainer sListenerContainer = (SimpleMessageListenerContainer) container;
                            LOGGER.debug("container[" + sListenerContainer.getListenerId() + "] has " + sListenerContainer.getActiveConsumerCount() + " active consumers :: " + sListenerContainer);
                            if (sListenerContainer.getActiveConsumerCount() == minConsumer) {
                                // controllare lo stato di eventi IDLE accumulati dal container...
                                // se superiore a 5 min stoppare..
                                if (idleTimeContainerCollector.containsKey(sListenerContainer.getListenerId())) {
                                    Long totalIdleTime = idleTimeContainerCollector.get(sListenerContainer.getListenerId());
                                    if (totalIdleTime > (60000 * maxMinutesListeningTimeForLot)) {
                                        LOGGER.debug("idleTime max reached.. stopping container " + sListenerContainer.getListenerId());
                                        this.containerLifecycleJobExecutor.submit(() -> {
                                            this.stopListener(sListenerContainer.getListenerId(), false);
                                        });
                                    }
                                }
                            } else {
                                //  controlliamo comunque quanto tempo è passato dall'ultima ricezione per questo container:
//                              sListenerContainer.getLastReceive();  // :-( protected access
                                if (idleTimeContainerCollector.containsKey(sListenerContainer.getListenerId())) {
                                    Long totalIdleTime = idleTimeContainerCollector.get(sListenerContainer.getListenerId());
                                    LOGGER.debug("idleTime container[" + sListenerContainer.getListenerId() + "]=" + totalIdleTime);
                                    if (totalIdleTime > 0) {  // consumer bloccati?
                                        //TODO che fare?
                                    }
                                }
                            }
                        }
                    });
        }
    }

    /**
     * da invocare in casi in cui il servizio deve essere chiuso, per causa disservizi dei server PEC di postacert,
     * o per altre operazioni di manutenzione e monitoraggio
     */
    public void stopAllRunningContainers(){

        List<Callable<String>> jobsList = new ArrayList<>();
        rabbitListenerEndpointRegistry.getListenerContainers().stream().forEach(
                container -> {
                    LOGGER.debug("Check container[" + container + "] - status " + ((container.isRunning()) ? "RUNNING" : "STOPPED"));
                    if (container.isRunning()) {
                        final SimpleMessageListenerContainer sListenerContainer = (SimpleMessageListenerContainer) container;
                        jobsList.add(
                                () -> {
                                    stopListener(sListenerContainer.getListenerId(), false);
                                    return "STOPPED " + sListenerContainer.getListenerId();
                                });
                    }
                }
        );

        try {

            List<Future<String>> jobResults = this.batchContainerLifecycleJobExecutor.invokeAll(jobsList);  // operazione bloccante sul thread!
            // TODO fare qualcosa con questi risultati.. log etc

        } catch (InterruptedException e) {
            LOGGER.error("Errore nell'invocazione dei jobs di stop e chiusura dei listener! " + e.getMessage() + "\n cause: " + e.getCause(), e);
        }
    }

    /**
     * IMPORTANTE: eseguire sempre in un thread separato
     *
     * @param id l'identificativo del container di message-listener, per convenzione corrisponde all'id di un lotto
     * @param unregister se il container deve anche essere de-registrato e quindi garbage-collected
     */
    public void stopListener(String id, boolean unregister) {
        rabbitListenerEndpointRegistry.getListenerContainer(id).stop();
        if(unregister) rabbitListenerEndpointRegistry.unregisterListenerContainer(id);
    }

}
