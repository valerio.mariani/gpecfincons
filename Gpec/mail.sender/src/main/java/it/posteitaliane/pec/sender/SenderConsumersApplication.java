package it.posteitaliane.pec.sender;

import brave.Tracer;
import brave.Tracing;
import brave.context.log4j2.ThreadContextScopeDecorator;
import brave.http.HttpTracing;
import brave.propagation.B3Propagation;
import brave.propagation.CurrentTraceContext;
import brave.propagation.ExtraFieldPropagation;
import brave.propagation.ThreadLocalCurrentTraceContext;
import brave.sampler.Sampler;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import it.posteitaliane.pec.common.integration.EventMessagePublisher;
import it.posteitaliane.pec.sender.model.MailAccount;
import it.posteitaliane.pec.sender.model.MailBoxesConfig;
import it.posteitaliane.pec.sender.model.ServiceConfiguration;
import lombok.*;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.sleuth.zipkin2.ZipkinProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.StringUtils;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import zipkin2.Span;
import zipkin2.reporter.AsyncReporter;
import zipkin2.reporter.Reporter;
import zipkin2.reporter.Sender;
import zipkin2.reporter.amqp.RabbitMQSender;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

//import org.springframework.cloud.sleuth.metric.SpanMetricReporter;
//import org.springframework.cloud.sleuth.zipkin.HttpZipkinSpanReporter;
//import org.springframework.cloud.sleuth.zipkin.ZipkinProperties;
//import org.springframework.cloud.sleuth.zipkin.ZipkinSpanReporter;
//import org.springframework.context.annotation.Bean;
//import zipkin.Span;

@SpringBootApplication
@EnableEurekaClient
@EnableScheduling
@EnableAsync
@EnableCaching
@EnableConfigurationProperties
@EnableSwagger2
public class SenderConsumersApplication {

	@Autowired private EurekaClient eurekaClient;
	@Autowired private ZipkinProperties zipkinProperties;


	@Value("${spring.sleuth.web.skipPattern}") private String skipPattern;
	@Value("${spring.application.name:MailProducer}") private String applicationName;

	public static void main(String[] args) {
		SpringApplication.run(SenderConsumersApplication.class, args);
	}


	@Bean
	public InstanceInfo getCurrentServiceInstanceEurekaID(){
		return eurekaClient.getApplicationInfoManager().getInfo();
	}

	@Bean(name = "smtpMailProperties")
	@ConfigurationProperties( prefix = "spring.mail.properties" )
	public Properties smtpMailProperties(){
		return new Properties();
	}

//	@Bean(name = "pecAccounts")
//	public MailBoxesConfig pecAccounts(){
//		return new ArrayList<>();
//	}

	@Bean
	CurrentTraceContext log4jTraceContext() {
		return ThreadLocalCurrentTraceContext.newBuilder()
				                   .addScopeDecorator(ThreadContextScopeDecorator.create())
				                   .build();
	}

	@Bean
	HttpTracing httpTracing(Tracing tracing) {
		return HttpTracing.create(tracing);
	}

	@Bean
	Reporter<Span> spanReporter(Sender sender) {
		return AsyncReporter.create(sender);
	}

	@Bean
	Sender sender(@Value("${spring.rabbitmq.host}") String rabbitmqHostUrl,
				  @Value("${rabbit.custom.queue.event.actions}") String zipkinQueue,
				  @Value("${spring.rabbitmq.username}") String user,
				  @Value("${spring.rabbitmq.password}") String psw) throws IOException {
		return RabbitMQSender.newBuilder()
				.queue(zipkinQueue)
				.username(user)
				.password(psw)
				.addresses(rabbitmqHostUrl).build();
	}

	@Bean
	Tracing tracing(
			@Value("${spring.application.name:spring-tracing}") String serviceName,
			Reporter<Span> spanReporter,
			CurrentTraceContext log4jTraceContext) {
		return Tracing
				.newBuilder()
				.sampler(Sampler.ALWAYS_SAMPLE)
				.localServiceName(StringUtils.capitalize(serviceName))
				.propagationFactory(ExtraFieldPropagation
						.newFactory(B3Propagation.FACTORY, "client-id"))
				.currentTraceContext(log4jTraceContext)
				.spanReporter(spanReporter)
				.build();
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("it.posteitaliane.pec.mail.sender"))
				.paths(PathSelectors.any())
				.build();
	}

	@Bean
//    @RefreshScope
	public EventMessagePublisher eventsPublisher(
			@Value("${rabbit.custom.routing-key.event.topic.workflow}")
			String managementTopicRoutingKey,
			Tracer tracer,
			RabbitTemplate rabbitTemplate,
			TopicExchange globalManagementExchange ){
		return new EventMessagePublisher(
				applicationName,
				managementTopicRoutingKey,
				tracer,
				rabbitTemplate, globalManagementExchange);
	}

}
