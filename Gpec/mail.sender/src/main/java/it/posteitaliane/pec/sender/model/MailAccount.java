package it.posteitaliane.pec.sender.model;


import lombok.*;

import javax.validation.constraints.Pattern;
import java.io.Serializable;

//@Data
//@ToString
//@AllArgsConstructor
//@Builder
//@EqualsAndHashCode
public class MailAccount implements Serializable {

    @Pattern(regexp = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$")
    private String address;
    private String password;
    private boolean available;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}