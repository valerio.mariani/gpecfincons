package it.posteitaliane.pec.sender.config;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.netflix.appinfo.InstanceInfo;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.*;
import it.posteitaliane.pec.common.configuration.RabbitMQConfiguration;
import it.posteitaliane.pec.common.json.utils.XMLGregorianCalendarConverter;
import it.posteitaliane.pec.sender.listener.EmailDeliveryMessageReceiver;
import it.posteitaliane.pec.sender.listener.EventsMessageListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.RabbitConnectionFactoryBean;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler;
import org.springframework.amqp.rabbit.listener.DirectMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.util.ErrorHandler;

import javax.annotation.PostConstruct;
import javax.xml.datatype.XMLGregorianCalendar;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@RefreshScope
//@EnableRabbit
public class RabbitMQConfig implements RabbitListenerConfigurer, RabbitMQConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQConfig.class);

    @Value("${spring.rabbitmq.host}")
    private String host;
    @Value("${spring.rabbitmq.username}")
    private String user;
    @Value("${spring.rabbitmq.password}")
    private String psw;

    @Value("${spring.rabbitmq.admin.username:#{null}}")
    private String adminUser;

    @Value("${spring.rabbitmq.admin.password:#{null}}")
    private String adminPassword;


    @Value("${rabbit.custom.min.consumer}")
    private Integer minConsumer;
    @Value("${rabbit.custom.max.consumer}")
    private Integer maxConsumer;
    @Value("${rabbit.custom.active.trigger}")
    private Integer activeTrigger;
    @Value("${rabbit.custom.start.min.interval}")
    private Long startMinInterval;
    @Value("${rabbit.custom.stop.min.consumer}")
    private Long stopMinInterval;
    @Value("${rabbit.custom.consecutive.idle}")
    private Integer consecutiveIdle;
    @Value("${rabbit.custom.heartbeat.interval}")
    private Integer heartbeatPeriod;
    @Value("${spring.rabbitmq.ssl.enabled:false}")
    private Boolean sslEnabled;
    @Value("${spring.rabbitmq.virtual-host:/}")
    private String virtualHost;


    private RabbitListenerEndpointRegistrar registrar;

    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar registrar) {
        registrar.setMessageHandlerMethodFactory(messageHandlerMethodFactory());
        this.registrar = registrar;
    }

//    @Bean
//    public RabbitListenerEndpointRegistrar registrar(){
//        return this.registrar;
//    }

    @Bean
    @Primary
    public CachingConnectionFactory factory() {
        return cachingConnectionFactory(sslEnabled, host, virtualHost, user, psw, heartbeatPeriod);
    }


    public ConnectionFactory declareConnectionFactory(){
        String userName = (adminUser != null ? adminUser : user);
        String userPsw = (adminPassword != null ? adminPassword : psw);
        return adminConnectionFactory(host, userName, userPsw);
    }

    @Bean
    public MessageConverter converter() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(localDateTimeModule());
        Jackson2JsonMessageConverter jackson2Json = new Jackson2JsonMessageConverter(mapper);
        return jackson2Json;
    }

    public Module localDateTimeModule(){
        JavaTimeModule module = new JavaTimeModule();
        LocalDateTimeDeserializer localDateTimeDeserializer =  new
                LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        module.addDeserializer(LocalDateTime.class, localDateTimeDeserializer);
        return module;
    }

    @Bean
    public MappingJackson2MessageConverter consumerJackson2MessageConverter() { return new MappingJackson2MessageConverter(); }

    @Bean
    @Primary
    public Gson gson(){
       return new GsonBuilder()
                .registerTypeAdapter(XMLGregorianCalendar.class, new XMLGregorianCalendarConverter.GsonDeserializer())
                .registerTypeAdapter(XMLGregorianCalendar.class, new XMLGregorianCalendarConverter.GsonSerializer())
//                .registerTypeAdapterFactory(new BaseResponseTypeAdapterFactor())
                .create();
    }

    @Bean(name = competingConsumersContainerFactory)
    public SimpleRabbitListenerContainerFactory competingConsumersContainerFactory(){

        return competingConsumersContainerFactory(this.minConsumer, this.maxConsumer,
                factory(), converter(),
                activeTrigger, consecutiveIdle, startMinInterval, stopMinInterval);

    }

    @Bean
    public SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory(){
       return simpleRabbitListenerContainerFactory( factory(), converter());
    }

//    @Bean
//    public HealthIndicator rabbitQueueCheckHealthIndicator()
//    {
//        RabbitQueueCheckHealthIndicator healthIndicator = new RabbitQueueCheckHealthIndicator();
//        healthIndicator.addQueueCheck(simpleQueue(), 1000, 1);
//        healthIndicator.addQueueCheck(simpleQueueDlx(), 1, 0);
//        return healthIndicator;
//    }

    /**
     *
     * @return
     */
    @Bean
    public Client rabbitAdminClient(){

        String userName = (adminUser != null ? adminUser : user);
        String userPsw = (adminPassword != null ? adminPassword : psw);

        Client c = null;
        try {
            c = createRabbitAPIClient(userName, userPsw, host);
        } catch (IllegalStateException e) {
            LOGGER.error("Errore nell'accesso all'API rest di RabbitMQ (hosts:" + host + ")", e);
        }
        return c;
    }

    /**
     * Required for executing administration functions against an AMQP Broker
     */
    @Bean
    @Primary
    public RabbitAdmin rabbitAdmin() {
        return new RabbitAdmin(declareConnectionFactory());
    }

    @Bean
    public DefaultMessageHandlerMethodFactory messageHandlerMethodFactory() {
        return messageHandlerMethodFactory(consumerJackson2MessageConverter());
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(EmailDeliveryMessageReceiver receiver) {
        //in questo modo sto dicendo che il il consumer della coda
        //e' il metodo "receiveMessage" della classe "EmailDeliveryMessageReceiver"
        MessageListenerAdapter adapter = new MessageListenerAdapter(receiver, "receiveMessage");
        adapter.setMessageConverter(converter());
        return adapter;
    }

    /**
     * sistema di tracing: tutte le trace passano attraverso questa coda
     *
     * @param trace_actions_queue
     * @return
     */
    @Bean
    public Queue traceEventsZipkinQueue(@Value("${rabbit.custom.queue.event.actions}") final String trace_actions_queue){
        return tracerZipkinQueue(trace_actions_queue);
    }

    @Bean
    public TopicExchange globalManagementExchange(@Value("${rabbit.custom.exchange.event.management}") final String eventManagementExchange){
        return new TopicExchange(eventManagementExchange);
    }

    @Bean
    public TopicExchange globalNotificationsExchange(@Value("${rabbit.custom.exchange.event.notifications}") final String notificationsExchange){
        return new TopicExchange(notificationsExchange);
    }

    // delivery:
    @Bean
    public DirectExchange lotItemDeliveryExchange(@Value("${rabbit.custom.exchange.lot.deliveries}") final String exchangeName){
        return new DirectExchange(exchangeName);
    }


    @Autowired
    InstanceInfo currentInstanceInfo;

    @Bean
    public Queue workflowEvents(
            @Value("${rabbit.custom.queue.event.management}") final String queueName,
            Client rabbitAdminClient){
        Queue eventQueue = new Queue(queueName + "_" +
                    currentInstanceInfo.getAppName() + "[" +
                    currentInstanceInfo.getId().split(":")[0] +  // .split(".")[0]
                "]", true);
//        eventQueue.setShouldDeclare(true);
//        eventQueue.setActualName(queueName);
//        rabbitAdminClient.declareQueue("/", eventQueue.getName(), new QueueInfo(true, false, false));
        return eventQueue;
    }

    @Bean
    @ConditionalOnProperty(prefix = "rabbit.custom", name = "cluster.ha", havingValue = "dynamic")
    public Definitions declareBindingPolicies(
            @Value("${rabbit.custom.exchange.event.notifications}") final String exchange,
            @Value("${rabbit.custom.routing-key.event.topic.notifications}") final String routingKey,
            Queue workflowEvents,
            Queue notificationsQueue,
            TopicExchange globalNotificationsExchange,
            TopicExchange globalManagementExchange,
            Client rabbitAdminClient ){

        rabbitAdminClient.declarePolicy("/",
                "HA-federate-Policy",
                federatedExchangeHAStrategyPolicy( "lot." ) );

//        rabbitAdminClient.bindQueue("/", notificationsQueue.getName(), exchange, routingKey);
        int clusterSize = rabbitAdminClient.getNodes().size();
        rabbitAdminClient.declarePolicy("/",
                "HA-Lots-Policy",
                quorumBasedQueueMirroringHAStrategyPolicy(null, "lot.", "queues"));

        rabbitAdminClient.declarePolicy("/",
                "HA-Manage-Policy",
                quorumBasedQueueMirroringHAStrategyPolicy(clusterSize, "gpec", "queues"));

        return rabbitAdminClient.getDefinitions();
    }

    @Bean
    public Binding eventManagementTopicBinding(
            final Queue workflowEvents,
            final TopicExchange globalManagementExchange,
            @Value("${rabbit.custom.routing-key.event.topic.workflow}") final String routingKey){
        return BindingBuilder.bind(workflowEvents).
                    to(globalManagementExchange).
                    with(routingKey + ".lot");
    }

    @Bean
    public Queue notificationsQueue(
            @Value("${rabbit.custom.queue.event.notifications}") final String queueName,
            Client rabbitAdminClient){
        Queue notificationsQueue = new Queue(queueName, true);
//        notificationsQueue.setShouldDeclare(true);
//        notificationsQueue.setActualName(queueName);
        rabbitAdminClient.declareQueue("/", notificationsQueue.getName(), new QueueInfo(true, false, false));
        return notificationsQueue;
    }


    @Bean
    public DirectMessageListenerContainer eventsQueueListenerContainer(
            CachingConnectionFactory factory,
            Queue workflowEvents,
            EventsMessageListener listener){
        DirectMessageListenerContainer container = new DirectMessageListenerContainer(factory);
        container.setQueueNames(workflowEvents.getName());
        container.setMessageListener(listener);

        // Set Exclusive Consumer 'ON'
//        container.setExclusive(true);
        container.setConsumerTagStrategy(queue -> currentInstanceInfo.getId() + "." + queue);
//        container.setMessageConverter();
        // Should be restricted to '1' to maintain data consistency.
        container.setConsumersPerQueue(1);
//        container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
        return container;
    }



    @Bean
    public ErrorHandler deserializeMessagesErrorHandler() {

        // SMTPRetriesFailuresStrategy()
        return new ConditionalRejectingErrorHandler(
                new DeserializeMessageErrorStrategy()
        );
    }

    @Bean
    @Primary
    public RabbitTemplate configuredRabbitTemplate( MessageConverter converter ) {
        return configuredRabbitTemplate(factory(), converter);
    }


    // UTILITIES & DEBUGGING
    // Todo
//    @PostConstruct
//    void printConnectionConfig(CachingConnectionFactory factory) {
//        logger.info("connectionFactory={}, queue={}, addresses={}",
//                factory.getRabbitConnectionFactory(),
//                this.queue,
//                config.determineAddresses());
//    }

}
