package it.posteitaliane.pec.sender.controller;


import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.QueueInfo;
import it.posteitaliane.pec.sender.model.JobPriority;
import it.posteitaliane.pec.sender.model.ManagementJob;
import it.posteitaliane.pec.sender.processing.PriorityJobScheduler;
import it.posteitaliane.pec.sender.services.ListenersContainersRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/management")
//@Profile("local")
public class SendManagementController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SendManagementController.class);

    @Value("${spring.rabbitmq.virtual-host:/}")
    private String virtualHost;

    @Autowired Client rabbitClient;
    @Autowired ListenersContainersRegistry listenersContainersRegistry;

//    private final HazelcastInstance hazelcastInstance;

//    @Autowired
//    LotManagementController(HazelcastInstance hazelcastInstance) {
//        this.hazelcastInstance = hazelcastInstance;
//    }
//
//
//    @PostMapping(value = "/create")
//    public String writeLotDataToHazelcast(@RequestParam String lot, @RequestParam String value) {
//        Map<String, String> hazelcastMap = hazelcastInstance.getMap("processing.lot");
//        hazelcastMap.put(lot, value);
//        return "Data is stored.";
//    }
//
//    @GetMapping(value = "/current")
//    public Integer readCurrentProcessedMail(@RequestParam String lot) {
//        Map<String, Integer> hazelcastMap = hazelcastInstance.getMap("processing.lot");
//        return hazelcastMap.get(lot);
//    }
//
//
//    @GetMapping(value = "/")
//    public Map<String, Integer> readAllDataFromHazelcast() {
//        Map<String, Integer> hazelcastMap = hazelcastInstance.getMap("processing.lot");
//        return hazelcastMap;
//    }


    @GetMapping( value = "/queue/{queue-id}/info" )
    public QueueInfo getQueueInfo(@PathVariable(name = "queue-id") String id) {
        QueueInfo q = rabbitClient.getQueue(virtualHost, id);
        return q;
    }


    private static final PriorityJobScheduler priorityJobScheduler = new PriorityJobScheduler(
            1, 10);

    @RequestMapping( value = "/lot/{queue-id}/stop", method = GET )
    @ResponseBody
    public ResponseEntity<?> stopQueue(@PathVariable("queue-id") final String id){

        priorityJobScheduler.scheduleJob( ManagementJob.builder().
                jobName("stop-listening-to-queue-" + id).
                jobPriority(JobPriority.MEDIUM).
                task(() -> {
                    try {
                        listenersContainersRegistry.stopListener(id, false);
                    }catch(Exception e){

                    }

                }).
                build());

        return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
    }


    @RequestMapping( value = "/lot/{queue-id}", method = DELETE )
    @ResponseBody
    public ResponseEntity<?> deleteQueue(@PathVariable("queue-id") final String id){
        priorityJobScheduler.scheduleJob( ManagementJob.builder().
                jobName("deleting-queue-" + id).
                jobPriority(JobPriority.LOW).
                task(() -> {
                    QueueInfo info = rabbitClient.getQueue(virtualHost, id);
                    if(info == null) return;

                    try {
                        listenersContainersRegistry.stopListener(id, true);
                    }catch(Exception e){

                    }
                    rabbitClient.deleteQueue(virtualHost, id);

                }).
                build());

        return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
    }



    @RequestMapping( value = "/lot/{queue-id}/start", method = GET )
    @ResponseBody
    public ResponseEntity<?> startListeningQueue(@PathVariable("queue-id") String id){

        try {
            listenersContainersRegistry.createAndRegisterLotListenerContainer(id);
        } catch (Exception e) {
            LOGGER.error("Error starting listener", e);
            return new ResponseEntity<>("ERROR", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
    }

}
