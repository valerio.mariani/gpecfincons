package it.posteitaliane.pec.sender.model;


import lombok.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

//@Data
//@ToString
//@AllArgsConstructor
//@Builder
//@EqualsAndHashCode
@Component
//@PropertySource("classpath:application-local.yml")
@ConfigurationProperties( prefix = "email.sender", ignoreUnknownFields = false)
public class MailBoxesConfig {

    private List<ServiceConfiguration> services = new ArrayList<>();

    public MailBoxesConfig() { }

    public List<ServiceConfiguration> getServices() {
        return services;
    }

    public void setServices(List<ServiceConfiguration> services) {
        this.services = services;
    }
}
