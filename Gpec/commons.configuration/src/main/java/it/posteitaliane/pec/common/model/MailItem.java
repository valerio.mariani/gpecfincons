package it.posteitaliane.pec.common.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class MailItem {

    private final EventType status;
    private final String mailDeliveryID;

    public MailItem(EventType status, String mailDeliveryID) {
        this.status = status;
        this.mailDeliveryID = mailDeliveryID;
    }
}
