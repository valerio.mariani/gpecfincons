package it.posteitaliane.pec.common.repositories;


import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.MailCsv;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import org.springframework.cloud.sleuth.annotation.ContinueSpan;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface MailDeliveryRepository extends CrudRepository<MailDeliveryRequest, String> {
    @NewSpan("Search Delivered Mails by event")
    List<MailDeliveryRequest> findByEvents(@SpanTag("event") EventType event);
    List<MailDeliveryRequest> findByLotId(String lotId);


    /*
    * SELECT
    * META(`lots`).id AS _ID, META(`lots`).cas AS _CAS, `lots`.* FROM `lots`   --> #{#n1ql.selectEntity}
    * WHERE
    * `_class` = \"it.posteitaliane.pec.common.model.MailDeliveryRequest\" --> #{#n1ql.filter}
    * AND lotId = $1 AND ANY v IN events SATISFIES v.event = $2 END;
    *
    * */
    @Query("#{#n1ql.selectEntity} WHERE #{#n1ql.filter} AND lotId = $1 AND ANY v IN events SATISFIES v.event = $2 END;")
    List<MailDeliveryRequest> findByLotIdAndEvent(String lotId,String event);


    @Query("#{#n1ql.selectEntity} WHERE #{#n1ql.filter} AND ANY v IN events SATISFIES v.event = $1 END;")
    List<MailDeliveryRequest> findByEvent(String event);

    @Query("SELECT count(*) FROM `lots` WHERE #{#n1ql.filter} AND ANY v IN events SATISFIES v.event = $1 END;")
    long findByEventAndCount(String event);



    /*
    * amount: quantità del delta temporale
    * type: tipo del delta: giorni, ore etc ..
    * es: amount=3 e type=day , indica un delta di 3 giorni
    *
    * Valori accettati per il campo type:
    * millennium
    * century
    * decade
    * year
    * quarter
    * month
    * week
    * day
    * hour
    * minute
    * second
    * millisecond
    *
    * */
    @Query("#{#n1ql.selectEntity} WHERE #{#n1ql.filter} AND (ANY v IN events SATISFIES (v.event = 'MESSAGGIO_INVIABILE' AND CLOCK_MILLIS() > DATE_ADD_MILLIS(v.time, $1, $2)) END) EXCEPT #{#n1ql.selectEntity} WHERE #{#n1ql.filter} AND (ANY v IN events SATISFIES v.event = 'MESSAGGIO_INVIO_FALLITO' OR  v.event = 'MESSAGGIO_INVIO_SUCCESSO' END)")
    List<MailDeliveryRequest> findProcessingTimeTooLong(String amount,String type);



    /*
     * dateIsoFrom e dateIsoTo formato ISO (es. 2016-02-01T00:00:00)
     *
     * */
    @Query("SELECT count(*)\n" +
            "FROM `lots`\n" +
            "UNNEST events v\n" +
            "WHERE \n" +
            "v.event = 'MESSAGGIO_CONSEGNA_OK' AND\n" +
            "MILLIS_TO_STR(v.time) BETWEEN $1 AND $2")
    long countElaboratedMessageInDateRange(String dateIsoFrom,String dateIsoTo);


    /*
     * dateIsoFrom e dateIsoTo formato ISO (es. 2016-02-01T00:00:00)
     *
     * */
    @Query("SELECT META(`lots`).id AS _ID, META(`lots`).cas AS _CAS, `lots`.* FROM `lots`\n" +
            "UNNEST events v\n" +
            "WHERE \n" +
            "v.event = 'MESSAGGIO_CONSEGNA_OK' AND\n" +
            "MILLIS_TO_STR(v.time) BETWEEN $1 AND $2")
    List<MailCsv> elaboratedMessageInDateRange(String dateIsoFrom, String dateIsoTo);

}
