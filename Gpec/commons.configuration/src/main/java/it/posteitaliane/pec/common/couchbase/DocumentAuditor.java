package it.posteitaliane.pec.common.couchbase;

import org.springframework.data.domain.AuditorAware;
import java.util.Optional;

/**
 *
 */
public class DocumentAuditor implements AuditorAware<String> {

    private String auditor = "no_auditor_selected";


    public DocumentAuditor(String auditor) {
        this.auditor = auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(this.auditor);
    }
}