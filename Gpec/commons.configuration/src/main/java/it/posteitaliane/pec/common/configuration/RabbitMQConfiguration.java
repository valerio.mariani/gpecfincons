package it.posteitaliane.pec.common.configuration;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.domain.OverviewResponse;
import com.rabbitmq.http.client.domain.PolicyInfo;
import org.aopalliance.aop.Advice;
import org.apache.http.conn.HttpHostConnectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.util.Assert;
import org.springframework.web.client.RestClientException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public interface RabbitMQConfiguration {

    String rabbitListenerContainerFactory =  "simpleRabbitListenerContainerFactory";
    String competingConsumersContainerFactory = "competingConsumersContainerFactory";

    /**
     *
     * @param hosts
     * @param user
     * @param psw
     * @return
     */
    default CachingConnectionFactory cachingConnectionFactory(
            Boolean isSecured,
            String hosts,
            String virtualHost,
            String user,
            String psw,
            Integer heartbeatPeriod) {
        Logger LOGGER = LoggerFactory.getLogger("RabbitMQ:ConnectionFactory:configuration");
        com.rabbitmq.client.ConnectionFactory rabbitmqConnectionfactory = new com.rabbitmq.client.ConnectionFactory();

        if(isSecured){  // SSL connection over TLS

            SSLContext sslContext;
            try {

                sslContext = SSLContext.getInstance("TLS");
                sslContext.init(null,null,null);  // ??

            } catch (Exception e) {

                LOGGER.error("Unable to configure SSL for connecting to secured AMQP"
                        + e.getMessage(), e);

                //do not startup application
                throw new RuntimeException(e.getMessage());
            }

            SSLSocketFactory sslSocketFactory = (SSLSocketFactory)sslContext.getSocketFactory();

            rabbitmqConnectionfactory.setSocketFactory(sslSocketFactory);
        }

//        rabbitmqConnectionfactory.setHost(hosts);
        rabbitmqConnectionfactory.setVirtualHost(virtualHost);
        rabbitmqConnectionfactory.setUsername(user);
        rabbitmqConnectionfactory.setAutomaticRecoveryEnabled(false);
        rabbitmqConnectionfactory.setPassword(psw);

        //create spring connection factory based on rabbitmq connection factory
        CachingConnectionFactory factory = new CachingConnectionFactory(rabbitmqConnectionfactory);
        if(hosts.indexOf(",") != -1 ) factory.setAddresses(hosts);
        else factory.setHost(hosts);
        factory.setRequestedHeartBeat( (heartbeatPeriod / 1000));

//        MeterRegistry jmxRegistry = new JmxMeterRegistry(JmxConfig.DEFAULT, Clock.SYSTEM);
//        MicrometerMetricsCollector metricsCollector = new MicrometerMetricsCollector(
//                jmxRegistry, "rabbitmq.client"
//        );
//
//        factory.getRabbitConnectionFactory().setMetricsCollector(metricsCollector);
        factory.afterPropertiesSet();
        factory.setPublisherReturns(true);
        return factory;
    }


    /**
     * per accedere alle funzionalità di amministrazione è necessaria una connection facory con l'utenze di Admin
     * TODO migliorare
     * @param hosts
     * @param adminUser
     * @param adminPassword
     * @return
     */
    default ConnectionFactory adminConnectionFactory(String hosts, String adminUser, String adminPassword){

        CachingConnectionFactory connectionFactory =
                new CachingConnectionFactory();
        if(hosts.indexOf(",") != -1 ) connectionFactory.setAddresses(hosts);
        else connectionFactory.setHost(hosts);
        connectionFactory.setUsername(adminUser);
        connectionFactory.setPassword(adminPassword);
        connectionFactory.setCacheMode(CachingConnectionFactory.CacheMode.CHANNEL);
        return connectionFactory;
    }


    /**
     * La Bean factory per produrre un container che implementa il competing consumer..
     * usare solamente per i sender che implmentaranno un high throughput message consuming
     *
     * @param minConsumer
     * @param maxConsumer
     * @param factory
     * @param converter
     * @param activeTrigger
     * @param consecutiveIdle
     * @param startMinInterval
     * @param stopMinInterval
     * @return
     */
    default SimpleRabbitListenerContainerFactory competingConsumersContainerFactory(
            Integer minConsumer,
            Integer maxConsumer,
            ConnectionFactory factory,
            MessageConverter converter,
            Integer activeTrigger,
            Integer consecutiveIdle,
            Long startMinInterval,
            Long stopMinInterval ) {

        SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory = new SimpleRabbitListenerContainerFactory();

        simpleRabbitListenerContainerFactory.setConcurrentConsumers(minConsumer);
        simpleRabbitListenerContainerFactory.setMaxConcurrentConsumers(maxConsumer);

        simpleRabbitListenerContainerFactory.setConnectionFactory(factory);
        simpleRabbitListenerContainerFactory.setMessageConverter(converter);
        simpleRabbitListenerContainerFactory.setChannelTransacted(true);
        // verificare...
        simpleRabbitListenerContainerFactory.setTxSize(minConsumer);
        simpleRabbitListenerContainerFactory.setPrefetchCount(minConsumer);

        simpleRabbitListenerContainerFactory.setConsecutiveActiveTrigger(activeTrigger);
        simpleRabbitListenerContainerFactory.setConsecutiveIdleTrigger(consecutiveIdle);
        simpleRabbitListenerContainerFactory.setStartConsumerMinInterval(startMinInterval);
        simpleRabbitListenerContainerFactory.setStopConsumerMinInterval(stopMinInterval);
        simpleRabbitListenerContainerFactory.setErrorHandler(new ConditionalRejectingErrorHandler(
                new DeserializeMessageErrorStrategy()
        ));

        //TODO elaborare meglio le policies di retry o di rifiuto dei messaggi che saranno abbastanza complesse
//        container.setDefaultRequeueRejected(false)
        simpleRabbitListenerContainerFactory.setAdviceChain(new Advice[] {
                org.springframework.amqp.rabbit.config.RetryInterceptorBuilder
                        .stateless()
                        .maxAttempts(5)
                        .backOffOptions(1000, 2, 5000)
                        .build()
        });

        return simpleRabbitListenerContainerFactory;
    }


    /**
     * La Bean factory per produrre dei listener container per connessioni a code di gestione.. che prevedon
     * solitamente solo un producer -> un consumer SUL SERVIZIO.. (es: la coda degli eventi, delle notifiche, etc)
     * @param factory
     * @param converter
     * @return
     */
    default SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory(
            ConnectionFactory factory,
            MessageConverter converter
    ){
        SimpleRabbitListenerContainerFactory simpleRabbitListenerContainerFactory = new SimpleRabbitListenerContainerFactory();
        simpleRabbitListenerContainerFactory.setConcurrentConsumers(1);
        simpleRabbitListenerContainerFactory.setMaxConcurrentConsumers(2);
        simpleRabbitListenerContainerFactory.setIdleEventInterval(60000L);  // solo per test... poi rimuovere
        simpleRabbitListenerContainerFactory.setConnectionFactory(factory);
        simpleRabbitListenerContainerFactory.setMessageConverter(converter);

        return simpleRabbitListenerContainerFactory;
    }


    default DefaultMessageHandlerMethodFactory messageHandlerMethodFactory(MappingJackson2MessageConverter converter) {
        DefaultMessageHandlerMethodFactory factory = new DefaultMessageHandlerMethodFactory();
        factory.setMessageConverter(converter);
        return factory;
    }


    default MappingJackson2MessageConverter defaultJackson2MessageConverter() {
        MappingJackson2MessageConverter mappingJackson2MessageConverter = new MappingJackson2MessageConverter();
        mappingJackson2MessageConverter.getObjectMapper().setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        mappingJackson2MessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return mappingJackson2MessageConverter;
    }

    default PolicyInfo quorumBasedQueueMirroringHAStrategyPolicy(Integer clusterSize, String policyPatternIdentifier, String applyTo){
        Assert.notNull(policyPatternIdentifier, "la policy richiede un identificativo obbligatorio");
        String param = "all";
        Map<String, Object> definitions = new HashMap<>();
        if(clusterSize != null ) {
            definitions.put("ha-mode", "exactly");
            definitions.put("ha-params", (clusterSize/2 + 1));
        }else{
            definitions.put("ha-mode", "all");
        }
        definitions.put("ha-sync-mode","automatic");
        return new PolicyInfo("^" + policyPatternIdentifier, 1, applyTo, definitions);
    }

    default PolicyInfo federatedExchangeHAStrategyPolicy(String policyPatternIdentifier){
        Map<String, Object> definitions = new HashMap<>();
        definitions.put("federation-upstream-set", "all");
        return new PolicyInfo("^" + policyPatternIdentifier, 1, "exchanges", definitions);
    }

    /**
     *
     * @return
     */
    default RabbitTemplate configuredRabbitTemplate(ConnectionFactory factory, MessageConverter converter) {
        RabbitTemplate rt = new RabbitTemplate();
        rt.setConnectionFactory(factory);
        rt.setMessageConverter(converter);
        rt.setUsePublisherConnection(true);
        return rt;
    }

    /**
     * metodo che cerca di ottenere un client da uno o più host
     * @param userName
     * @param userPsw
     * @param host
     * @return
     * @throws IllegalArgumentException
     */
    default Client createRabbitAPIClient(String userName, String userPsw, String host)
            throws IllegalStateException {
        Logger LOGGER = LoggerFactory.getLogger("RabbitMQ:ClientAdmin:configuration");
        String[] hosts = host.split(",");
        OverviewResponse overview = null;
        Client c = null;

        for (String s : hosts) {  // tentiamo per ogni host fino a  che non ne prendiamo uno che risponde
            String aRabbitHost = "http://" + s + ":15672/api/";
            try {
                c = new Client(aRabbitHost, userName, userPsw);
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException ("La url di accesso al nodo RabbitMQ " + aRabbitHost + " risulta malformata", e);
            } catch (URISyntaxException e) {
                throw new IllegalArgumentException ("La url di accesso al nodo RabbitMQ " + aRabbitHost + " presenta errori di sintassi", e);
            }
            try {
                overview = c.getOverview();  // sia a scopo di log che per testare la connessione. non è sufficiente avere il client...
            } catch (RestClientException clientConnectException) {
                LOGGER.error("Errore nell'accesso all'API rest di RabbitMQ (host:" + aRabbitHost + ")", clientConnectException);
                continue;
            }
            if(c != null && overview != null) break;
        }

        if(c == null || overview == null) throw new IllegalStateException("Impossibile ottenere un Client di amministrazione sul cluster RabbitMQ " + host);
        LOGGER.info("Successful connecting to RabbitMQ host: " + overview);
        return c;
    }



    // * * * * * * * * * CODE * * * * * * * * * * * //
    /**
     * sistema di tracing: tutte le trace passano attraverso questa coda
     * @param trace_actions_queue
     * @return
     */
    default Queue tracerZipkinQueue(final String trace_actions_queue){
        return new Queue(trace_actions_queue, true);
    }


    class DeserializeMessageErrorStrategy extends ConditionalRejectingErrorHandler.DefaultExceptionStrategy {

        private final Logger logger = org.slf4j.LoggerFactory.getLogger(getClass());

        @Override
        public boolean isFatal(Throwable t) {
            if (t instanceof ListenerExecutionFailedException) {
                ListenerExecutionFailedException lefe = (ListenerExecutionFailedException) t;
                logger.error("Failed to process inbound message from queue "
                        + lefe.getFailedMessage().getMessageProperties().getConsumerQueue()
                        + "; failed message: " + lefe.getFailedMessage(), t);
            }
            return super.isFatal(t);
        }

    }

}
