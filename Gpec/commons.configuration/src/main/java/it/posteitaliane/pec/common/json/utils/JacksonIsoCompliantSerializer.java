package it.posteitaliane.pec.common.json.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;


public class JacksonIsoCompliantSerializer extends StdSerializer<Date> {

    //private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    //Calendar cal = Calendar.getInstance();
    // create formatter
    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyy HH:mm:ss");

    // parse string and set the resulting date to Calendar



    public JacksonIsoCompliantSerializer() {
        this(null);
    }

    public JacksonIsoCompliantSerializer(Class<Date> t) {
        super(t);
    }

    @Override
    public void serialize(
            Date value,
            JsonGenerator gen,
            SerializerProvider arg2)
            throws IOException, JsonProcessingException {

        gen.writeString(df.format(value));
    }


}
