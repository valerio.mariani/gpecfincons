package it.posteitaliane.pec.common.model;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import it.posteitaliane.pec.common.json.utils.JacksonIsoCompliantSerializer;
import it.posteitaliane.pec.common.json.utils.JacksonLocalDateTimeSerializer;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.query.N1qlSecondaryIndexed;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
/*
"version": "0.0.4-SNAPSHOT",
"artifact": "gateway",
"name": "Application <name>",
"group": "it.posteitaliane.pec",
"time": "2019-03-05T14:32:25.126Z"
 */
public class ServiceStatusInfoDetails {

    @Field
    private String version;

    @Field
    private String artifact;

    @Field
    private String name;

    @Field
    private String group;

    @Field
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    @JsonSerialize(using = JacksonIsoCompliantSerializer.class)
    private Date time;

}
