package it.posteitaliane.pec.common.model;


import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class SendingDetails {

    private Integer sendAttempt;
    private String address;

    private Long executionTime;

}
