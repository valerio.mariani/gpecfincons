package it.posteitaliane.pec.common.configuration;

import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import com.netflix.appinfo.InstanceInfo;

public abstract class DefaultHazelcastConfiguration {

    public Config hazelCastConfig(InstanceInfo currentInstanceInfo) {
        Config config = new Config();
        config.setProperty("hazelcast.jmx", "true");
        config.setInstanceName("cache:" + currentInstanceInfo.getInstanceId())
                .addMapConfig(
                        new MapConfig()
                                .setName("current-mail-sendings")
                                .setMaxSizeConfig(new MaxSizeConfig(200, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
                                .setEvictionPolicy(EvictionPolicy.LRU)
                                .setTimeToLiveSeconds(60));  // un minuto ?
        return config;
    }
}
