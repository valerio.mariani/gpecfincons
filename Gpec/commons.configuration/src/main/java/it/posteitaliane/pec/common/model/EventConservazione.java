package it.posteitaliane.pec.common.model;

public enum EventConservazione {

    TAKEN_OVER("Presa in carico"),
    MESSAGE_CREATE("Messaggio creato"),
    MESSAGE_CREATE_ERROR("Creazione Messaggio errore "),
    MESSAGE_SEND("Messaggio inviato"),
    CHUNCK_ZIP_LOTTO_SENDER("Zip lotto inviato"),
    CHUNCK_ZIP_LOTTO_SENDER_ERROR("Zip lotto inviato errore"),
    LOTTO_CHUNK_SENDER("Lotto parzialemnte completato"),
    LOTTO_FULL_SENDER("Lotto interamente completato");
    private String description;

    EventConservazione(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}