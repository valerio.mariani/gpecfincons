package it.posteitaliane.pec.common.model;

import com.couchbase.client.java.repository.annotation.Id;
import com.opencsv.bean.CsvBindByName;
import org.springframework.data.couchbase.core.mapping.Document;

import java.util.Objects;


@Document
public class LotCsv {
    @CsvBindByName(column = "ID LOTTO")
    @Id
    private final String lotId;
    @CsvBindByName(column = "CODICE CLIENTE")
    private final String customerCode;
    @CsvBindByName(column = "SERVIZIO")
    private final String service;
    @CsvBindByName(column = "STATO LOTTO")
    private final EventType currentStatus;
    @CsvBindByName(column = "N. MESSAGGI")
    private final Integer messagesProcessed;


    public LotCsv(String lotId, String customerCode, String service, EventType currentStatus, Integer messagesProcessed) {
        this.lotId = lotId;
        this.customerCode = customerCode;
        this.service = service;
        this.currentStatus = currentStatus;
        this.messagesProcessed = messagesProcessed;
    }

    public String getLotId() {
        return this.lotId;
    }

    public String getCustomerCode() {
        return this.customerCode;
    }

    public String getService() {
        return this.service;
    }

    public  EventType getCurrentStatus() {
        return this.currentStatus;
    }

    public Integer getMessagesProcessed() {
        return this.messagesProcessed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LotCsv)) return false;
        LotCsv lotCsv = (LotCsv) o;
        return Objects.equals(getLotId(), lotCsv.getLotId()) &&
                Objects.equals(getCustomerCode(), lotCsv.getCustomerCode()) &&
                Objects.equals(getService(), lotCsv.getService()) &&
                getCurrentStatus() == lotCsv.getCurrentStatus() &&
                Objects.equals(getMessagesProcessed(), lotCsv.getMessagesProcessed());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getLotId(), getCustomerCode(), getService(), getCurrentStatus(), getMessagesProcessed());
    }
}