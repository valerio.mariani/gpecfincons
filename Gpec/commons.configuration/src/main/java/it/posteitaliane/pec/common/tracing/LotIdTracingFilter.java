package it.posteitaliane.pec.common.tracing;


import brave.Span;
import brave.Tracer;
import brave.Tracing;
import brave.propagation.TraceContextOrSamplingFlags;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

public class LotIdTracingFilter extends GenericFilterBean {

    private final Tracer tracer;
    private final Tracing tracing;

    public LotIdTracingFilter(Tracer tracer, Tracing tracing) {
        this.tracer = tracer;
        this.tracing = tracing;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
//        HeaderC
        Span currentSpan = this.tracer.currentSpan();
        if (currentSpan == null) {
            // proviamo a fare il join dello span con il lotId come header
//            TraceContextOrSamplingFlags result =
//                    tracing.propagation().extractor(request.).extract(request);
            currentSpan = this.tracer.newTrace().name("start-endpoint").start();

//            chain.doFilter(request, response);
//            return;
        }



        // for readability we're returning trace id in a hex form
        ((HttpServletResponse) response).addHeader("ZIPKIN-TRACE-ID",
                currentSpan.context().traceIdString());
        // we can also add some custom tags
//        currentSpan.tag("custom", "tag");
        chain.doFilter(request, response);
    }
}
