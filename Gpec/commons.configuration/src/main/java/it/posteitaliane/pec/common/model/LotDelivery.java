package it.posteitaliane.pec.common.model;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import it.posteitaliane.pec.common.json.utils.JacksonLocalDateTimeSerializer;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.query.N1qlSecondaryIndexed;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
@N1qlSecondaryIndexed( indexName = "idx_mailDeliveriesIDs")
public class LotDelivery {

    @NotNull
    @Id
    private String lotId;

//    @Version
//    @Null
//    private long version = 0L;

    @Field
    private String service;

    @NotNull
    @Field
    private String customerCode;

    @Field
    private EventType currentStatus;

    @Field
    @NotNull
    private String created;

    @Field
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = JacksonLocalDateTimeSerializer.class)
    private LocalDateTime updated;

    @Field
//    @JsonSerialize(using = JacksonLocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime closed;

    @Field
    private List<Events> events = new ArrayList<>();

    @Field
    private List<String> mailDeliveriesIDs;


    /**
     * Numero di messaggi contenuti nel lotto
    */
    @Field
    private Integer messagesProcessed;


    /**
     * Numero di messaggi che non hanno passato lo step di validazione
     */
    @Field
    private Integer messagesNotValidated;


    /**
     * Numero di messaggi per i quali è arrivata ricevuta di mancata accettazione
     */
    @Field
    private Integer messagesNotAccepted;


    /**
     * Numero di messaggi per i quali è arrivata ricevuta di mancata consegna
     */
    @Field
    private Integer messagesNotDelivered;


    /**
     * Numero di messaggi per i quali è arrivata ricevuta di avvenuta accettazione
     */
    @Field
    private Integer messagesSuccess;

    //messagesProcessed= messagesNotValidated + messagesNotAccepted + messagesNotDelivered + messagesSuccess

    /*
     * * * * *    A U D I T IN G     D A T A    * * * * *
     */

    @CreatedBy
    private String creator;

    @LastModifiedBy
    private String lastModifiedBy;

    @LastModifiedDate
    private LocalDateTime lastModification;

    @Field
    private List<InfoConservazione> infoConservazioneLotto;

    @Field
    private String statoConservazione;
}
