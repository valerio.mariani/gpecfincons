package it.posteitaliane.pec.common.json.utils;

import com.google.gson.*;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.lang.reflect.Type;

/**
 * @see https://github.com/google/gson/issues/368
 *
 *  L'XMLGregorianCalendar non è supportato nativamente in Gson o Jackson,
 *  per Gson si fornisce questo converter con cui fornire i JsonSerializer,
 *
 *  nel caso di uno swith a Jackson2 fornire le implementazioni corrispondenti sempre in questa classe
 *
 * 12-Giugno-2018
 * Stefano
 */
public class XMLGregorianCalendarConverter {

	public static class GsonSerializer implements JsonSerializer<XMLGregorianCalendar> {

		@Override
		public JsonElement serialize(XMLGregorianCalendar xmlGregorianCalendar, Type type, JsonSerializationContext jsonSerializationContext) {
			return new JsonPrimitive(xmlGregorianCalendar.toXMLFormat());
		}
	}

	public static class GsonDeserializer implements JsonDeserializer<XMLGregorianCalendar> {
		@Override
		public XMLGregorianCalendar deserialize(JsonElement jsonElement, Type type,
												JsonDeserializationContext jsonDeserializationContext) {
			try {
				return DatatypeFactory.newInstance().newXMLGregorianCalendar(jsonElement.getAsString());
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}