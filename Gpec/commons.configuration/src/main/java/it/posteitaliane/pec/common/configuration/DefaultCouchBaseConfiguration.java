package it.posteitaliane.pec.common.configuration;

import it.posteitaliane.pec.common.couchbase.DocumentAuditor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.core.query.Consistency;
import org.springframework.data.couchbase.repository.support.IndexManager;

import java.util.Arrays;
import java.util.List;


public abstract class DefaultCouchBaseConfiguration extends AbstractCouchbaseConfiguration {

    private String lotBucketPassword;
    private String bucketName;
    private String[] couchbaseBootstrapHosts;


    public DefaultCouchBaseConfiguration(String lotBucketPassword, String bucketName, String[] couchbaseBootstrapHosts) {
        this.lotBucketPassword = lotBucketPassword;
        this.bucketName = bucketName;
        this.couchbaseBootstrapHosts = couchbaseBootstrapHosts;
    }

    @Override
    public Consistency getDefaultConsistency() {
        return Consistency.STRONGLY_CONSISTENT;
    }


    //this is for dev so it is ok to auto-create indexes
    @Override
    public IndexManager indexManager() {
        return new IndexManager(true, false, true);
    }

    @Override
    protected List<String> getBootstrapHosts() {
        return Arrays.asList(couchbaseBootstrapHosts);
    }

    @Override
    protected String getBucketName() {
        return bucketName;
    }

    @Override
    protected String getBucketPassword() {
        return lotBucketPassword;
    }


    public DocumentAuditor documentAuditorAware(String auditorIdentifier) {
        return new DocumentAuditor(auditorIdentifier);
    }

}
