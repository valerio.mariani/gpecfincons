package it.posteitaliane.pec.common.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import it.posteitaliane.pec.common.json.utils.JacksonLocalDateTimeSerializer;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@ToString
//@AllArgsConstructor
//@RequiredArgsConstructor
@Builder
@EqualsAndHashCode
public class Events {

    @NotNull
    private final EventType event;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = JacksonLocalDateTimeSerializer.class)
    private final LocalDateTime time;

    private SendingDetails details;

}
