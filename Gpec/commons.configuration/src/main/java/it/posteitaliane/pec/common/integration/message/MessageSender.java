package it.posteitaliane.pec.common.integration.message;

import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.Queue;

/**
 *
 */
public interface MessageSender {

    /**
     * invia messaggi sulla coda specifica alla lavorazione degli items per invii del lotto
     * @param correlationId correlare il message ID con la logica dell'invio all'interno del lotto in elaborazione
     * @param msg Value Object contenente il messaggio composto
     * @param routingKey
     * @param messagePostProcessor
     */
    void produceMsg(String correlationId, MailDeliveryRequest msg, String routingKey, MessagePostProcessor messagePostProcessor);

//METODO TOLTO DA QUESTA INTERFACCIA A SEGUITO DEL REWORK DELLO SPOSTAMENTO DI IndexController NEL PROGETTO CONTROLLER
//    /**
//     * pubblica un binding corrispondente all'ID del lotto sulla coda
//     * @param lotIdAsRoutingKey
//     * @param lotQueue
//     */
//    Binding bindToKey(String lotIdAsRoutingKey, Queue lotQueue);

    /**
     * rimuove il binding tra l'exchange ed una coda (da usare solo sulle code dinamiche del lotto)
     * @param binding
     */
    void deleteBind(Binding binding);

}
