package it.posteitaliane.pec.common.custom.exception;

/*
* Qui la definizione delle eccezioni di uso comune
*
*
* */

public class TemplateNotFoundException extends Exception {
    public TemplateNotFoundException(String fileName){
        super("File di template "+fileName+" non trovato");
    }


}
