package it.posteitaliane.pec.common.integration;

import brave.Span;
import brave.Tracer;
import brave.Tracing;
import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.Events;
import it.posteitaliane.pec.common.model.LotDelivery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.time.LocalDateTime;

public class EventMessagePublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventMessagePublisher.class);
    private String AppId;

    RabbitTemplate rabbitTemplate;
    TopicExchange globalManagementExchange;

    Tracer tracer;

    String managementTopicRoutingKey;

    /**
     * Riutilizzato in tutti i micro-servizi che devono pubblicare eventi sulla coda di gestione/sistema
     *
     * @param managementTopicRoutingKey la routing key che verrà usata dalla topic per distribuire l'evento ai subscribers che sono in match
     * @param tracer il tracer degli span verso Zipkin
     * @param rabbitTemplate l'utilità di publishing verso rabbitMQ
     * @param globalManagementExchange la topic-exchange che riceverà il messaggio
     */
    public EventMessagePublisher(
            String appId,
            String managementTopicRoutingKey,
            Tracer tracer,
            RabbitTemplate rabbitTemplate,
            TopicExchange globalManagementExchange) {
        AppId = appId;
        this.managementTopicRoutingKey = managementTopicRoutingKey;
        this.tracer = tracer;
        this.rabbitTemplate = rabbitTemplate;
        this.globalManagementExchange = globalManagementExchange;
    }

    public void publishNewLotArrivedEvent(LotDelivery delivery){
        delivery.setCurrentStatus(EventType.LOTTO_RICEVUTO);
        publishToMessageBroker(delivery, ".received");
    }

//    public void publishLotValidationEvent(LotDelivery delivery){
//        delivery.setCurrentStatus(EventType.LOTTO_VALIDATO);
//        publishToMessageBroker(delivery, ".accepted");
//    }

    public void publishLotStartProcessingEvent(LotDelivery delivery){
        delivery.setCurrentStatus(EventType.LOTTO_IN_ELABORAZIONE);
        publishToMessageBroker(delivery, ".sending");
    }

    public void publishLotDiscardedEvent(LotDelivery delivery){
        delivery.setCurrentStatus(EventType.LOTTO_SCARTATO);
        publishToMessageBroker(delivery, ".discarded");
    }

    public void publishLotOutcomeEvent(LotDelivery delivery){
        delivery.setCurrentStatus(EventType.LOTTO_OUTCOME_GENERATO);
        publishToMessageBroker(delivery, ".completed");
    }

    public void publishLotDeletedEvent(LotDelivery delivery){
        delivery.setCurrentStatus(EventType.LOTTO_ELIMINATO);
        publishToMessageBroker(delivery, ".deleted");
    }


//    public void publishLotElaboratedEvent(LotDelivery delivery){
//        delivery.setCurrentStatus(EventType.LOTTO_ELABORATO);
//        publishToMessageBroker(delivery, ".exhausted");
//    }


    private void publishToMessageBroker(LotDelivery delivery, String suffix){

        String routingKey = managementTopicRoutingKey + ".lot";  // + suffix
        Span eventPublishScope = tracer.nextSpan().name("publish.event.message." + routingKey).kind(Span.Kind.PRODUCER);
        eventPublishScope.tag("EVENT", delivery.getCurrentStatus().name());
        eventPublishScope.start();

        try (Tracer.SpanInScope ws = tracer.withSpanInScope(eventPublishScope)) {
            delivery.getEvents().add(Events.builder().event( delivery.getCurrentStatus()).time( LocalDateTime.now()).build() );
            rabbitTemplate.convertAndSend(globalManagementExchange.getName(), routingKey, delivery, message -> {
                message.getMessageProperties().setContentType("application/json)");
                message.getMessageProperties().setAppId(AppId);
                message.getMessageProperties().getHeaders().put("EVENT-TYPE", delivery.getCurrentStatus().name());
                message.getMessageProperties().getHeaders().put("lotId", delivery.getLotId());
                return message; }
            );

            LOGGER.info("PUBBLICATO EVENTO[" + delivery.getCurrentStatus() + " - " + delivery.getLotId() + "] SU EXCHANGE " + globalManagementExchange.getName() + " CON ROUTING_KEY[" + routingKey + "]");
            System.out.println("PUBBLICATO EVENTO [" + delivery.getCurrentStatus() + " - " + delivery.getLotId() + "] to " + globalManagementExchange.getName() + " CON ROUTING_KEY[" + routingKey + "]");

        } finally {
            // start the client side and flush instead of finish
            eventPublishScope.flush();
        }

    }
}
