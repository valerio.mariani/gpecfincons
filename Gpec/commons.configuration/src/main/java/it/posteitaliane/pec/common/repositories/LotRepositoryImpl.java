package it.posteitaliane.pec.common.repositories;

import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.N1qlQuery;
import it.posteitaliane.pec.common.model.LotDelivery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.couchbase.core.CouchbaseOperations;
import org.springframework.data.couchbase.core.CouchbaseTemplate;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class LotRepositoryImpl {

    private CouchbaseTemplate template;



    public LotDelivery updateSingleKeys(@NotNull Object theId, @NotNull Map<String, Object> updates){
        String paStatement = "UPDATE LOTS USE KEYS $id " +

                "RETURNING *";
        JsonObject paramValues = JsonObject.from(updates).put("id", theId);
        N1qlQuery query = N1qlQuery.parameterized(paStatement, paramValues);
//        return template.getCouchbaseBucket().query(query);
        return null;
    }
}
