package it.posteitaliane.pec.common.repositories;

import it.posteitaliane.pec.common.model.EventType;
import it.posteitaliane.pec.common.model.LotCsv;
import it.posteitaliane.pec.common.model.LotDelivery;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.data.couchbase.core.query.Query;
import org.springframework.data.repository.CrudRepository;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Spring-Data repository per metodi di manipolazione/persitenza dati su CouchBase
 */


public interface LotRepository extends CrudRepository<LotDelivery, String> {

    List<LotDelivery> findByService(String service);
    List<LotDelivery> countLotDeliveriesByCustomerCodeEquals(String customerCode);
    List<LotDelivery> findLotDeliveryByCurrentStatusIsAndClosedIsNullOrderByUpdatedDesc(EventType currentStatus);
    LotDelivery findLotDeliveryByMailDeliveriesIDsIsIn(@NotNull String... messageId);
    List<LotDelivery> findByCurrentStatus(String status);
    List<LotDelivery> findByCurrentStatusIn(List<String> stats);
   // List<LotCsv> findByCurrentStatusInOrderByCurrentStatusDesc(List<String> stats);
    long countByCurrentStatusIn(List<String> stats);
    List<LotDelivery> findByLotIdIgnoreCaseContaining(String place);
    LotDelivery findByLotId(String lotId);

    @Query("#{#n1ql.selectEntity} WHERE #{#n1ql.filter} AND currentStatus IN ['LOTTO_IN_ELABORAZIONE','LOTTO_OUTCOME_GENERATO'] AND MILLIS_TO_STR(updated) BETWEEN $1 AND $2")
    List<LotCsv> findByCurrentStatusInOrderByCurrentStatusDesc(String dataDa,String dataA);



    /*
    * SELECT
    * META(`lots`).id AS _ID, META(`lots`).cas AS _CAS, `lots`.* FROM `lots` --> #{#n1ql.selectEntity}
    * WHERE
    * `_class` = \"it.posteitaliane.pec.common.model.LotDelivery\"  --> #{#n1ql.filter}
    * AND currentStatus = $2 AND ANY v IN events SATISFIES v.event = $1 END;
    *
    * */
    @Query("#{#n1ql.selectEntity} WHERE #{#n1ql.filter} AND META(`lots`).id = $1 AND ANY v IN events SATISFIES v.event = $2 END;")
    List<LotDelivery> findByLotIdAndEvent(String lotId,String event);


    /*
    * dateIsoFrom e dateIsoTo formato ISO (es. 2016-02-01T00:00:00)
    *
    * */
    @Query("SELECT COUNT(*) AS count FROM #{#n1ql.bucket} WHERE #{#n1ql.filter} AND currentStatus = 'LOTTO_OUTCOME_GENERATO' AND MILLIS_TO_STR(updated) BETWEEN $1 AND $2")
    long countElaboratedLotsInDateRange(String dateIsoFrom,String dateIsoTo);


    @Query("SELECT COUNT(*) AS count FROM #{#n1ql.bucket} WHERE #{#n1ql.filter} AND currentStatus = $1")
    long countLotsInStatus(String status);
}
