package it.posteitaliane.pec.common.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data

@Builder
@ToString
public class InfoConservazione {

    private EventConservazione stato;
    private String zipRiferimento;
    private String dataCreazione;
    private String idLottoAzienda;
    private String tipoDoc;
    private String nickAzienda;
}
