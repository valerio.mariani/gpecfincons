package it.posteitaliane.pec.common.integration;

import brave.Span;
import brave.Tracer;
import it.posteitaliane.pec.common.model.LotDelivery;
import it.posteitaliane.pec.common.model.MailDeliveryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;


public class DataOperationsMessagePublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataOperationsMessagePublisher.class);
    private String AppId;

    RabbitTemplate rabbitTemplate;
    TopicExchange globalNotificationsExchange;
    Tracer tracer;

//    String managementTopicRoutingKey;

    /**
     *
     * @param appId
     * @param tracer
     * @param rabbitTemplate
     * @param globalNotificationsExchange
     */
    public DataOperationsMessagePublisher(
            String appId,
            Tracer tracer,
            RabbitTemplate rabbitTemplate,
            TopicExchange globalNotificationsExchange) {
        AppId = appId;
        this.tracer = tracer;
        this.rabbitTemplate = rabbitTemplate;
        this.globalNotificationsExchange = globalNotificationsExchange;
    }


    /**
     *
     * @param lotDelivery
     */
    public void publishLotCreationRequest(LotDelivery lotDelivery){
        publishToMessageBroker(lotDelivery, "LotDelivery", "save",lotDelivery.getLotId());
    }

    /**
     *
     * @param mailDeliveryRequest
     */
    public void publishMailDeliveryUpdateRequest(MailDeliveryRequest mailDeliveryRequest){
        publishToMessageBroker(mailDeliveryRequest, "MailDeliveryRequest", "save", mailDeliveryRequest.getMessageID());
    }

    /**
     *
     * @param document
     * @param domain
     * @param operation
     * @param documentId
     */
    private void publishToMessageBroker(Object document, String domain, String operation, String documentId){

        String routingKey = "data." + domain + "." + operation;
        Span eventPublishScope = tracer.nextSpan().name("publish.data.operation.message." + routingKey).kind(Span.Kind.PRODUCER);
        eventPublishScope.tag("EVENT", domain + "." + operation);
        eventPublishScope.start();

        try (Tracer.SpanInScope ws = tracer.withSpanInScope(eventPublishScope)) {
            rabbitTemplate.convertAndSend(globalNotificationsExchange.getName(), routingKey, document, message -> {
                message.getMessageProperties().setContentType("application/json)");
                message.getMessageProperties().setAppId(AppId);
                message.getMessageProperties().getHeaders().put("NOTIFICATION-TYPE", domain);
                message.getMessageProperties().getHeaders().put("OPERATION", operation);
                        message.getMessageProperties().getHeaders().put("DOCUMENT-ID", documentId);
                return message; }
            );

            LOGGER.info("Publish notification[" + domain + "." + operation + " - " + documentId + "] to " + globalNotificationsExchange.getName() + " with ROUTING_KEY[" + routingKey + "]");

        } finally {
            // start the client side and flush instead of finish
            eventPublishScope.flush();
        }

    }
}
