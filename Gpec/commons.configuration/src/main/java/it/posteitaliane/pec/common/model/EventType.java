package it.posteitaliane.pec.common.model;

/**
 * raccoglie tutti gli stati e eventi che definiscono il ciclo di vita degli oggetti di business di GPEC
 */
public enum EventType {

    SYSTEM_SUSPEND("Sospende il flusso di acquisizione e elaborazione"),
    SYSTEM_RESTART("Riavvia il flusso di acquisizione e elaborazione"),

   //******************STATI DEL MESSAGGIO***********************************/

    MESSAGGIO_INVIABILE("il messaggio ha completato la fase di 'message composition'"),         // impostato nella messagge composition e primo stato salvato
    MESSAGGIO_NON_INVIABILE("il messaggio non ha completato la fase di 'message composition'"),     // impostato nella messagge composition O nel sender service
    //MESSAGGIO_INVIATO,         // USATO SOLO NEI TEST , OBSOLETO
    MESSAGGIO_INVIO_FALLITO("il server postecert non può spedire il messaggio"),     // impostato nel MailStatusListener nel metodo messageNotDelivered
    MESSAGGIO_INVIO_SUCCESSO("il server postecert ha ricevuto correttamente il messaggio"),    // impostato nel MailStatusListener nel metodo messageDelivered
    MESSAGGIO_CONSEGNA_PARZIALE("il server postecert non ha potuto inviare il messaggio a tutti i possibili destinatari, cc, bcc"), // impostato nel MailStatusListener nel metodo messagePartiallyDelivered  ---> CHE VUOL DIRE?
    //MESSAGGIO_ATTESA_RICEVUTA, //USATO SOLO NEI TEST  , OSBOLETO

    MESSAGGIO_ACCETTAZIONE_OK("il server postecert ha inviato correttamente il messaggio al server PEC destinatario"),   // impostato nel processReceipt
    MESSAGGIO_ACCETTAZIONE_KO("il server postecert non è stato in grado di inviare il messaggio al server PEC destinatario"),   // impostato nel processReceipt
    MESSAGGIO_CONSEGNA_OK("il server PEC destinatario ha consegnato il messaggio ad un'indirizzo"),  // impostato nel processReceipt
    MESSAGGIO_CONSEGNA_KO("il server PEC destinatario non ha consegnato il messaggio ad un'indirizzo"),       // impostato nel processReceipt



    //******************STATI DEL LOTTO***********************************/
    LOTTO_RICEVUTO("un lotto è stato ricevuto"), // MESSO IN QUESTO STATO da /lot

   //LOTTO_VALIDATO, // probabilmente questo stato lo cancelleremo, perche' quando supera la validazione , lo stato diventa subito LOTTO_IN_ELABORAZIONE

    LOTTO_SCARTATO("il lotto presenta errori errori di validazione"), //messo in questo stato da /lot e /getXmlAckInput

    LOTTO_IN_ELABORAZIONE("il lotto è nella fase di processamento e invio i messaggi"),  //E' MESSO IN QUESTO STATO DAL SERVIZIO:  /lot/start-process

    //LOTTO_ELABORATO,  // mettemo questo stato per indicare la fine dell'invio dei messaggi, ma non l'abbiamo piu' usato , almeno per ora

    LOTTO_OUTCOME_GENERATO("il file di outcome è stato generato"),  // FILE OUTCOME PRODOTTO, messo in questo stato da responseService

    LOTTO_ACK_OUTPUT_RICEVUTO("ricezione del del FILE_ACK_OUTPUT da parte del mittente"), //indica che abbiamo ricevuto il FILE_ACK_OUTPUT che indica che il cliente l'ha ricevuto, messo in questo stato da AckOutputWatch watcher

    LOTTO_ELIMINATO("il lotto è stato cancellato") // messo in questo stato da outcomeFindAndDeleteOlderZipService
    ;

    private String description;

    EventType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
