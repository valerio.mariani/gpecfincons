package it.posteitaliane.pec.common.model;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.opencsv.bean.CsvBindByName;
import lombok.*;
import org.springframework.data.couchbase.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Document
public class MailCsv {

    @CsvBindByName(column = "ID LOTTO")
    private final String lotId;
    @CsvBindByName(column = "DESTINATARIO")
    private final String to;
    @CsvBindByName(column = "NOME FILE")
    private String acceptanceFileName;
    @CsvBindByName(column = "CHIAVE ESTERNA")
    private final String extID;
    @CsvBindByName(column = "SUBJECT")
    private final String subject;


    public MailCsv(String lotId, String to, String acceptanceFileName, String extID, String subject) {
        this.lotId = lotId;
        this.to = to;
        this.acceptanceFileName = acceptanceFileName;
        this.extID = extID;
        this.subject = subject;
    }

    public String getLotId() {
        return lotId;
    }

    public String getTo() {
        return to;
    }

    public String getAcceptanceFileName() {
        return acceptanceFileName;
    }

    public String getExtID() {
        return extID;
    }

    public String getSubject() {
        return subject;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MailCsv)) return false;
        MailCsv mailCsv = (MailCsv) o;
        return Objects.equals(getLotId(), mailCsv.getLotId()) &&
                Objects.equals(getTo(), mailCsv.getTo()) &&
                Objects.equals(getAcceptanceFileName(), mailCsv.getAcceptanceFileName()) &&
                Objects.equals(getExtID(), mailCsv.getExtID()) &&
                Objects.equals(getSubject(), mailCsv.getSubject());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getLotId(), getTo(), getAcceptanceFileName(), getExtID(), getSubject());
    }
}
