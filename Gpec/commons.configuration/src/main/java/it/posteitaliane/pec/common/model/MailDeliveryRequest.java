package it.posteitaliane.pec.common.model;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.*;
import org.springframework.data.annotation.Version;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Document
@Data
@AllArgsConstructor
@ToString
@Builder
@EqualsAndHashCode
public class MailDeliveryRequest {

    @NotNull
    @Id
    private final String messageID;

//    @Version
//    @Nullable
//    private long version = 0L;  // extremely important to note that you should never attempt to access or modify this field

    @Field
    private final String extID;

    @Field
    @NotNull
    private final String lotId;

    @NotNull
    @Field
    private final String to;

    @Field
    private final String cc;

    @Field
    private final String subject;


    @Field
    private final String content;

    @Field
    private final List<Events> events;

    @Field
    private final List<String> attachments;


    @Field
    private String validationOutcome;

    @Field
    private String acceptanceOutcome;

    @Field
    private String deliveryOutcome;


    @Field
    private String acceptanceFileName;

    @Field
    private  String deliveryFileName;

    @Field
    private InfoConservazione infoConservazione;

}
