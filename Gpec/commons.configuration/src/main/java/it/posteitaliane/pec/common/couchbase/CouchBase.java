package it.posteitaliane.pec.common.couchbase;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;

public class CouchBase {
	Logger log = LoggerFactory.getLogger(CouchBase.class);

	private Cluster cluster;
	private List<String> nodes;
	
	public CouchBase(List<String> nodes) {
		this.nodes = nodes;
	}
	
	public void connect() {
		disconnect();
		cluster = CouchbaseCluster.create(nodes);
	}

	public void connect(List<String> nodes) {
		this.nodes = nodes;
		connect();
	}
	
	public Cluster authenticate(String username, String password) {
		if (cluster != null)
			cluster = cluster.authenticate(username, password);
		return cluster;
	}
	
	public void disconnect() {
		if (cluster != null) {
			cluster.disconnect();
			cluster = null;
		}
	}
	
	public Bucket open() {
		if (cluster != null)
			return cluster.openBucket();
		return null;
	}

	public Bucket open(String name) {
		if (cluster != null)
			return cluster.openBucket(name);
		return null;
	}

	public Bucket open(String name, String password) {
		if (cluster != null)
			return cluster.openBucket(name, password);
		return null;
	}
}
