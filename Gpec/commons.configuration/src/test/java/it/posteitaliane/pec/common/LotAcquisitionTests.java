package it.posteitaliane.pec.common;

import it.posteitaliane.pec.common.model.xml.*;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static java.util.stream.Collectors.*;
import static org.junit.jupiter.api.TestInstance.*;

@TestInstance(Lifecycle.PER_CLASS)
@DisplayName("Creazione di file LOTTI ZIP da usare per test :")
public class LotAcquisitionTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(LotAcquisitionTests.class);
    Random r = new Random();

    //************************************************************************************//
    // PRIMA DI LANCIARE IL TEST IMPOSTARSI LA FOLDER DESIDERATA IN CUI SI VUOLE VENGA CREATO IL FILE
//    String zipPathWithoutFile = "C:\\Program Files (x86)\\nifi-1.8.0\\data-in\\";
    // path f.i.
     String zipPathWithoutFile = "C:\\FINCONS\\POSTE-INVIO-PEC\\lotti-esempio\\generati-da-noi\\";
    //************************************************************************************//

    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    JAXBContext jc;
    {
        try {
            jc = JAXBContext.newInstance(Lot.class);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @ParameterizedTest
    @DisplayName("test creazione lotto con messaggi diversi")
    @ValueSource(strings = {"POSTEPCL"}) // ,"M23L","F23L","C23L"})
    public void PER_SVILUPPO_createAndPublishLotZipConMessaggiDiversi(String customerCode) throws IOException {

        //casella sviluppo
        String to1="pectest23l6@coll.postecert.it";

        //casella sviluppo
        String to2="pectest23l7@coll.postecert.it";

        String service="G23L";

        corePerCreateAndPublishLotZipConMessaggiDiversi(customerCode,service, to1, to2);

    }


    @ParameterizedTest
    @DisplayName("test creazione lotto con TANTI messaggi diversi")
    @ValueSource(strings = {"POSTEPCL"}) // ,"M23L","F23L","C23L"})
    public void PER_SVILUPPO_createAndPublishLotZipCon_TANTI_MessaggiDiversi(String customerCode) throws IOException {

        //casella sviluppo
        String to1="pectest23l10@coll.postecert.it"; //<------- DESTINATARIO DI TUTTE I MESSAGGI CREATI
                                                    // mettere una folder per cui non  si e' configurato il copyprocess

        String service="G23L";

        int numeroMessaggi=1000;// <-------INDICARE NUMERO DI MESSAGGI VOLUTI

        corePerCreateAndPublishLotZipCon_TANTI_MessaggiDiversi(customerCode,service, to1, numeroMessaggi);

    }


    @ParameterizedTest
    @DisplayName("test creazione lotto con TANTI messaggi diversi")
    @ValueSource(strings = {"POSTEPCL"}) // ,"M23L","F23L","C23L"})
    public void PER_COLLAUDO_createAndPublishLotZipCon_TANTI_MessaggiDiversi(String customerCode) throws IOException {



        //casella COLLAUDO
        String to="gpeccoll10@coll.postecert.it";//<------- DESTINATARIO DI TUTTE I MESSAGGI CREATI


        String service="G23L";

        int numeroMessaggi=1000; // <-------INDICARE NUMERO DI MESSAGGI VOLUTI

        corePerCreateAndPublishLotZipCon_TANTI_MessaggiDiversi(customerCode,service, to, numeroMessaggi);

    }






    @ParameterizedTest
    @DisplayName("test creazione lotto senza customer code")
    @ValueSource(strings = {""}) // ,"M23L","F23L","C23L"})
    public void PER_SVILUPPO_CreateLotZipSenzaCustomerCode(String customerCode) throws IOException {

        //casella sviluppo
        String to1="pectest23l6@coll.postecert.it";

        //casella sviluppo
        String to2="pectest23l7@coll.postecert.it";

        String service="G23L";

        corePerCreateAndPublishLotZipConMessaggiDiversi(customerCode,service, to1, to2);

    }



    @ParameterizedTest
    @DisplayName("test creazione lotto senza service")
    @ValueSource(strings = {"POSTEPCL"}) // ,"M23L","F23L","C23L"})
    public void PER_SVILUPPO_CreateLotZipSenzaService(String customerCode) throws IOException {

        //casella sviluppo
        String to1="pectest23l6@coll.postecert.it";

        //casella sviluppo
        String to2="pectest23l7@coll.postecert.it";

        String service="";

        corePerCreateAndPublishLotZipConMessaggiDiversi(customerCode,service, to1, to2);

    }

    @ParameterizedTest
    @DisplayName("test creazione lotto con un errore di mancata consegna")
    @ValueSource(strings = {"POSTEPCL"}) // ,"M23L","F23L","C23L"})
    public void PER_SVILUPPO_CreateLotZipPerMancataConsegna(String customerCode) throws IOException {


        String to1="asdasdasdas@coll.postecert.it";//<<<------METTO UN DESTINATARIO INESISTNE SU DOMINIO CORRETTO PER CREARE UN  MESSAGGIO DI MANCATA CONSEGNA

        //casella sviluppo
        String to2="pectest23l6@coll.postecert.it";

        String service="G23L";

        corePerCreateAndPublishLotZipConMessaggiDiversi(customerCode,service, to1, to2);

    }

    @ParameterizedTest
    @DisplayName("test creazione lotto con un errore rivelato dalle api java")
    @ValueSource(strings = {"POSTEPCL"}) // ,"M23L","F23L","C23L"})
    public void PER_SVILUPPO_CreateLotZipConErroreRilevatoDaJavaMail(String customerCode) throws IOException {


        String to1="pectest23l3@pectest23l3@coll.postecert.it";//<<<------METTO UN DESTINATARIO CON ERRORE DI DOPPIA CHIOCCIOLA


        //casella sviluppo
        String to2="pectest23l7@coll.postecert.it";

        String service="G23L";

        corePerCreateAndPublishLotZipConMessaggiDiversi(customerCode,service, to1, to2);

    }






    @ParameterizedTest
    @DisplayName("Creazione lotto con messaggi, potenzialmente molti, tutti uguali ad eccezione dell' extId. e dei contatori. Gli allegati sono solo 2, gli stessi per tutti i messaggi")
    @ValueSource(strings = {"POSTEPCL"  } ) // ,"M23L","F23L","C23L"})
    public void PER_SVILUPPO_createAndPublishLotZipConMessaggiSimiliConStessiAllegati(String customerCode) throws IOException {

        int messageListSize =   4;  //<-------NUMERO DI MESSAGGI CONTENUTI NEL LOTTO

        //casella SVILUPPO
        String campoTo="pectest23l7@coll.postecert.it";//<------- DESTINATARIO DI TUTTE I MESSAGGI CREATI


        String service = "G23L";

        corePerCreateAndPubblishLotZipConMessaggiSimiliConStessiAllegati(customerCode,service, messageListSize, campoTo);

    }


    @ParameterizedTest
    @DisplayName("Creazione lotto con messaggi, potenzialmente molti, tutti uguali ad eccezione dell' extId. e dei contatori. Gli allegati sono solo 2, gli stessi per tutti i messaggi")
    @ValueSource(strings = {"POSTEPCL"  } ) // ,"M23L","F23L","C23L"})
    public void PER_COLLAUDO_createAndPublishLotZipConMessaggiSimiliConStessiAllegati(String customerCode) throws IOException {

        int messageListSize =   4;  //<-------NUMERO DI MESSAGGI CONTENUTI NEL LOTTO

        //casella COLLAUDO
        String campoTo="gpeccoll3@coll.postecert.it";//<------- DESTINATARIO DI TUTTE I MESSAGGI CREATI

        String service = "G23L";

        corePerCreateAndPubblishLotZipConMessaggiSimiliConStessiAllegati(customerCode, service, messageListSize, campoTo);

    }



    @ParameterizedTest
    @DisplayName("test creazione lotto con messaggi diversi")
    @ValueSource(strings = {"POSTEPCL"}) // ,"M23L","F23L","C23L"})
    public void PER_COLLAUDO_createAndPublishLotZipConMessaggiDiversi(String customerCode) throws IOException {

        //casella collaudo
        String to1="gpeccoll2@coll.postecert.it";

        //casella collaudo
        String to2="gpeccoll4@coll.postecert.it";

        String service="G23L";

        corePerCreateAndPublishLotZipConMessaggiDiversi(customerCode,service, to1, to2);

    }






    private static String getLotPrefix(String service){
        return service + "_" + LocalDateTime.now().format(formatter) + "_";
    }

    //metodo core usato dai metodi test PER_SVILUPPO_  e PER_COLLAUDO_
    private void corePerCreateAndPubblishLotZipConMessaggiSimiliConStessiAllegati(String customerCode, String service, int messageListSize, String campoTo) throws IOException {
        File directory = new File("./src/test/resources/xml/attachments/normal1");
        File[] fileInclusiNelloZip = directory.listFiles((dir, name) ->  (name.endsWith(".pdf") || name.endsWith(".zip"))   );



        Lot testLot = creaLotto(customerCode,service);


        for (int i = 1; i < messageListSize+1; i++) {


            Message messaggio = creaMessaggio(campoTo,null,"G23L", fileInclusiNelloZip,"MARIO_"+i,"ROSSI_"+i,"CODICEAR_"+i,"CODICEAG_"+i,"Procura antimafia_"+i);


            testLot.getMessageList().getMessage().add(messaggio);
        }


        String zipPath = zipPathWithoutFile + testLot.getHeaders().getLotId() + ".zip";
        creazFileZipLotto(fileInclusiNelloZip, testLot.getHeaders().getLotId(), testLot, zipPath);
    }


    private Lot creaLotto(String customerCode,String service){


        //info base del lotto
        String lotPrefix = getLotPrefix(service);

        int index = r.ints(1, 999999).limit(1).findFirst().getAsInt();
        String lotId = lotPrefix + StringUtils.leftPad("" + index, 6, "0");

        Lot testLot = new Lot();
        testLot.setHeaders(new Headers());
        testLot.getHeaders().setLotId(lotId);
        testLot.getHeaders().setCustomerCode(customerCode);
        testLot.getHeaders().setService(service);  // (index % 2 == 0 ) ? "G23L" : "ESUB" );
        testLot.setMessageList(new MessageList());
        testLot.getMessageList().setMessage(new ArrayList<>());

        return  testLot;
    }


    private void corePerCreateAndPublishLotZipConMessaggiDiversi(String customerCode, String service, String to1, String to2) throws IOException {


        Lot testLot = creaLotto(customerCode,service);

        //messaggio 1

        File directory1 = new File("./src/test/resources/xml/attachments/normal1");
        File[] allegatiMessaggio1 = directory1.listFiles((dir, name) ->  (name.endsWith(".pdf") || name.endsWith(".zip"))   );


        Message msg1 = creaMessaggio(to1,null,"G23L", allegatiMessaggio1,"MARIO","ROSSI","111111","111","Procura antimafia");

        //aggiungo messaggio al lotto
        testLot.getMessageList().getMessage().add(msg1);


        //messaggio 2
        File directory2 = new File("./src/test/resources/xml/attachments/normal2");
        File[] allegatiMessaggio2 = directory2.listFiles((dir, name) ->  (name.endsWith(".pdf") || name.endsWith(".zip"))   );


        Message msg2 = creaMessaggio(to2,null,"G23L", allegatiMessaggio2,"CARLO","BIANCHI","22222","222","comune di roma");

        //aggiungo messaggio al lotto
        testLot.getMessageList().getMessage().add(msg2);

        File[] fileDaIncludereNelloZip = ArrayUtils.addAll(allegatiMessaggio1, allegatiMessaggio2);


        //String zipPath = "C:\\Program Files (x86)\\nifi-1.8.0\\data-in\\" + lotId + ".zip";
        String zipPath = zipPathWithoutFile + testLot.getHeaders().getLotId() + ".zip";
        creazFileZipLotto(fileDaIncludereNelloZip, testLot.getHeaders().getLotId(), testLot, zipPath);
    }

    private void corePerCreateAndPublishLotZipCon_TANTI_MessaggiDiversi(String customerCode, String service, String to1, int numeroMessaggi) throws IOException {


        File[] fileDaIncludereNelloZip = new File[0];

        Lot testLot = creaLotto(customerCode,service);

        File directory = new File("./src/test/resources/xml/attachments/allegati");

        for(int i=1;i<= numeroMessaggi;i++){

            int finalI = i;
            File[] allegatiMessaggio = directory.listFiles((dir, name) ->  (name.endsWith("("+ finalI +").pdf") || name.endsWith("("+ finalI +").zip"))   );

            Message msg = creaMessaggio(to1,null,"G23L", allegatiMessaggio,"MARIO_"+i,"ROSSI_"+i,"CODICEAR_"+i,"CODICEAG_"+i,"Procura antimafia_"+i);

            //aggiungo messaggio al lotto
            testLot.getMessageList().getMessage().add(msg);

            fileDaIncludereNelloZip = ArrayUtils.addAll(fileDaIncludereNelloZip, allegatiMessaggio);
        }

        String zipPath = zipPathWithoutFile + testLot.getHeaders().getLotId() + ".zip";
        creazFileZipLotto(fileDaIncludereNelloZip, testLot.getHeaders().getLotId(), testLot, zipPath);
    }



    private Message creaMessaggio(String to, String cc, String idTemplate, File[] allegatiMessaggio, String pNOME, String pCOGNOME,String pCODICE_AR,String pCODICE_AG,String pDENOMINAZIONE_ENTE) {

        List<Parameter> parameterList = new ArrayList<>();
        parameterList.add(Parameter.builderForParameter().name("NOME").val(pNOME).build());
        parameterList.add(Parameter.builderForParameter().name("COGNOME").val(pCOGNOME).build());
        parameterList.add(Parameter.builderForParameter().name("CODICE_AR").val(pCODICE_AR).build());
        parameterList.add(Parameter.builderForParameter().name("CODICE_AG").val(pCODICE_AG).build());
        parameterList.add(Parameter.builderForParameter().name("DENOMINAZIONE_ENTE").val(pDENOMINAZIONE_ENTE).build());

        Parameters messageParameters = Parameters.builderForParameters().parameter(
                parameterList
        ).build();


        Message msg= new Message();
        msg.setExtId(UUID.randomUUID().toString());
        msg.setTo(to);
        if(cc!=null){
            msg.setCc( cc);
        }

        msg.setTemplateRef(new TemplateRef(messageParameters, idTemplate));
        List<String> attachments = Arrays.stream( allegatiMessaggio ).map(file -> file.getName()).collect(toList());
        msg.setAttachmentList(new AttachmentList(attachments));

        return msg;
    }

    private void creazFileZipLotto(File[] fileInclusiNelloZip, String lotId, Lot testLot, String zipPath) throws IOException {
        ZipOutputStream zos = null;
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(zipPath);
            zos = new ZipOutputStream(fileOutputStream);
            // add zip-entry descriptor
            ZipEntry ze1 = new ZipEntry(lotId + ".xml");
            zos.putNextEntry(ze1);

            // add zip-entry data

            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            marshaller.marshal(testLot, zos);

            //

            final ZipOutputStream fZos = zos;
            Arrays.stream( fileInclusiNelloZip ).forEach(f ->
                    {
                        unchecked(() -> {
                            zipFile(f, "", fZos);
                            return f;
                        });
                    }
            );

            zos.flush();

            //creazione del file .t
            String tPath = zipPath+".t";

            FileOutputStream fout = new FileOutputStream(tPath);
            fout.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } finally {
            if (zos != null) {
                zos.close();
                fileOutputStream.close();
            }
        }
    }


    public static <T> T unchecked(ThrowingSupplier<T> supplier) {
        try {
            return supplier.get();
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    public interface ThrowingSupplier<T> {
        T get() throws Throwable;
    }

    public static void zipFile(File inputFile,String parentName,ZipOutputStream zipOutputStream) {

        try {
            // A ZipEntry represents a file entry in the zip archive
            // We name the ZipEntry after the original file's name
            ZipEntry zipEntry = new ZipEntry(parentName + inputFile.getName());
            zipOutputStream.putNextEntry(zipEntry);

            FileInputStream fileInputStream = new FileInputStream(inputFile);
            byte[] buf = new byte[1024];
            int bytesRead;

            // Read the input file by chucks of 1024 bytes
            // and write the read bytes to the zip stream
            while ((bytesRead = fileInputStream.read(buf)) > 0) {
                zipOutputStream.write(buf, 0, bytesRead);
            }

            // close ZipEntry to store the stream to the file
            zipOutputStream.closeEntry();

            System.out.println("Regular file :" + inputFile.getCanonicalPath()+" is zipped to archive.");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }



//    @ParameterizedTest
//    @DisplayName("test creazione lotto con allegati pdf di dimensione grande")
//    @ValueSource(strings = {"G23L"}) // ,"M23L","F23L","C23L"})
//    @Deprecated
//    public void createAndPublishLotZip(String customerCode) throws IOException {
//
//        String lotPrefix = "G23L_20181229_";
//        int messageListSize = 3; // 300;
//        File directory = new File("./src/test/resources/xml/attachments/big");
//        File[] fileInclusiNelloZip = directory.listFiles((dir, name) -> name.endsWith(".pdf"));
//
//
//
//        int index = r.ints(1, 999999).limit(1).findFirst().getAsInt();
//        String lotId = lotPrefix + StringUtils.leftPad("" + index, 6, "0");
//
//        Lot testLot = new Lot();
//        testLot.setHeaders(new Headers());
//        testLot.getHeaders().setLotId(lotId);
//        testLot.getHeaders().setCustomerCode(customerCode);
//        testLot.getHeaders().setService("G23L");
//        testLot.setMessageList(new MessageList());
//        testLot.getMessageList().setMessage(new ArrayList<>());
//
//
//        List<Parameter> parameterList = new ArrayList<>();
//        parameterList.add(Parameter.builderForParameter().name("NOME").val("MARIO").build());
//        parameterList.add(Parameter.builderForParameter().name("COGNOME").val("ROSSI").build());
//        parameterList.add(Parameter.builderForParameter().name("CODICE_AR").val("32423654").build());
//        parameterList.add(Parameter.builderForParameter().name("CODICE_AG").val("003").build());
//        parameterList.add(Parameter.builderForParameter().name("DENOMINAZIONE_ENTE").val("Procura antimafia").build());
//        Parameters messageParameters = Parameters.builderForParameters().parameter(
//                parameterList
//        ).build();
//        for (int i = 0; i < messageListSize; i++) {
//            Message msg = new Message();
//            msg.setExtId(UUID.randomUUID().toString());
//            msg.setTo("PEC1.multe@localhost.com");
//           // msg.setCc( (((index & 1) != 0) ? "festano@gmail.com" : "") );
//            msg.setTemplateRef(new TemplateRef(messageParameters, "G23L"));
//            List<String> attachments = Arrays.stream( fileInclusiNelloZip ).map(file -> file.getName()).collect(toList());
//            msg.setAttachmentList(new AttachmentList(attachments));
//            testLot.getMessageList().getMessage().add(msg);
//        }
//
//        String zipPath = zipPathWithoutFile + lotId + ".zip";
//        creazFileZipLotto(fileInclusiNelloZip, lotId, testLot, zipPath);
//
//
//    }

}
