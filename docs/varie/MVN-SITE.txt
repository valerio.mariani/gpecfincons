Per generare il Maven Site di documentazione:

da terminale, as root , lanciare prima:

mvn clean site -DskipTests --> genera i site dentro le target dei vari moduli

e poi

mvn site:stage -DskipTests  --> Il sito verrà creato nella folder MVN-SITE-STAGING (allo stesso livello della root Gpec)

(c'e' pero' un  problema: creaa anche altre cartelle inutili  allo stesso livello di MVN-SITE-STAGING
una per modulo...

e poi:

mvn site:deploy -DskipTests  -->pubblica sul server http di postecom


---------------------------

PER DEPLOYARE LE VERSIONI SNAPSHOT SU POSTECOM:

mvn deploy -DskipTests

ma e' necessario che ci sia , dentro     <distributionManagement>
    <snapshotRepository>

--------------------------------

MAVENE RELEASE

-DdryRun=true : PER FARE UN PROVA

-Darguments="-DskipTests" : PER FAR SKIPPARE I TEST

-DautoVersionSubmodules=true: PER NON FARMI FARE LE DOMANDE SULLE VERSIONI E I TAG PER OGNI SOTTOMODULI

-Dresume=false: PER POTERE RIINIZARE DA CAPO UNA PREPARE (ALTRIMENTI DOPO UNA PREPARE SONO OBBLIGATO A FARE UNA PERFORM)

mvn release:prepare -DdryRun=true -Darguments="-DskipTests"
nessun errore..ha creato dei pom  next..backup..tag e un release.properties...
ha funzionato lo skip dei test


mvn release:prepare -DdryRun=true -Darguments="-DskipTests" -DautoVersionSubmodules=true -Dresume=false
nessun errreo, ha funzionato lo skip delle versioni per i submoduli

ora la faccio vera:

mvn release:prepare -Darguments="-DskipTests" -DautoVersionSubmodules=true -Dresume=false

OK, HA FUNZIONATO, HA FATTO TAG SU SVN , E MI HA AUMENTATO A 0.0.2-SNAPSHOT LA VERSIONE DEI POM
MA NON MI HA, GIUSTAMENTE, ANCORA DEPLOYATO LA VERSIONE SU MAVEN RELASE
http://192.168.52.121:8081/nexus/content/repositories/releases/it/posteitaliane/

ORA FACCIO LA PERFORN
mvn release:perform -Darguments="-DskipTests"

    quello che fa e':
    fare il checkout da svn del tag fatto allo step prepare, ossia lo 0.0.1

  ATTENZIONE..
  E' ANDATA IN ERRORE LA PERFORM PERCHE' AVEVO DIMENTICTO IL TAG REPOSITORU NEL DISTR MANAGEMENT...

  PROVO A FARE ROLLBACK , CON:

mvn release:rollback
SEMBRA ESSERE RIUSCITA
HA FATTO SPARIRE TUTTI I POM DI BACK E NEXT.
MA NON HA CANCELLATO IL TAG SU SVM, MA CIO' ERA DOCUMENTATO
LO CANCELLO A MANO CON  TORTOISE
E RIPARTO:

mvn release:prepare -Darguments="-DskipTests" -DautoVersionSubmodules=true

OK
(HO NOTATO CONE , SICCOME COMPILA E FA PACKAGE, MI HA FATTO ANCHE L'ASSEMLBY DELLO ZIP, CON PERO' LA VERSIONE GIUSTA 0.0.1

ORA RIVADO DI PERFORM:

mvn release:perform -Darguments="-DskipTests"

e' andata in error il progetto commons.configuration probabilmente nella fase di generazione dei javadoc..(copyprocce e gateway invece
li ha deployati correttamente)

provo ad escludere la generazione dei java doc con questa opzione
mvn release:perform -Darguments="-DskipTests" -DuseReleaseProfile=false

ok, ha funzionato, build success,
il site e' aggiornato..
ma non si scarica lo zip...


---
dunque a regime:

1)
mvn release:prepare -Darguments="-DskipTests" -DautoVersionSubmodules=true

2)
mvn release:perform -Darguments="-DskipTests" -DuseReleaseProfile=false

pero' in tal modo ho lo zip mezzo vuoto..

provo a fare cosi:
mi metto in
C:\DEV\SRC\postegestorepec\svn-postecom\GPEC\GestorePEC\trunk\target\checkout
che e' la cartella in cui il mvn deploy si e' preso da svc il codice col tag da deplpyare (tag creato dalla fase prepare)

quella folder e' una root maven
gli eseguo prima il goal assembly per rifarmi lo zip completo
mvn assembly:single -N

e poi il goal deploy (non la fase, ma il goall) per vedere se riesco a deplouyarlo:

mvn deploy:deploy
--> non ha funzionato..il Gpec l'ha superatno, ma non ha riuploadato il nuovo zipppone
e poi e' andato in errore sul copyprocess:
ERROR] Failed to execute goal org.apache.maven.plugins:maven-deploy-plugin:2.7:
eploy (default-cli) on project copyprocess: The packaging for this project did not assign a file to the build artifact -> [Help 1]

provo a rilanciare tutta la fase deploy:

mvn deploy -DskipTests  (senza il clean!!)

forse va..
rifacendo anche la fase package mi sta facendo l'assembly dello zippone, quindi non server rilanciarlo singolo prima..
e ora lo sta uploadando

Uploading to dist-release: http://192.168.52.121:8081/nexus/content/repositories
/releases/it/posteitaliane/pec/GestorePec/0.0.2/GestorePec-0.0.2.pom
Uploaded to dist-release: http://192.168.52.121:8081/nexus/content/repositories/
releases/it/posteitaliane/pec/GestorePec/0.0.2/GestorePec-0.0.2.pom (22 kB at 36
 kB/s)
Downloading from dist-release: http://192.168.52.121:8081/nexus/content/reposito
ries/releases/it/posteitaliane/pec/GestorePec/maven-metadata.xml
Downloaded from dist-release: http://192.168.52.121:8081/nexus/content/repositor
ies/releases/it/posteitaliane/pec/GestorePec/maven-metadata.xml (341 B at 2.5 kB
/s)
Uploading to dist-release: http://192.168.52.121:8081/nexus/content/repositories
/releases/it/posteitaliane/pec/GestorePec/maven-metadata.xml
Uploaded to dist-release: http://192.168.52.121:8081/nexus/content/repositories/
releases/it/posteitaliane/pec/GestorePec/maven-metadata.xml (341 B at 1.2 kB/s)
Uploading to dist-release: http://192.168.52.121:8081/nexus/content/repositories
/releases/it/posteitaliane/pec/GestorePec/0.0.2/GestorePec-0.0.2-web-app-dist.zi
p


dunque a regime, se tutto va bene:



1)
cd C:\DEV\SRC\postegestorepec\svn-postecom\GPEC\GestorePEC\trunk\

2)
mvn release:prepare -Darguments="-DskipTests" -DautoVersionSubmodules=true

3)
mvn release:perform -Darguments="-DskipTests" -DuseReleaseProfile=false

4)
cd C:\DEV\SRC\postegestorepec\svn-postecom\GPEC\GestorePEC\trunk\target\checkout

5)
mvn deploy -DskipTests



se va lo zippone sta tipo qua:
http://192.168.52.121:8081/nexus/content/repositories/releases/it/posteitaliane/pec/GestorePec/0.0.2/

GestorePec-0.0.2-web-app-dist.zip

alla prossima release potrei provare a rilevare , almeno nel nome il web-app-dist..
perche' forse il problema era un altro, ossia il group id poste italiane...anziche postecom..

altra prova per accelerare:
forse lo step 5) si potrebbe fare escludendo i figli, ossia cosi: mvn deploy -DskipTests -N



********************************************************************************************************
giro completo dall'inizio per fare release:

PRE CONDIZIONI:
1) DECIDERE LE VERSIONI DEI VARI SERVIZI : SE LA STESSA, SE DIVERSE, E METTERLE DI CONSEGUENZA NEI RISPETTIVI .yml SOTTO install-ansible

2) AVER PRODOTTO LO ZIP ansible-install.zip CON LE CORRETTE CONFIGURAZIONI E MESSO NEGLI ALLEGATI DEL MVN SITE



dall'inizio:
1)verificare  le differenze fra i 2 workspace il git fincons e il svn poste
    con  un toll di diff (es: meld)

    a sinistra git:
	C:\DEV\SRC\postegestorepec\git-fincons\GPEC\Gpec

	a destra svn
	C:\DEV\SRC\postegestorepec\svn-postecom\GPEC\GestorePEC\trunk



2) sincronizzare col tool di diff ( o in  caso con rsync se e' possibile portare tutto)
3)da cmd come ADMIN: ,
 cd C:\DEV\SRC\postegestorepec\svn-postecom\GPEC\GestorePEC\trunk\
 mvn clean


4) con tortois fare "check for modification"  e committare

5) mvn -e clean install -DskipTests

6) mvn clean site -DskipTests
7) mvn site:stage -DskipTests
8)
mvn release:prepare -Darguments="-DskipTests" -DautoVersionSubmodules=true

9)
mvn release:perform -Darguments="-DskipTests" -DuseReleaseProfile=false

10)
cd C:\DEV\SRC\postegestorepec\svn-postecom\GPEC\GestorePEC\trunk\target\checkout

11)
mvn deploy -DskipTests

12)fare su git fincon un  tag con la versione deployata su maven postecom
git tag -a 0.0.3 -m 'versione 0.0.3 rilasciata il 13-02-2019'
git push origin 0.0.3

13) con meld, riportare sui pom.xml di git-fincons



(nota sul punto 11:
parte facendo l'assempbly plugin del parent, ossia il pacchetto zip..
e poi per ogni modulo: compila a deploya il jar sul maven release


_________________________________________________

NOTA:
SE SI HA LA NECESSITA' DI RIFARE IL DEPLOY SOLO DEL MAVEN SITE , MAGARI PER CORREZIONI VELOCI DI REFUSI
SULL HTML O SUI FILE ALLEGATI,
SI DEVE FARE LA SEGUENTE PROCEDURA
(NOTA: SEGUENDO I SEGUENTI PASSI, IL PACCHETTO ZIP DEL DOWNLOAD PACKAGE NON  VINENE NE RICREATO NE RIDEPLOYATO)

1)
cd C:\DEV\SRC\postegestorepec\svn-postecom\GPEC\GestorePEC\trunk

2)
ASSICURARSI CHE I pom.xml INDICHINO LA VERSIONE VOLUTA , OSSIA QUELLA CHE APPARE NEL TITOLO,
(INFATTI SE STIAMO FACENDO QUESTO RI-DEPLOY DEL MVN SITE DOPO UNA RELEASE PREPARE-PERFORM, LE VERSIONI SARANNO SALITE AD UNA SNAPSHOT SUCCESSIVA...

3)
mvn clean site -DskipTests

4)
mvn site:stage -DskipTests

5)
mvn site:deploy -DskipTests
