
UN NODO DI RABBITMQ NON STA SU:

lo si capisce da:
sudo systemctl status rabbitmq-server.service
oppure tentando di accedere alla console (porta 15672)
es:
http://svm-pec1.svil.postecom.local:15672/
http://svm-gpec2.svil.postecom.local:15672/

i log sono qui: /var/log/rabbitmq/
                /var/log/rabbitmq/log

tail -1000f /var/log/rabbitmq/rabbit@svm-pec1.log

LA CAUSA CHE NON RIESCE A RIPARTIRE POTREBBE ESSERE CHE NON SONO SODDISFATTI I REQUISITI MINIMI, AD ESEMPIO
SPAZIO DISCO LIBERO DI 3 GIGA

*******************************************************************************

IL CONTROLLER O ALTRI MICROSERVIZI NON RIESCONO A PARTIRE:
- POTREBBE ESSERE DOVUTO A RABBIT CHE NON STA SU
- NON RIESCONO A OTTENERE LE PROPRIETA' dal server di configurazione


*******************************************************************************

se si accede ad un cluster RABBIT_MQ attraverso un singolo nodo che presenta un malfunzionamento, si può incontrare in varie forme
il seguente messaggio, ad esempio estratto dal file di log del nodo, come esposto in precedenza:
2019-02-28 17:28:18.577 [error] <0.20174.0> Error on AMQP connection <0.20174.0> (192.168.52.69:36984 -> 192.168.52.59:5672, vhost: 'none', user: 'rabbitmq', state: opening), channel 0:
 {handshake_error,opening,
                 {amqp_error,internal_error,
                             "access to vhost '/' refused for user 'rabbitmq': vhost '/' is down",
                             'connection.open'}}

Il problema ha molteplici cause, come un crash dovuto all'esaurimento dello spazio fisico su disco, a cui è seguito un restart
forzato dell'host.

verificando lo stato del servizio si nota la ripetizione continua del seguente messaggio, sia nel crash log oppure provando a riavviare il virtual host
dalla console grafica di RabbitMQ:

2019-02-28 16:46:28 =CRASH REPORT====
  crasher:
    initial call: rabbit_vhost_process:init/1
    pid: <0.418.0>
    registered_name: []
    exception exit: {{badmatch,{error,{{{badmatch,{error,{not_a_dets_file,"/var/lib/rabbitmq/mnesia/rabbit@svm-pec1/msg_stores/vhosts/628WB79CIFDYO9LJI6DKMI09L/recovery.dets"}}},[{rabbit_recovery_terms,open_table,1,[{file,"src/rabbit_recovery_terms.erl"},{line,191}]},{rabbit_recovery_terms,init,1,[{file,"src/rabbit_recovery_terms.erl"},{line,171}]},{gen_server,init_it,2,[{file,"gen_server.erl"},{line,374}]},{gen_server,init_it,6,[{file,"gen_server.erl"},{line,342}]},{proc_lib,init_p_do_apply,3,[{file,"proc_lib.erl"},{line,249}]}]},{child,undefined,rabbit_recovery_terms,{rabbit_recovery_terms,start_link,[<<"/">>]},transient,30000,worker,[rabbit_recovery_terms]}}}},[{gen_server2,init_it,6,[{file,"src/gen_server2.erl"},{line,583}]},{proc_lib,init_p_do_apply,3,[{file,"proc_lib.erl"},{line,249}]}]}
    ancestors: [<0.416.0>,rabbit_vhost_sup_sup,rabbit_sup,<0.345.0>]
    message_queue_len: 0
    messages: []
    links: [<0.416.0>]
    dictionary: []
    trap_exit: true
    status: running
    heap_size: 6772
    stack_size: 27
    reductions: 23623
  neighbours:

l'elemento 'exception_exit' ci dice che non può essere letto il file
'/var/lib/rabbitmq/mnesia/rabbit@svm-pec1/msg_stores/vhosts/628WB79CIFDYO9LJI6DKMI09L/recovery.dets'.
Probabilemente il file è corrotto.
Procedere semplicemente alla rimozione del file e al riavvio del servizio di sistema 'rabbitmq-server' nel nodo che presenta il problema:

>> sudo systemctl restart rabbitmq-server.service


*******************************************************************************

NON VIENE RICEVUTO L'ACK INPUT:
una possibile causa è che stia giu' RABBIT: quindi il servizi dei controller , invocati da NIFI, vanno in errore perche' non
riescono a chiamaare RABBIT

*******************************************************************************
