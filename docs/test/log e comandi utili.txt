per vedere tutti i servizi installati su una macchina

sudo systemctl status


LOG NIFI:

/var/log/nifi

sudo tail -f

TODO: DOBBIAMO DIRE A NIFI DI NON LOGGARE GLI HEARTBEAT

LOG CONTROLLER:

sudo tail -1000f /home/sbuser/var/log/controller.log


LOG SENDER:

/home/sbuser/var/log/sender.log



PER VEDERE LO STATUS DI TUTTI I SERVIZI:
sudo systemctl status

PER VEDERE LO STATUS DI UN SERVIZIO:
sudo systemctl status <nome_servizio>

PER STOPPARE UN SERVIZIO:
sudo systemctl stop <nome_servizio>

PER STARTARE UN SERVIZIO:
sudo systemctl start <nome_servizio>
