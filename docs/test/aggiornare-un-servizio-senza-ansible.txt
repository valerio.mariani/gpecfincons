
esempio il composer:
stoppo, sovarscivo il jar , e riavvio, ossia:

sudo systemctl stop composer.service

sovrascio il jar in
/opt/gpec/services/SENDER/0.0.3-SNAPSHOT

sudo systemctl start composer.service

per vedere se ok:

sudo systemctl status composer.service

*********CONTROLLER ****************************


sudo systemctl status controller.service
sudo systemctl stop controller.service

cd /opt/gpec/services/CONTROLLER/0.0.4-SNAPSHOT

sudo chown sbuser:sbgroup controller-0.0.4-SNAPSHOT.jar
sudo chmod 500 controller-0.0.4-SNAPSHOT.jar

sudo systemctl start controller.service


*********COMPOSER ****************************

sudo systemctl status composer.service
sudo systemctl stop composer.service

cd /opt/gpec/services/COMPOSER/0.0.4-SNAPSHOT

sudo chown sbuser:sbgroup Composer-0.0.4-SNAPSHOT.jar
sudo chmod 500 Composer-0.0.4-SNAPSHOT.jar

sudo systemctl start composer.service
sudo systemctl status composer.service

*********SENDER ****************************


sudo systemctl status sender.service
sudo systemctl stop sender.service

cd /opt/gpec/services/SENDER/0.0.4-SNAPSHOT

sudo chown sbuser:sbgroup Sender-0.0.4-SNAPSHOT.jar
sudo chmod 500 Sender-0.0.4-SNAPSHOT.jar

sudo systemctl start sender.service