per windows:

PER INSTALLARE CERTIFICATO:

-APRIRE PROMPT COME ADMIN
-POSIZIONARSI SOTTO LA SEGUENTE FOLDER DELL JDK USATA DA NOI, ES:
  cd C:\Program Files\Java\jdk1.8.0_74\jre\lib\security

- lanciare il seguente comando: (modificando il path )
-- per alcune VM Java 8 che hanno IT come language aggiungere questa opzione per evitare un errore: '-J-Duser.language=en'
keytool.exe -importcert -file C:\DEV\SRC\postegestorepec\git-fincons\GPEC\docs\certificati\Postecom_CA_Interna-SVILUPPO-MAIL.crt -alias mail.pec.postecom.sviluppo -storepass changeit -keystore cacerts
